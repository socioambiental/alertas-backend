# Alertas Backend

Sistema de download, incorporação, cruzamento de dados e API sobre pressões e
ameaças socioambientais usada no sistema [Alertas+](https://alertas.socioambiental.org).

Compila diversos conjuntos de dados, incluindo:

* Eventos de pressão/ameaça:
  * [DETER / INPE](http://www.obt.inpe.br/OBT/assuntos/programas/amazonia/deter/deter)
    disponibilizados na plataforma [TerraBrasilis](http://terrabrasilis.dpi.inpe.br).
  * [PRODES / INPE](http://www.obt.inpe.br/OBT/assuntos/programas/amazonia/prodes).
  * [FIRMS MODIS e VIIRS / NASA](https://firms.modaps.eosdis.nasa.gov/).
  * [Amazon Dashboard / GFED](https://globalfiredata.org/pages/pt/amazon-dashboard/).
  * [SAD / Imazon](https://imazon.org.br/publicacoes/faq-sad/).

* Recortes espaciais:
  * Base de Áreas Protegidas do [Instituto Socioambiental](https://socioambiental.org).
  * Unidades políticas (UFs e Municípios) do [IBGE](https://ibge.gov.br).

**ATENÇÃO: o projeto se encontra numa fase estável, porém ainda requer
aprimoramento na documentação e de usabilidade para que seja mais facilmente
utilizado pela comunidade. Testes, sugestões e merge requests são bem vindos :)**

## Documentação

A documentação geral do projeto encontra-se em https://alertas.socioambiental.org/doc/pt-br/

## Requisitos

Expertise:

* Para rodar uma instância do backend, é necessário ter conhecimentos básicos
  e familiaridade com ambientes conteinerizados e também é recomendável
  experiência com administração de sistemas GNU/Linux.

Aplicações:

* [GNU Make](https://www.gnu.org/software/make/), para gerir tarefas no nível
  do projeto (arquivo [Makefile](Makefile)).
* [GNU Screen](https://www.gnu.org/software/screen), para monitoria geral dos
  serviços.
* [Docker v20.10](https://docs.docker.com) ou superior, para a criação dos
  contêineres de serviço.
* [Docker Compose v1.27](https://docs.docker.com/compose/) ou superior, para
  gerenciar os serviços.

Serviços:

* Base de dados PostgreSQL 12 com as extensões `postgis`, `postgis_raster`,
  `pgcrypto` e opcionalmente `dblink` (apenas para importadores `_dblink`).
* Base de dados PostgreSQL 12 para o agendador de importações [Apache
  Airflow](https://airflow.apache.org), caso deseje usar essa funcionalidade.

Recursos mínimos para desenvolvimento:

* 4 CPUs.
* 8GBB RAM.
* 50GB+ de espaço em disco.

A depender da escala de acesso da quantidade de importadores, recomenda-se
aumentar a quantidade de recursos.

Detalhes de instalação em ambiente de produção são abordados [neste
documento](https://alertas.socioambiental.org/doc/install.html).

## Instalação

A instalação básica do backend envolve as seguintes etapas:

0. Instalação das dependências.

1. Criação da base de dados PostgreSQL com extensões, usuários e permissões
   necessárias.

2. Edição dos arquivos de configuração a partir dos modelos disponibilizados
   neste repositório.

3. Provisionamento dos serviços (contêineres).

### Dependências

Num sistema do tipo Debian GNU/Linux, as dependências de software podem ser
instaladas com

    sudo apt-get install -y make screen

Para o caso [Docker](https://docs.docker.com) e do [Docker
Compose](https://docs.docker.com/compose/), recomenda-se consultar as
respectivas documentações.

### Base de dados

No caso de sistemas baseados em Debian GNU/Linux, existem scripts de
conveniência que podem ser usados e que requerem acesso a superusuário (sudo):

    make provision-database

Isto irá instalar o PostgreSQL no seu sistema e criará a base de dados do backend.

Caso prefira, configure manualmente um usuário e um banco de dados PostgreSQL
com os schemas necessários.

Caso você pretenda importar dados diretamente de outras bases PostgreSQL, o
`dblink` também precisa ser configurado assim como um usuário com privilégios
especiais para executar as importações.

### Arquivos de configuração

* Copie o arquivo [config.ini.dist](config/config.ini.dist) para `config/config.ini`
  e edite de acordo com os dados de acesso ao banco PostgreSQL que você criou.

* Inclua a informação de acesso ao banco no arquivo `config/pgpass`
  no [Formato do .pgpass](https://wiki.postgresql.org/wiki/Pgpass),
  como por exemplo `host.docker.internal:5432:alertas:alertas_query:n7clJVsv4eY8mCgz96N`.
  Este arquivo pode ser usado para autenticação no banco em algumas rotinas de manutenção.

* Opcionalmente, inclua a informação de acesso ao banco no arquivo `~/.pgpass` do usuário que
  acessará a base no [Formato do .pgpass](https://wiki.postgresql.org/wiki/Pgpass),
  como por exemplo `localhost:5432:deter:deter:n7clJVsv4eY8mCgz96N`. Isto facilitará o acesso
  à base de dados sem ter que entrar com a senha.

### Provisionamento

Primeiro, é necessário criar um arquivo `.env` com os parâmetros necessários para o
funcionamento do sistema:

    make provision-container-env

Caso deseje, customize o arquivo `.env` antes de rodar a próxima etapa.

Em seguida, o provisionamento pode ser realizado com o seguinte comando:

    make run-containers

Ele dará conta de:

* Construir os [serviços](compose.yaml) básicos, instalando todas as dependências necessárias.
* Iniciar os serviços definidos na variável `COMPOSE_PROFILES` do arquivo `.env`.

Por padrão, todos os serviços são construídos (`COMPOSE_PROFILES=full`). Os perfis estão
disponíveis no [arquivo de definição de serviços](compose.yaml). Por exemplo, para rodar
apenas os serviços básicos sem o [Apache Airflow](https://airflow.apache.org), use a
seguinte configuração no arquivo `.env`:

    COMPOSE_PROFILES=main

### Inicialização

Assim que os serviços básicos foram iniciados, o sistema:

* Fará o bootstrap das bases de dados.
* Importará os conjuntos de dados mais básicos que são requisitos para os
  alertas.

Para monitorar os serviços, use o comando

    make container-logs

Ou, se preferir, inicie o console de monitoria geral do sistema usando

    make console

## Interfaces web

O backend sobe uma série de interfaces web definidas na configuração dos
[serviços](compose.yaml). Em especial:

* Um servidor proxy para acesso aos serviços.
* Uma API de consulta.
* Um servidor de tiles (Tile Server) para mapas.
* A interface web do [Apache Airflow](https://airflow.apache.org).

## Rodando

Por convenção, comandos são rodados dentro do serviço `worker` definido no `compose.yaml`.
Comandos podem ser invocados dentro desse container usando o script `./bin/runner`.

Por exemplo, para inicializar a base de dados, baixar e incorporar os dados
mais recentes, use o comando

    ./bin/runner worker make bootstrap
    ./bin/runner worker make import

## Produção

Para detalhes sobre como configurar este backend em ambiente de produção,
consulte a [respectiva documentação](doc/install.md).

## Créditos

* Desenvolvido no [Instituto Socioambiental](https://www.socioambiental.org).
* Distribuída sob [Licença GNU GPLv3+](./LICENSE).
