#
# Painel de Alertas Makefile.
#
# This Makefile is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3 of the License, or any later version.
#
# This Makefile is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307, USA
#

# Parameters
PROJECT      = alertas-backend
PROJECT_DIR  = $(shell pwd)
PRODUCTION   = $(PROJECT)@drsin.socioambiental.org:/var/sites/$(PROJECT)/
DEVELOPMENT  = $(PROJECT)@heart.socioambiental.org:/var/sites/$(PROJECT)/
EXCLUDES     = --exclude=backups --exclude=downloads --exclude=data --exclude=config/config.ini --exclude=config/pgpass --exclude=config/logging.ini --exclude=lock --exclude=log --exclude=.env --exclude=__pycache__ --exclude=./cache --exclude=.pytest_cache --exclude=doc/_build/doctrees/*/* --exclude=doc/_build/html/*/*
DATE         = $(shell date +%Y%m%d)
HOSTNAME     = $(shell hostname --fqdn)
#COMMIT      = $(shell git log -1 --pretty=format:"%H" --no-patch)
#COMMIT      = $(shell cat .git/ORIG_HEAD)
TIME         = $(shell date +%k:%M:%S)

# API internal port
PORT = 5001

# Tileserver internal port
TILESERVER_PORT = 5002

# Airflow test webserver internal port
AIRFLOW_PORT = 5003

# Host to bind gunicorn to
HOST = 0.0.0.0

# Number of gunicorn workers
# See https://docs.gunicorn.org/en/stable/settings.html#worker-processes
# https://docs.gunicorn.org/en/stable/design.html
WORKERS = 4

# Max requests
WORKER_MAX_REQUESTS = 10000

# Worker timeout
WORKER_TIMEOUT = 240

# Worker threads
WORKER_THREADS = 4

# Worker max requests jitter
WORKER_MAX_REQUESTS_JITTER = 500

# Gunicorn options
GUNICORN_OPTS = -w $(WORKERS) -t $(WORKER_TIMEOUT) --max-requests $(WORKER_MAX_REQUESTS) --max-requests-jitter $(WORKER_MAX_REQUESTS_JITTER) --threads $(WORKER_THREADS)

# The caching server
CACHING = caching

# Importers configuration
#ISA_IMPORTER = isa_dblink
ISA_IMPORTER  = isa

# Paths
# Thanks https://stackoverflow.com/questions/18136918/how-to-get-current-relative-directory-of-your-makefile
MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
CWD           := $(notdir  $(patsubst %/,%,$(dir $(MAKEFILE_PATH))))
BASEPATH      := $(shell dirname $(MAKEFILE_PATH))

# Include the environment file so Makefile to include custom configs and overrides
-include .env

# Thanks https://stackoverflow.com/questions/8941110/how-i-could-add-dir-to-path-in-makefile/8942216#8942216
# 		   https://unix.stackexchange.com/questions/11530/adding-directory-to-path-through-makefile
#export PATH := $(BASEPATH)/src:$(PATH)

.PHONY: doc

#
# DevOps targets
#

all: install

# Installation
install: post_receive vendor

# Vendorized dependencies
vendor:
	@pipenv install

production:
	@rsync -avz --delete $(EXCLUDES) ./ $(PRODUCTION)/

development:
	@rsync -avz --delete $(EXCLUDES) ./ $(DEVELOPMENT)/

# Configure a git post-receive hook
post_receive:
	git config receive.denyCurrentBranch ignore
	test -s bin/post-receive && cd .git/hooks && ln -sf ../../bin/post-receive

# Deploy code pushed to remote host
deploy:
	#git reset HEAD
	git checkout -f
	git submodule update --init --recursive
	make vendor

console:
	@PROJECT_DIR=$(PROJECT_DIR) screen -c screenrc -S $(PROJECT)

service-logs:
	@tail -F log/*log

#
# Database targets
#

provision-postgresql:
	@bin/provision-postgresql

provision-database: provision-postgresql
	@bin/provision-database

dumpdb:
	@mkdir -p backups
	@pg_dump -U alertas_admin -h localhost alertas > backups/$(DATE).sql
	@( cd backups && bzip2 $(DATE).sql )

coldbackup:
	@sudo service postgresql stop
	@sudo tar jcvf backups/postgresql.$(DATE).tar.bz2 /var/lib/postgresql
	@sudo service postgresql start

#
# Containers
#

provision-container-env:
	@bin/provision-container-env

run-containers:
	@docker-compose up -d

watch-containers:
	@watch docker-compose ps

containers-logs:
	@docker-compose logs -f

#
# API targets
#

api_prod:
	@#pipenv run $(BASEPATH)/src/api.py
	@cd src && pipenv run gunicorn $(GUNICORN_OPTS) -b $(HOST):$(PORT) api:app

api_dev:
	@#FLASK_ENV=development pipenv run $(BASEPATH)/src/api.py
	@#FLASK_ENV=development FLASK_RUN_HOST=$(HOST) FLASK_RUN_PORT=$(PORT) pipenv run $(BASEPATH)/src/api.py
	@#FLASK_ENV=development FLASK_APP=$(BASEPATH)/src/api.py pipenv run flask run --host=$(HOST) --port=$(PORT)
	@cd src && pipenv run gunicorn $(GUNICORN_OPTS) -b $(HOST):$(PORT) --reload --log-level debug api:app

#
# Tileserver targets
#

tileserver_prod:
	@#pipenv run $(BASEPATH)/src/api.py
	@cd src && pipenv run gunicorn $(GUNICORN_OPTS) -b $(HOST):$(TILESERVER_PORT) tileserver:app

tileserver_dev:
	@#FLASK_ENV=development FLASK_APP=$(BASEPATH)/src/tileserver.py pipenv run flask run --host=$(HOST) --port=$(TILESERVER_PORT)
	@cd src && pipenv run gunicorn $(GUNICORN_OPTS) -b $(HOST):$(TILESERVER_PORT) --reload tileserver:app

#
# Airflow targets
#

#airflow_webserver:
#	airflow webserver --port $(AIRFLOW_PORT)
#
#airflow_scheduler:
#	airflow scheduler

#
# Test targets
#

test_query:
	@pipenv run $(BASEPATH)/src/query.py

# Useful to test the Airflow instance
test_error:
	@false

test_failure:
	@pipenv run $(BASEPATH)/src/importer.py test_random_failure

#
# Performance targets
#

benchmark:
	@#pipenv run $(BASEPATH)/src/benchmark.py
	@#pipenv run pytest --benchmark-histogram=log/benchmark/benchmark_$(HOSTNAME)_$(COMMIT)_$(DATE)_$(TIME) --benchmark-storage=log/benchmark --benchmark-autosave --benchmark-save-data
	@#pipenv run pytest --benchmark-histogram=log/benchmark/benchmark_$(HOSTNAME)_$(COMMIT)_$(DATE)_$(TIME) --benchmark-storage=log/benchmark --benchmark-autosave --benchmark-save-data
	@pipenv run pytest --benchmark-histogram=$(BASEPATH)/log/benchmark/benchmark_$(HOSTNAME)_$(DATE)_$(TIME) --benchmark-storage=$(BASEPATH)/log/benchmark --benchmark-autosave --benchmark-save-data

#cache_clear: cache_clear_redis
cache_clear:
	@pipenv run src/cache.py clear

cache_clear_redis:
	@redis-cli -h $(CACHING) flushall

cache_purge:
	@pipenv run src/cache.py purge
	# Only working for "dbm" caches
	@rm -f cache/*

cache_refresh:
	@pipenv run src/cache.py refresh

#
# Documentation targets
#

doc:
	@pipenv  run make -C doc/pt-br html
	@#pipenv run make -C doc/pt-br latexpdf
	@pipenv  run make -C doc/en    html
	@#pipenv run make -C doc/en    latexpdf
	@pipenv  run make -C doc/es    html
	@#pipenv run make -C doc/es    latexpdf

#
# Run stages
# These are aliases for the actual run schedules.
#

stage0: refresh

stage1: daily

stage2: monthly

stage3: biannual

stage4: manual

# Run the full iteration
#import: install weekly daily monthly biannual manual
import: install stage0 stage1 stage2 stage3 stage4

#
# Data targets
#

# Prune redundant data such as extracted datasets
prune:
	@bin/prune

# Database initialization
bootstrap:
	@bin/bootstrap

# Alias to bootstrap
init: bootstrap

# Refresh canonical datasets from external databases
refresh:
	@pipenv run $(BASEPATH)/src/importer.py biomass
	@pipenv run $(BASEPATH)/src/importer.py land_cover
	@pipenv run $(BASEPATH)/src/importer.py $(ISA_IMPORTER)

# Weekly tasks
weekly: refresh

# Daily tasks
#daily: extract load transform
daily:
	@pipenv run $(BASEPATH)/src/importer.py amazon_dashboard
	@pipenv run $(BASEPATH)/src/importer.py deter
	@pipenv run $(BASEPATH)/src/importer.py firms_modis
	@pipenv run $(BASEPATH)/src/importer.py firms_viirs

# Monthly tasks
monthly:
	@pipenv run $(BASEPATH)/src/importer.py firms_modis_arquive
	@pipenv run $(BASEPATH)/src/importer.py firms_viirs_arquive
	#@pipenv run $(BASEPATH)/src/importer.py sad_dblink

# Biannual tasks
biannual:
	true

# Manual tasks for eventual runs
manual:
	@pipenv run $(BASEPATH)/src/importer.py prodes_dblink

# Extract datasets that should be importer regularly
#extract:
#	@pipenv run $(BASEPATH)/src/importer.py amazon_dashboard  --phase extract
#	@pipenv run $(BASEPATH)/src/importer.py deter             --phase extract
#	@pipenv run $(BASEPATH)/src/importer.py firms_modis       --phase extract
#	@pipenv run $(BASEPATH)/src/importer.py firms_viirs       --phase extract
#
## Load datasets that should be importer regularly
#load:
#	@pipenv run $(BASEPATH)/src/importer.py amazon_dashboard  --phase load
#	@pipenv run $(BASEPATH)/src/importer.py deter             --phase load
#	@pipenv run $(BASEPATH)/src/importer.py sad               --phase load
#	@pipenv run $(BASEPATH)/src/importer.py firms_modis       --phase load
#	@pipenv run $(BASEPATH)/src/importer.py firms_viirs       --phase load
#
## Transform datasets that should be importer regularly
#transform:
#	@pipenv run $(BASEPATH)/src/importer.py amazon_dashboard  --phase transform
#	@pipenv run $(BASEPATH)/src/importer.py deter             --phase transform
#	@pipenv run $(BASEPATH)/src/importer.py sad               --phase transform
#	@pipenv run $(BASEPATH)/src/importer.py firms_modis       --phase transform
#	@pipenv run $(BASEPATH)/src/importer.py firms_viirs       --phase transform

# Metadata
metadata:
	@pipenv run $(BASEPATH)/src/metadata.py

# Exporter
exporter:
	@pipenv run $(BASEPATH)/src/exporter.py
