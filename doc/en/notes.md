# Technical notes

## Shapefile of territories kept by ISA

Protected areas (Indigenous Lands and Conservation Units) in Legal Amazonia
were digitized in a GIS environment (geographical information system) based on
the descriptive memorials of official documents of creation or alteration of
the perimeter. Areas are plotted at 1:100.000 scale. For the state Conservation
Units, we adopted the ICMBio/MMA databases as a reference for spatial data,
complemented with information from SISNAMA state agencies. When there is no
descriptive memorial of the protected area or if it has errors that prevent
plotting the perimeter, vector and matrix data obtained from official federal
and state agencies (i.e. FUNAI, MMA and state secretariats) are used for review
and/or complementation, as well as non-cartographic documents, such as
management plans for Conservation Units.

Spatial data of cities and states used as reference for plotting protected
areas comes from both SIVAM (Amazon Surveillance System, 2004), on the 250
thousand scale, and the continuous cartographic base for Brazil on the 250
thousand scale (IBGE, 2015). In addition to these official databases, global
rasters (i.e. Google) and satellite images of the Landsat series also assist in
the process of building system boundary if there are limitations in vector
data.

Every feature was transformed for the geographic coordinate system and
referenced to Datum Sirgas 2000.

The spatial data referring to the buffer zones of the protected areas (buffer)
were delimited in a GIS environment using the buffer function for the linear
distances: 0-3 km, 0-10km and 3-10km.

### Dictionary of data

Description of the table fields and shapefile of Territories kept by ISA and [available at Alertas+](https://alertas.socioambiental.org/downloads).

* `ID_ORIG`:
  * Description: persistent tag in the source database:
    * If a Protected Area or buffer of a Protected Area, it concerns the `id_arp` of the Protected Area System kept by ISA. Buffers have the same tag of the respective Protected Area.
    * If Federation Units and Municipalities, it concerns the IBGE code.
* `TYPE`:
  * Description: type of territory.
  * Possible values:
    * `UF`: Federation Units.
    * `MUN`: Municipality.
    * `APAE`: State Environmental Protection Area.
    * `APAE_B1`: State Environmental Protection Area (0-3km buffer).
    * `APAE_B2`: State Environmental Protection Area (0-10km buffer).
    * `APAE_B3`: State Environmental Protection Area (3-10km buffer).
    * `APAF`: Federal Environmental Protection Area.
    * `APAF_B1`:  Federal Environmental Protection Area (0-3km buffer).
    * `APAF_B2`:  Federal Environmental Protection Area (0-10km buffer).
    * `APAF_B3`:  Federal Environmental Protection Area (3-10km buffer).
    * `TI`: Indigenous Land
    * `TI_B1`: Indigenous Land (0-3km buffer).
    * `TI_B2`: Indigenous Land (0-10km buffer).
    * `TI_B3`: Indigenous Land (3-10km buffer).
    * `UCE`: State Conservation Units.
    * `UCE_B1`: State Conservation Units (0-3km buffer).
    * `UCE_B2`: State Conservation Units (0-10km buffer).
    * `UCE_B3`: State Conservation Units (3-10km buffer).
    * `UCF`: Federal Conservation Units.
    * `UCF_B1`: Federal Conservation Units (0-3km buffer).
    * `UCF_B2`: Federal Conservation Units (0-10km buffer).
    * `UCF_B3`: Federal Conservation Units (3-10km buffer).
* `NAME`: territory name.
* `ID`: non-persistan record tag just for administrative purposes of the database; it is recommended not to use this field, but the conjunction `ID_ORIG` and `TYPE` to distinctively identify a territory.
