# Homologation

## Alertas+ data homologation

* By Alana Almeida de Souza, Cicero Augusto, William Pereira Lima -
  08/08/2021.

Alertas+ data homologation evaluated the following aspects related to the
dataset available in the system:

* Completeness;
* Calculations of the total of alerts in the Data Panel by the different types
  of territories, categories and spatial subsets;
* Calculations of the affected area by the alerts (when relevant) in the Data
  Panel by different types of territories, categories and spatial subsets;
* Check of graphical outputs in the Data Panel;
* Check of tabular outputs in the Data Panel;
* Calculations shown at  Alerts+ homepage

Completeness was assessed through a simple data check between the data present
in Alertas+ system status and the total of events and area (when relevant)
available in the data shapefiles that were made available in their respective
sources for the same day of reference. Variations between computations of less
than +/- 10% were considered acceptable and associated with data modelling and
changes in cartographic projection.

The calculations of the total of alerts and areas were verified by sampling,
considering the complexity of the queries available. Thus, this verification
considered the different topological relationships between territories, being:
non-overlapping territories (ex.: States, municipalities, protected areas that
don't overlap each other) and overlapping territories (ex.: indigenous lands
contained within conservation units).

The verification of the graphical and tabular outputs included visual
inspection of the results of the queries and content check of the exports in
the different formats.
