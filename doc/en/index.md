# Alertas+ - Documentation

[Português](/doc/pt-br/) | [Español](/doc/es/)

[Alertas+](https://alertas.socioambiental.org) is a data dashboard that aims
to provide qualified information on the state of forests and issue
alerts on pressures and threats that impact protected areas. With the variety
of alert sources available on the panel and the multiple query options, the
user can formulate numerous questions to Alertas+, and obtain, for example,
answers about the level of degradation caused by fire within conservation
units, how illegal mining is advancing within indigenous lands or how is the
deforestation processs in protected areas or its surroundings. The data panel
also allows paired queries, where the user can make comparisons between different
protected areas or assess the dynamics of an alert from within a specific
protected area or from the outside.

This documentation details the technical aspects of the system, from design to
operation.

![Instituto Socioambiental](_static/images/isa.png) Alertas+ is developed and
maintained by [Instituto Socioambiental](https://www.socioambiental.org).

## Index


```eval_rst
.. toctree::
   :glob:
   :maxdepth: 2

   specs
   review
   notes
   code
```

<!--
## Índices e tabelas

```eval_rst
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```
-->

## Contact

The Alertas+ Team can by contacted via
[alertas@socioambiental.org](mailto:alertas@socioambiental.org).
