# Code documentation

```eval_rst
.. toctree::
   :glob:
   :maxdepth: 2

   code/*
```
