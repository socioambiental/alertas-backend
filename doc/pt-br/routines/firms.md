# Atualização de dados FIRMS

As seguintes séries precisam ser baixadas periodicamente:

* MODIS Collection 6: Temporal Coverage: 11 November 2000 - present.
* VIIRS S-NPP 375m: Temporal Coverage: 20 January 2012 - present,

## Procedimento para cada série

1. Vá até a [págna de download do projeto FIRMS](https://firms.modaps.eosdis.nasa.gov/download/).
2. Crie um novo pedido.
3. Submeta o fórmulário de acordo com a série almejada.
4. Um identificador será emitido para cada série e que precisa constar no `config.py` do importador correspondente, substituindo o antigo.
5. Após a substituição do identificador, faça um commit no projeto.
6. Após a geração do conjunto de dados (o FIRMS emitirá um email de confirmação), envie o código para as instâncias de desenvolvimento e/ou produção.

Parâmetros para FIRMS MODIS:

![](/_static/images/firms/modis.png)

Parâmetros para FIRMS VIIRS:

![](/_static/images/firms/viirs.png)

Página de acompanhamento de pedidos:

![](/_static/images/firms/downloads.png)

## Observações

* Os IDs/links de download antigos expiram após um período de cerca de 45 dias.
* Assim, o procedimento precisa ser rodado antes de cada expiração, por exemplo mensalmente.
