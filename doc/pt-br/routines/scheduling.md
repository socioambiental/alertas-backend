# Agendamento de tarefas

## Com o Apache Airflow

No caso do uso do [Apache Airflow](https://airflow.apache.org), uma [coleção de
DAGs](https://gitlab.com/socioambiental/alertas-backend/-/tree/main/src/airflow/dags)
está disponível tanto para importações automáticas quanto para manuais. A
periodicidade das importações automáticas já foi escolhida para otimizar a
importação dos dados dentro da escala de atualização de cada um deles mas sem
que sejam importados com frequência excessiva.

## Sem o Apache Airflow

Caso seja escolhida a operação do sistema sem o Apache Airflow, é possível usar
alguns alvos do
[Makefile](https://gitlab.com/socioambiental/alertas-backend/-/blob/main/Makefile).

<!--
Vale considerar a seguinte lógica de estágios de importação.

Run stages:

* Stage 0: those importers should be the first run in a fresh database,
  then run in a regular basis.
* Stage 1+: should be run only after the database was populated by Stage 0
  importers at least once.

This can be stated with the following diagram:

    stage 0                           stage 1+
    importers/isa/(load|transform) >> importers/((?!isa)).*/(extract|load|transform)

In summary, ordering should be:

* Stage 0: upon initialization, then weekly.
* Stage 1: daily.
* Stage 2: monthly.
* Stage 3: biannual.
* Stage 4: manual tasks.

A fresh system should run those sequences above one after another first and
then schedule each according to it's timeframe.
-->

## Necessidades específicas

Alguns importações possuem necessidades específicas em sua operação normal:

```eval_rst
* FIRMS MODIS e VIIRS Arquive:
    * Requerem geração manual de link com 45 dias de validade, vide procedimento
      detalhado numa :doc:`rotina própria </routines/firms>`.
    * Assumir demora de 6 horas para geração do link.
```
