# Alertas+ - Documentação

[English](/doc/en/) | [Español](/doc/es/)

O [Alertas+](https://alertas.socioambiental.org) é um painel informativo de
transparência que rastreia diariamente os alertas de desmatamento e degradação
dentro e fora de Áreas Protegidas na Amazônia Legal e tem o objetivo de
fornecer dados que auxiliem na qualificação de pautas para jornalistas, no
trabalho de gestores e no exercício da cidadania e da participação social para
a proteção da Amazônia.

Esta documentação detalha tecnicamente o funcionamento do sistema, desde sua
concepção até a sua operação.

![Instituto Socioambiental](_static/images/isa.png) O Alertas+ é desenvolvido e
mantido pelo [Instituto Socioambiental](https://www.socioambiental.org).

## Índice


```eval_rst
.. toctree::
   :glob:
   :maxdepth: 2

   specs
   review
   notes
   install
   routines
   research
   development
   code
```

<!--
## Índices e tabelas

```eval_rst
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```
-->

## Contato

A Equipe do Alertas+ pode ser contatada através do email
[alertas@socioambiental.org](mailto:alertas@socioambiental.org).
