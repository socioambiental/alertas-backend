---
title: >
  \ \ \ \ \ \ \ \ \ \ \ \ \ \
  ![](../_static/images/isa.png){width=1in}
  ![](../_static/images/logo.png){width=1.5in}\newline
  Alertas+ - Estudo de Viabilidade - Ciclo 3
author:
  - Equipe Alertas+ (alertas@socioambiental.org)
abstract: >
  Documento de planejamento da Equipe Alertas+, integrante do Programa de
  Monitoramento de Áreas Protegidas do Instituto Socioambiental, contendo estudos
  de viabilidade para atividades relevantes e passíveis de
  inclusão no Ciclo 3.0.0 do Alertas+, previsto para 2022.
date: 28/10/2021
header-includes:
  - \usepackage[portuguese]{babel}
  - \usepackage[margin=1in]{geometry}
---

## Introdução

Este documento contém estudos de viabilidade para atividades elencadas com
algum nível de importância -- pela Equipe do Alertas+[^alertas-mais], por
outros profissionais do ISA, por parceiros ou mesmo pelo público -- e que podem
ser incluídas no Ciclo 3 de desenvolvimento, previsto para 2022.

O Estudo de Viabilidade apenas indica se uma atividade é factível, sem
considerar a disponibilidade de equipe e recursos ou a priorização das
atividades entre si ou em relação a outras iniciativas planejadas.

Tal estudo também pode apoiar tanto a especificação técnica do Ciclo 3 quanto
ser usado como base para a redação de Concept Papers para projetos de
financiamento das atividades planejadas.

[^alertas-mais]: [https://alertas.socioambiental.org](https://alertas.socioambiental.org).

## Atividades relevantes

Considerando as discussões com diversos atores ocorridas ao longo dos Ciclos 1 e 2, as
seguintes atividades foram consideradas com relevância suficiente para serem
avaliadas:

* Incorporação de conjuntos de dados **SIRAD** como novos tipos de evento.
* Incorporação de **Florestas Públicas Não Destinadas** como novo tipo de território.
* **Expansão da abrangência territórial** do Alertas+ para além da Amazônia Legal.
* Painel básico com cômputos e outras **informações gerais e fundamentais** na
  abrangência do sistema.
* Melhorias de **comportamento** e **usabilidade** do sistema.
* Subsistema de **notificação personalizada** de alertas recentes.
* Inclusão de **estimativas de carbono**.

## Sumário Executivo

A Equipe do Alertas+ concluiu que o Ciclo 3 é viável a depender das seguintes
escolhas necessárias:

1. Backend: escolher entre expansão de abrangência (Cerrado/Pantanal) OU
   incorporação de dados (Florestas Públicas/SIRADs).
2. Frontend: escolher entre painel básico e melhorias de uso OU subsistema de notificações.
3. Análises: inclusão das estimativas de carbono.

Apesar dessas 3 linhas serem compatíveis com um desenvolvimento paralelo, a
Equipe do Alertas+ pode não ter disponibilidade suficiente para tal.

Assim, pode ser importante escolher apenas uma combinação dessas linhas, como
por exemplo:

* Expansão de abrangência (Cerrado/Pantanal).
* Painel básico e melhorias de uso.

A fim de melhor embasar as escolhas do escopo, a seguir há um detalhamento
avaliando cada uma dessas atividades.

## Incorporação de conjuntos SIRAD

Os conjuntos do tipo SIRAD[^sirad] são desenvolvidos e mantidos por diversas
equipes do Instituto Socioambiental, porém a ausência de painéis dinâmicos de
consulta para os mesmos os tornam importantes candidatos para a inclusão no
Alertas+.

[^sirad]: Sistema de Indicação por Radar de Desmatamento

Esta avaliação considerou os seguintes sistemas SIRAD atualmente em uso:

* SIRAD-X
* SIRAD-Y (renomeado recentemente de SMGI - Sistema de Monitoramento de Garimpo Ilegal)
* SIRAD Isolados

Para tal avaliação, um quadro comparativo[^comparacao-sirad] foi desenvolvido,
permitindo que os principais aspectos relevantes de cada sistema sejam
ressaltados:

[^comparacao-sirad]: Disponível em [https://docs.google.com/spreadsheets/d/1RojH0r--UQkSgEy1UruIBfZ5ZcWmxzh4Y7S3ndIUUlg/edit#gid=0](https://docs.google.com/spreadsheets/d/1RojH0r--UQkSgEy1UruIBfZ5ZcWmxzh4Y7S3ndIUUlg/edit#gid=0).

### SIRAD-X

* Está bem estruturado, sendo o SIRAD mais antigo em operação.
* A equipe do Xingu+ possui rotina de monitoramento mensal.
* Recentemente foram implementadas rotinas de fluxo para extração, edição e
  consolidação em base corporativa (ArcGIS/Postgres).
* Dados já estão num banco PostGIS institucional, o que facilitaria a
  incorporação. Ou seja, não seria necessário o trânsito de shapefile.
* Os metadados precisam ser incorporados no banco corporativo.

### SIRAD-Y

* Não estão usando mais dados dos satélites Sentinel, mas sim imagens da
  Planet, por atender melhor as demandas (alta resolução espacial).
* O sistema tem novas características que o distancia do sistema SIRAD,
  por isso é chamado de Sistema de Monitoramento de Garimpo Ilegal (SMGI).
* A nova metodologia ainda não está consolidada e a estrutura de
  dados precisa ser definida.
* A periodicidade ainda não é regular, ou seja, não há histórico de ocorrência.
* A equipe do SIRAD-Y está aberta à mudanças de metodologia, porém não acredita
  que os dados devam ser disponibilizados no Alertas+
* Os resultados podem ser questionados, pois alguns mosaicos possuem deslocamentos,
  o que impacta em polígonos redimensionados de um mês para outro.
* O monitoramento atual é imediatista: tem enfoque na localização e denúncia.
  Não é uma metodologia SIRAD.
* Os dados não estão públicos.
* A conclusão é que o sistema ainda não está num estágio pronto para
  incorporação e disponibilização.

### SIRAD Isolados

* É sistemático: tem regularidade no tempo e no espaço.
* O mapeamento é realizado em um conjunto pré-selecionado de áreas
  protegidas e não são contínuas espacialmente.

### Roteiro de implementação

Considerando apenas os sistemas que estão com metodologia e dados numa situação
satisfatória para incorporação, os seguintes pontos precisam ser considerados:

* Checagem do fluxo de trabalho, garantindo que as atualizações dos conjuntos
  estejam no banco PostGIS.

* Avaliação da implementação no Painel de uso, seja com disclaimers/avisos,
  painel de seleção própria ou seletor de abrangência, indicando de algum modo
  para a(o) usuária(o) que numa dada seleção a ausência de dados do SIRAD não
  implica necessariamente na ausência de eventos, já que a abrangência
  territorial de todos os conjuntos SIRAD é limitada.

### Alocação de equipe

Esta atividade requer a alocação dos seguintes perfis profissionais em
cronograma sincronizado:

* Codificação backend:
  * Analista de desenvolvimento com orientação a geoprocessamento.
  * Analista de desenvolvimento de sistemas.
* Homologação:
  * Analista em geoprocessamento.
* Frontend:
  * Analista de desenvolvimento de sistemas.

### Conclusão

* São viáveis para o Ciclo 3: SIRAD-X e SIRAD Isolados, com esforço médio:
  * SIRAD-X: pela sua abrangência territorial contínua, convém avaliar a
    criação de um painel específico separado do painel geral, ou mesmo a
    proposta de criação de um painel de seleção no próprio site do Xingu+.
  * SIRAD Isolados: pela sua abrangência territorial mais dispersa, é
    necessário um estudo adicional para definir a interface mais apropriada
    para navegação deste conjunto.
* Já o SIRAD-Y ainda necessita de amadurecimento.

## Incorporação de Florestas Públicas Não Destinadas

A Florestas Públicas Não Destinadas são aquelas localizadas em áreas da União
mas cujo uso ainda não foi definido[^florestas-nao-destinadas].

[^florestas-nao-destinadas]: Vide [https://ifn.florestal.gov.br/cadastro-nacional-de-florestas-publicas](https://ifn.florestal.gov.br/cadastro-nacional-de-florestas-publicas).

### Pré-avaliação

Uma pré-avaliação elencou os seguintes aspectos:

* Importância: alta, sendo uma demanda dos potenciais usuários do Alertas+.

* Esforço: baixo:
  * O conjunto de dados foi atualizado recentemente[^florestas-publicas-dados].
  * O processamento do conjunto no Alertas+ seria análogo ao já existente para
    Áreas Protegidas.

[^florestas-publicas-dados]: [https://ifn.florestal.gov.br/cadastro-nacional-de-florestas-publicas](https://ifn.florestal.gov.br/cadastro-nacional-de-florestas-publicas).

Um inventário inicial indica que o conjunto mais recente tem as seguintes
características:

* Total de polígonos: 1694.
* Área total (ha): 63 milhões de hectares.
* Possível existência dos polígonos com atributos desencontrados/descontínuos.

### Discussão

Uma discussão dentro da Equipe trouxe considerações adicionais.

A seguir são pontuadas algumas dessas considerações.

Em especial, o assunto da Florestas Públicas Não Destinadas trouxe também o
questionamento da incorporação de dados do CAR (Cadastro Ambiental Rural) no
Alertas+.

#### Da importância do dado

A incorporação das Florestas Públicas Não Destinadas, juntamente com dados do
CAR, permitiria ao Alertas+ ter um retrato completo do desmatamento na
Anazônia, indicando quando do desmatamento ocorre dentro de cada tipo área,
incluindo CAR, Áreas Protegidas e Áreas Não-Destinadas.

No entando, surje a questão de qual o nível de incidência que o ISA tem nesses
territórios hoje, isto é, se o esforço de incorporação dos dados refletirá na
incidência do ISA quanto às políticas públicas para tais territórios.

Nesse sentido, surgiu o questionamento de qual a prioridade desta incorporação
dentro da estratégica não só do Monitoramento quando de toda a instituição.

#### Da incorporação conjunta com o CAR

Como dito anteriormente, a ingestão no Alertas+ dos polígonos das Florestas
Públicas Não Destinadas possibilitaria uma "radiografia" da Amazônia, mas para
haver completude nesse quadro seria necessária também uma incorporação do CAR.

Mas a Equipe do Alertas+ considera que a incorporação do CAR, neste momento, é
inviável pelos seguintes motivos:

1. Foi mencionado que somente 3% do CAR foi analisada até hoje pelo poder
   público e existem indicações de que a área correspondente aos polígonos do CAR
   é muito maior do que de fato poderia, seja pela sobreposição interna (áreas
   CAR sobrepostas entre si), seja por sobreposição com áreas protegidas ou mesmo
   com Florestas Públicas Não Destinadas. O fato do CAR ser uma base alimentada
   por documentos auto-declaratórios cria um conjunto de representatividade
   questionável.

2. As camadas territoriais usadas no Alertas+ atualmente tem "nome e sobrenome",
   como por exemplo Terras Indígenas, Unidades de Conservação ou Unidades da
   Federação. Mas uma massa de polígonos de florestas públicas e CAR podem ser
   polígonos anônimos.
   Numa análise inicial dos dados das Florestas Públicas Não Destinadas de 2020,
   foi observado que cada gleba tem um nome. Mas acredita-se que o caso do CAR
   seja oposto, com glebas anônimas.

Possíveis soluções para ambos problemas:

* A utilização de um polígono CAR no formato de máscara, usando por exemplo a
  compilação do Atlas da Agropecuária[^atlas-agropecuaria]. Esta abordagem
  foi denominada de "chapadão", ou seja, composta por um único shape com o dado
  consolidado.

* Estimativa das áreas privadas a partir de uma máscara de exclusão das Áreas
  Protegidas juntamente com as Florestas Públicas Não Destinadas, porém a
  equipe não discutiu a viabilidade do procedimento.

Tais abordagens aumentam consideravelmente o trabalho de implementação e requer
novas rotinas de atualização do dado, o que a torna questionável neste momento
à luz das considerações do tópico anterior.

[^atlas-agropecuaria]: [http://atlasagropecuario.imaflora.org/](http://atlasagropecuario.imaflora.org/)

#### Das considerações de performance

Algumas considerações também foram elencadas para avaliar como essa
incorporação afeta a performance do sistema:

1. A incorporação de Florestas Públicas Não Destinadas terá um impacto mediano
   na performance, pois todos os tipos de eventos também terão de ser
   interseccionados em cerca de 1700 novos polígonos totalizando aproximadamente
   63 milhões de hectares.

1. Uma eventual incorporação do CAR terá bastante impacto na performance.

### Roteiro de implementação

Grosso modo, a implementação das rotinas de incorporação de Florestas Públicas
Não Destinadas envolveria as seguintes etapas:

* Criação de um importador automático no backend do Alertas+, usando o
  shapefile estático ou o serviço WMS disponibilizados pelo Serviço Florestal.

* Adequação da interface do Painel de dados para exibir também informações
  deste novo tipo e território.

* Homologação da atividade.

No caso do CAR, o roteiro de implementação não foi esboçado.

### Alocação de equipe

Esta atividade requer a alocação dos seguintes perfis profissionais em
cronograma sincronizado:

* Codificação backend:
  * Analista de desenvolvimento com orientação a geoprocessamento.
  * Analista de desenvolvimento de sistemas.
* Homologação:
  * Analista em geoprocessamento.
* Frontend:
  * Analista de desenvolvimento de sistemas.

### Conclusão

Da discussão precedente, conclui-se que:

* A incorporação de Florestas Públicas Não Destinadas no sistema Alertas+ é
  viável, porém a capacidade de incidência no curto e médio prazo não é tão
  grande. Prioridade média; esforço baixo.

* A incorporação do CAR no sistema Alertas+ é inviável neste momento por conta
  do custo de performance e da dificuldade de extrair relevância do dado.
  Prioridade: média; esforço: alto.

## Expansão da abrangência territorial

Uma das questões mais colocadas à Equipe do Alertas+ foi a da abrangência
territorial do sistema para além da Amazônia Legal Brasileira, o que levou à
avaliação da expansão territorial para o Cerrado e, opcionalmente, também para
o Pantanal.

### Pré-avaliação

Uma pré-avaliação da atividade elencou os seguintes aspectos:

* Importância: alta, pois tal demanda foi apresentada em várias ocasiões por
  distintos atores.

* Esforço: alto, por requerer uma revisão geral do subsistema de importação,
  já que atualmente os algoritmos possuem filtragem espacial para a Amazônia
  Legal e heurísticas que assumem tal abrangência em comportamentos diversos
  do sistema.

\newpage

### Abrangência dos dados

Com relação à abrangência dos dados, coube avaliar quais dos conjuntos
utilizados possuem disponibilidade não somente para o Cerrado como também para
o Pantanal ou somente para o bioma amazônico, tal como sumarizado na tabela a
seguir.

| Conjunto   | Am. Legal? | Bioma Amazônico? | Cerrado? | Pantanal? |
|------------|------------|------------------|----------|-----------|
| SAD        | sim        | não              | não      | não       |
| DETER      | sim        | sim              | sim      | não       |
| PRODES     | sim        | sim              | sim      | não       |
| FIRMS      | sim        | sim              | sim      | sim       |
| VIIRS      | sim        | sim              | sim      | sim       |
| Am. Dashb. | sim        | sim              | sim      | sim       |

Vale observar que, no caso do PRODES Cerrado, os dados são anuais a partir de 2013. Bianuais de 2002 a 2012. E o baseline é o desmatamento acumulado até 2000.[^prodes-cerrado], o que pode gerar complexidade adicional na interface de seleção
do painel.

Para o bioma Pantanal, as fontes de dados de desmatamento e degradação florestal, adotadas no painel, não cobrem o bioma.

[^prodes-cerrado]: De acordo com [http://cerrado.obt.inpe.br/monitoramento-do-desmatamento-no-cerrado-brasileiro-por-satelite/](http://cerrado.obt.inpe.br/monitoramento-do-desmatamento-no-cerrado-brasileiro-por-satelite/)

### Discussão

Uma discussão dentro da Equipe trouxe considerações adicionais.

A seguir são pontuadas algumas dessas considerações.

Em especial, a questão mais importante para esta incorporação foi: a
abrangência do sistema deve deixar o recorte político (Amazônia Legal
Brasileira) e passar para o bioma (Amazônia, Cerrado etc)?

Por um lado, alguns conjuntos de dados trabalham com o recorte político
da Amazônia Legal Brasileira. Algumas ações de incidência política do ISA 
tratam deste recorte espacial.

Por outro, foi colocado durante a discussão que a noção de "Amazônia Legal"
está ameaçada e traz incômodos a alguns atores.

#### Recorte por biomas

Uma consideração estrita do bioma pela cobertura vegetal traria uma grande
complexidade para o sistema, já que as rotinas de importação e de consulta
necessitariam de um arranjo como o esboçado a seguir:

* Na fase de importação de shape dos biomas, duas possibilidades a serem
  consideradas:
  * Uso de um único shape com os limites mais atuais dos biomas, isto é,
    trabalho adicional da definição do critério a partir de dados disponíveis.
  * Ou uso shapes anuais, de acordo com a mudança do bioma, ou seja, trabalho
    adicional de cruzamento de dados.
* Para a consulta de eventos:
  * No caso de shapes anuais dos biomas, seria necessário avaliar como a seleção de
    eventos deve ser feita no caso da seleção por bioma, possivelmente separada em
    consultas por ano para, em cada um deles, seja feito o cálculo de emissões
    de acordo com o shape do bioma para aquele ano; eventualmente isso permitiria
    calcular também a redução do bioma, mas este caminho de desenvolvimento pode
    ser bem complexo e até inviável dados os recursos disponíveis.

Para evitar problemas como estes, houve uma discussão sobre quais seriam os limites
de um bioma, independentemente da variação da cobertura vegetal ao longo do tempo,
sendo talvez o principal pressuposto necessário para realizar a expansão de abrangência:
bioma, no contexto do Alertas+, seria uma região onde as condições climáticas,
geográficas e de solo favorecem uma dada cobertura vegetal.

Foi argumentado que uma vegetação pode desaparecer repentinamente -- por
exemplo no caso de queimadas --, porém as condições de favorecimento da sua
aparição mudam muito mais lentamente.

Assim, revisões nos limites dos biomas seriam possíveis, porém não haveria necessidade
de tratá-las numa escala anual e sim em períodos mais longos, como por exemplo na
escala climática, haja vista o movimento para o norte do bioma Cerrado com as mudanças
climáticas.

Tais revisões nos polígonos dos biomas poderiam ser tratadas de forma análoga
às rotinas de atualização das Terras Indígenas: havendo alterações de limite,
os respectivos shapes são atualizados e uma nova importação dos dados atualiza
todos os cômputos.

Perde-se a informação das transições de bioma, porém ganha-se informação de
alertas recentes em cada bioma.

Para tal, recomenda-se a adoção do IBGE como a fonte oficial dos limites dos
biomas, como já ocorre hoje em rotinas realizadas pelo Monitoramento em outras
iniciativas através de rotinas periódicas.

É importante também considerar a mesma versão de biomas adotada pelo INPE, para
que não haja grandes divergências entre os resultados das consultas por ambos
sistemas.

Também é necessário verificar qual escala de mapeamento foi adotada pelo INPE.

##### Substituição ou complementação da Amazônia Legal

Uma vez que a Equipe chegou a um consenso sobre a abordagem do bioma, foi
possível abordar a seguinte questão: o recorte político da Amazônia Legal seria
substituído pelo bioma Amazônico?

Pela discussão realizada, concluiu-se que não haveria a necessidade de uma
substituição, mas sim que ambos recortes poderiam ser complementares.

O sistema poderia ingerir todos os eventos no recorte político e nos recortes
por bioma mas trabalhando com as sobreposições existentes entre conjuntos como
PRODES Amazônia e PRODES Cerrado, por exemplo, já que algumas porções da
Amazônia Legal incidem no bioma Cerrado.

Outras fontes, como os conjuntos FIRMS, não possuem tais sobreposições e não
precisam de tratamento adicional.

Em termos de experiência de uso (UX) da interface de seleção do Painel, seria
possível pensar numa macro-abrangência, isto é, um filtro inicial num nível
hierárquico acima dos existentes.

Com isso, a usuária inicialmente selecionaria a abrangência dentre as
disponíveis -- seja Amazônia Legal ou biomas -- e a partir disso ter acesso a
cômputos específicos para essas regiões.

### Roteiro de implementação

Em linhas gerais, a implementação desta atividade consistiria das seguintes etapas:

* Carga de dados:
  * Importação dos limites dos biomas.
* Processamento:
  * Remoção de todas as filtragens para Amazônia Legal:
    * De cada importador, se houver.
    * Nas rotinas gerais/comuns de importação.
  * Inclusão de rotinas específicas de filtragem por bioma. Foi considerado que
    isto poderia ser feito pela atribuição, em cada polígono da tabela
    `event_territory`, de colunas adicionais que situariam tal polígono na
    macro-abrangência através do seu centróide.
* API/Tileserver:
  * Remoção de filtragens para a Amazônia Legal.
  * Estimativa de árvores removidas precisa considerar o(s) bioma(s) em que se
    encontra o recorte territorial escolhido.
* Painel:
  * Mudança na lógica do formulário para incluir a seleção por bioma/Amazônia legal.
* Narrativa:
  * Atualização dos textos e elementos gráficos para incluir as novas abrangências.
* Comunicação:
  * Plano e preparações para lançamento da nova versão do sistema.

### Alocação de equipe

Esta atividade requer a alocação dos seguintes perfis profissionais em
cronograma sincronizado:

* Codificação backend:
  * Analista de desenvolvimento com orientação a geoprocessamento.
  * Analista de desenvolvimento de sistemas.
* Homologação:
  * Analista em geoprocessamento.
* Frontend:
  * Analista de desenvolvimento de sistemas.
* Narrativa:
  * Analista de dados/conteúdo.
* Comunicação:
  * Jornalista.

### Conclusão

* A expansão de abrangência é viável. Prioridade alta; esforço: alto.

## Painel básico

Foi identificada a necessidade de um nível de análise básico com nível de
complexidade situado entre a página inicial e o Painel de seleção.

O time de desenvolvimento frontend avaliou a criação de um painel básico e
geral com cerca de 10 tipos de cômputos essenciais.

Tal painel teria mais informações do que a página inicial, mas na mesma linha:
apresentação dos dados mais solicitados, pouca ou nenhuma possibilidade de
escolha (sem formulário ou com formulário de seleção bem limitado).

Tal avaliação não passou por discussão da Equipe geral do Alertas+, resumindo-se
aos seguintes aspectos:

* Importância: alta, pois está relacionada a um aumento do uso do sistema.
* Esforço: médio, por requerer implementações tanto no backend quando no frontend.

### Roteiro de implementação

Em linhas gerais, a implementação desta atividade consistiria das seguintes etapas:

* Definição sobre quais cômputos são prioritários e viáveis.
* Implementação no backend.
* Codificação de componentes (widgets) dos cômputos.
* Criação do painel básico.
* Novo esquema de painéis: básico, interativo e detalhado.

### Alocação de equipe

Esta atividade requer a alocação dos seguintes perfis profissionais em
cronograma sincronizado:

* Analista de desenvolvimento de backend para a geração de novos cômputos.
* Analista de desenvolvimento de frontend para a criação da nova interface.

### Conclusão

* A atividade é viável, com prioridade alta e esforço médio.

## Melhorias de comportamento e usabilidade do sistema

Outra necessidade identificada para o Alertas+ foi a melhoria geral de
usabilidade (UX), consistindo na continuidade do estudo e implementação de
melhorias no comportamento das páginas internas, especialmente a do Painel
interativo.

A avaliação realizada por analista de frontend é resumida a seguir:

* Importância: alta, pois as páginas internas não estão otimizadas,
  dificultando o uso do Painel.
* Esforço: alto, por requerer sinergia com equipe externa de desenvolvimento de UX.

### Roteiro de implementação

Em linhas gerais, a implementação desta atividade consistiria das seguintes etapas:

* Estudo de usabilidade (UX).
* Criação de novos mockups para as páginas internas.
* Implementação, preservando toda a lógica de componentes existente.
* Refatoração dos placares da página inicial, para que utilizem a mesma lógica
  do painel interno.

### Alocação de equipe

Esta atividade requer a alocação dos seguintes perfis profissionais em
cronograma sincronizado:

* Analista de desenvolvimento de frontend.
* Analista de desenvolvimento de frontend com especialidade em UX.

### Conclusão

* Atividade viável, com importância alta e esforço alto.

## Subsistema de notificação personalizada de alertas recentes

Inspirado pela implementação experimental no sistema Xingu+, um dos propósitos
originais do Alertas+ consistia numa ferramenta de notificação para alertas
recentes em seleções personalizadas.

Tal funcionalidade também foi questionada por uma das organizações consultadas
pela Equipe do Alertas+ durante o desenvolvimento do Ciclo 2.

A avaliação realizada por analista de frontend e backend é resumida a seguir:

* Importância: alta, permitindo a detecção automatizada de incidentes.
* Esforço: alto, requerendo o desenvolvimento de um novo subsistema com gestão de usuários.

### Roteiro de implementação

Em linhas gerais, a implementação desta atividade consistiria das seguintes etapas:

* Backend:
  * Criação de subsistema do backend para registro básico de usuários e gestão de
    suas seleções de interesse.
  * Rotina de lote para cálculo e emissão de alertas personalizados (Email,
    Telegram, WhatsApp etc).
  * Rotina de lote para envio via mídias públicas (Bot de Twitter) de alertas
    nas seleções de interesse geral (por exemplo toda a Amazônia).
* Frontend:
  * Implementação de funcionalidade de notificação de avisos de alertas para cada
    seleção do painel, similar aos alertas de passagens aéreas dos sites de
    viagens.
  * Comportamento codificado de tal modo que cada conta de usuário permita o
    acesso a uma página listando todas as seleções cadastradas, além de links
    para os respectivos dashboards, constituindo assim um "meta-dashboard".

### Alocação de equipe

Esta atividade requer a alocação dos seguintes perfis profissionais em
cronograma sincronizado:

* Analista de desenvolvimento de backend.
* Analista de desenvolvimento de frontend.
* Equipe externa de backend dedicada.
* Equipe externa de UX dedicada.

### Conclusão

* Atividade viável, com prioridade baixa e esforço alto.

## Inclusão de estimativas de carbono

Durante o Ciclo 2, foram implementadas rotinas para estimativa de emissão de
carbono tanto para eventos pontuais quanto poligonais. A interface do Painel
também ganhou cômputos sobre emissão.

Por conta de limitações no cronograma, a homologação dessas estimativas não
pôde ser realizada, e tal funcionalidade atualmente está desativada na
interface.

A avaliação realizada por gestor de projeto é resumida a seguir:

* Importância: alta, dada a relevância da informação.
* Esforço: alto, requerendo estudos sobre outras fontes de emissões, para comparabilidade, e muitos testes/calibrações. Caso haja expansão da abrangência geográfica do Alertas+ para outros biomas, não temos dados de emissões de carbono para além do bioma Amazônia e essas estimativas nos demais biomas é bem mais complexa.
* Descrição: continuidade do procedimento de homologação da saída de carbono.

### Roteiro de implementação

Em linhas gerais, a implementação desta atividade consistiria das seguintes etapas:

* Ativação da consulta de carbono no site de desenvolvimento.
* Consulta a especialistas e projetos (Amazon Dashboard, INPE etc).
* Definição de valores de referência e acurácia.
* Recalibração dos coeficientes.
* Ativação da consulta de carbono na produção.
* Lançamento público/live?

### Alocação de equipe

* Analista em geoprocessamento.
* Eventual consulta a especialistas em emissões.

### Conclusão

* Atividade viável, com prioridade alta e esforço alto.
* A conclusão desta atividade já está estruturada, podendo ocorrer durante
  período de manutenção anterior ao início do Ciclo 3.0.0, por exemplo num
  Ciclo 2.5.0.

\newpage

## Conclusões

A tabela a seguir resume as avaliações realizadas. Por conta das situações
distintas dos conjuntos de dados, os mesmos foram separados[^impacto-performance].

| Atividade                | Prioridade | Esforço | Risco | Impacto de perf. | Observações                                          |
|--------------------------|------------|---------|-------|------------------|------------------------------------------------------|
| SIRAD-X                  | alta       | médio   | baixo | baixo            | Req. design de UX                                    |
| SIRAD Isolados           | alta       | médio   | baixo | baixo            | Req. design de UX                                    |
| SIRAD-Y                  | alta       | alto    | médio | baixo            | Req. revisão da metodologia                          |
| Florestas Não-Destinadas | média      | baixo   | baixo | médio            |                                                      |
| CAR                      | média      | alto    | alto  | alto             | Req. redução de complexidade                         |
| Expansão de abrangência  | alta       | alto    | médio | alto             | Muito trabalhoso                                     |
| Painel básico            | alta       | médio   | baixo | nenhum           |                                                      |
| Melhorias de UX          | alta       | alto    | baixo | nenhum           | Req. sinergia com eq. externa                        |
| Notificações             | baixa      | alto    | médio | médio            | Req. sinergia com eq. externa                        |
| Estimativas de carbono   | alta       | alto    | baixo | nenhum           | Não temos dados para outros biomas, além da Amazônia |

[^impacto-performance]: O impacto de performance refere-se ao incremento no
                        custo computacional para o cruzamento de dados no backend.
