# Homologação

## Homologação dos dados do Alertas+

* Por Alana Almeida de Souza, Cicero Augusto, William Pereira Lima -
  05/08/2021.

A homologação do Alertas+ avaliou os seguintes aspectos relacionados aos
conjuntos de dados disponíveis no sistema:

* Completude;
* Cômputos do total de alertas no Painel de dados por diferentes tipos de
  territórios, categorias e recortes temporais;
* Cômputos da área afetada pelos alertas (quando pertinente) no Painel de dados
  por diferentes tipos de territórios, categorias e recortes temporais;
* Verificação das saídas gráficas do Painel de dados;
* Verificação das saídas tabulares do Painel de dados;
* Cômputos apresentados na página inicial do Alertas+.

A completude foi avaliada por meio da simples conferência entre os dados
apresentados no status do sistema do Painel Alertas+ e o total de eventos e de
área (quando pertinente) disponíveis nos shapefiles dos dados conforme estes
foram disponibilizados em suas respectivas fontes para o mesmo dia de
referência. Variações entre cômputos inferiores a +/- 10% foram consideradas aceitáveis e
associadas à modelagem de dados e à mudanças de projeção cartográfica.

Os cômputos dos totais de alertas e de áreas foram verificados por amostragem
considerando a complexidade das consultas disponíveis. Assim, essa verificação
considerou os diferentes relacionamentos topológicos entre territórios, quais
sejam: territórios sem sobreposição (ex.: Estados, municípios, áreas protegidas
sem sobreposição entre si) e territórios que se sobrepõem (ex. terras indígenas
contidas dentro de unidades de conservação).

A verificação das saídas gráficas e tabulares incluíram a inspeção visual dos
resultados das consultas e conferência do conteúdo das exportações nos
diferentes formatos.
