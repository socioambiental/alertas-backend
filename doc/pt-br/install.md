# Instalação

**ATENÇÃO: este documento está em desenvolvimento.**

A instalação básica é abordada nos seguintes documentos:

* [README](https://gitlab.com/socioambiental/alertas-backend/-/blob/main/README.md)
  do [Backend](https://gitlab.com/socioambiental/alertas-backend).
* [README](https://gitlab.com/socioambiental/alertas/-/blob/main/README.md)
  do [Frontend](https://gitlab.com/socioambiental/alertas).

Este documento contém detalhes para a instalação em ambiente de produção ou em
situações de desenvolvimento customizado (por exemplo na ausência de ambiente
contêinerizado).

## Produção

Esta seção trata sobre como rodar o sistema em ambiente de produção.

### Requisitos mínimos

* O sistema foi desenvolvido e testado em Ubuntu GNU/Linux 20.04 LTS e
  possivelmente será suportado em versões LTS superiores.

* Da mesma maneira, foi testado no gerenciador de bases de dados PostgreSQL 12
  e possivelmente é compatível com versões posteriores.

* Caso o sistema possua alguma rotina de atualizações e reinicializações
  de segurança, certifique-se de que esta não ocorra com periodiciade
  menor do que um mês, já que intervalos curtos podem impedir a importação
  de conjuntos de dados mais pesados como o PRODES / INPE.

* Espaço em disco: 100GB para abrigar os conjuntos de dados completos.
  O espaço pode ser reduzido caso não haja intenção de utilizar todos os
  conjuntos ou manter dados por longos períodos de tempo (salvaguarda).

* Memória RAM: 16GB para o backend (8GB para máquina de desenvolvimento local),
  ou superior, a depender da quantidade de acessos esperada e da velocidade
  desejada para as consultas.

* CPUs: 4 para o backend.

* Os requisitos de RAM e CPU para o frontend são bem menores, pois trata-se de
  uma aplicação estática compilada usando [Webpack](https://webpack.js.org).

### Instalação em ambiente de produção

A instalação é dividida em dois ambientes básicos:

* Ambiente para o backend.
* Ambiente para o frontend.

Ambos ambientes podem se encontrar no mesmo servidor/máquina virtual, porém é
recomendado que estejam em máquinas distintas.

Aqui será tratado apenas do ambiente backend,

Há uma série de scripts de provisionamento na pasta
[bin/](https://gitlab.com/socioambiental/alertas-backend/-/tree/main/bin).

#### Provisionamento básico do ambiente

Tanto o Docker quanto a base PostgreSQL podem ser provisionados manualmente.

O backend conta com os seguintes scripts de conveniência que podem ser usados
ou adaptados caso a caso:

    ./bin/provision-container-host
    ./bin/provision-postgresql

#### Provisionamento da base de dados

Analogamente, há um script de conveniência para a criação da base de dados
principal:

    ./bin/provision-database

Caso se queira rodar o [Apache Airflow](https://airflow.apache.org) para o
agendamento e importação automática de conjuntos, uma base de dados adicional
precisa ser criada, incluindo as credenciais de acesso.

#### Provisionamento do sistema

Uma vez que o ambiente de hospedagem e as bases de dados estejam provisionadas,
basta seguir de acordo com o
[README](https://gitlab.com/socioambiental/alertas-backend/-/blob/main/README.md)
do [Backend](https://gitlab.com/socioambiental/alertas-backend).

## Avançado

Procedimentos avançados de desenvolvimento, uso e teste do backend.

### Instalação em ambiente sem containerização

Para casos onde é desejado instalar o sistema diretamente numa máquina virtual
ou direto no "bare metal".

#### Requisitos

* Desenvolvido em Ubuntu GNU/Linux 20.04 (Focal Fossa).
* [GNU Make](https://www.gnu.org/software/make/).
* [Pipenv](https://docs.pipenv.org).
* [Banco PostgreSQL](https://www.postgresql.org) 13+ configurado com a
  [extensão PostGIS](https://postgis.net) versão 3+.
* Opcional: [dblink](https://www.postgresql.org/docs/current/dblink.html) e
  acesso aos bancos de dados internos do ISA caso se queira rodar os
  importadores `_dblink`.

#### Procedimento

Instale as dependências com o comando

    make vendor

#### Rodando

No caso da instalação sem containerização, os comandos podem ser invocados diretamente
usando o `make`.

Por exemplo, para inicializar a base de dados, baixar e incorporar os dados
mais recentes, use o comando

    make bootstrap
    make import

