# Guia de tradução da interface

Este é um roteiro básicao para tradução dos textos gerais e de interface web
do sistema, desde botões até legendas, títulos etc.

## Repositório e arquivos de tradução

Esses textos gerais e de interface são mantidos no site diretamente em arquivos
especiais, um por idioma. Esses arquivos lembram um pouco planilhas, mas são
mais simples e são mantidos num "repositório de código" dentro do Gitlab do
ISA.

Basicamente é preciso focar apenas nos arquivos de mensagens de cada idioma,
como por exemplo:

* [Textos gerais e de interface originais, em português](https://gitlab.com/socioambiental/alertas/-/blob/develop/config/messages/ptBr.js).
* [Textos em inglês](https://gitlab.com/socioambiental/alertas/-/blob/develop/config/messages/en.js).
* [Textos em espanhol](https://gitlab.com/socioambiental/alertas/-/blob/develop/config/messages/es.js).

## Como editar

É possível editar os arquivos direto no Gitlab com o seguinte roteiro: abrir o
arquivo, clicar em "Edit", fazer as alterações e salvá-las pelo botão "Commit
changes" (somente depois de clicar nesse botão que as alterações são salvas,
isto é, é diferente da dinâmica do Google Docs onde tudo é salvo
automaticamente)

Também é possível usar um editor de texto do tipo "Bloco de notas" (mas não
Word) pra trabalhar antes de enviar as mudanças ao Gitlab e assim ter um backup
no caso de problemas.

## Formato dos arquivos

Esses arquivos adotam o formato do [Vue I18n](https://kazupon.github.io/vue-i18n/pt/):

    'chave': 'valor',

As chaves são identificadores únicos usados pelo site pra encontrar a porção de texto correta.
Os três arquivos tem as mesmas chaves: só o conteúdo que muda de acordo com o idioma.

Exemplo para o item 'navigation':

    'navigation'                                  : 'Navigation',

    'navigation'                                  : 'Navigation',

    'navigation'                                  : 'Navegación',

As entradas de texto sempre estão dentro de aspas simples e são finalizadas por
uma vírgula.

## Dica de fluxo de trabalho

Uma sugestão de trabalho é manter o arquivo em português numa aba e o inglês ou
espanhol numa outra e passar entrada por entrada.

Muita coisa já estará traduzida, mas vale a pena revisar todas as entradas pois
nem sempre foram criadas pela equipe de tradução.
