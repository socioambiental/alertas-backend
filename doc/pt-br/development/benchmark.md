# Benchmark

## 28/04/2021

### pg_top

Rodando no heart durante uma importação do DETER:

    last pid: 95863;  load avg:  3.26,  3.02,  2.59;       up 0+03:17:27 13:01:09
    20 processes: 5 other background task(s), 13 idle, 2 active
    CPU states: 37.0% user,  0.0% nice,  4.8% system, 58.0% idle,  0.2% iowait
    Memory: 12G used, 3223M free, 0K shared, 198M buffers, 9105M cached
    Swap: 0K used, 1020M free, 0K cached, 0K in, 0K out

        PID USERNAME    SIZE   RES STATE   XTIME  QTIME  %CPU LOCKS COMMAND
      95864 postgres   4300M   29M active   0:00   0:00   0.0     8 postgres: 12/main: postgres postgres [local] idle
      81242 alertas    4436M 1991M active  20:22  20:22 100.1    97 postgres: 12/main: alertas alertas 172.18.0.10(46788) CREATE TABLE AS
      77508 airflow    4299M   24M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.8(50960) idle
       1222            4299M 1921M          0:00   0:00   0.0     0 postgres: 12/main: checkpointer
      77506 airflow    4299M   24M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.8(50956) idle
       1225            4298M 8516K          0:00   0:00   0.0     0 postgres: 12/main: autovacuum launcher
       1223            4298M   42M          0:00   0:00   0.0     0 postgres: 12/main: background writer
      95791 airflow    4299M   26M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.9(56126) idle
      76961 airflow    4299M   22M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.6(55798) idle
       1224            4297M   22M          0:00   0:00   0.0     0 postgres: 12/main: walwriter
      77509 airflow    4299M   24M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.8(50962) idle
      76948 airflow    4300M   54M idle     0:00   0:00   2.0     0 postgres: 12/main: airflow airflow 172.18.0.7(45008) idle
      77510 airflow    4299M   24M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.8(50964) idle
      95336 airflow    4299M   26M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.9(56062) idle
       1227 postgres   4298M 6784K          0:00   0:00   0.0     0 postgres: 12/main: logical replication launcher
      77514 airflow    4299M   24M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.8(50966) idle
      77037 airflow    4299M   27M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.9(52770) idle
      94404 airflow    4299M   26M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.9(55814) idle
      94864 airflow    4299M   26M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.9(55876) idle
      76950 airflow    4300M   27M idle     0:00   0:00   0.0     0 postgres: 12/main: airflow airflow 172.18.0.7(45010) idle

### pgbench

#### Comandos e saídas

##### VM laptop Silvio

      alertas-backend.localhost.socioambiental.org  user   feature/refactor  /  srv  shared  pgbench -U alertas_admin -h localhost -i alertas
    dropping old tables...
    NOTICE:  table "pgbench_accounts" does not exist, skipping
    NOTICE:  table "pgbench_branches" does not exist, skipping
    NOTICE:  table "pgbench_history" does not exist, skipping
    NOTICE:  table "pgbench_tellers" does not exist, skipping
    creating tables...
    generating data...
    100000 of 100000 tuples (100%) done (elapsed 0.03 s, remaining 0.00 s)
    vacuuming...
    creating primary keys...
    done.
      alertas-backend.localhost.socioambiental.org  user   feature/refactor  /  srv  shared  pgbench -U alertas_admin -h localhost alertas
    starting vacuum...end.
    transaction type: <builtin: TPC-B (sort of)>
    scaling factor: 1
    query mode: simple
    number of clients: 1
    number of threads: 1
    number of transactions per client: 10
    number of transactions actually processed: 10/10
    latency average = 5.133 ms
    tps = 194.805955 (including connections establishing)
    tps = 242.506336 (excluding connections establishing)
      alertas-backend.localhost.socioambiental.org  user   feature/refactor  /  srv  shared  

##### VM Heart

Diferença em relação ao benchmarking do dia 26/04/2021: incremendo para 4096M nos shared_buffers do postgresql:

    0 20210428 07:18:55 alertas-backend@heart:~ $ pgbench -U alertas_admin -h localhost -i alertas
    dropping old tables...
    NOTICE:  table "pgbench_accounts" does not exist, skipping
    NOTICE:  table "pgbench_branches" does not exist, skipping
    NOTICE:  table "pgbench_history" does not exist, skipping
    NOTICE:  table "pgbench_tellers" does not exist, skipping
    creating tables...
    generating data (client-side)...
    100000 of 100000 tuples (100%) done (elapsed 0.04 s, remaining 0.00 s)
    vacuuming...
    creating primary keys...
    done in 0.50 s (drop tables 0.00 s, create tables 0.01 s, client-side generate 0.25 s, vacuum 0.12 s, primary keys 0.12 s).
    0 20210428 07:19:05 alertas-backend@heart:~ $ pgbench -U alertas_admin -h localhost alertas
    starting vacuum...end.
    transaction type: <builtin: TPC-B (sort of)>
    scaling factor: 1
    query mode: simple
    number of clients: 1
    number of threads: 1
    number of transactions per client: 10
    number of transactions actually processed: 10/10
    latency average = 5.651 ms
    tps = 176.956254 (including connections establishing)
    tps = 281.256847 (excluding connections establishing)
    0 20210428 07:19:08 alertas-backend@heart:~ $

## 26/04/2021

### hdparm

#### Resumo

| Máquina          | Parâmetro                   | Valor          |
|------------------|-----------------------------|----------------|
| VM laptop Silvio | Timing cached reads         | 5217.80 MB/sec |
| VM laptop Silvio | Timing buffered disk reads  | 168.91 MB/sec  |
| heart            | Timing cached reads         | 5100.36 MB/sec |
| heart            | Timing buffered disk reads  | 113.09 MB/sec  |
| krisiun          | Timing cached reads         | 7384.48 MB/sec |
| krisiun          | Timing buffered disk reads  | 111.26 MB/sec  |

#### Comandos e saídas

##### VM laptop Silvio

Trata-se de uma VM na máquina de desenvolvimento do Silvio:

      alertas-backend.localhost.socioambiental.org  user   feature/refactor  /  srv  shared  sudo hdparm -Tt /dev/vda

    /dev/vda:
     Timing cached reads:   10406 MB in  1.99 seconds = 5217.80 MB/sec
     HDIO_DRIVE_CMD(identify) failed: Inappropriate ioctl for device
     Timing buffered disk reads: 508 MB in  3.01 seconds = 168.91 MB/sec
      alertas-backend.localhost.socioambiental.org  user   feature/refactor  /  srv  shared  

##### VM Heart

    0 20210426 12:57:14 root@heart:/var/sites/alertas-backend (develop) # hdparm -Tt /dev/sda

    /dev/sda:
     Timing cached reads:   10156 MB in  1.99 seconds = 5100.36 MB/sec
    SG_IO: bad/missing sense data, sb[]:  70 00 05 00 00 00 00 0a 00 00 00 00 20 00 00 c0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
     Timing buffered disk reads: 340 MB in  3.01 seconds = 113.09 MB/sec
    0 20210426 12:57:34 root@heart:/var/sites/alertas-backend (develop) #

##### VM Krisiun

Máquina de desenvolvimento auxiliar, usada noutros projetos e constando aqui
apenas como comparativo para o heart, já que se encontra na mesma estrutura.

    0 20210426 10:49:10 root@krisiun:~ # hdparm -Tt /dev/sda

    /dev/sda:
     Timing cached reads:   14658 MB in  1.98 seconds = 7384.48 MB/sec
     Timing buffered disk reads: 334 MB in  3.00 seconds = 111.26 MB/sec
    0 20210426 12:57:48 root@krisiun:~ #
