# Acesso a Bancos de Dados Corporativos via dblink

Procedimento para criar acesso somente leitura a alguns dados de bases
geográficas institucionais do ISA.

Login no guapore2:

    ssh guapore2

Criação de usuário "alertas"

    su postgres -c "createuser alertas"

Mudança de senha:

    psql -U postgres -h localhost -c "ALTER ROLE alertas WITH ENCRYPTED PASSWORD 'uma-senha-longa'"

Criei os GRANTS apenas de selação para o usuário "alertas:

    bd_monitoramento="isa.areacons_isa_pl isa.consest_isa_pl isa.areaindi_isa_pl isa.tquilombos_isa_pl"
    bd_base_250="isa.LIM_Unidade_Federacao_A isa.LIM_Municipios_A_MD2017"
    bd_desmatamento="isa.desmatacumuladoamz_f isa.desmatacumuladocer_f isa.desmatestimadoamz isa.desmatestimadoamz isa.alertasareassad"

    for table in $bd_monitoramento; do
      grant="GRANT SELECT ON ${table} TO alertas;"
      psql -U postgres -h localhost bd_monitoramento -c "$grant"
    done

    for table in $bd_base_250; do
      grant="GRANT SELECT ON ${table} TO alertas;"
      psql -U postgres -h localhost bd_base_250 -c "$grant"
    done

    for table in $bd_desmatamento; do
      grant="GRANT SELECT ON ${table} TO alertas;"
      psql -U postgres -h localhost bd_monitoramento -c "$grant"
    done

Nota: o PostgreSQL não está pedindo senha para o usuário "postgres"! É fácil de repdroduzir:

    psql -U postgres -h localhost # nenhuma senha é pedida e não há um ~/.pgpass para meu usuário

Bônus points! Fazendo um [túnel de SSH](https://dev.socioambiental.org/infraestrutura/procedimentos/ssh.html#tunel-ssh)
do laptop para o guapore2 através do heart para não precisar mexer no
`pg_hba.conf`:

    ssh heart -L 5432:guapore2:5432

Assim é possível acessar o postgres do guapore2 direto do laptop:

    psql -U alertas -h 127.0.0.1 bd_monitoramento

Ou mesmo de dentro de uma máquina virtual do laptop:

    psql -U alertas -h 10.0.2.2 bd_monitoramento
