# Notas técnicas

## Shapefile de territórios mantidos pelo ISA

As áreas protegidas (Terras Indígenas e Unidades de Conservação) na Amazônia
Legal foram digitalizadas em ambiente SIG (sistema de informações geográficas)
baseadas nos memoriais descritivos dos documentos oficiais de criação ou
alteração do perímetro. As áreas são plotadas na escala 1:100.000. Para as
Unidades de Conservação estaduais, adotamos as bases do ICMBio/MMA como
referência dos dados espaciais, complementadas com as informações dos órgãos
estaduais do SISNAMA. Quando não há memorial descritivo da área protegida ou se
ele possui erros que impedem a plotagem do perímetro, dados vetoriais e
matriciais obtidos em órgãos oficiais federais e estaduais (i.e. FUNAI, MMA e
secretarias estaduais) são utilizados para revisão e/ou complementação, bem
como documentos não cartográficos como, por exemplo, planos de manejo das
Unidades de Conservação.

Os dados espaciais de municípios e estados utilizados como referência para a
plotagem das áreas protegidas têm origem tanto do SIVAM (Sistema de Vigilância
da Amazônia, 2004), na escala 250 mil, quanto da base cartográfica contínua
para o Brasil na escala 250 mil (IBGE, 2015). Além destas bases oficiais,
rasters globais (i.e. Google) e imagens de satélite da série Landsat também
auxiliam no processo de construção dos limites caso existam limitações nos
dados vetoriais.

Todas as feições foram transformadas para o sistema de coordenadas geográficas
e referenciadas ao Datum Sirgas 2000.

Os dados espaciais referentes às zonas de amortecimento das áreas protegidas
(buffer) foram delimitados em ambiente SIG a partir da função buffer para as
distâncias lineares: 0-3 km, 0-10km e 3-10km.

### Dicionário de dados

Descrição dos campos da tabela e shapefile de Territórios mantida pelo ISA e
[disponibilizada no Alertas+](https://alertas.socioambiental.org/downloads).

* `ID_ORIG`:
  * Descrição: identificador persistente na base de dados de origem:
    * No caso de Área Protegida ou buffer de uma Área Protegida, diz respeito
      ao `id_arp` do Sistema de Áreas Protegidas mantido pelo ISA. Buffers
      possuem o mesmo identificador da respectiva Área Protegida.
    * No caso de Unidades da Federação e Municipios, diz respeito ao código IBGE.
* `TYPE`:
  * Descrição: tipo de território.
  * Valores possíveis:
    * `UF`: Unidade da Federação.
    * `MUN`: Município.
    * `APAE`: Área de Proteção Ambiental estadual.
    * `APAE_B1`: Área de Proteção Ambiental estadual (buffer de 0-3km).
    * `APAE_B2`: Área de Proteção Ambiental estadual (buffer de 0-10km).
    * `APAE_B3`: Área de Proteção Ambiental estadual (buffer de 3-10km).
    * `APAF`: Área de Proteção Ambiental federal.
    * `APAF_B1`:  Área de Proteção Ambiental federal (buffer de 0-3km).
    * `APAF_B2`:  Área de Proteção Ambiental federal (buffer de 0-10km).
    * `APAF_B3`:  Área de Proteção Ambiental federal (buffer de 3-10km).
    * `TI`: Terra Indígena.
    * `TI_B1`: Terra Indígena (buffer de 0-3km).
    * `TI_B2`: Terra Indígena (buffer de 0-10km).
    * `TI_B3`: Terra Indígena (buffer de 3-10km).
    * `UCE`: Unidades de Conservação Estadual.
    * `UCE_B1`: Unidades de Conservação Estadual (buffer de 0-3km).
    * `UCE_B2`: Unidades de Conservação Estadual (buffer de 0-10km).
    * `UCE_B3`: Unidades de Conservação Estadual (buffer de 3-10km).
    * `UCF`: Unidades de Conservação Federal.
    * `UCF_B1`: Unidades de Conservação Federal (buffer de 0-3km).
    * `UCF_B2`: Unidades de Conservação Federal (buffer de 0-10km).
    * `UCF_B3`: Unidades de Conservação Federal (buffer de 3-10km).
* `NAME`: nome do território.
* `ID`: identificador não-persistente do registro apenas para fins
        administrativos de bases de dados; recomenda-se não usar este campo mas sim a
        conjunção `ID_ORIG` e `TYPE` para identificar unicamente um território.
