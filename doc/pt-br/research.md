# Pesquisa

Esta seção contém pesquisas realizadas durante o desenvolvimento e servem de
referência para decisões de design e implementação do sistema.

```eval_rst
.. toctree::
   :glob:
   :maxdepth: 2

   research/*
```
