# Rotinas Operacionais

Esta seção trata das rotinas de manutenção e operação do sistema.

```eval_rst
.. toctree::
   :glob:
   :maxdepth: 2

   routines/*
```
