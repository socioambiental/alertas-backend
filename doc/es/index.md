# Alertas+ - Documentación

[Português](/doc/pt-br/) | [English](/doc/en/)

[Alertas+](https://alertas.socioambiental.org) tiene como objetivo brindar
transparencia y datos que ayuden a periodistas, gerentes y ciudadanos a
ejercer el control a través de la participación social. La herramienta ofrece
transparencia a la sociedad sobre las dinámicas de ocupación territorial,
cambios en el uso del suelo y sus impactos sociales y ambientales.

Esta documentación detalla los aspectos técnicos del sistema, desde el diseño hasta el
funcionamiento.

![Instituto Socioambiental](_static/images/isa.png) Alertas+ es desarrollado y
mantenido por [Instituto Socioambiental](https://www.socioambiental.org).

## Índice


```eval_rst
.. toctree::
   :glob:
   :maxdepth: 2

   specs
   review
   notes
   code
```

<!--
## Indices y tabelas

```eval_rst
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```
-->

## Contacto

Póngase en contacto con el equipo de Alertas+ a través de
[alertas@socioambiental.org](mailto:alertas@socioambiental.org).
