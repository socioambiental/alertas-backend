# Notas técnicas

## Shapefile de territorios mantenidos por ISA

Las áreas protegidas (Tierras Indígenas y Unidades de Conservación) en la Amazonía
Legal brasileña se han digitalizado en un entorno SIG (Sistema de Información Geográfica)
con base en los memoriales descriptivos para la creación o cambio de perímetro
de los documentos oficiales. Las áreas se trazan a una escala 1 : 100.000. Para las
Unidades de Conservación estatales, adoptamos las bases de ICMBio / MMA como
referencia para los datos espaciales, complementadas con información de las agencias
estatales del SISNAMA. En los casos en que no hay memorial descriptivo del área protegida o
este contiene errores que impiden el trazado del perímetro, los datos vectoriales y
matriciales obtenidos de agencias federales y estatales (i.e. FUNAI, MMA y
secretarías estatales) se utilizan para revisión o complementación, así como
documentos no cartográficos como planes de manejo de Unidades de Conservación.

Los datos espaciales de municipios y estados utilizados como referencia para el
trazado de las áreas protegidas se originan tanto en SIVAM (Sistema de Vigilancia
da la Amazonía, 2004), en la escala de 250 mil, como en la base cartográfica continua
para Brasil, en la escala de 250 mil (IBGE, 2015). Además de estas bases de datos oficiales,
los rásteres globales (i.e. Google) y las imagenes satelitales de la serie Landsat también
ayudan en el proceso de construcción de los límites si existen limitaciones en los
datos vectoriales.

Todos los elementos geoespaciales se han convertido al sistema de coordenadas geográficas
y referenciados a Datum Sirgas 2000.

Los datos espaciales referentes a las zonas de amortiguamiento de las áreas protegidas
(buffer) han sido delimitados en entorno SIG utilizando la función buffer para las
distancias lineales: 0-3 km, 0-10 km y 3-10 km.

### Diccionario de datos

Descripción de los campos de la tabla y shapefile de territorios mantenida por ISA y
[disponible en Alertas+](https://alertas.socioambiental.org/downloads).

* `ID_ORIG`:
  * Descripción: identificador persistente en la base de datos de origen:
    * En el caso de Área Protegida o buffer de un Área Protegida, se trata
      de `id_arp` del Sistema de Áreas Protegidas mantenido por ISA. Buffers
      tienen el mismo identificador que el Área Protegida respectiva;
    * En el caso de Unidades de la Federación (UF) y municipios, se trata del código IBGE.
* `TYPE`:
  * Descripción: tipo de territorio.
  * Valores posibles:
    * `UF`: Unidad de la Federación;
    * `MUN`: Municipio;
    * `APAE`: Área de Protección Ambiental Estatal;
    * `APAE_B1`: Área de Protección Ambiental Estatal (buffer de 0-3 km);
    * `APAE_B2`: Área de Protección Ambiental Estatal (buffer de 0-10 km);
    * `APAE_B3`: Área de Protección Ambiental Estatal (buffer de 3-10 km);
    * `APAF`: Área de Protección Ambiental Federal;
    * `APAF_B1`:  Área de Protección Ambiental Federal (buffer de 0-3 km);
    * `APAF_B2`:  Área de Protección Ambiental Federal (buffer de 0-10 km);
    * `APAF_B3`:  Área de Protección Ambiental Federal (buffer de 3-10 km);
    * `TI`: Tierra Indígena;
    * `TI_B1`: Tierra Indígena (buffer de 0-3 km);
    * `TI_B2`: Tierra Indígena (buffer de 0-10 km);
    * `TI_B3`: Tierra Indígena (buffer de 3-10 km);
    * `UCE`: Unidad de Conservación Estatal;
    * `UCE_B1`: Unidad de Conservación Estatal (buffer de 0-3 km);
    * `UCE_B2`: Unidad de Conservación Estatal (buffer de 0-10 km);
    * `UCE_B3`: Unidad de Conservación Estatal (buffer de 3-10 km);
    * `UCF`: Unidad de Conservación Federal;
    * `UCF_B1`: Unidad de Conservación Federal (buffer de 0-3 km);
    * `UCF_B2`: Unidad de Conservación Federal (buffer de 0-10 km);
    * `UCF_B3`: Unidad de Conservación Federal (buffer de 3-10 km).
* `NAME`: nombre del territorio.
* `ID`: identificador no-persistente de registro, solo para fines
        administrativos de la base de datos; se recomienda no utilizar este campo sino la
        conjunción `ID_ORIG` y `TYPE` para identificar uno solo territorio.
