# Homologación

## Homologación de los datos de Alertas+

* Por Alana Almeida de Souza, Cicero Augusto, William Pereira Lima -
  05/08/2021.

La homologación de Alertas+ evaluó los siguientes aspectos relacionados
con los conjuntos de datos disponibles en el sistema:

* Completitud;
* Balances del total de alertas en el Panel por tipos de
  territorios, categorías y marcos de tiempo;
* Balances del área afectada por las alertas (cuando procede) en el Panel,
  por tipos de territorios, categorías y marcos de tiempo;
* Verificación de las salidas gráficas del Panel;
* Verificación de las salidas tabulares del Panel;
* Balances presentados en la página de inicio de Alertas+.

La completitud se evaluó a través de la simple inspección de los datos
presentados en el sistema de Alertas+ con el total de eventos
y el área (cuando procede) proporcionados en los shapefiles de datos, mientras se hacían
disponibles en sus respectivas fuentes para el mismo día de referencia.
Las variaciones entre los balances de menos del +/- 10 % se consideraron aceptables y
fueron asociadas con el modelado de datos y los cambios en la proyección cartográfica.

Los balances del total de alertas y áreas afectadas se verificaron mediante muestreo
por la complejidad de las consultas disponibles. De esta manera, en la verificación
se consideró las diferentes relaciones topológicas entre territorios, a saber:
territorios sin superposición (ej. estados, municipios, áreas protegidas
sin superposición entre sí) y territorios que se superponen (ej. Tierras Indígenas
contenidas en Unidades de Conservación).

La verificación de las salidas gráficas y tabulares incluyó la inspección visual de los
resultados de las consultas y la revisión del contenido de las exportaciones
en diferentes formatos.
