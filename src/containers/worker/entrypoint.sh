#!/usr/bin/env bash
#
# Entrypoint for the worker container service.
#

# Parameters
USER="${APP:-user}"
USERDIR="/home/$USER"
KEYTYPE="ed25519"
KEYFILE="$USERDIR/.ssh/id_${KEYTYPE}"

# Start SSH Server
service ssh start

# Ensure proper permissions at the SSH user config.
chmod 700 $USERDIR/.ssh

# Populate user's known_hosts
# Always run this as server keys might change upon container respawning
ssh-keyscan localhost  > $USERDIR/.ssh/known_hosts
ssh-keyscan worker    >> $USERDIR/.ssh/known_hosts

# Ensure there's a key pair
if [ ! -e "$KEYFILE" ]; then
  # We're using empty passphrases
  echo "Generating keypeir at $KEYFILE..."
  ssh-keygen -t ${KEYTYPE} -P '' -f "$KEYFILE" -C "$USER@backend"

  # Populate authorized_keys
  touch $USERDIR/.ssh/authorized_keys
  chmod 640 $USERDIR/.ssh/authorized_keys
  cat $KEYFILE.pub > $USERDIR/.ssh/authorized_keys
fi

# Set proper permissions at user's SSH folder
chown -R $USER. $USERDIR/.ssh

# Do nothing, preventing the container to stop
tail -f /dev/null
