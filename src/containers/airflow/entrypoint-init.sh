#!/usr/bin/env bash
#
# Airflow initialization for the test instance.
#

# Parameters
CONNECTION="alertas_backend_worker"
DESCRIPTION="Alertas Backend Worker"
URI="ssh://alertas-backend:@worker/"

# Initialize the database
airflow db init

# Add a SSH connection to the worker instance
if ! airflow connections get $CONNECTION &> /dev/null; then
  airflow connections delete $CONNECTION # remove any dangling connections
  airflow connections add    $CONNECTION --conn-description "$DESCRIPTION" \
                                         --conn-uri "$URI"                 \
                                         --conn-extra '{ "key_file": "/home/alertas-backend/.ssh/id_ed25519", "no_host_key_check": false }'

  # Initialize the database again to ensure that the DAGs will get the
  # connection
  airflow db init
fi
