FROM docker.io/ubuntu:focal AS airflow
MAINTAINER Silvio <silvio@socioambiental.org>

ENV APP="${COMPOSE_PROJECT_NAME:-alertas-backend}"
ENV APP_BASE="/var/sites/"
ENV SHELL="/bin/bash"

# UID and GID might be read-only values, so use non-conflicting ones
ARG CONTAINER_UID="${CONTAINER_UID:-1000}"
ARG CONTAINER_GID="${CONTAINER_GID:-1000}"

ENTRYPOINT [ "airflow", "scheduler" ]

ARG DEBIAN_FRONTEND=noninteractive
RUN mkdir -p /var/sites /home/${APP}/.ssh

COPY . ${APP_BASE}/${APP}
RUN ${APP_BASE}/${APP}/bin/provision-airflow
RUN rm -rf /var/lib/apt/lists/*
RUN rm -rf ${APP_BASE}/${APP}

# Switch to a regular user
RUN groupadd -r -g ${CONTAINER_GID} ${APP} && \
    useradd --no-log-init -r -u ${CONTAINER_UID} -g ${APP} ${APP} && \
    mkdir -p /home/${APP} && chown ${APP}. /home/${APP}
RUN mkdir -p /home/${APP}/airflow && chown ${APP}. /home/${APP}/airflow
USER ${APP}
WORKDIR /home/${APP}
