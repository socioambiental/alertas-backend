#!/usr/bin/env bash
#
# Airflow workers entrypoint.
#

# Remove airflow-worker.pid and other dangling lockfiles.
# It might prevent airflow celery worker to start.
# Then start the celery worker.
rm -f ~/airflow/airflow-worker.pid && airflow celery worker
