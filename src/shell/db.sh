#!/bin/bash
#
# Database helpers for shell scripts.
#

# Issue a commant to PostgreSQL
function psql_cmd {
  local database="$1"
  shift

  sudo su postgres -c "psql $database -c \"$*;\""
}

# Create a PostgreSQL user
function psql_createuser {
  local user="$1"

  sudo su postgres -c "createuser $user"
}

# Create a PostgreSQL database
function psql_createdb {
  local user="$1"
  local database="$2"

  sudo su postgres -c "createdb -O $user $database"
}

function psql_setpass {
  local user="$1"
  local pass="$2"

  sudo su postgres -c "psql -c \"ALTER ROLE ${user} WITH ENCRYPTED PASSWORD '${pass}';\""
}

# Add or update a ~/.pgpass entry for a given database/user pair and pasword.
# See https://wiki.postgresql.org/wiki/Pgpass
function update_pgpass {
  local database="$1"
  local user="$2"
  local pass="$3"

  line="localhost:5432:${database}:${user}"

  # Remove any existing ~/.pgpass entry from previous installations
  if [ -e "$HOME/.pgpass" ] && grep -q "$line" $HOME/.pgpass; then
    sed -i "/^$line/d" $HOME/.pgpass
  fi

  # Add ~/.pgpass entry
  touch $HOME/.pgpass
  chmod 600 $HOME/.pgpass
  echo "$line:${pass}" >> $HOME/.pgpass
}

function db_genpass {
  dd bs=1 count=30 if=/dev/urandom status=none | base64 | tr -d '\n=/'
}

#function psql_create_schemas {
#  psql_cmd $DBNAME CREATE SCHEMA IF NOT EXISTS $BASE_SCHEMA            AUTHORIZATION $DBUSER #&> /dev/null
#  psql_cmd $DBNAME CREATE SCHEMA IF NOT EXISTS $EVENT_CANONICAL_SCHEMA AUTHORIZATION $DBUSER #&> /dev/null
#  psql_cmd $DBNAME CREATE SCHEMA IF NOT EXISTS $EVENT_PROCESSED_SCHEMA AUTHORIZATION $DBUSER #&> /dev/null
#  psql_cmd $DBNAME CREATE SCHEMA IF NOT EXISTS $METADATA_SCHEMA        AUTHORIZATION $DBUSER #&> /dev/null
#
#  # Grant access
#  psql_cmd $DBNAME GRANT USAGE ON SCHEMA $BASE_SCHEMA                           TO $DBUSER_QUERY #&> /dev/null
#  psql_cmd $DBNAME GRANT USAGE ON SCHEMA $EVENT_PROCESSED_SCHEMA                TO $DBUSER_QUERY #&> /dev/null
#  psql_cmd $DBNAME GRANT USAGE ON SCHEMA $METADATA_SCHEMA                       TO $DBUSER_QUERY #&> /dev/null
#  psql_cmd $DBNAME GRANT SELECT ON ALL TABLES IN SCHEMA $EVENT_PROCESSED_SCHEMA TO $DBUSER_QUERY #&> /dev/null
#  psql_cmd $DBNAME GRANT SELECT ON ALL TABLES IN SCHEMA $METADATA_SCHEMA        TO $DBUSER_QUERY #&> /dev/null
#
#  # GRANTs for tables created in the future
#  # See https://stackoverflow.com/questions/10352695/grant-all-on-a-specific-schema-in-the-db-to-a-group-role-in-postgresql#10353730
#  #     https://www.postgresql.org/docs/current/sql-alterdefaultprivileges.html#SQL-ALTERDEFAULTPRIVILEGES-NOTES
#  psql_cmd $DBNAME ALTER DEFAULT PRIVILEGES FOR ROLE $DBUSER IN SCHEMA $EVENT_PROCESSED_SCHEMA GRANT SELECT ON TABLES TO $DBUSER_QUERY
#  psql_cmd $DBNAME ALTER DEFAULT PRIVILEGES FOR ROLE $DBUSER IN SCHEMA $METADATA_SCHEMA        GRANT SELECT ON TABLES TO $DBUSER_QUERY
#}

function psql_create_extensions {
  # Must run by the admin user
  psql_cmd $DBNAME CREATE EXTENSION postgis         #&> /dev/null
  psql_cmd $DBNAME CREATE EXTENSION postgis_raster  #&> /dev/null
  psql_cmd $DBNAME CREATE EXTENSION pgcrypto        #&> /dev/null
  psql_cmd $DBNAME CREATE EXTENSION dblink          #&> /dev/null

  # Adds dblink_fdw support on $DBNAME
  psql_cmd $DBNAME GRANT USAGE ON FOREIGN DATA WRAPPER dblink_fdw TO ${DBUSER_ADMIN} &> /dev/null
}

# Check BASEPATH
if [ -z "$BASEPATH" ]; then
  echo "Please set BASEPATH in the calling script"

  exit 1
fi

#PSQL_VERSION="11" # Debian Buster
#PSQL_VERSION="13" # Debian Bullseye
PSQL_VERSION="12"  # Ubuntu Focal

# Database and schemas name and random passwords
DBNAME="alertas"
DBUSER="alertas"
DBUSER_ADMIN="alertas_admin"
DBUSER_QUERY="alertas_query"
DBPASS="`db_genpass`"
DBPASS_ADMIN="`db_genpass`"
DBPASS_QUERY="`db_genpass`"
DBPORT="5432"
BASE_SCHEMA="base"
EVENT_CANONICAL_SCHEMA="event_canonical"
EVENT_PROCESSED_SCHEMA="event_processed"
METADATA_SCHEMA="meta"

# Pgpass for the runtime scripts
#
# This is not the regular user pgpass but the custom one for the application, which
# might or might not be running inside a container.
#export PGPASSFILE="$HOME/.pgpass"
export PGPASSFILE="$BASEPATH/config/pgpass"
