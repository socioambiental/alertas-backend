#!/bin/bash
#
# Sudo helper for shell scripts.
#

# Set sudo config
if [ "`whoami`" != 'root' ]; then
  SUDO="sudo"
fi
