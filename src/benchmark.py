#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from datetime              import datetime
from itertools             import product
from lib.benchmark.queries import param_spaces
from lib.cache.manager     import get, clear
from lib.logger            import logger
from query                 import Query

class Benchmark:
    def __init__(self, args = {}):
        # Parameters
        dirname         = os.path.dirname(os.path.abspath(__file__))
        export_filename = dirname + os.sep + '..' + os.sep + 'log' + os.sep + 'benchmark.log'
        self.output     = open(export_filename, 'a')

        # Initial parameters
        if 'param_space' not in args:
            args['param_space'] = '0'

        # Set parameters
        self.set_combinations(args['param_space'])

        # Instantiate
        self.query = Query()

    def set_combinations(self, param_space):
        """Set param combinations based in a given param space"""

        params            = param_spaces[param_space]
        self.keys         = params.keys()
        self.values       = [params[x] for x in self.keys]
        self.combinations = product(*self.values)

    def parametrize(self, combination):
        """Build parameters for a single query"""

        return dict(zip(self.keys, combination))

    def event(self, combination, caching = True):
        """Single event iteration with optional caching, useful for benchmarking/test suites"""

        params = self.parametrize(combination)

        if caching is True:
            result = get('events', self.query.events, **params)
        else:
            result = self.query.events(**params)

        return result

    def run(self, caching = True):
        """Standalone benchmark operation, optionally using cache"""

        i = 0

        # Print initial values
        logger('info', self.values)

        """Run queries"""
        for values in self.combinations:
            i      = i + 1
            params = self.parametrize(values)

            logger('info', params)

            t0     = datetime.now()

            if caching is True:
                result = get('events', self.query.events, **params)
            else:
                result = self.query.events(**params)

            logger('info', result)

            duration = (datetime.now() - t0).total_seconds()

            self.output.write(str((i, duration, values,)))
            self.output.write('\n')

            logger('info', 'Iteration: {iteration} - duration: {duration}'.format(
                iteration=i, duration=duration))

        self.output.close()

if __name__ == "__main__":
    # Clear cache
    clear()

    # Instantiate
    instance = Benchmark()

    # Run twice: first time is uncached, second is cached
    instance.run()
    instance.run()
