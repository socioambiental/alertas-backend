#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Apache Airflow DAG to test the worker instance.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from datetime                               import timedelta
from airflow                                import DAG
from airflow.utils.dates                    import days_ago
from airflow.contrib.hooks.ssh_hook         import SSHHook
from airflow.contrib.operators.ssh_operator import SSHOperator
from helpers.config                         import cmd_base, ssh_conn_id, default_args

dag = DAG(
        'alertas_backend_test_worker',
        default_args=default_args,
        description='Test the Alertas Backend Worker instance',
        schedule_interval='@hourly',
        start_date=days_ago(2),
        tags=['alertas_backend'],
        )

hook = SSHHook(ssh_conn_id=ssh_conn_id)

test_worker = SSHOperator(
        task_id="tests",
        #command="make -C {cmd_base} test_query".format(cmd_base=cmd_base),
        #command="/bin/true",
        command="uptime",
        ssh_hook=hook,
        do_xcom_push=True,
        # Disable timeout by setting it to None, see
        # https://airflow-apache.readthedocs.io/en/latest/_modules/airflow/contrib/operators/ssh_operator.html
        # https://airflow-apache.readthedocs.io/en/latest/_modules/airflow/contrib/hooks/ssh_hook.html
        # http://docs.paramiko.org/en/stable/api/client.html
        # http://docs.paramiko.org/en/stable/api/channel.html#paramiko.channel.Channel.settimeout
        timeout=None,
        dag=dag)

test_worker
