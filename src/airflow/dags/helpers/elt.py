#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Apache Airflow DAG ELT helper.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from airflow.decorators                     import dag
from airflow.contrib.hooks.ssh_hook         import SSHHook
from airflow.contrib.operators.ssh_operator import SSHOperator
from helpers.dag                            import build_args, dag_name
from helpers.config                         import cmd_base, ssh_conn_id

def build_elt_ssh_dag(filename, interval, prefix = 'importer_'):
    name     = dag_name(filename, prefix)
    dag_args = build_args(name, interval)
    importer = elt_ssh_factory(name, dag_args, ssh_conn_id)

    return importer

# Based on https://airflow.apache.org/docs/stable/tutorial_taskflow_api.html
def elt_ssh_factory(importer, dag_args, ssh_conn_id):
    #@dag(**dag_args)
    def elt_ssh_dag():
        """
        An ELT pipeline for Alertas Backend using SSH.
        """

        hook         = SSHHook(ssh_conn_id=ssh_conn_id)
        cmd_template = 'cd {cmd_base} && pipenv run src/importer.py --phase {phase} {importer}'
        tasks        = {}

        for phase in [ 'extract', 'load', 'transform' ]:
            tasks[phase] = SSHOperator(
                    task_id=phase,
                    ssh_hook=hook,
                    do_xcom_push=True,
                    timeout=None,
                    command=cmd_template.format(
                        cmd_base=cmd_base,
                        importer=importer,
                        phase=phase
                        ),
                    )

        tasks['extract'] >> tasks['load'] >> tasks['transform']

    # Apply the ecorator only after changing the function name
    elt_ssh_dag.__name__ = 'alteras_backend_importer_' + importer
    decorator            = dag(**dag_args)

    return decorator(elt_ssh_dag)
