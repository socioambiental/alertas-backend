#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Apache Airflow DAG configurations
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from datetime import timedelta

#
# Email config
#

# Use a non-default Airflow environment variable to get the destination email config
if 'AIRFLOW__SMTP__MAILTO' in os.environ:
    email = os.environ['AIRFLOW__SMTP__MAILTO'].split(',')
else:
    email = [ 'airflow@example.org' ]

#
# Basic config
#

cmd_base     = '/var/sites/alertas-backend'
ssh_conn_id  = 'alertas_backend_worker'
default_args = {
        'owner'            : 'airflow',
        'depends_on_past'  : False,
        'email'            : email,
        'email_on_failure' : True,
        'email_on_retry'   : False,
        'retries'          : 1,
        'retry_delay'      : timedelta(minutes=30),
        # Use a shorter delay for development/debugging
        #'retry_delay'     : timedelta(minutes=1),
        }

#
# Schedule presets
#

# Every day on midnight except on Sunday (which is maintenance day)
daily_midnight = '0 0 * * 1-6'

# Every day early in the morning except on Sunday
daily_early_morning = '0 4 * * 1-6'

# Every day in the morning
daily_morning = '0 8 * * *'

# Every weekday on midnight
weekdays_midnight = '0 0 * * 1-5'

# Every weekday early in the morning
weekdays_early_morning = '0 4 * * 1-5'

# Every weekday in the morning
weekdays_morning = '0 8 * * 1-5'

# Weekly tasks on Saturday
weekly_saturday = '0 0 * * 6'

# At the begining of every week
week_begining = '0 1 * * 1'

# During business hours
business_hours = '10 8-18/2 * * *'
