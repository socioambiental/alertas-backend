#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Apache Airflow make helper.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from airflow.decorators                     import dag
from airflow.contrib.hooks.ssh_hook         import SSHHook
from airflow.contrib.operators.ssh_operator import SSHOperator
from helpers.config                         import cmd_base, ssh_conn_id
from helpers.dag                            import build_args, dag_name

def build_make_dag(filename, interval, prefix = False):
    name     = dag_name(filename)
    target   = dag_name(filename, prefix)
    dag_args = build_args(name, interval)
    importer = make_factory(name, target, dag_args, ssh_conn_id)

    return importer

def make_factory(name, target, dag_args, ssh_conn_id):
    """
    Run a single makefile target in the remote backend.
    """

    #@dag(**dag_args)
    def make_dag():
        hook = SSHHook(ssh_conn_id=ssh_conn_id)

        cmd_template = "make -C {cmd_base} {target}"
        task_connect = SSHOperator(
                task_id=target,
                command=cmd_template.format(cmd_base=cmd_base, target=target),
                ssh_hook=hook,
                do_xcom_push=True,
                timeout=None,
                )

    # Apply the ecorator only after changing the function name
    make_dag.__name__ = 'alertas_backend_' + name
    decorator         = dag(**dag_args)

    return decorator(make_dag)
