#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Apache Airflow DAG helpers.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
#from datetime              import timedelta
from airflow.utils.timezone import datetime
from airflow.utils.dates    import days_ago
from helpers.config         import default_args

def build_args(name, interval, args=default_args):
    dag_args = {
            'default_args'      : args,
            'description'       : 'Run {name} action'.format(name=name),
            'schedule_interval' : interval,

            # This won't match weekly schedules
            #'start_date'       : days_ago(1),

            # Using timedelta for start_date gives the following error:
            # on Airflow 2.0.1:
            #
            # Broken DAG: [/home/alertas-backend/airflow/dags/test_success.py]
            # Traceback (most recent call last):
            #   File
            #   "/usr/local/lib/python3.8/dist-packages/airflow/models/dag.py",
            #   line 2306, in factory
            #     with DAG(*dag_bound_args.args, **dag_bound_args.kwargs) as
            #     dag_obj:
            #   File
            #   "/usr/local/lib/python3.8/dist-packages/airflow/models/dag.py",
            #   line 294, in __init__
            #     if start_date and start_date.tzinfo:
            # AttributeError: 'datetime.timedelta' object has no attribute 'tzinfo'
            #
            #'start_date'       : timedelta(weeks=2),

            'start_date'        : datetime(2021, 5, 9),
            'tags'              : [ 'alertas_backend' ],
            'catchup'           : False,
            }

    return dag_args

def dag_name(filename, strip_prefix = False):
    basename = os.path.basename(filename)
    prefix   = basename.split('.')
    name     = prefix[0]

    if strip_prefix is not False:
        name = name.replace(strip_prefix, '')

    return name
