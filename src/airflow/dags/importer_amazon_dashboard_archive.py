#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Apache Airflow DAG to import a dataset.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import datetime
from airflow.decorators                     import dag
from airflow.contrib.hooks.ssh_hook         import SSHHook
from airflow.contrib.operators.ssh_operator import SSHOperator
from helpers.dag                            import build_args, dag_name
from helpers.config                         import cmd_base, ssh_conn_id
from helpers.config                         import weekly_saturday

def build_amazon_dashboard_archive_dag(filename, interval, prefix = 'importer_'):
    name     = dag_name(filename, prefix)
    dag_args = build_args(name, interval)
    importer = amazon_dashboard_archive_factory(name, dag_args, ssh_conn_id)

    return importer

# Based on helpers/elt.py
def amazon_dashboard_archive_factory(importer, dag_args, ssh_conn_id):
    def amazon_dashboard_archive_dag():
        """
        An Amazon Dashboard to download historic data from previous years.

        For each year from the previous years since the initial Amazon Dashboard,
        extract and load every dataset from that year, ensure the canonical table
        has all the historical data.

        """

        hook         = SSHHook(ssh_conn_id=ssh_conn_id)
        cmd_template = 'cd {cmd_base} && pipenv run src/importer.py --phase {phase} amazon_dashboard {date}'
        tasks        = {}

        # Setup the date range
        today         = datetime.datetime.today()
        initial_year  = 2020
        final_year    = int(today.strftime('%Y'))
        years         = range(initial_year, final_year)

        for year in years:
            # Apply heuristics: as of 2021 and given that Amazon Dashboard is a
            # new project, we don't know yet the dating scheme for past
            # datasets. By the turn of 2021 the past year's dataset final date
            # was 20201231 but around May 2021 it changed to 20201103.
            #
            # Solve this using a simple lookup table.
            if year == 2020:
                date = str(year) + '1103'
            else:
                # Assume that defaults to the last day of that year
                date = str(year) + '1231'

            # Setup the EL tasks for that year
            #
            # The transform phase is not needed since it should be handled by
            # the regular Amazon Dashboard importer
            for phase in [ 'extract', 'load' ]:
                phase_name        = phase + '_' + str(year)
                tasks[phase_name] = SSHOperator(
                        task_id=phase_name,
                        ssh_hook=hook,
                        do_xcom_push=True,
                        timeout=None,
                        command=cmd_template.format(
                            cmd_base=cmd_base,
                            importer=importer,
                            phase=phase,
                            date=date,
                            ),
                        )

            # Chain the current year with the tasks from previous years
            if year != initial_year:
                tasks['load_' + str(year - 1)] >> tasks['extract_' + str(year)]

            # Chain current year's tasks
            tasks['extract_' + str(year)] >> tasks['load_' + str(year)]

    # Apply the ecorator only after changing the function name
    amazon_dashboard_archive_dag.__name__ = 'alteras_backend_importer_' + importer
    decorator                             = dag(**dag_args)

    return decorator(amazon_dashboard_archive_dag)

importer = build_amazon_dashboard_archive_dag(__file__, weekly_saturday)
run      = importer()
