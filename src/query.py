#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Query operations for the pressures/menaces dataset.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from datetime                         import datetime
from lib.config                       import config
from lib.database.decorators.psycopg2 import connect, teardown
from lib.database.timeout             import retry_wait
from lib.logger                       import logger
from lib.params                       import srid
from psycopg2                         import sql, Error, OperationalError
from psycopg2.extensions              import QueryCanceledError
from psycopg2.extras                  import RealDictCursor
from decimal                          import Decimal

import lib.query.helpers as helpers
import lib.cache.manager

class Query:
    """Database query class"""

    # Database connection using RealDictCursor to ease JSON conversion
    # See https://www.psycopg.org/docs/extras.html
    #     https://www.peterbe.com/plog/from-postgres-to-json-strings
    @connect(
            config_section='main_db_query', cursor_factory=RealDictCursor,
            readonly=True, autocommit=True, lock_timeout=True)
    def __init__(self, config = {}, args = {}):
        return

    def run_query(self, query, verbose=False):
        """
        Run a query, calculating elapsed time, handling errors, timeouts
        and other operational issues.

        """

        query      = self.cur.mogrify(query)
        #statement = str(query)

        helpers.log_query(query, verbose)

        # Execute query
        init_time = datetime.now()

        # Submit query
        try:
            self.cur.execute(query)
        except (OperationalError, QueryCanceledError) as e:
            retry_wait()

            return self.run_query(query, verbose)
        except Error as e:
            logger('exception', e)
            #self.close()
            #return False
            raise Exception('Error when running query: {}'.format(e))

        # Get values
        output  = self.cur.fetchall()
        elapsed = helpers.log_elapsed(init_time, verbose)

        return [ output, elapsed ]

    def is_polygon(self, event_type):
        """
        Use a query to check if the geometry of requested events is of type Point
        """

        query  = "select st_geometrytype(geom) from event_processed.event_territory_{event_type} limit 1"
        query  = sql.SQL(query.format(event_type=event_type))
        result = self.run_query(query)

        if result[0][0]['st_geometrytype'] == 'ST_Point':
            is_pol=False
        else:
            is_pol=True

        return is_pol

    def validate(self, params):
        """
        Validate common params.
        """

        if 'temporal_unit' in params:
            if str(params['temporal_unit']).lower() not in [ 'year','quarter', 'month', 'week', 'day' ]:
                return False

        if 'group_by' in params:
            for item in params['group_by']:
                if item not in [ 'territory', 'territory_type', 'uf', 'mun', 'basin' ]:
                    return False

        # Date regexp
        import re
        from datetime import date
        pattern = r"^[0-9]{4}-[0-9]{2}-[0-9]{2}$"

        if 'date_initial' in params:
            match = re.match(pattern, params['date_initial'])

            if not match:
                return False

            try:
                date = date.fromisoformat(params['date_initial'])
            except ValueError as e:
                return False

        if 'date_final' in params:
            match = re.match(pattern, params['date_final'])

            if not match:
                return False

            try:
                date = date.fromisoformat(params['date_final'])
            except ValueError as e:
                return False

        return True

    def events(self,
               territory_types=[ 'MUN' ],
               event_type='deter',
               event_class=1,
               output_geometry_aggregated=False,
               output_geometry_disaggregated=False,
               centroid=False,
               by_timeperiod=True,
               temporal_unit='month',
               temporal_aggregation_on_interval_end=False,
               date_initial='2019-01-01',
               date_final='2021-01-01',
               #id_territory=0,
               #id_uf=0,
               #id_mun=0,
               #name_territory="",
               #name_uf="",
               #name_mun="",
               #name_basin="",
               id_territory=[],
               id_uf=[],
               id_mun=[],
               name_territory=[],
               name_uf=[],
               name_mun=[],
               name_basin=[],
               group_by=[ 'mun' ],
               compute_emissions=False,
               verbose=False,
               smart_area=False,
               bounds=[],
               geojson=False,
               json=False):
        """
        Return pressures/menaces events from a given space/time definition

        :type  territory_types: list, optional
        :param territory_types: Specify territory type.
                                Available types: 'TI', 'UCF', 'UCE', 'APAF', 'APAE', 'MUN', 'UF'.
                                Buffers can be invoked using {type}_B[1-3] for territory types
                                'TI', 'UCF', 'UCE', 'APAF', 'APAE'.
                                An empty list [] means no selection by territory type.
                                Defaults to [ 'MUN' ],

        :type  event_type: str, optional
        :param event_type: Type of alert event.
                           Defaults to 'deter'.

        :type  event_class: int, optional
        :param event_class: For 'deter' events, classes are
                            1=Deforestation class,
                            2=Burning class,
                            3=Logging class.
                            Defaults to 1.

        :type  output_geometry_aggregated: boolean, optional
        :param output_geometry_aggregated: Whether the output should have geometry attributes.
                                           Defaults to False.

        :type  output_geometry_disaggregated: boolean, optional
        :param output_geometry_disaggregated: Whether the output should have geometry
                                              attributes, but without territory intersection.
                                              Defaults to False.

        :type  centroid: boolean, optional
        :param centroid: Whether to include centroid coordinates in the output.
                         Defaults to False.

        :type  by_timeperiod: boolean, optional
        :param by_timeperiod: Whether the output should be aggregated on time units.
                              Defaults to True.

        :type  temporal_unit: str, optional
        :param temporal_unit: Unit of the temporal aggregation. Can be
                              'Year','Quarter', 'Month', 'Week' or 'Day'.
                              Defaults to 'Month'.

        :type  temporal_aggregation_on_interval_end: boolean, optional
        :param temporal_aggregation_on_interval_end: Whether the temporal desaggregation should be set to the end of the interval. If set to False, aggregation will default to the begining of the interval using PostgreSQL's DATE_TRUNC(). If set to True, aggregated dates will be set to the end of the interval. Does not work for "prodes" events. Defaults to False.

        :type  date_initial: str, optional
        :param date_initial: Initial analysis date (inclusive).
                             Defaults to '2019-01-01'.

        :type  date_final: str, optional
        :param date_final: Final analysis date (exclusive).
                           Defaults to '2021-01-01'.

        :type  id_territory: list, optional
        :param id_territory: Specify IDs of territory entitiess if you want only
                             results relative to those units.
                             Defaults to [] (no clippping entity filtering).

        :type  id_uf: list, optional
        :param id_uf: Specify IDs of UFs if you want only results relative to
                      those units.
                      Defaults to [] (no clippping entity filtering).

        :type  id_mun: list, optional
        :param id_mun: Specify IDs of municipalities if you want only results
                       relative to those units.
                       Defaults to [] (no clippping entity filtering).

        :type  name_territory: list, optional
        :param name_territory: Specify names of territory entities if you want
                               results relative to those units.
                               Defaults to [] (no clippping entity filtering).

        :type  name_uf: list, optional
        :param name_uf: Specify names of UFs if you want results relative to
                        those administrative units.
                        Defaults to [] (no clippping entity filtering).

        :type  name_mun: list, optional
        :param name_mun: Specify names of Municipalities if you want results
                         relative to those administrative units.
                         Defaults to "" (no clippping entity filtering).

        :type  name_basin: str, optional
        :param name_basin: Specify the name of a hydrographic basin if you want only results
                         relative to this basin.
                         Defaults to "" (no clippping entity filtering).

        :type  group_by: list, optional
        :param group_by: Specify which groupings (GROUP BY) should be used in the query.
                         Available groupings: 'territory', 'territory_type', 'uf', 'mun', 'basin'.
                         Defaults to [ 'territory', 'territory_type '].

        :type  compute_emissions: boolean, optional
        :param compute_emissions: Whether the output should  include the associated Carbon emissions.
                                  Defaults to False.

        :type  verbose: boolean, optional
        :param verbose: Output console messages. Defaults to False.

        :type  smart_area: boolean, optional
        :param smart_area: Uses advanced method to compute total area.
                               Have no effect for territory types without superpositions like 'UF' and 'MUN'.
                               Defaults to False.

        :type  bounds: list, optional
        :param bounds: Restrict selection to the given bounding box.
                       Format is [ southwest_lng, southwest_lat, northeast_lng, northeast_lat ]
                       Defaults to an empty list (no bounding box restriction).

        :type  geojson: boolean, optional
        :param geojson: Whether to use GeoJSON in the geometry outputs.
                        Defaults to False.

        :type  json: boolean, optional
        :param json: Whether to envelope the output with JSON.
                     Defaults to False.

        :rtype: list
        :return: Events satisfying the given conditions or
                 False on error

        """
        # Save argument list: this should comes first of anything else
        args = locals()
        del args['self']

        #
        # Checks
        #

        # Additional param validation
        if not self.validate(args):
            return False

        # Check some unwanted parameter combinations
        if output_geometry_aggregated and output_geometry_disaggregated:
            # Asking for both does not make sense
            return False

        # Check if event type is poligonal
        try:
            # Uncached check, lower performance
            #is_pol = self.is_polygon(event_type)

            # Cached check, higher performance
            is_pol = lib.cache.manager.get('events', self.is_polygon, event_type)
        except Exception as e:
            logger('exception', e)
            self.close()
            return False

        # Some territory types cannot have superposition by definition.
        # For those cases, smart_area should be always disabled for performance reasons.
        if 'UF' in territory_types or 'MUN' in territory_types:
            smart_area = False

        #
        # Query initialization
        #

        query_select = []; query_where = []; query_group = []; query_order = []; query_from = ""

        #
        # Query composition: SELECT
        #

        query_select.append("COUNT(distinct et.e_id) AS events")

        if len(territory_types) > 0:
            if is_pol:
                if smart_area:
                    query_select.append("""
                            ROUND(
                                smart_area(
                                    ARRAY_AGG(et.area_ha),
                                    ARRAY_AGG(et.geom),
                                    ARRAY_AGG(et.ovlp_int),
                                    ARRAY_AGG(et.ovlp_ext),
                                    ARRAY_AGG(et.ovlp_extnobuff),
                                    ARRAY_AGG(t_type)
                                ),
                                2
                            ) AS area_ha""")
                else:
                    query_select.append("""
                            ROUND(
                                ST_AREA(
                                    ST_UNION(et.geom)::geography)::numeric/10000,
                                    2
                                )::numeric AS area_ha""")
        else:
            query_select.append("SUM(et.area_ha) AS area_ha")

        if compute_emissions:
            if is_pol:
                query_select.append("""
                        ROUND(
                            smart_emissions(
                                ARRAY_AGG(et.area_ha),
                                ARRAY_AGG(et.c_emission),
                                ARRAY_AGG(et.geom),
                                ARRAY_AGG(et.ovlp_int),
                                ARRAY_AGG(et.ovlp_ext),
                                ARRAY_AGG(et.ovlp_extnobuff),
                                ARRAY_AGG(t_type)
                            ),
                            2
                        ) AS carbon_emission_mgceq""")
            else:
                query_select.append("""
                    ROUND(
                        smart_emissions_pt(
                            ARRAY_AGG(et.e_id),
                            ARRAY_AGG(et.c_emission)
                        ),
                        2
                    ) AS carbon_emission_mgceq"""
                )

        if 'territory_type' in group_by:
            query_select.append("et.t_type AS territory_type")

        if 'territory' in group_by:
            query_select.append("et.t_id_orig AS t_id_orig")
            query_select.append("et.t_name AS name_territory")

        if 'uf' in group_by:
            query_select.append("et.name_uf")

        if 'mun' in group_by:
            query_select.append("et.name_mun")

        if 'basin' in group_by:
            query_select.append("et.basin")

        if by_timeperiod:
            # PRODES is already stored in the database with all events set to the same
            # year date in the begining of the "PRODES year", so a simple aggregation
            # method is used.
            if event_type.lower() == 'prodes':
                query_select.append("""
                    TO_CHAR(
                        et.e_date,
                        'yyyy-mm-dd'
                    ) AS {temporal_unit_identifier}"""
                )

            elif temporal_aggregation_on_interval_end:
                # This method sets the date to end of the interval, which
                # represents the number of events in a period ending in the given
                # date.
                #
                # Thanks https://dba.stackexchange.com/a/179877
                query_select.append("""
                    TO_CHAR(
                        CAST(
                            DATE_TRUNC(
                                {temporal_unit},
                                et.e_date
                            )
                            + interval {temporal_unit_offset} - interval '1 day' AS date
                        ),
                        'yyyy-mm-dd'
                    ) AS {temporal_unit_identifier}"""
                )

            else:
                # This method sets the date to the begining of the interval, which
                # represents the number of events in a period begining in the given
                # date.
                query_select.append("""
                    TO_CHAR(
                        DATE_TRUNC(
                            {temporal_unit},
                            et.e_date
                        ),
                        'yyyy-mm-dd'
                    ) AS {temporal_unit_identifier}"""
                )

        if output_geometry_aggregated:
            if geojson:
                query_select.append("ST_AsGeoJSON(ST_union(et.geom)) AS geoJson")
            else:
                query_select.append("ST_union(et.geom) AS geom")
        elif output_geometry_disaggregated:
            if geojson:
                query_select.append("ST_AsGeoJSON(ST_collect(et.geom)) AS geoJson")
            else:
                query_select.append("ST_collect(et.geom) AS geom")

        if centroid:
            if geojson:
                query_select.append("ST_AsGeoJSON(ST_Centroid(et.geom)) AS centroid")
            else:
                query_select.append("ST_AsText(ST_Centroid(et.geom)) AS centroid")

        #
        # Query composition: FROM
        #

        # Query directly from the partition
        #query_from = " FROM event_processed.event_territory_{event_type_unq} et"

        # Query directly from the main table
        query_from = " FROM event_processed.event_territory et"

        #
        # Query composition: WHERE
        #

        query_where.append("et.e_type={event_type}")

        if event_class > 0:
            query_where.append("et.e_class_1={event_class}")

        if len(territory_types) > 0:
            query_where.append("(et.t_type={where_territory_types})")
        else:
            query_where.append("et.t_type='MUN'")

        # Use a closed date interval
        query_where.append("et.e_date >= {date_initial}")
        query_where.append("et.e_date <= {date_final}")

        #if id_territory > 0:
        #    query_where.append("et.t_id_orig={id_territory}")
        if len(id_territory) > 0:
            query_where.append("(et.t_id_orig={where_id_territory})")

        #if id_uf > 0:
        #    query_where.append("et.id_uf={id_uf}")
        if len(id_uf) > 0:
            query_where.append("(et.id_uf={where_id_uf})")

        #if id_mun > 0:
        #    query_where.append("et.id_mun={id_mun}")
        if len(id_mun) > 0:
            query_where.append("(et.id_mun={where_id_mun})")

        #if name_territory != "":
        #    query_where.append("(et.t_name={name_territory}")
        if len(name_territory) > 0:
            query_where.append("(et.t_name={where_name_territory}")

        #if name_uf != "":
        #    query_where.append("et.name_uf={name_uf}")
        if len(name_uf) > 0:
            query_where.append("(et.name_uf={where_name_uf})")

        #if name_mun != "":
        #    query_where.append("et.name_mun={name_mun}")
        if len(name_mun) > 0:
            query_where.append("(et.name_mun={where_name_mun})")

        #if name_basin != "":
        #    query_where.append("et.basin={name_basin}")
        if len(name_basin) > 0:
            query_where.append("(et.basin={where_name_basin})")

        # See https://gis.stackexchange.com/questions/25797/select-bounding-box-using-postgis
        #     http://postgis.refractions.net/docs/geometry_overlaps.html
        #     http://postgis.refractions.net/docs/ST_MakeEnvelope.html
        if len(bounds) == 4:
            xmin, ymin, xmax, ymax = bounds

            query_where.append("et.geom && ST_MakeEnvelope({xmin}, {ymin}, {xmax}, {ymax}, {srid})")
        else:
            xmin = ymin = xmax = ymax = 0

        #
        # Query composition: GROUP
        #

        if by_timeperiod:
            query_group.append("{temporal_unit_identifier}")

        if len(territory_types) > 0:
            if 'territory' in group_by:
                query_group.append("et.t_id_orig")
                query_group.append("et.t_name")

            if 'territory_type' in group_by and len(territory_types) > 0:
                query_group.append("et.t_type")

        if 'uf' in group_by:
            query_group.append("et.name_uf")

        if 'mun' in group_by:
            query_group.append("et.name_mun")

        if 'basin' in group_by:
            query_group.append("et.basin")

        if centroid:
            query_group.append("et.geom")

        #
        # Query composition: ORDER
        #

        if by_timeperiod:
            query_order.append("{temporal_unit_identifier}")

        if len(territory_types) > 0:
            if 'territory' in group_by and is_pol:
             query_order.append("area_ha DESC")

            if 'territory_type' in group_by:
                query_order.append("et.t_type")

        if 'uf' in group_by:
            query_order.append("et.name_uf")

        if 'mun' in group_by:
            query_order.append("et.name_mun")

        if 'basin' in group_by:
            query_order.append("et.basin")

        #
        # Query processing
        #

        # Build query
        query = helpers.build_query(query_select, query_from, query_where, query_group, query_order)

        # Envelope if needed
        if json:
            # Thanks https://stackoverflow.com/questions/11198625/json-output-in-postgresql
            query = "SELECT array_to_json(array_agg(row_to_json(t))) FROM ({query}) t".format(query=query)

        # Now proceed to a safe SQL string composition
        #
        # Since this function might be exposed to general users via API, it's vital
        # that proper sanitization takes place.
        #
        # See https://www.psycopg.org/docs/usage.html#query-parameters
        #     https://www.psycopg.org/docs/sql.html#psycopg2.sql.SQL.join
        #     https://stackoverflow.com/questions/45128902/psycopg2-and-sql-injection-security#45128973
        query = sql.SQL(query).format(
                                      event_class=sql.Literal(event_class),
                                      event_type_unq=sql.SQL(event_type),
                                      event_type=sql.Literal(event_type),
                                      temporal_unit=sql.Literal(temporal_unit),
                                      temporal_unit_offset=sql.Literal('1 ' + str(temporal_unit)),
                                      #temporal_unit_identifier=sql.Identifier(temporal_unit),
                                      temporal_unit_identifier=sql.Identifier('date'),
                                      date_initial=sql.Literal(date_initial),
                                      date_final=sql.Literal(date_final),
                                      #id_territory=sql.Literal(id_territory),
                                      #id_uf=sql.Literal(id_uf),
                                      #id_mun=sql.Literal(id_mun),
                                      #name_territory=sql.Literal(name_territory),
                                      #name_uf=sql.Literal(name_uf),
                                      #name_mun=sql.Literal(name_mun),
                                      #name_basin=sql.Literal(name_basin),
                                      where_id_territory=sql.SQL(' OR et.t_id_orig=').join(map(sql.Literal, id_territory)),
                                      where_id_uf=sql.SQL(' OR et.id_uf=').join(map(sql.Literal, id_uf)),
                                      where_id_mun=sql.SQL(' OR et.id_mun=').join(map(sql.Literal, id_mun)),
                                      where_name_territory=sql.SQL(' OR et.t_name=').join(map(sql.Literal, name_territory)),
                                      where_name_uf=sql.SQL(' OR et.name_uf=').join(map(sql.Literal, name_uf)),
                                      where_name_mun=sql.SQL(' OR et.name_mun=').join(map(sql.Literal, name_mun)),
                                      where_name_basin=sql.SQL(' OR et.basin=').join(map(sql.Literal, name_basin)),
                                      xmin=sql.Literal(Decimal(xmin)),
                                      ymin=sql.Literal(Decimal(ymin)),
                                      xmax=sql.Literal(Decimal(xmax)),
                                      ymax=sql.Literal(Decimal(ymax)),
                                      srid=sql.Literal(Decimal(srid)),
                                      where_territory_types=sql.SQL(' OR et.t_type=').join(map(sql.Literal, territory_types)),
                                      )

        try:
            output, elapsed = self.run_query(query, verbose)
        except Exception as e:
            logger('exception', e)
            self.close()
            return False

        #
        # Output handling
        #

        # Build JSON envelope
        if json:
            output = {
                       'data': output[0]['array_to_json'],
                       'meta': {
                                 'timestamp': str(datetime.now()),
                                 'elapsed'  : elapsed,
                                 'args'     : args,
                                 #'query'   : statement,
                               }
                     }

        return output

    def tiles(self,
               territory_types=[ 'MUN' ],
               event_type='deter',
               event_class=1,
               date_initial='2019-01-01',
               date_final='2021-01-01',
               #id_territory=0,
               #id_uf=0,
               #id_mun=0,
               #name_territory="",
               #name_uf="",
               #name_mun="",
               #name_basin="",
               id_territory=[],
               id_uf=[],
               id_mun=[],
               name_territory=[],
               name_uf=[],
               name_mun=[],
               name_basin=[],
               verbose=False,
               tile_x=0,
               tile_y=0,
               tile_z=0):
        """
        Return pressures/menaces tiles from a given space/time definition

        :type  territory_types: list, optional
        :param territory_types: Specify territory type.
                               Available types: 'TI', 'UCF', 'UCE', 'APAF', 'APAE', 'MUN', 'UF'.
                               Buffers can be invoked using {type}_B[1-3]
                               An empty list [] means no selection by territory type.
                               Defaults to [ 'MUN' ],

        :type  event_type: str, optional
        :param event_type: Type of alert event.
                           Defaults to 'deter'.

        :type  event_class: int, optional
        :param event_class: For 'deter' events, classes are
                            1=Deforestation class,
                            2=Burning class,
                            3=Logging class.
                            Defaults to 1.

        :type  date_initial: str, optional
        :param date_initial: Initial analysis date (inclusive).
                             Defaults to '2020-01-01'.

        :type  date_final: str, optional
        :param date_final: Final analysis date (exclusive).

        :type  id_territory: list, optional
        :param id_territory: Specify IDs of territory entitiess if you want only
                             results relative to those units.
                             Defaults to [] (no clippping entity filtering).

        :type  id_uf: list, optional
        :param id_uf: Specify IDs of UFs if you want only results relative to
                      those units.
                      Defaults to [] (no clippping entity filtering).

        :type  id_mun: list, optional
        :param id_mun: Specify IDs of municipalities if you want only results
                       relative to those units.
                       Defaults to [] (no clippping entity filtering).
                           Defaults to '2021-01-01'.

        :type  name_territory: list, optional
        :param name_territory: Specify names of territory entities if you want
                               results relative to those units.
                               Defaults to [] (no clippping entity filtering).

        :type  name_uf: list, optional
        :param name_uf: Specify names of UFs if you want results relative to
                        those administrative units.
                        Defaults to [] (no clippping entity filtering).

        :type  name_mun: list, optional
        :param name_mun: Specify names of Municipalities if you want results
                         relative to those administrative units.
                         Defaults to "" (no clippping entity filtering).

        :type  name_basin: str, optional
        :param name_basin: Specify the name of a hydrographic basin if you want only results
                         relative to this basin.
                         Defaults to "" (no clippping entity filtering).

        :type  verbose: boolean, optional
        :param verbose: Output console messages. Defaults to False.

        :type  tile_x: int, optional
        :param tile_x: Indicate the {x} coordinate for vector tiles output.
                       Defaults to zero, no coordinate specified.

        :type  tile_y: int, optional
        :param tile_y: Indicate the {y} coordinate for vector tiles output.
                       Defaults to zero, no coordinate specified.

        :type  tile_z: int, optional
        :param tile_z: Indicate the map zoom level {z} for vector tiles output.
                       Defaults to zero, no coordinate specified.

        :rtype: bytes
        :return: Mapbox Vector Tiles
                 False on error

        """
        # Save argument list: this should comes first of anything else
        args = locals()
        del args['self']

        #
        # Checks
        #

        # Additional param validation
        if not self.validate(args):
            return False

        if not helpers.tileIsValid(tile_x, tile_y, tile_z):
            return False

        #
        # Query initialization
        #

        query_select = []; query_where = []; query_group = []; query_order = []; query_from = ""

        #
        # Query composition: SELECT
        #

        query_select.append("ST_AsMVTGeom(ST_Transform(et.geom, 3857), bounds.b2d) AS geom")

        #
        # Query composition: FROM
        #

        query_from  = " FROM event_processed.event_territory et"

        # MVT
        query_from += ", bounds"

        #
        # Query composition: WHERE
        #

        query_where.append("et.e_type={event_type}")

        if event_class > 0:
            query_where.append("et.e_class_1={event_class}")

        if len(territory_types) > 0:
            query_where.append("(et.t_type={where_territory_types})")
        else:
            query_where.append("et.t_type='MUN'")

        # Use a closed date interval
        query_where.append("et.e_date >= {date_initial}")
        query_where.append("et.e_date <= {date_final}")

        #if id_territory > 0:
        #    query_where.append("et.t_id_orig={id_territory}")
        if len(id_territory) > 0:
            query_where.append("(et.t_id_orig={where_id_territory})")

        #if id_uf > 0:
        #    query_where.append("et.id_uf={id_uf}")
        if len(id_uf) > 0:
            query_where.append("(et.id_uf={where_id_uf})")

        #if id_mun > 0:
        #    query_where.append("et.id_mun={id_mun}")
        if len(id_mun) > 0:
            query_where.append("(et.id_mun={where_id_mun})")

        #if name_territory != "":
        #    query_where.append("(et.t_name={name_territory}")
        if len(name_territory) > 0:
            query_where.append("(et.t_name={where_name_territory}")

        #if name_uf != "":
        #    query_where.append("et.name_uf={name_uf}")
        if len(name_uf) > 0:
            query_where.append("(et.name_uf={where_name_uf})")

        #if name_mun != "":
        #    query_where.append("et.name_mun={name_mun}")
        if len(name_mun) > 0:
            query_where.append("(et.name_mun={where_name_mun})")

        #if name_basin != "":
        #    query_where.append("et.basin={name_basin}")
        if len(name_basin) > 0:
            query_where.append("(et.basin={where_name_basin})")

        # MVT
        query_where.append("ST_Intersects(et.geom, ST_Transform(bounds.geom, {srid}))")

        #
        # Query composition: GROUP
        #

        # MVT
        #query_group.append('et.geom')
        #query_group.append('bounds.b2d')

        #
        # Query composition: ORDER
        #

        #
        # Query processing
        #

        # Build envelope params
        xmin, ymin, xmax, ymax, seg_size = helpers.tileToEnvelope(tile_x, tile_y, tile_z)

        # Build query
        query = helpers.build_query(query_select, query_from, query_where, query_group, query_order)
        query = helpers.mvt_query_envelope({ 'events': query })

        # Now proceed to a safe SQL string composition
        #
        # Since this function might be exposed to general users via API, it's vital
        # that proper sanitization takes place.
        #
        # See https://www.psycopg.org/docs/usage.html#query-parameters
        #     https://www.psycopg.org/docs/sql.html#psycopg2.sql.SQL.join
        #     https://stackoverflow.com/questions/45128902/psycopg2-and-sql-injection-security#45128973
        query = sql.SQL(query).format(
                                      event_class=sql.Literal(event_class),
                                      event_type=sql.Literal(event_type),
                                      date_initial=sql.Literal(date_initial),
                                      date_final=sql.Literal(date_final),
                                      #id_territory=sql.Literal(id_territory),
                                      #id_uf=sql.Literal(id_uf),
                                      #id_mun=sql.Literal(id_mun),
                                      #name_territory=sql.Literal(name_territory),
                                      #name_uf=sql.Literal(name_uf),
                                      #name_mun=sql.Literal(name_mun),
                                      #name_basin=sql.Literal(name_basin),
                                      where_id_territory=sql.SQL(' OR et.t_id_orig=').join(map(sql.Literal, id_territory)),
                                      where_id_uf=sql.SQL(' OR et.id_uf=').join(map(sql.Literal, id_uf)),
                                      where_id_mun=sql.SQL(' OR et.id_mun=').join(map(sql.Literal, id_mun)),
                                      where_name_territory=sql.SQL(' OR et.t_name=').join(map(sql.Literal, name_territory)),
                                      where_name_uf=sql.SQL(' OR et.name_uf=').join(map(sql.Literal, name_uf)),
                                      where_name_mun=sql.SQL(' OR et.name_mun=').join(map(sql.Literal, name_mun)),
                                      where_name_basin=sql.SQL(' OR et.basin=').join(map(sql.Literal, name_basin)),
                                      xmin=sql.Literal(Decimal(xmin)),
                                      ymin=sql.Literal(Decimal(ymin)),
                                      xmax=sql.Literal(Decimal(xmax)),
                                      ymax=sql.Literal(Decimal(ymax)),
                                      seg_size=sql.Literal(Decimal(seg_size)),
                                      srid=sql.Literal(Decimal(srid)),
                                      where_territory_types=sql.SQL(' OR et.t_type=').join(map(sql.Literal, territory_types)),
                                      )

        try:
            output, elapsed = self.run_query(query, verbose)
        except Exception as e:
            logger('exception', e)
            self.close()
            return False

        #
        # Output handling
        #

        # MVT
        # Build response from the RealDictCursor
        #return output
        return bytes(list(output[0].items())[0][1])

    def territories(self,
               territory_types=[ 'MUN' ],
               #id_territory=0,
               id_territory=[],
               id_uf=[],
               id_mun=[],
               name_basin=[],
               verbose=False,
               tile_x=0,
               tile_y=0,
               tile_z=0):
        """
        Return territory tiles from a given space definition

        :type  territory_types: list, optional
        :param territory_types: Specify territory type.
                               Available types: 'TI', 'UCF', 'UCE', 'APAF', 'APAE', 'MUN', 'UF'.
                               Buffers can be invoked using {type}_B[1-3]
                               An empty list [] means no selection by territory type.
                               Defaults to [ 'MUN' ].

        :type  id_territory: list, optional
        :param id_territory: Specify IDs of territory entitiess if you want only
                             results relative to those units.
                             Defaults to [] (no clippping entity filtering).

        :type  id_uf: list, optional
        :param id_uf: Specify IDs of UFs if you want only results relative to
                      those units.
                      Defaults to [] (no clippping entity filtering).

        :type  id_mun: list, optional
        :param id_mun: Specify IDs of municipalities if you want only results
                       relative to those units.
                       Defaults to [] (no clippping entity filtering).

        :type  name_basin: str, optional
        :param name_basin: Specify the name of a hydrographic basin if you want only results
                         relative to this basin.
                         Defaults to "" (no clippping entity filtering).

        :type  verbose: boolean, optional
        :param verbose: Output console messages. Defaults to False.

        :type  tile_x: int, optional
        :param tile_x: Indicate the {x} coordinate for vector tiles output.
                       Defaults to zero, no coordinate specified.

        :type  tile_y: int, optional
        :param tile_y: Indicate the {y} coordinate for vector tiles output.
                       Defaults to zero, no coordinate specified.

        :type  tile_z: int, optional
        :param tile_z: Indicate the map zoom level {z} for vector tiles output.
                       Defaults to zero, no coordinate specified.

        :rtype: bytes
        :return: Mapbox Vector Tiles
                 False on error

        """
        # Save argument list: this should comes first of anything else
        args = locals()
        del args['self']

        #
        # Checks
        #

        # Additional param validation
        if not self.validate(args):
            return False

        if not helpers.tileIsValid(tile_x, tile_y, tile_z):
            return False

        if len(territory_types) == 0 and len(id_territory) == 0 and \
           len(id_uf)           == 0 and len(id_mun)       == 0 and \
           len(name_basin)      == 0:
            return False

        #
        # Query processing
        #

        # Build envelope params
        xmin, ymin, xmax, ymax, seg_size = helpers.tileToEnvelope(tile_x, tile_y, tile_z)

        # Build query
        queries = {}
        params  = {
                  #'id_territory'     : sql.Literal(id_territory),
                  'where_id_territory': sql.SQL(' OR t.id_orig=').join(map(sql.Literal, id_territory)),
                  'where_id_uf'       : sql.SQL(' OR t.id_uf=').join(map(sql.Literal, id_uf)),
                  'where_id_mun'      : sql.SQL(' OR t.id_mun=').join(map(sql.Literal, id_mun)),
                  'where_name_basin'  : sql.SQL(' OR t.basin=').join(map(sql.Literal, name_basin)),
                  'xmin'              : sql.Literal(Decimal(xmin)),
                  'ymin'              : sql.Literal(Decimal(ymin)),
                  'xmax'              : sql.Literal(Decimal(xmax)),
                  'ymax'              : sql.Literal(Decimal(ymax)),
                  'seg_size'          : sql.Literal(Decimal(seg_size)),
                  'srid'              : sql.Literal(Decimal(srid)),
                }

        if len(territory_types) > 0:
            for index, item in enumerate(territory_types):
                # Create a query for this territory
                #
                # Use multiple queries so territories with a given type stays
                # in the same MVT layer
                queries[item.lower()] = helpers.build_territories_query(
                        territory_type_index=index,
                        id_territory=id_territory,
                        id_uf=id_uf,
                        id_mun=id_mun,
                        name_basin=name_basin)

                # Append territory type into parameters dict
                params['territory_type_' + str(index)] = sql.Literal(item)
        else:
                queries['territory'] = helpers.build_territories_query(
                        id_territory=id_territory,
                        id_uf=id_uf,
                        id_mun=id_mun,
                        name_basin=name_basin,
                        )

        query = helpers.mvt_query_envelope(queries)

        # Now proceed to a safe SQL string composition
        #
        # Since this function might be exposed to general users via API, it's vital
        # that proper sanitization takes place.
        #
        # See https://www.psycopg.org/docs/usage.html#query-parameters
        #     https://www.psycopg.org/docs/sql.html#psycopg2.sql.SQL.join
        #     https://stackoverflow.com/questions/45128902/psycopg2-and-sql-injection-security#45128973
        query = sql.SQL(query).format(**params)

        try:
            output, elapsed = self.run_query(query, verbose)
        except Exception as e:
            logger('exception', e)
            self.close()
            return False

        #
        # Output handling
        #

        # MVT
        # Build response from the RealDictCursor
        #return output
        return bytes(list(output[0].items())[0][1])

    def area(self,
             territory_types=[ 'MUN' ],
             #id_territory=0,
             id_territory=[],
             id_uf=[],
             id_mun=[],
             name_basin=[],
             verbose=False,

             # Accept anything else for compatibility with default API args
             **kwargs):
        """
        Return the total area of a given space definition

        :type  territory_types: list, optional
        :param territory_types: Specify territory type.
                               Available types: 'TI', 'UCF', 'UCE', 'APAF', 'APAE', 'MUN', 'UF'.
                               Buffers can be invoked using {type}_B[1-3]
                               An empty list [] means no selection by territory type.
                               Defaults to [ 'MUN' ].

        :type  id_territory: list, optional
        :param id_territory: Specify IDs of territory entitiess if you want only
                             results relative to those units.
                             Defaults to [] (no clippping entity filtering).

        :type  id_uf: list, optional
        :param id_uf: Specify IDs of UFs if you want only results relative to
                      those units.
                      Defaults to [] (no clippping entity filtering).

        :type  id_mun: list, optional
        :param id_mun: Specify IDs of municipalities if you want only results
                       relative to those units.
                       Defaults to [] (no clippping entity filtering).

        :type  name_basin: str, optional
        :param name_basin: Specify the name of a hydrographic basin if you want only results
                         relative to this basin.
                         Defaults to "" (no clippping entity filtering).

        :type  verbose: boolean, optional
        :param verbose: Output console messages. Defaults to False.

        :rtype: int
        :return: The total area for the selection
                 False on error

        """
        # Save argument list: this should comes first of anything else
        args = locals()
        del args['self']

        #
        # Checks
        #

        # Additional param validation
        if not self.validate(args):
            return False

        if len(territory_types) == 0 and len(id_territory) == 0 and \
           len(id_uf)           == 0 and len(id_mun)       == 0 and \
           len(name_basin)      == 0:
            return False

        params  = {
                  'where_id_territory'   : sql.SQL(' OR t.id_orig=').join(map(sql.Literal, id_territory)),
                  'where_id_uf'          : sql.SQL(' OR t.id_uf=').join(map(sql.Literal, id_uf)),
                  'where_id_mun'         : sql.SQL(' OR t.id_mun=').join(map(sql.Literal, id_mun)),
                  'where_name_basin'     : sql.SQL(' OR t.basin=').join(map(sql.Literal, name_basin)),
                  'where_territory_types': sql.SQL(' OR t.type=').join(map(sql.Literal, territory_types)),
                }

        query = helpers.build_territories_query(
                territory_types=territory_types,
                id_territory=id_territory,
                id_uf=id_uf,
                id_mun=id_mun,
                name_basin=name_basin,
                output='area_ha')

        # Now proceed to a safe SQL string composition
        #
        # Since this function might be exposed to general users via API, it's vital
        # that proper sanitization takes place.
        #
        # See https://www.psycopg.org/docs/usage.html#query-parameters
        #     https://www.psycopg.org/docs/sql.html#psycopg2.sql.SQL.join
        #     https://stackoverflow.com/questions/45128902/psycopg2-and-sql-injection-security#45128973
        query = sql.SQL(query).format(**params)

        try:
            output, elapsed = self.run_query(query, verbose)
        except Exception as e:
            logger('exception', e)
            self.close()
            return False

        return float(output[0]['area_ha'])

    def municipality(
            self,
            #uf=0,
            uf=[],
            verbose=False
            ):
        """
        Retrieve municipality data from canonical Base 250 table.

        :type  uf: list, optional
        :param uf: Restrict to the given Federal Unit state codes.

        :type  verbose: boolean, optional
        :param verbose: Output console messages. Defaults to False.

        :rtype:  list
        :return: List of municipalities.
                 Currently restricts municipalies which are inside the Legal Amazon.
                 False on error
        """

        # Save argument list: this should comes first of anything else
        args = locals()
        del args['self']

        #
        # Query initialization
        #

        query_select = []; query_where = []; query_group = []; query_order = []; query_from = ""

        #
        # Query composition: SELECT
        #

        query_select.append("m.nome    AS text")
        query_select.append("m.cod_mun AS value")

        #
        # Query composition: FROM
        #

        query_from = " FROM base.mun_base250 m "

        #
        # Query composition: WHERE
        #

        query_where.append("m.amaleg = {amaleg}")

        #if uf > 0:
        #    query_where.append("m.cod_uf = {uf}")
        if len(uf) > 0:
            query_where.append("(m.cod_uf={where_uf})")

        #
        # Query composition: ORDER
        #

        query_order.append("m.nome ASC")

        #
        # Query processing
        #

        # Build query
        query = helpers.build_query(query_select, query_from, query_where, query_group, query_order)
        query = sql.SQL(query).format(
                                      #uf=sql.Literal(uf),
                                      where_uf=sql.SQL(' OR m.cod_uf=').join(map(sql.Literal, uf)),
                                      amaleg=sql.Literal(int(1)),
                                      )

        try:
            output, elapsed = self.run_query(query, verbose)
        except Exception as e:
            logger('exception', e)
            self.close()
            return False

        #
        # Output handling
        #

        output = {
                   'data': output,
                   'meta': {
                             'timestamp': str(datetime.now()),
                             'elapsed'  : elapsed,
                             'args'     : args,
                             #'query'   : statement,
                           }
                 }

        return output

    def basin(self, verbose=False):
        """
        Retrieve basin data from canonical basins_lvl2_ana.

        :type  verbose: boolean, optional
        :param verbose: Output console messages. Defaults to False.

        :rtype:  list
        :return: List of basins.
                 Currently restricts basins which are inside the Legal Amazon.
                 False on error
        """

        # Save argument list: this should comes first of anything else
        args = locals()
        del args['self']

        #
        # Query initialization
        #

        query_select = []; query_where = []; query_group = []; query_order = []; query_from = ""

        #
        # Query composition: SELECT
        #

        query_select.append("b.bacia AS text")
        query_select.append("b.bacia AS value")
        #query_select.append("b.nivel2 AS value")

        #
        # Query composition: FROM
        #

        query_from = " FROM base.basins_lvl2_ana b, base.bla_base250 a "

        #
        # Query composition: WHERE
        #

        #query_where.append("m.amaleg = {amaleg}")
        query_where.append("ST_intersects(b.shape, a.shape)")

        #
        # Query composition: ORDER
        #

        query_order.append("b.bacia ASC")

        #
        # Query processing
        #

        # Build query
        query = helpers.build_query(query_select, query_from, query_where, query_group, query_order)
        query = sql.SQL(query).format(
                                      #amaleg=sql.Literal(int(1)),
                                     )

        try:
            output, elapsed = self.run_query(query, verbose)
        except Exception as e:
            logger('exception', e)
            self.close()
            return False

        #
        # Output handling
        #

        output = {
                   'data': output,
                   'meta': {
                             'timestamp': str(datetime.now()),
                             'elapsed'  : elapsed,
                             'args'     : args,
                             #'query'   : statement,
                           }
                 }

        return output

    @teardown
    def close(self):
        return

if __name__=="__main__":
    # Instantiate
    query = Query()

    # Execute a vanilla query
    result = query.events()

    # Custom query, overriding some parameters
    #result = query.events(territory_types=[],
    #                     event_class=1,
    #                     event_type='deter',
    #                     geojson=True,
    #                     by_timeperiod=False,
    #                     group_by=[],
    #                     temporal_unit='Month',
    #                     date_initial='2019-01-01',
    #                     date_final='2021-11-01',
    #                     id_territory=0,
    #                     compute_emissions=True,
    #                     smart_area=True,
    #                     verbose=True)

    # Another custom query
    #result = query.events(event_type='firms_viirs',
    #                      group_by=[],
    #                      by_timeperiod=False,
    #                      territory_types=[],
    #                      verbose=False,
    #                      json=True)

    logger('info', result)
