#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import argparse
import lib.cache.manager as manager
from lib.params import copyright_notice
from lib.logger import logger

class Cache:
    """User interaction with the cache manager"""

    def __init__(self, args):
        """
        Class initialization.

        Instantiate class properties.

        :param args dict: Configuration arguments.
        """

        self.args = args

    def run(self):
        if 'action' in dir(self.args) and self.args.action in dir(manager):
            logger('info', 'Running action ' + self.args.action + ' in the cache...')
            getattr(manager, self.args.action)()

def cmdline():
    """
    Evalutate the command line.

    :return: Command line arguments.
    """

    basename = os.path.basename(__file__)
    notice   = copyright_notice(basename)

    # Parse CLI
    examples  = "Examples:\n\t" + basename + " clear\n"
    examples  = "\t"            + basename + " refresh\n"

    epilog = examples + "\n\n" + notice
    parser = argparse.ArgumentParser(description='Run a cache action',
                                     epilog=epilog,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,)

    parser.add_argument('action',
            help='Run the given action. Available actions: clear, purge, refresh')

    # Add default values and get args
    args = parser.parse_args()

    return args

if __name__ == "__main__":
    args = cmdline()

    # Instantiate
    instance = Cache(args)

    # Run
    instance.run()
