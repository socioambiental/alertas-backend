#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import argparse
from importlib          import import_module
from lib.params         import importers_path
from lib.params         import copyright_notice, pipeline_phases
from lib.logger         import logger
from lib.cache.manager  import clear
#from lib.metadata.info import clear_metadata_cache

from lib.metadata.imports      import ImportMetadataManager
from lib.metadata.exports      import ExportMetadataManager
#from lib.metadata.events      import EventMetadataManager
#from lib.metadata.territories import TerritoryMetadataManager

# Importers list
importers = os.listdir(importers_path)

# Current folder
dirname = os.path.dirname(__file__)

class Metadata:
    """Process metadata"""

    def __init__(self, args):
        """
        Class initialization.

        Instantiate class properties and run basic checks.

        :param args dict: Configuration arguments.
        """

        if args.importer is not None:
            self.importers = [ args.importer ]
        else:
            self.importers = importers

        self.abort_on_error = args.abort_on_error

    def run(self):
        """Refresh metadata in the database"""

        # Refresh import metadata
        self.imports()

        # Refresh export metadata
        self.exports()

    def cache_clear(self):
        # Cleanup metadata cache, ensuring that the API will refresh this
        # information automatically upon the next request
        #logger('info', 'Clearing metadata cache...')
        #clear_metadata_cache()

        # Clear all caches after an import has concluded
        #logger('info', 'Clearing all caches...')
        #clear()

        # Clear the misc cache after an import has concluded
        logger('info', 'Clearing the misc cache...')
        clear([ 'misc' ])

    def imports(self):
        """Handle import metadata"""

        meta      = ImportMetadataManager()
        completed = meta.completed(short=False)

        logger('info', 'Refreshing imports metadata...')

        for importer in self.importers:
            if importer not in completed:
                logger('info', 'Skipping ' + importer + ' as it was never fully imported...')
                continue

            logger('info', 'Processing ' + importer + '...')

            # Load importer config
            package_name = 'importers.' + importer
            config_name  = package_name + '.config'
            config       = import_module(config_name)

            # Defaults to an ELT pipeline
            if 'pipeline' not in dir(config):
                config.pipeline = 'elt'

            # Determine the last phase
            last_phase = pipeline_phases[config.pipeline][-1]

            # A simple update is enough to update all config metadata for the
            # last phase
            meta.update(completed[importer]['import_id'], last_phase, config)

        self.cache_clear()

    def exports(self):
        """Handle export metadata"""

        meta   = ExportMetadataManager()
        latest = meta.latest(short=False)

        logger('info', 'Refreshing exports metadata...')

        from importlib import import_module
        from lib.utils import to_camel_case

        for export in latest:
            exporter  = latest[export]['name']
            operation = latest[export]['operation']

            # Load exporter config
            package_name = 'exporters.' + exporter
            config_name  = package_name + '.config'
            config       = import_module(config_name)

            # Currently there's only one exporter phase
            phases = [ 'export' ]

            for phase in phases:
                if os.path.exists(os.path.join(dirname, 'exporters', exporter, phase + '.py')):
                    module_name = package_name + '.' + phase
                    class_name  = to_camel_case(exporter) + to_camel_case(phase)
                    module      = import_module(module_name)
                    args        = {}

                    # Get the appropriate class
                    module_class = getattr(module, class_name)

                    # Instantiate
                    instance = module_class(config, args)

                    if instance is False:
                        logger('error', 'Error initializing {instance}'.format(
                            instance=module_name
                            ))

                        if self.abort_on_error:
                            exit(1)
                        else:
                            break

                    if operation in instance.operations:
                        logger('info', 'Processing operation {operation} in exporter {exporter}'.format(
                        operation=operation, exporter=exporter))

                        # A simple update is enough to update all config metadata for the
                        # latest export
                        meta.update(
                                latest[export]['export_id'],
                                operation,
                                config,
                                instance.operations,
                                )

        self.cache_clear()

def cmdline():
    """
    Evalutate the command line.

    :return: Command line arguments.
    """

    basename = os.path.basename(__file__)
    notice   = copyright_notice(basename)

    # Parse CLI
    examples  = "Examples:\n\t" + basename + " deter\n"

    epilog = examples + "\n\n" + notice
    parser = argparse.ArgumentParser(description='Run a metadata action',
                                     epilog=epilog,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,)

    parser.add_argument('importer', nargs='?',
            help='Run only for a given importer. Available importers: ' + \
                    ' '.join(importers) + '. Default: all importers.')

    parser.add_argument('--abort-on-error', dest='abort_on_error', action='store_true',
                        help='Abort on the first import error.')

    parser.add_argument('--do-not-abort-on-error', dest='abort_on_error', action='store_false',
                        help='Do not abort on errors.')

    # Add default values and get args
    parser.set_defaults(abort_on_error=True)
    args = parser.parse_args()

    # Print copyright notice
    #print(notice + "\n")

    return args

if __name__ == "__main__":
    args = cmdline()

    metadata = Metadata(args)
    metadata.run()
