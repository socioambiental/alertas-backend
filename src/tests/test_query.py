#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Expand the search path to include the parent folder
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))

# Dependencies
import pytest
from benchmark import Benchmark
#from query    import Query

benchmarking = Benchmark()
#query       = Query()
combinations = list(benchmarking.combinations)

#@pytest.mark.parametrize("combination", combinations)
#def test_query(combination):
#    # Run query directly from query instance
#    #params = benchmarking.parametrize(combination)
#    #result = query.events(**params)
#
#    # Run query through benchmaking instance
#    result = benchmarking.event(combination, False)

@pytest.mark.parametrize("combination", combinations)
def test_benchmark(benchmark, combination):
    # Run query directly from query instance
    #params = benchmarking.parametrize(combination)
    #result = benchmark(query.events, **params)

    # Run query through benchmaking instance
    result = benchmark(benchmarking.event, combination, False)

#@pytest.mark.parametrize("combination", combinations)
#def test_benchmark_caching(benchmark, combination):
#    # Run query through benchmaking instance
#    result = benchmark(benchmarking.event, combination, True)
