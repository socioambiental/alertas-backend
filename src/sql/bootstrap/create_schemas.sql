-- Schemas definition

-- Create schemas
CREATE SCHEMA IF NOT EXISTS {base_schema}            AUTHORIZATION {db_user};
CREATE SCHEMA IF NOT EXISTS {event_canonical_schema} AUTHORIZATION {db_user};
CREATE SCHEMA IF NOT EXISTS {event_processed_schema} AUTHORIZATION {db_user};
CREATE SCHEMA IF NOT EXISTS {metadata_schema}        AUTHORIZATION {db_user};

-- USAGE and SELECT GRANTs
GRANT USAGE  ON SCHEMA {base_schema}                          TO {db_user_query};
GRANT USAGE  ON SCHEMA {event_processed_schema}               TO {db_user_query};
GRANT USAGE  ON SCHEMA {metadata_schema}                      TO {db_user_query};
GRANT SELECT ON ALL TABLES IN SCHEMA {event_processed_schema} TO {db_user_query};
GRANT SELECT ON ALL TABLES IN SCHEMA {metadata_schema}        TO {db_user_query};

-- GRANTs for tables created in the future
-- See https://stackoverflow.com/questions/10352695/grant-all-on-a-specific-schema-in-the-db-to-a-group-role-in-postgresql#10353730
--     https://www.postgresql.org/docs/current/sql-alterdefaultprivileges.html#SQL-ALTERDEFAULTPRIVILEGES-NOTES
ALTER DEFAULT PRIVILEGES FOR ROLE {db_user} IN SCHEMA {event_processed_schema} GRANT SELECT ON TABLES TO {db_user_query};
ALTER DEFAULT PRIVILEGES FOR ROLE {db_user} IN SCHEMA {metadata_schema}        GRANT SELECT ON TABLES TO {db_user_query};
