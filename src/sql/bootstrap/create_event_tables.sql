DO
$$
BEGIN
  IF NOT EXISTS(SELECT * FROM information_schema.tables WHERE table_schema='event_processed' and table_name='event') THEN
        CREATE TABLE event_processed.event (
            id       BIGSERIAL NOT NULL,
            orig_id  INTEGER,
            type     VARCHAR,
            class_1  INTEGER,
            class_2  VARCHAR,
            date     DATE,
            coduf    INTEGER,
            uf       VARCHAR,
            codmun   INTEGER,
            mun      VARCHAR,
            geom     geometry,
            PRIMARY KEY (id, type)
            ) PARTITION BY LIST (type);
        PERFORM UpdateGeometrySRID('event_processed','event', 'geom', {srid});
        --PERFORM Populate_Geometry_Columns('public.event'::regclass);
        CREATE INDEX sidx_event_geom
            ON event_processed.event USING gist(geom);
        CREATE INDEX idx_event_type ON event_processed.event(type);
  END IF;
END;
$$
LANGUAGE plpgsql;

DO
$$
BEGIN
  IF NOT EXISTS(SELECT * FROM information_schema.tables WHERE table_schema='event_processed' and table_name='event_territory') THEN
        CREATE TABLE event_processed.event_territory
        (
            id         BIGSERIAL NOT NULL,
            e_id       integer,
            e_type     character varying COLLATE pg_catalog."default",
            e_class_1  integer,
            e_class_2  character varying COLLATE pg_catalog."default",
            e_date     date,
            t_type     character varying COLLATE pg_catalog."default",
            t_name     character varying COLLATE pg_catalog."default",
            t_id_orig  integer,
            id_mun     integer,
            id_uf      integer,
            name_mun   character varying COLLATE pg_catalog."default",
            name_uf    character varying COLLATE pg_catalog."default",
            basin      character varying COLLATE pg_catalog."default",
            area_ha    numeric,
            c_emission numeric,
            geom       geometry(Geometry,{srid}),
            ovlp_ext   boolean,
            ovlp_int   boolean,
            ovlp_extnobuff boolean,
            PRIMARY KEY (id, e_type)
            ) PARTITION BY LIST (e_type);
        PERFORM UpdateGeometrySRID('event_processed','event', 'geom', {srid});
        CREATE INDEX sidx_event_territory_geom
            ON event_processed.event_territory USING gist(geom);
        CREATE INDEX idx_event_territory_type ON event_processed.event_territory(e_type,e_class_1,e_date,t_type,t_id_orig,id_mun,id_uf,basin);

  END IF;
END;
$$
LANGUAGE plpgsql;
