--  This script will define a set of three spatial functions created to avoid topology errors
--  while computing difference, intersection or union operations.
--  Firstly, the functions try to perform the ordinary operation
--  If this fails, it will try to perform the same operation
--  on a buffered version of the conflicting features.

CREATE OR REPLACE FUNCTION safe_diff(geom_a geometry, geom_b geometry)
    RETURNS geometry AS
    $$
    BEGIN
        RETURN ST_difference(geom_a,geom_b);
        EXCEPTION
            WHEN OTHERS THEN
                BEGIN
                    RAISE NOTICE 'trying to solve exception';
                    RETURN ST_difference(ST_Buffer(geom_a, 0.0000001), ST_Buffer(geom_b, 0.0000001));
                    EXCEPTION
                        WHEN OTHERS THEN
                            RAISE NOTICE 'Not able to solve exception';
                        RETURN ST_GeomFromText('POLYGON EMPTY');
                END;
    END;
$$
LANGUAGE 'plpgsql' STABLE STRICT;

CREATE OR REPLACE FUNCTION safe_intersection(geom_a geometry, geom_b geometry)
    RETURNS geometry AS
    $$
    BEGIN
        RETURN ST_intersection(geom_a,geom_b);
        EXCEPTION
            WHEN OTHERS THEN
                BEGIN
                    RAISE NOTICE 'trying to solve exception';
                    RETURN ST_intersection(ST_Buffer(geom_a, 0.0000001), ST_Buffer(geom_b, 0.0000001));
                    EXCEPTION
                        WHEN OTHERS THEN
                            RAISE NOTICE 'Not able to solve exception';
                        RETURN ST_GeomFromText('POLYGON EMPTY');
                END;
    END;
$$
LANGUAGE 'plpgsql' STABLE STRICT;

CREATE OR REPLACE FUNCTION safe_intersects(geom_a geometry, geom_b geometry)
    RETURNS boolean AS
    $$
    BEGIN
        RETURN ST_intersects(geom_a,geom_b);
        EXCEPTION
            WHEN OTHERS THEN
                BEGIN
                    RAISE NOTICE 'trying to solve exception';
                    RETURN ST_intersects(ST_Buffer(geom_a, 0.0000001), ST_Buffer(geom_b, 0.0000001));
                    EXCEPTION
                        WHEN OTHERS THEN
                            RAISE NOTICE 'Not able to solve exception';
                        RETURN ST_GeomFromText('POLYGON EMPTY');
                END;
    END;
$$
LANGUAGE 'plpgsql' STABLE STRICT;

-- Smart_area: this spatial function will compute the total area of a set of polygons,
-- checking for their overlapping flags to optimize non-overlapping area computation

CREATE OR REPLACE FUNCTION smart_area(areas numeric array,
                                      geoms geometry array,
                                      int_flags boolean array,
                                      ext_flags boolean array,
                                      extnbf_flags boolean array,
                                      rtypes varchar array
                                      )
    RETURNS numeric AS
    $$
    DECLARE
      a numeric;
      b numeric;
      intersec boolean array;
    BEGIN

        -- Initially, assumes that the event_territory polygons belong to mixed territory types and fixes the
        -- level of intersection analysis to the most general

        intersec := ext_flags;

        -- Determines if the territory types of the event_territory polygons include buffer territories
        -- If not, the intersection analysis level simplifies to the non-buffered territories

        IF  (SELECT COUNT(*) FROM (SELECT rtype FROM UNNEST (rtypes) rtype) AS temp WHERE rtype like '%B_%')=0 THEN
          intersec := extnbf_flags;
        END IF;

        -- Determines if there's only one territory type on the event_territory polygons
        -- If that's the case, the level of interesection analysis will be the simplest, the internal intersection flag

        IF  (SELECT COUNT(*) FROM (SELECT DISTINCT rtype FROM UNNEST (rtypes) rtype) AS temp) = 1 THEN
          intersec := int_flags;
        END IF;

        -- Compute areas:

        -- First, the simple sum of areas of the polygons which has no intersections on the deduced level of analysis
        a := COALESCE(SUM(area),0) FROM UNNEST(areas,intersec) AS tbl1 (area,intflag)
                      WHERE NOT intflag;
        -- Then, compute the area of overlapping polygons using (costly) dissolve spatial operations
        b := COALESCE(ST_AREA(ST_UNION(geom)::geography)/10000,0) FROM unnest(geoms,intersec) AS tbl2 (geom,intflag)
                    WHERE intflag;

        -- Finally, returns the final area

        RETURN a+b;
    END;
$$
LANGUAGE "plpgsql" STABLE STRICT;

CREATE OR REPLACE FUNCTION smart_emissions(areas numeric array,
                                      emissions numeric array,
                                      geoms geometry array,
                                      int_flags boolean array,
                                      ext_flags boolean array,
                                      extnbf_flags boolean array,
                                      rtypes varchar array
                                      )
    RETURNS numeric AS
    $$
    DECLARE
      a numeric;
      b numeric;
      intersec boolean array;
    BEGIN

        -- Initially, assumes that the event_territory polygons belong to mixed territory types and fixes the
        -- level of intersection analysis to the most general

        intersec := ext_flags;

        -- Determines if the territory types of the event_territory polygons include buffer territories
        -- If not, the intersection analysis level simplifies to the non-buffered territories

        IF  (SELECT COUNT(*) FROM (SELECT rtype FROM UNNEST (rtypes) rtype) AS temp WHERE rtype like '%B_%')=0 THEN
          intersec := extnbf_flags;
        END IF;

        -- Determines if there's only one territory type on the event_territory polygons
        -- If that's the case, the level of interesection analysis will be the simplest, the internal intersection flag

        IF  (SELECT COUNT(*) FROM (SELECT DISTINCT rtype FROM UNNEST (rtypes) rtype) AS temp) = 1 THEN
          intersec := int_flags;
        END IF;

        -- Compute emissions:

        -- First, the simple sum of areas times the emission of every polygons which has no intersections on the deduced level of analysis
        a := COALESCE(SUM(area*emission),0) FROM UNNEST(areas,emissions,intersec) AS tbl1 (area,emission,intflag)
                      WHERE NOT intflag;
        -- Then, compute the area of overlapping polygons using (costly) dissolve spatial operations and multiply by the mean emission
        b := COALESCE(AVG(emission)*ST_AREA(ST_UNION(geom)::geography)/10000,0) FROM UNNEST(geoms,emissions,intersec) AS tbl2 (geom,emission,intflag)
                    WHERE intflag;

        -- Finally, returns the final area

        RETURN a+b;
    END;
$$
LANGUAGE "plpgsql" STABLE STRICT;

-- This function will compute the total emission of a set of hotspot entities. To avoid duplicity, we group the entities
-- by e_id and compute the average emission (could be the first value of the group, but postgresql doesnt have this function)
CREATE OR REPLACE FUNCTION smart_emissions_pt(ids numeric array,
                                      emissions numeric array
                                      )
    RETURNS numeric
    LANGUAGE sql AS $$
    WITH unique_emissions AS (
            SELECT  AVG(c_emission)  FROM UNNEST(ids,emissions) AS tbl1 (e_id,c_emission) group by e_id
            )
        SELECT sum(avg) from unique_emissions

$$;
