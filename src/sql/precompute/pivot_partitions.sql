-- Set search path to include the wanted schemas in order
SET search_path to event_processed,public;

--
-- BEGIN transaction for data update
--
BEGIN;

DROP TABLE IF EXISTS {parent_table}_{e_type};

ALTER TABLE {parent_table}_{e_type}_new RENAME TO {parent_table}_{e_type};

-- Attach the table only after data loading to avoid locking on the event table
-- in case of simultaneous imports
ALTER TABLE {parent_table} ATTACH PARTITION {parent_table}_{e_type}
  FOR VALUES IN ('{e_type}');

--
-- COMMIT transaction for data update
--
COMMIT;

-- Run ANALYZE whenever a bulk loading takes place
-- See https://www.2ndquadrant.com/en/blog/postgresql-vacuum-and-analyze-best-practice-tips/
--     https://andreigridnev.com/blog/2016-04-01-analyze-reindex-vacuum-in-postgresql/
--     https://wiki.postgresql.org/wiki/Introduction_to_VACUUM,_ANALYZE,_EXPLAIN,_and_COUNT
ANALYZE {parent_table}_{e_type};
