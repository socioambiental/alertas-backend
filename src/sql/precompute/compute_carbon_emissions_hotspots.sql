-- This procedure will compute the carbon emissions
-- associated to a point event_territory feature of a specific class

BEGIN;
    UPDATE event_processed.event_territory_{e_type}
        SET c_emission = {c_factor} * orig.frp
        FROM event_canonical.{e_type}_canonical orig
        WHERE e_class_1 = {class_1} AND e_id = orig.id AND c_emission IS NULL;
COMMIT;
