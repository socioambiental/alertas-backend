-- This procedure will flag the event_territory polygons which overlaps others,
-- abling the smart_area spatial function.
--
-- The script analyzes if a event_territory polygon intersect other polygons on
-- 3 levels of analysis.
--

-- Set search path to include the wanted schemas in order
SET search_path to event_processed,public;

--ALTER TABLE event_territory DROP COLUMN IF EXISTS ovlp_ext;
--ALTER TABLE event_territory ADD COLUMN ovlp_ext BOOLEAN;

--
-- BEGIN transaction for computation
--
BEGIN;

-- With all territory types:
UPDATE event_territory_{e_type}_new SET ovlp_ext=false;
UPDATE event_territory_{e_type}_new SET ovlp_ext=true WHERE id IN (
  SELECT a.id
  FROM event_territory_{e_type}_new a
  INNER JOIN event_territory_{e_type}_new b ON a.geom && b.geom
  WHERE a.id != b.id
        AND a.e_class_1=b.e_class_1
        AND a.t_type!='MUN'  AND b.t_type!='MUN'
        AND a.t_type!='UF'   AND b.t_type!='UF'
        AND a.t_type!='MUND' AND b.t_type!='MUND'
        AND a.t_type!='UFD'  AND b.t_type!='UFD'
        AND st_area(st_intersection(a.geom,b.geom))>1E-7);

-- Within its own territory type:
--ALTER TABLE event_territory DROP COLUMN IF EXISTS ovlp_int;
--ALTER TABLE event_territory ADD COLUMN ovlp_int BOOLEAN;
UPDATE event_territory_{e_type}_new SET ovlp_int=false;
UPDATE event_territory_{e_type}_new SET ovlp_int=true WHERE id IN (
  SELECT a.id
  FROM       event_territory_{e_type}_new a
  INNER JOIN event_territory_{e_type}_new b ON a.geom && b.geom
  WHERE a.id != b.id
        AND a.t_type=b.t_type
        AND a.e_class_1=b.e_class_1
        AND a.t_type!='MUN'  AND b.t_type!='MUN'
        AND a.t_type!='UF'   AND b.t_type!='UF'
        AND a.t_type!='MUND' AND b.t_type!='MUND'
        AND a.t_type!='UFD'  AND b.t_type!='UFD'
        AND st_area(st_intersection(a.geom,b.geom))>1E-7);

-- Within all the non-buffer territory types:
--ALTER TABLE event_territory DROP COLUMN IF EXISTS ovlp_extnobuff;
--ALTER TABLE event_territory ADD COLUMN ovlp_extnobuff BOOLEAN;
UPDATE event_territory_{e_type}_new SET ovlp_extnobuff=false;
UPDATE event_territory_{e_type}_new SET ovlp_extnobuff=true WHERE id IN (
  SELECT a.id
  FROM       event_territory_{e_type}_new a
  INNER JOIN event_territory_{e_type}_new b ON a.geom && b.geom
  WHERE a.id != b.id
        AND a.t_type=b.t_type
        AND a.e_class_1=b.e_class_1
        AND a.t_type!='MUN'  AND b.t_type!='MUN'
        AND a.t_type!='UF'   AND b.t_type!='UF'
        AND a.t_type!='MUND' AND b.t_type!='MUND'
        AND a.t_type!='UFD'  AND b.t_type!='UFD'
        AND a.t_type NOT LIKE '%_B%' AND b.t_type NOT LIKE '%_B%'
        AND st_area(st_intersection(a.geom,b.geom))>1E-7);

-- Drop table temporary indexes before attaching to avoid conflicts with
-- indexes of the master table
DROP INDEX sidx_event_territory_{e_type}_new_geom;
DROP INDEX idx_event_territory_{e_type}_new_type;

--
-- COMMIT transaction for computation
--
COMMIT;
