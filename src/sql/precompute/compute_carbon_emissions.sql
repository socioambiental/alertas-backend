-- This procedure will compute the carbon emission factor
-- associated to a polygonal event_territory feature of a specific class

--
-- BEGIN transaction for computation
--
BEGIN;

-- First we create a temporal table with the warning
-- polygons centroids
DROP TABLE IF EXISTS public.carbon_emissions_{e_type}_tmp;
CREATE TABLE public.carbon_emissions_{e_type}_tmp as
SELECT id, ST_SetSRID(ST_Centroid(geom),4326) as geom
FROM event_processed.event_territory_{e_type}
WHERE e_class_1 = {class_1};
CREATE INDEX sidx_carbon_emissions_{e_type}_tmp_geom ON public.carbon_emissions_{e_type}_tmp USING gist(geom);

-- Then we compute the biomass density associated
DROP TABLE IF EXISTS public.carbon_emissions_{e_type}_tmp2;
CREATE TABLE public.carbon_emissions_{e_type}_tmp2 AS
    SELECT p.id, ST_Value(b.rast, p.geom) AS biomassdensity
        FROM public.carbon_emissions_{e_type}_tmp AS p
        JOIN  base.biomassdensity b ON ST_Intersects(b.rast,p.geom) ;

--
-- COMMIT transaction for computation
--
COMMIT;

--
-- BEGIN transaction for data update
--
BEGIN;

-- And finally we add the total emissions (biomass * combustion factor)
-- to the original table
UPDATE event_processed.event_territory_{e_type}
SET c_emission = {c_factor} * b.biomassdensity
FROM public.carbon_emissions_{e_type}_tmp2 b
WHERE e_class_1 = {class_1} AND event_processed.event_territory_{e_type}.id = b.id;

--
-- COMMIT transaction for data update
--
COMMIT;

--
-- Cleanup
--
DROP TABLE IF EXISTS public.carbon_emissions_{e_type}_tmp;
DROP TABLE IF EXISTS public.carbon_emissions_{e_type}_tmp2;
