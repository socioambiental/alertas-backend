BEGIN;

-- Removal of data points outside Brazil Legal Amazon (BLA)
-- This filter is needed if the backend is configured to only include events
-- inside the BLA.
DELETE FROM event_{e_type}_new f WHERE
  f.id IN (
    SELECT a.id
    FROM event_{e_type}_new a, base.bla_base250 b
    WHERE NOT ST_intersects(a.geom, b.shape)
  );

COMMIT;
