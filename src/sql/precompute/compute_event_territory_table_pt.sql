-- This script will compute the intersection of a punctual event dataset with a
-- set of polygons defining territories.
--
-- The script should be formatted, substituting the placeholder {e_type} with
-- the type of event to be intersected.

-- Set search path to include the wanted schemas in order
SET search_path to event_processed,public;

--
-- BEGIN transaction for computation
--
BEGIN;

-- Deletes any previous work table
DROP TABLE IF EXISTS event_territory_{e_type}_new;

--CREATE TABLE event_territory_{e_type} PARTITION OF event_territory
--  FOR VALUES IN ('{e_type}');

-- Inserts temporary data on event_territory global table
CREATE TABLE event_territory_{e_type}_new (LIKE event_territory INCLUDING DEFAULTS INCLUDING CONSTRAINTS);

-- A SELECT with "ORDER BY" event_territory partitions helps avoiding
-- fragmentation in the data, keeping the index less complex and decreasing
-- query time around date intervals.
INSERT INTO event_territory_{e_type}_new (e_id,e_type, e_class_1, e_class_2, e_date, t_type, t_name, t_id_orig, id_mun, id_uf, name_mun, name_uf, basin, geom)
  SELECT e.orig_id, e.type, e.class_1, e.class_2, e.date, t.type, t.name, t.id_orig, e.codmun, e.coduf, e.mun, e.uf, basin.bacia, e.geom AS geom
        FROM event_{e_type} e
        JOIN base.territory t ON st_intersects(e.geom,t.geom)
        JOIN base.BASINS_lvl2_ana basin ON st_within(ST_Centroid(e.geom),basin.shape)
        ORDER BY e.date;
        --WHERE e.UF='AC';

--
-- COMMIT transaction for computation
--
COMMIT;
