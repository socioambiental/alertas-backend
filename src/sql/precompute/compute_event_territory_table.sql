-- This script will compute the intersection of a polygonal event dataset with
-- a set of polygons defining territories.
--
-- The script should be formatted, substituting the placeholder {e_type} with
-- the type of event to be intersected.

-- Set search path to include the wanted schemas in order
SET search_path to event_processed,public;

--
-- BEGIN transaction for computation
--
BEGIN;

-- Deletes any previous work table
DROP TABLE IF EXISTS event_territory_{e_type}_tmp;

-- Deletes any previous work indexes
DROP INDEX IF EXISTS sidx_event_territory_{e_type}_tmp_geom;

-- Creates intersection temporary table
CREATE TABLE event_territory_{e_type}_tmp AS
  SELECT e.orig_id AS e_id,e.type AS e_type,e.class_1 AS e_class_1,e.class_2 AS e_class_2,e.date AS e_date,
         t.type AS t_type, t.name AS t_name, t.id_orig AS t_id_orig,
         safe_intersection(e.geom,t.geom) AS geom
        --(st_dump(safe_intersection(e.geom,t.geom))).geom AS geom
        FROM event_{e_type} e
        JOIN base.territory t ON st_intersects(e.geom,t.geom);
        --WHERE e.UF='AC';

-- Optimizes table
--SELECT UpdateGeometrySRID('event_territory_{e_type}_tmp', 'geom', {srid});
--SELECT Populate_Geometry_Columns('public.event_territory_{e_type}_tmp'::regclass);
ALTER TABLE event_territory_{e_type}_tmp ADD ID SERIAL PRIMARY KEY;
CREATE INDEX sidx_event_territory_{e_type}_tmp_geom
    ON event_territory_{e_type}_tmp USING gist
    (geom)
    TABLESPACE pg_default;

-- Drop any previous new table
DROP TABLE IF EXISTS event_territory_{e_type}_new;

-- Deletes any existing indexes
DROP INDEX IF EXISTS sidx_event_territory_{e_type}_new_geom;
DROP INDEX IF EXISTS idx_event_territory_{e_type}_new_type;

-- Create the new table
CREATE TABLE event_territory_{e_type}_new (LIKE event_territory INCLUDING DEFAULTS INCLUDING CONSTRAINTS);

-- Create temporary indexes to aid precomputations
CREATE INDEX sidx_event_territory_{e_type}_new_geom
    ON event_territory_{e_type}_new USING gist
    (geom)
    TABLESPACE pg_default;
CREATE INDEX idx_event_territory_{e_type}_new_type
    ON event_processed.event_territory(e_type,t_type,e_date,e_class_1);

-- Inserts temporary data on event_territory global table
--
-- A SELECT with "ORDER BY" event_territory partitions helps avoiding
-- fragmentation in the data, keeping the index less complex and decreasing
-- query time around date intervals.
INSERT INTO event_territory_{e_type}_new (e_id, e_type, e_class_1, e_class_2, e_date,
           t_type, t_name, t_id_orig, id_mun, id_uf, name_mun, name_uf, basin, area_ha, geom)
    SELECT et.e_id AS e_id, et.e_type,et.e_class_1,et.e_class_2,et.e_date,
           et.t_type,et.t_name,et.t_id_orig,
           mun.cod_mun AS id_mun,
           mun.cod_uf AS id_uf,
           mun.nome AS name_mun,
           mun.siglauf AS name_uf,
           basin.bacia AS basin,
           st_area(et.geom::geography)::numeric/10000 AS area_ha, et.geom
    FROM event_territory_{e_type}_tmp et
    JOIN base.MUN_base250 mun ON st_within(ST_Centroid(et.geom),mun.shape)
    JOIN base.BASINS_lvl2_ana basin ON st_within(ST_Centroid(et.geom),basin.shape)
    ORDER BY e_date;
    --WHERE st_geometrytype(et.geom)='ST_Polygon';

-- Ensure the index is populated so precomputations runs faster
ANALYZE event_territory_{e_type}_new;

-- Deletes the temporary table
DROP TABLE event_territory_{e_type}_tmp;

--SELECT UpdateGeometrySRID('event_territory', 'geom', {srid});
--SELECT Populate_Geometry_Columns('event_territory'::regclass);
--ALTER TABLE event_territory ADD ID SERIAL PRIMARY KEY;
--CREATE INDEX sidx_event_territory_geom
--    ON public.event_territory USING gist
--    (geom)
--    TABLESPACE pg_default;

--
-- COMMIT transaction for computation
--
COMMIT;
