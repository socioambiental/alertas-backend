#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Helper tileserver functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask               import make_response
from webargs.flaskparser import abort

def build_response(result):
    if result is False:
        abort(404)
    elif len(result) == 0:
        return ''

    response = make_response(result)

    #response.headers["Content-type"] = "application/vnd.mapbox-vector-tile"
    response.headers["Content-type"]  = "application/x-protobuf"

    return response
