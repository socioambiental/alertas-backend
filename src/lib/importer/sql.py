#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# SQL importer class.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import datetime
from psycopg2                         import Error
from lib.params                       import srid
from lib.database.decorators.psycopg2 import bootstrap, commit, teardown
from lib.logger                       import logger
from lib.importer.base                import BaseImporter, lock, check_dependencies, set_import_id
from collections                      import OrderedDict
from lib.metadata.decorators          import check_import_metadata, save_import_metadata

class SqlImporter(BaseImporter):
    """
    Class with common methods for importing data via SQL.

    """

    @set_import_id
    def __init__(self, config, args = {}):
        super().__init__(config, args)

        # Leave that undefined to make sure children define their dirnames
        #if self.dirname is None:
        #    self.dirname = os.path.dirname(__file__)

        # SQL statements
        self.sql_local  = os.path.join(self.dirname, "sql") + os.sep
        self.sql_global = os.path.join(self.dirname, "..", "..", "sql", "precompute") + os.sep

        # Operations
        # Use an OrderedDict to ensure ordering
        # Should be customized by child classes
        self.operations = OrderedDict()

    @lock
    @check_dependencies
    # We could boostrap the database here just to make sure, but that might lead
    # to locking issues if multiple importers run concurrently.
    #@bootstrap
    @check_import_metadata
    @commit
    @save_import_metadata
    def run(self):
        for key, operation in self.operations.items():
            logger('info', operation['info'].format(
                name=self.config.name,
                collection=self.config.collection) + '...'
                )

            # Set default parameters if needed
            if 'params' not in operation:
                operation['params'] = {
                        'srid'  : srid,
                        'e_type': self.config.collection,
                        }

            # Operation presets
            #if key == 'pivot_event' or key == 'pivot_event_territory':
            #    operation['info'] = 'Installing the new {collection} event table into the event partition'
            #    operation['sql']  = open(self.sql_global + "pivot_partitions.sql", "r").read(),
            #    operation['params'] {
            #        'parent_table': 'event' if key == 'pivot_event' else 'event_territory',
            #        'e_type'      : self.config.collection,
            #        },
            #    }

            try:
                if key == 'carbon_emissions':
                    for i in range(1, len(self.classes) + 1):
                        logger('info', 'Processing class ' + self.classes[i]['name'] + '...')

                        c_factor = self.classes[i]['emission_factor']

                        self.cur.execute(
                                operation['sql'].format(
                                    srid=srid,
                                    e_type=self.config.collection,
                                    class_1=i,
                                    c_factor=c_factor,
                                    )
                                )
                else:
                    self.cur.execute(operation['sql'].format(**operation['params']))

                # Post processing
                if 'pivot_partition' in operation:
                    logger('info',
                            'Installing the new ' + self.config.collection + \
                                    ' table into the ' + operation['pivot_partition'] + \
                                    ' partition')

                    sql = open(self.sql_global + "pivot_partitions.sql", "r").read()

                    self.cur.execute(
                            sql.format(
                                e_type       = self.config.collection,
                                parent_table = operation['pivot_partition'],
                                )
                            )

            except Error as e:
                logger('exception', e)
                self.close()
                return False

            logger('info', "Done.")

    @teardown
    def close(self):
        return
