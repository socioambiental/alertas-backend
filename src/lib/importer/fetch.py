#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Fetch importer class.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import requests
import datetime
import re
from tqdm                    import tqdm
from lib.params              import imports_path
from lib.logger              import logger
from lib.importer.base       import BaseImporter, lock
from lib.metadata.decorators import check_import_metadata, save_import_metadata

class FetchImporter(BaseImporter):
    """Base class to fetch a dataset from a remote URL."""

    def __init__(self, config, args = {}):
        """
        Class initialization.

        Instantiate class properties and run basic checks.

        :param config module: Configuration in python module format.
        :param args   dict:   Configuration arguments.
        """

        super().__init__(config, args)

        # Configure base path from downloads and variable to hold output information
        self.output      = {}
        self.output_base = os.path.join(imports_path, self.config.name.lower(),
                                        'packaged') + os.sep

    def build_url(self, dataset):
        """Determine download url from base_url and dataset name"""

        if 'download_id' in dir(self.config):
            url = self.config.base_url.format(dataset=dataset, download_id=self.config.download_id)
        else:
            url = self.config.base_url.format(dataset=dataset)

        return url

    def process_headers_info(self, dataset, r):
        """
        Process HTTP Headers from a dataset request,
        setting appropriate file name and date.

        :param dataset str: Dataset name.
        :param r object: Requests object from dataset endpoint.
        """

        # Set the downloaded filename path prefix
        self.output[dataset] = os.path.join(self.output_base, dataset) + os.sep

        # Try to determine the file name by the content disposition in the
        # response headers, falling back to use just the dataset name and current date.
        #
        # Some datasets has fixed URL and return the filename (and dataset date) in
        # the content disposition header.
        #
        # Fallback to setting date via datetime.now() and using it to set the filename
        # if a download_id is not available.
        if 'Content-disposition' in r.headers and hasattr(self.config, 'date_pattern'):
            # Get filename
            self.output[dataset] += re.findall("filename=(.+)",
                                               r.headers['Content-disposition'])[0]

            # Get date from filename
            basename      = os.path.splitext(os.path.basename(self.output[dataset]))[0]
            date          = basename.replace(self.config.base_prefix + dataset + self.config.base_suffix, '')
            date          = re.sub(self.config.date_pattern, r"\1-\2-\3", date)
            date_split    = date.split('-')
            date_split[1] = self.config.months[date_split[1].title()]
            self.id       = ''.join(date_split)
        else:
            date = datetime.datetime.now()
            date = date.strftime('%Y%m%d')

            if 'download_id' in dir(self.config):
                self.id = str(self.config.download_id)
            else:
                self.id = date

            if 'filename' in dir(self.config):
                self.output[dataset] += self.config.filename.format(
                        download_id = self.id,
                        dataset     = dataset,
                        date        = date,
                        )
            else:
                self.output[dataset] += self.config.base_prefix + dataset + '-'

                if 'extension' in dir(self.config):
                    extension = self.config.extension
                else:
                    extension = 'zip'

                self.output[dataset] += self.id + '.' + extension

    def check_download(self, dataset, url = None):
        base_file = None

        if dataset in self.output:
            base_file = self.output[dataset]

        if url:
            base_file = os.path.join(self.output_base, dataset, os.path.basename(url))

        if base_file is not None:
            if os.path.exists(base_file):
                logger('info', 'Dataset ' + dataset + ' already downloaded as file ' + \
                      os.path.normpath(base_file) + '.')

                return True

        return False

    def get(self, dataset):
        """
        Download a single dataset.

        :param dataset str: Dataset name.
        """

        # Determine download url from base_url and dataset name
        url = self.build_url(dataset)

        if url is False:
            return False

        # Check if the dataset was already downloaded using the initial filename.
        #
        # This initial check prevents even to begin a download in the case that
        # there's a file matching the initial output filename.
        if self.check_download(dataset, url):
            return

        info = 'Downloading dataset ' + dataset + ' from ' +  url + '...'

        # If there's no downloaded file matching the initial output name, try to
        # get it from upstream, re-setting the output filename from headers
        # if possible.
        try:
            s = requests.Session()

            # Defer downloading using stream=True headers can be checked
            # before deciding if the whole content should be downloaded.
            #
            # https://2.python-requests.org/en/master/user/advanced/
            with s.get(url, stream=True) as r:
                # Raise exceptions when they happen
                r.raise_for_status()

                # Set ID and re-set filename from headers in the case upstream
                # use different naming scheme.
                self.process_headers_info(dataset, r)

                # Check if the dataset was already downloaded
                if self.check_download(dataset):
                    return

                # Determine response size and chunks
                part       = self.output[dataset] + '.part'
                dirname    = os.path.dirname(part)
                chunk_size = 8192
                max        = int(r.headers['content-length']) // chunk_size \
                             if 'content-length' in r.headers else None

                if not os.path.exists(dirname):
                    try:
                        os.makedirs(dirname)
                    except Exception as e:
                        logger('exception', e)
                        return False

                with open(part, 'wb') as f:
                    # Progress bar handling
                    # Just prints a simple message if no progress bar is available
                    if self.progress != True:
                        progress_bar = False

                        logger('info', info)
                    else:
                        progress_bar = tqdm(total=max, unit='B',
                                       unit_scale=True, desc=info) if self.progress else False

                    # Download and write the full content at once, might be not informative
                    # to the user or have a performance impact when the dataset is huge.
                    #f.write(r.content)

                    # Download and write in chunks. Allows setting up a progress bar and saves memory
                    # if the dataset is huge.
                    #
                    # See https://2.python-requests.org/en/master/user/advanced/#body-content-workflow
                    #     https://stackoverflow.com/questions/16694907/download-large-file-in-python-with-requests#16696317
                    #     https://jdhao.github.io/2020/06/17/download_image_from_url_python/
                    #     https://gist.github.com/wy193777/0e2a4932e81afc6aa4c8f7a2984f34e2
                    for chunk in r.iter_content(chunk_size=chunk_size):
                        f.write(chunk)

                        if hasattr(progress_bar, 'update'):
                            progress_bar.update(chunk_size)

                    # Teardown the progress bar
                    if hasattr(progress_bar, 'close'):
                        progress_bar.close()

                    try:
                        f.close()
                        os.rename(part, self.output[dataset])
                    except OSError as e:
                        logger('exception', e)
                        return False
                        #exit(2)

        except requests.ConnectionError as e:
            logger('exception', e)
            return False
            #exit(1)

        except requests.exceptions.HTTPError as e:
            logger('exception', e)
            return False
            #exit(1)

        except requests.exceptions.Timeout as e:
            logger('exception', e)
            return False
            #exit(1)

        except requests.exceptions.TooManyRedirects as e:
            logger('exception', e)
            return False
            #exit(1)

    def dispatch(self, dataset):
        """
        Dispatch downloading of a single dataset.

        :param dataset str: Dataset name.
        """

        if self.get(dataset) is False:
            return False

    # Using metadata decorators in a process_metadata() method for the extract
    # phase and not in run(), since for it to be possible an import_id needs to
    # be determined, something that depending on the importer only happens
    # DURING the get() procedure.
    @check_import_metadata
    @save_import_metadata
    def process_metadata(self):
        pass

    @lock
    # Currently not using metadata decorators in the run() method for the
    # extract phase, since for it to be possible an import_id needs to be
    # determined, something that depending on the importer only happens DURING
    # the get() procedure.
    #@check_import_metadata
    #@save_import_metadata
    def run(self):
        """Dispatch all selected datasets."""

        for dataset in self.config.datasets:
            if self.dispatch(dataset) is False:
                return False

        # Save metadata afterwards, as now self.id is set
        self.process_metadata()
