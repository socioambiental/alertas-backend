#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Base importer class.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import datetime
from sys        import stdout
from lib.params import imports_path
from lib.base   import Base, lock, check_dependencies

class BaseImporter(Base):
    def __init__(self, config, args = {}):
        """
        Class initialization.

        Instantiate class properties and run basic checks.

        :param config dict: Importer configuration.

        :param args   dict: Invocation arguments.

        """

        super().__init__(config, args)

        # Set the lock prefix
        self.lock_prefix = 'importer'

        # Save config
        self.config = config

        # Save arguments
        self.args = args

        # Configure type
        if 'type' in dir(config):
            self.type = config.type
        else:
            self.type = 'event'

        # Configure progress bar
        if stdout.isatty():
            if 'progress' in args and args['progress'] != None:
                self.progress = args['progress']
            else:
                self.progress = True
        else:
            # Disable progress bar if stdout is not TTY-compatible
            self.progress = False

    @staticmethod
    def get_extracted_path(name):
        return os.path.join(imports_path, name, 'extracted')

    @staticmethod
    def get_shape_ids(name):
        """Return the list of IDs which have available datasets."""

        extracted_path = BaseImporter.get_extracted_path(name)

        if os.path.isdir(extracted_path):
            return sorted([c for c in os.listdir(extracted_path) if c != '.gitignore'], reverse=True)
        else:
            return list()

    #def close(self):
    #    self.release_lockfile()

def set_import_id(init):
    """
    Decorator to set self.id into the importer class, useful for load and
    transform stages.

    """

    def decorated(self, *args, **kwargs):
        # Initialize
        init(self, *args, **kwargs)

        # Configure ID
        if 'id' in self.args and self.args['id'] != None:
            self.id = str(self.args['id'])
        else:
            # The ID is set in the configuration?
            if 'download_id' in dir(self.config):
                self.id = str(self.config.download_id)
            else:
                # The ID can be guessed using the date?
                if 'date' in self.args and self.args['date'] != None:
                    self.id = self.args['date']
                else:
                    # The ID can be guessed by the extracted data?
                    # First test for self.get_shape_ids as this decorator might be reused
                    # somewhere else like in the BaseExporter
                    ids     = self.get_shape_ids(self.config.collection) if 'get_shape_ids' in dir(self) else list()
                    self.id = ids[0] if len(ids) > 0 and ids[0] != None else None

                    if self.id is None:
                        # Fallback: set the current date to be used during metadata check/save
                        today   = datetime.datetime.today()
                        self.id = today.strftime('%Y%m%d')

    return decorated
