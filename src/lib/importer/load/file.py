#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Bzip2 dataset extractor.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import subprocess
from lib.importer.base       import BaseImporter, lock, set_import_id
from lib.metadata.decorators import check_import_metadata, save_import_metadata
from lib.params              import imports_path, bin_path, config_path
from lib.logger              import logger

class FileLoad(BaseImporter):
    #@set_import_id
    def __init__(self, config, args = {}):
        super().__init__(config, args)

        self.id    = str(self.config.download_id)
        self.phase = 'load'

    @lock
    @check_import_metadata
    @save_import_metadata
    def run(self):
        loader = os.path.join(bin_path,    'sql-table-loader')
        pgpass = os.path.join(config_path, 'pgpass')

        for dataset in self.config.datasets:
            logger('info', 'Processing dataset ' + dataset + '...')

            filename = self.config.filename.format(
                    download_id = self.config.download_id,
                    dataset     = dataset,
                    )

            path = os.path.join(
                    imports_path, self.config.name, 'packaged', dataset, filename
                    )

            if not os.path.exists(path):
                logger('error', 'Missing file ' + path)

                return False

            if not os.path.exists(pgpass):
                logger('error', 'Missing pgpass configuration at ' + pgpass)

                return False

            command = loader + ' ' + path + ' ' + self.table.format(
                    dataset = dataset,
                    )

            try:
                subprocess.run(
                        command,
                        check=True,
                        shell=True
                        )

            except subprocess.CalledProcessError as e:
                logger('exception', e)
                return False
