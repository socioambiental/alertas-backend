#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Shapefile dataset loader using gdal's ogr.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import datetime
from osgeo                       import ogr
from psycopg2                    import Error
from lib.config                  import config
from lib.logger                  import logger
from lib.importer.load.shapefile import ShapefileLoad

# See https://gdal.org/api/python_gotchas.html
ogr.UseExceptions()

class OgrLoad(ShapefileLoad):
    """Process a shapefile using OSGeo's ogr."""

    def __init__(self, config, args = {}):
        """
        Class initialization.

        Instantiate class properties and run basic checks.

        :param args dict: Configuration arguments.
        """

        super().__init__(config, args)

        # Setup database engine
        self.start_server()

        # Configure shape opener
        self.shape_opener = ogr.Open

    def start_server(self):
        """Start a database server."""

        db_params = {
                'host'   : config['main_db']['host'],
                'port'   : config['main_db']['port'],
                'user'   : config['main_db']['user'],
                'passwd' : config['main_db']['password'],
                'db'     : config['main_db']['db'],
                #'echo'  : True,
                }

        connectionString = "PG:dbname={db} host={host} port={port} user={user} password={passwd}".format(**db_params)
        self.server      = ogr.Open(connectionString)

    def truncate_table(self):
        """
        Delete canonical data.
        """

        logger('info', "Deleting canonical data...")

        # The 'truncate_old_data' setting means that the current canonical table should
        # be renamed away so it can be used to recover data that is newer than those
        # available in the current package.
        #
        # This is useful in cases where an event type is fed by two importers, like
        # a daily near-real time data package and a monthly "science ready" pack with
        # corrected data.
        #
        # To avoid any gaps between the science ready and NRT packages, the 'old_data_only'
        # mode ensures that no data newer than the one included in the science ready package
        # should be destroyed.
        if self.update_mode == 'truncate_old_data':
            statement  = "SET search_path to event_canonical,public;"
            statement += "DROP  TABLE IF EXISTS event_canonical.{collection}_canonical_old;"
            statement += "DROP  INDEX IF EXISTS idx_{collection}_canonical_geom_old;"
            statement += "ALTER TABLE IF EXISTS event_canonical.{collection}_canonical RENAME TO {collection}_canonical_old;"
            statement += "ALTER INDEX IF EXISTS idx_{collection}_canonical_geom RENAME TO idx_{collection}_canonical_geom_old;"

        else:
            statement = "DROP TABLE IF EXISTS event_canonical.{collection}_canonical;"

        try:
            self.server.ExecuteSQL(
                    statement.format(
                        collection=self.config.collection
                        )
                    )
        except Error as e:
            logger('exception', e)
            self.close_shapefile()
            return False

        # Create the table again if there's an available model
        if 'model' in dir(self) and callable(self.model):
            model = self.model(self.config, self.args)

        logger('info', "Done.")

    def add_records(self, dataset, shape_item):
        """Copy shape to the database"""

        sourceLayer = self.shape.GetLayerByIndex(0)
        options     = ["GEOMETRY_NAME=geom", "OVERWRITE=YES"]

        logger('info', "Loading {collection} canonical table with arquive data...".format(
                collection=self.config.collection
                ))

        # Import the shape into the database using it's default name
        self.server.CopyLayer(sourceLayer, self.shape_filename, options)

        # Then load a custom add_records SQL procedure
        query = open(self.sql_local + "add_records.sql", "r").read()

        # In case the shape filename has an hifen that is converted to an
        # underline during table import
        input_table = self.shape_filename.replace('-', '_').lower()

        try:
            self.server.ExecuteSQL(
                    query.format(
                        input_table=input_table,
                        e_type=self.config.collection
                        )
                    )
        except Error as e:
            logger('exception', e)
            self.close()
            return False

        logger('info', "Done.")

    def get_latest_date(self, table, date_field):
        """ Get the latest date in a table """

        try:
            result = self.server.ExecuteSQL(
                    'SELECT DISTINCT {date_field} FROM {table} ORDER BY {date_field} DESC LIMIT 1;'.format(
                        table=table,
                        date_field=date_field,
                        )
                    )

            row    = result.GetFeature(0)
            latest = row.GetFieldAsDateTime(date_field)

            # Destroy the result set
            self.server.ReleaseResultSet(result)
        except Error as e:
            logger('exception', e)
            self.close_shapefile()
            return False

        if len(latest) >= 3:
            return datetime.date(latest[0], latest[1], latest[2])

    def add_newer_records(self):
        """
        Add events newer than the imported records into the canonical table


        The science-ready datasets might be old enough that daily runs for the
        non-arquive importer inserted newer events not present in the current
        Arquive package.
        """

        input_table = 'event_canonical.' + self.config.collection + '_canonical'

        # Get the latest date
        date_field = self.config.date_field
        latest     = self.get_latest_date(input_table, date_field)#.strftime('Y%-%m-%d')

        if latest is False:
            return False

        logger('info', "Adding events newer than the imported records into the canonical table...")
        logger('info', "Latest date in the imported data is {latest}.".format(latest=latest))

        query = open(self.sql_local + "newer.sql", "r").read()

        try:
            self.server.ExecuteSQL(
                    query.format(
                        e_type=self.config.collection,
                        date_field=date_field,
                        latest=latest,
                        )
                    )
        except Error as e:
            logger('exception', e)

            self.close()
            return False

        logger('info', "Done.")

    def close(self):
        # See https://gis.stackexchange.com/questions/80366/why-close-a-dataset-in-gdal-python
        self.server = None
