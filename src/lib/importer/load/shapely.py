#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Shapely dataset loader.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import datetime
import shapefile
from tqdm                        import tqdm
from lib.config                  import config
from lib.logger                  import logger
from lib.importer.load.shapefile import ShapefileLoad
from lib.database.sqlalchemy     import create_engine, start_session

class ShapelyLoad(ShapefileLoad):
    """Process a shapefile using the Shapely's shapefile."""

    def __init__(self, config, args = {}):
        super().__init__(config, args)

        # Setup database engine
        self.engine = create_engine()

        # Setup data model and database session
        self.model   = self.create_model()
        self.session = start_session(self.engine)

        # Configure shape opener
        self.shape_opener = shapefile.Reader

    def create_model(self):
        """
        Create the database model.

        Class descendants should implement this method.
        """

        return None

    def truncate_table(self):
        """
        Truncate a database table.

        :param session object: Database session.
        :param table   object: Database table (sqlalchemy Model).
        """

        session  = self.session
        table    = self.model

        if self.update_mode == 'truncate':
            # Delete any existing, previous content
            logger('info', 'Truncating canonical table...')
            session.query(table).delete()

        elif self.update_mode == 'truncate_dataset_year':
            # Truncate data only from the current dataset's year
            #
            # Useful when sequential daily runs from the same year needs to
            # remove previous data from the same year while preserving data
            # from past years
            year      = self.get_year()
            new_year  = datetime.date.fromisoformat(str(year)     + '-01-01')
            next_year = datetime.date.fromisoformat(str(year + 1) + '-01-01')

            logger('info', 'Truncating canonical table but only for events from ' \
                    + str(new_year) + ' until ' + str(next_year) + '...')

            # Delete any existing, previous content from a given year
            session.query(table).filter(
                    table.start_date >= new_year,
                    table.start_date <  next_year).delete()

        session.commit()

    def get_year(self):
        """
        Get the current dataset year from the dataset ID, assuming
        it refers to a date.
        """

        year = int(self.id[0:4])

        return year

    def get_total_records(self):
        return len(self.shape.shapeRecords())

    def add_record(self, record, session, Model):
        """
        Add a single shape record into the database.

        Class descendants should implement this method.

        :param record  object: Shapefile record (pyshp).
        :param session object: Database session.
        :param Model   object: Model.

        Records can be debuged as follows:

        print(record)
        print(record.record)
        print(record.shape)
        print(record.shape.__geo_interface__['type'])
        """

        return

    def add_records(self, dataset, shape_item):
        """Include all records from a dataset into the database."""

        if self.shape == None:
            #raise ValueError('Shape data not available.')
            logger('error', 'Shape data not available.')
            return False

        # Setup database engine, data model, metadata model and database session
        Model   = self.model
        session = self.session

        total = self.get_total_records()

        # Define the number of records to be processed
        if self.max_records:
            max = total if int(self.max_records) > total else int(self.max_records)
        else:
            max = total

        # Progress handling
        progress_bar = tqdm(total=max) if self.progress and max > 1 else False
        current      = 0

        # Iterate over all records in the dataset
        for record in self.shape.iterShapeRecords():
            current += 1

            if current > max:
                break

            self.add_record(record, session, Model)

            if hasattr(progress_bar, 'update'):
                progress_bar.update(1)

        # Write transaction into the database
        session.commit()

        # Teardown the progress bar
        if hasattr(progress_bar, 'close'):
            progress_bar.close()

    def close(self):
        self.engine.close()
