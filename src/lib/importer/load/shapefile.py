#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Shapefile dataset loader.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import argparse
from datetime                         import datetime
from lib.params                       import copyright_notice
from lib.logger                       import logger
from lib.importer.base                import BaseImporter, lock, check_dependencies, set_import_id
from lib.database.decorators.psycopg2 import bootstrap
from lib.metadata.decorators          import check_import_metadata, save_import_metadata

class ShapefileLoad(BaseImporter):
    """
    Class with common methods for shapefile handling.

    """

    @set_import_id
    def __init__(self, config, args = {}):
        super().__init__(config, args)

        self.shape_file = None
        self.shape      = None

        # Configure and check dataset
        if 'dataset' in args and args['dataset'] != None:
            if args.dataset in self.config.datasets:
                self.dataset = args['dataset']
            else:
                raise ValueError('Invalid dataset ' + args.dataset)
                return False
        else:
            self.dataset = None

        # Configure max number of records to be processed on each dataset
        if 'max_records' in args and args['max_records'] != None:
            self.max_records = args['max_records']
        else:
            self.max_records = None

        # Update mode
        if 'update_mode' in dir(self.config):
            self.update_mode = self.config.update_mode
        else:
            self.update_mode = 'truncate'

        # Set phase, used during metadata check/save
        self.phase = 'load'

    def open_shapefile(self, dataset, shape_item):
        """
        Open the dataset as a shape.

        :param dataset str: Dataset name.

        :param shape_item str: shape suffix.
        """

        # Apply format substitutions into the shape suffix
        shape_item_fmt = shape_item.format(id=self.id)

        # Determine shapefile and check it's existence
        if self.id != None:
            extracted_path = BaseImporter.get_extracted_path(self.config.name)
            shape_file     = os.path.join(extracted_path, self.id, dataset, shape_item_fmt)
            shape_filename = os.path.splitext(shape_item_fmt)[0]

            if os.path.isfile(shape_file):
               self.shape_file     = shape_file
               self.shape_filename = shape_filename

        # Try to open the shapefile
        if self.shape_file != None:
            #with shapefile.Reader(self.shape_file) as shp:
            #    self.shape = shp
            self.shape = self.shape_opener(self.shape_file)
        else:
            #raise ValueError('Shape file not found.')
            logger('error', 'Shape file not found.')
            return False

    def close_shapefile(self):
        """Close the shapefile."""

        if self.shape != None:
            if 'close' in dir(self.shape) and callable(self.shape.close):
                self.shape.close()

            self.shape = None

        self.shape_file = None

    def dispatch(self, dataset):
        """
        Dispatch inclusion of a single dataset.

        :param dataset str: Dataset name.
        """

        total_shapes = str(len(self.config.shapefiles))

        for shape_item in self.config.shapefiles:
            actual_shape = str(self.config.shapefiles.index(shape_item) + 1)

            if len(self.config.shapefiles) > 1:
                logger('info', 'Processing dataset ' + \
                        dataset + ' with ID '  + \
                        self.id + ' (' + actual_shape + ' of ' + total_shapes + ') ...')
            else:
                logger('info', 'Processing dataset ' + \
                        dataset + ' with ID ' + self.id + ' ...')

            if self.open_shapefile(dataset, shape_item) != False:
                result = self.add_records(dataset, shape_item)
                self.close_shapefile()

                if result is False:
                    return False
            else:
                return False

    @lock
    @check_dependencies
    # We could boostrap the database here just to make sure, but that might lead
    # to locking issues if multiple importers run concurrently.
    #@bootstrap
    @check_import_metadata
    @save_import_metadata
    def run(self):
        """Dispatch all selected self.config.datasets."""

        # Optionally ensure the database is clean before adding records
        if self.update_mode != 'accumulate':
            self.truncate_table()

        if self.dataset != None:
            result = self.dispatch(self.dataset)

            if result is False:
                return False
        else:
            for dataset in self.config.datasets:
                result = self.dispatch(dataset)

                if result is False:
                    return False

        # Ensure the dataset has newer records not present in any of the imported shapefiles
        #
        # The 'truncate_old_data' setting means that the current canonical table should
        # be renamed away so it can be used to recover data that is newer than those
        # available in the current package.
        #
        # This is useful in cases where an event type is fed by two importers, like
        # a daily near-real time data package and a monthly "science ready" pack with
        # corrected data.
        #
        # To avoid any gaps between the science ready and NRT packages, the 'old_data_only'
        # mode ensures that no data newer than the one included in the science ready package
        # should be destroyed.
        if self.update_mode == 'truncate_old_data':
            self.add_newer_records();
