#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Failure importer.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import random

class FailureImporter:
    """
    An importer class that fails randomly, useful for testing.
    """
    def __init__(self, config, args = {}):
        return

    def run(self):
        """
        Fail randomly.

        Thanks https://stackoverflow.com/questions/6824681/get-a-random-boolean-in-python#6824868
        """
        return bool(random.getrandbits(1))
