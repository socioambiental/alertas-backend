#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# File dataset extractor.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
#from lib.importer.base       import lock
from lib.importer.base        import set_import_id
from lib.importer.fetch       import FetchImporter
#from lib.metadata.decorators import check_import_metadata, save_import_metadata
#from lib.params              import imports_path
from lib.logger               import logger

class FileExtract(FetchImporter):
    @set_import_id
    def __init__(self, config, args = {}):
        super().__init__(config, args)

        self.phase = 'extract'

    # Old scaffolding code
    #@lock
    #@check_import_metadata
    #@save_import_metadata
    #def run(self):
    #    filename = self.config.filename.format(
    #            download_id = self.config.download_id,
    #            )

    #    for dataset in self.config.datasets:
    #        path = os.path.join(
    #                imports_path, self.config.name, 'packaged', dataset, filename
    #                )

    #        if not os.path.exists(path):
    #            logger('error', 'File not found: {filename}. Please place it under {path}').format(
    #                    filename = filename,
    #                    path     = path,
    #                    )

    #            return False
