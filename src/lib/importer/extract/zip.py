#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Base dataset extractor.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import shutil
import zipfile
#from sys                    import exit
from lib.params              import imports_path
from lib.logger              import logger
from lib.importer.base       import lock
from lib.importer.fetch      import FetchImporter
from lib.metadata.decorators import check_import_metadata, save_import_metadata

class ZipExtract(FetchImporter):
    """Base class to extract a dataset from a remote zipfile."""

    def __init__(self, config, args = {}):
        """
        Class initialization.

        Instantiate class properties and run basic checks.

        :param config module: Configuration in python module format.
        :param args   dict:   Configuration arguments.
        """

        super().__init__(config, args)

        # Set phase, used during metadata check/save
        self.phase = 'extract'

    # Using metadata decorators in the extract() method for the extract phase
    # and not in run(), since for it to be possible an import_id needs to be
    # determined, something that depending on the importer only happens DURING
    # the get() procedure.
    @check_import_metadata
    @save_import_metadata
    def extract(self, dataset):
        """
        Extract a given dataset.

        :param dataset str: Dataset name.
        """

        # Check if file already downloaded
        if not os.path.exists(self.output[dataset]):
            #raise FileNotFoundError('File not found: ' + self.output[dataset] + \
            #                        '. Did you downloaed it in the first place?')
            #exit(1)

            logger('error', 'File not found: ' + self.output[dataset] + \
                            '. Did you download it in the first place?')

            return False

        # Check if an ID is set, meaning that something was downloaded
        if self.id == None:
            #raise ValueError('Cannot find an ID for the downloaded dataset. ' + \
            #                 'Did the downloaded suceed?')

            logger('error', 'Cannot find an ID for the downloaded dataset. ' + \
                            'Did the download suceed?')

            return False

        # Try to extract dataset contents from zipfile
        try:
            extracted_path = os.path.join(imports_path, self.config.name.lower(), 'extracted')
            shape_path     = os.path.join(extracted_path, self.id, dataset)
            zipped         = zipfile.ZipFile(self.output[dataset])

            # Remove any previous extracted folder to avoid any dangling file
            # from previous extractions
            if os.path.isdir(shape_path):
                logger('info', 'Removing existing destination folder ' + \
                        shape_path + ' before extracting...')
                shutil.rmtree(shape_path)

            info = 'Extracting dataset ' + dataset + ' zipfile into ' + \
                    os.path.normpath(shape_path) + '...'

            logger('info', info)

            if not os.path.isdir(shape_path):
                os.makedirs(shape_path)

            zipped.extractall(shape_path)

        except zipfile.BadZipFile as e:
            # Remove the zipfile, otherwise in the last run it won't be
            # downloaded again, assuming that it was temporary corrupt in
            # transit or due to some upstream issue.
            os.remove(self.output[dataset])

            logger('exception', e)
            return False
            #exit(1)

    def dispatch(self, dataset):
        """
        Dispatch downloading of a single dataset.

        :param dataset str: Dataset name.
        """

        if self.get(dataset) is False:
            return False

        if self.extract(dataset) is False:
            return False

    @lock
    # Currently not using metadata decorators in the run() method for the
    # extract phase, since for it to be possible an import_id needs to be
    # determined, something that depending on the importer only happens DURING
    # the get() procedure.
    #@check_import_metadata
    #@save_import_metadata
    def run(self):
        """Dispatch all selected datasets."""

        for dataset in self.config.datasets:
            if self.dispatch(dataset) is False:
                return False
