#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Dataset base exporter.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import subprocess
import datetime
from lib.metadata.exports import ExportMetadataManager
from lib.params           import bin_path
from lib.logger           import logger
from lib.base             import Base, lock, check_dependencies
from lib.cache.manager    import clear
#from lib.metadata.info   import clear_metadata_cache

# Use set_import_id to set the export ID using the same algorithm
from lib.importer.base import set_import_id as set_export_id

class BaseExporter(Base):
    @set_export_id
    def __init__(self, config, args = {}):
        """
        Class initialization.

        Instantiate class properties and run basic checks.

        :param config dict: Importer configuration.

        :param args   dict: Invocation arguments.

        """

        super().__init__(config, args)

        # Save config
        self.config = config

        # Save arguments
        self.args = args

        # Set the lock prefix
        self.lock_prefix = 'exporter'

        # Instantiate metadata manager
        self.metadata = ExportMetadataManager()

        # Operations
        self.operations = {}

    @lock
    @check_dependencies
    def run(self):
        exporter_script = os.path.join(bin_path, 'exporter')

        if 'archive' not in dir(self.config) or self.config.archive['redistribute' ] is False:
            logger('info', 'Exporter ' + self.config.name + \
                   ' configured as non-redistributable, skipping export.')

            # Do not return False as this is not a fatal error
            #return False
            return

        for operation in self.operations:
            logger('info', self.operations[operation]['info'] + '...')

            # Check metadata
            check = self.metadata.check(self.id, operation, self.config, self.operations)

            if check == 'already_processed' and not self.args['force']:
                logger('info', 'Skipping operation.')

                continue

            # For operations using an external exporter script
            if self.operations[operation]['type'] in [ 'shapefile', 'schema', 'table', 'geotiff' ]:
                # Build command line
                command = exporter_script + ' ' + self.operations[operation]['type']   + ' '  + \
                                                  self.operations[operation]['prefix'] + ' '  + \
                                                  self.id             + ' "' + \
                                                  self.operations[operation]['params'] + '"'

                # Try to execute it
                try:
                    subprocess.run(
                            command,
                            check=True,
                            shell=True
                            )

                # Error handling
                except subprocess.CalledProcessError as e:
                    logger('exception', e)
                    return False

                # Save metadata
                self.metadata.upsert(self.id, operation, self.config, self.operations)

        # Cleanup metadata cache, ensuring that the API will refresh this
        # information automatically upon the next request
        #logger('info', 'Clearing metadata cache...')
        #clear_metadata_cache()

        # Clear all caches after an export has concluded
        #logger('info', 'Clearing all caches...')
        #clear()

        # Clear the misc cache after an export has concluded
        logger('info', 'Clearing the misc cache...')
        clear([ 'misc' ])
