#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# API cache refresh.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from importlib           import import_module
from datetime            import datetime, timedelta, date
from lib.logger          import logger
from lib.utils           import to_camel_case
from lib.metadata.events import EventMetadataManager

class RefreshApi():
    def __init__(self):

        #
        # Event metadata
        #

        metadata_event  = EventMetadataManager()
        metadata_events = metadata_event.all()

        #
        # Intervals
        #

        intervals = {}

        month = timedelta(days=30)
        year  = timedelta(days=365)

        for event in metadata_events:
            newest        = date.fromisoformat(metadata_events[event]['newest_event'])
            month_initial = (newest - month).isoformat()
            year_initial  = (newest - year).isoformat()

            intervals[event] = {
                    'month': {
                        'initial': month_initial,
                        'final'  : newest.isoformat(),
                        },
                    'year': {
                        'initial': year_initial,
                        'final'  : newest.isoformat(),
                        },
                    }

        # Make sure to use True/False and not integers for boolean fields
        actions = []

        # Landing page contents
        for event in [ 'amazon_dashboard', 'deter' ]:
            for action in [ 'outside', 'totals' ]:
                for interval in [ 'year', 'month' ]:
                    for unit in [ 'km2',  'trees' ]:
                        for event_class in range(len(metadata_events[event]['classes']) + 1):
                            actions.append({
                                'action': action,
                                'params': {
                                    'controls'             : True,
                                    'event_type'           : event,
                                    'territory_types'      : [ 'TI', 'UCF', 'UCE', 'APAF', 'APAE', ],
                                    'buffers'              : True,
                                    'event_class'          : event_class,
                                    'relative_dates'       : False,
                                    'date_initial'         : intervals[event][interval]['initial'],
                                    'date_final'           : intervals[event][interval]['final'],
                                    'temporal_unit'        : 'month',
                                    #'geojson'             : False,
                                    'group_by'             : [ 'territory_type' ],
                                    'compute_emissions'    : False,
                                    'smart_area'           : True,
                                    'max_items'            : 20,
                                    'output_format'        : 'table',
                                    'cummulative'          : False,
                                    'area_unit'            : unit,
                                    'carbon_emission_unit' : 'mgceq',
                                    'increase'             : True,
                                    'rate'                 : False,
                                    'territory_area'       : False,
                                    },
                                })

        # Add default action calls
        # This tests all API calls with default args
        # And also helps the API to answer fast when actions are called without args
        for action in [ 'meta', 'outside', 'totals', 'events', 'ranking', 'series' ]:
            actions.append({ 'action': action, 'params': {} })

        self.actions = actions

    def run(self):
        init_time = datetime.now()

        for item in self.actions:
            action = item['action']
            params = item['params']

            logger('info',
                    'Running API action {action} with params {params}...'.format(
                        action=action,
                        params=params
                        )
                    )

            # Instantiate the modules' action
            module_name  = 'lib.api.actions.' + action
            module       = import_module(module_name)
            module_class = getattr(module, to_camel_case('get_' + action))
            instance     = module_class()

            # Set cache
            instance.cache_strategy = 'set'

            # Run
            instance.getter(params)

        elapsed = str((datetime.now() - init_time))

        logger('info', 'Elapsed time: ' + elapsed)
