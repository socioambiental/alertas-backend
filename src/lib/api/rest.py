#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# REST functions.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import requests
import lib.cache.manager

def fetch(url, json = True):
    """Fetch data from an URL"""

    try:
        with requests.get(url) as r:
            r.raise_for_status()

    except requests.ConnectionError as e:
        print(e)
        #exit(1)
        return False

    except requests.exceptions.HTTPError as e:
        print(e)
        #exit(1)
        return False

    except requests.exceptions.Timeout as e:
        print(e)
        #exit(1)
        return False

    except requests.exceptions.TooManyRedirects as e:
        print(e)
        #exit(1)
        return False

    if json:
        return r.json()

    return r

class RestClient:
    """Interaction with an upstream API"""

    def get(self, action = 'list', *args, **kwargs):
        """Get data from the news API"""

        if hasattr(self, action):
            creator = getattr(self, action)
            key     = lib.cache.manager.key('misc', creator, *args, **kwargs)

            if not callable(creator):
                return
        else:
            return

        #return lib.cache.manager.get('misc', creator, *args, **kwargs)
        return lib.cache.regions.regions['misc'].get_or_create(key=key, creator=creator, creator_args=(args, kwargs), should_cache_fn=self.should_cache)

    def should_cache(self, value):
        """Check if a retrieved value should be cached (eg. don't cache if upstream is offline or if there's a network error)"""

        if value is None or value is False:
            return False

        return True
