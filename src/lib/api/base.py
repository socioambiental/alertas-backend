#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Base API class.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from flask_restful import Resource
import lib.cache.manager

class BaseApi(Resource):
    def __init__(self):
        super().__init__()

        self.cache_strategy = 'get'

    def cache_get_or_set(self, *args, **kwargs):
        if self.cache_strategy == 'get':
            return lib.cache.manager.get(*args, **kwargs)
        else:
            return lib.cache.manager.set(*args, **kwargs)
