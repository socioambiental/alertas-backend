#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# API-related functions.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from docstring_parser import parse
from inspect          import signature
from webargs          import fields
from apispec          import APISpec

from lib.config       import config

# OpenAPI
version         = '1.0.0'
spec_definition = {
        'title'  : 'Alertas+ API',
        'version': version,
        'openapi_version': "3.0.2",
        'info': {
            'description'   : "Pressures/menaces event query system",
            'termsOfService': "https://alertas.socioambiental.org/about",
            'license': {
                'name': 'CC BY-SA 4.0',
                'url' : "https://creativecommons.org/licenses/by-sa/4.0/",
                },
            'contact': {
                'name' :"Equipe Alertas+",
                'url'  :"https://alertas.socioambiental.org",
                'email':"alertas@socioambiental.org",
                },
            },
        'security': {
            "apikey": [],
            },
        'components': {
            'securitySchemes': {
                'apikey': {
                    'type': 'apiKey',
                    'name': 'apikey',
                    'in'  : 'query',
                    },
                },
            },
        'externalDocs': {
              "description": "Alertas+ Documentation",
              "url"       : "https://alertas.socioambiental.org/doc"
            },
        }

# Set servers information
if 'apispec' in config and 'server' in config['apispec']:
    spec_definition['servers'] = [{
        'url'        : config['apispec']['server'],
        'description': config['apispec']['server_description'],
        }]

# Action params
docstrings = { }
signatures = { }
parameters = { }
webargs    = { }
operations = { }
tags       = [
        {
            'name'       : 'events',
            'description': 'Event querying',
            },
        {
            'name'       : 'territories',
            'description': 'Territory querying',
            },
        {
            'name'       : 'news',
            'description': 'News querying',
            },
        {
            'name'       : 'meta',
            'description': 'Metadata querying',
            },
        #{
        #    'name'       : '',
        #    'description': '',
        #    },
        ]

# Some common params
common_params = {
        #
        # Post-processing parameters.
        #

        'area_unit': {
            'name'        : 'area_unit',
            'description' : 'Area unit (ha, km2, soccer or trees)',
            'in'          : 'query',
            'schema'      : {
                'type'   : 'string',
                'default': 'ha',
                },
            },
        'carbon_emission_unit': {
            'name'        : 'carbon_emission_unit',
            'description' : 'Carbon emission unit (mgceq or mgco2eq)',
            'in'          : 'query',
            'schema'      : {
                'type'   : 'string',
                'default': 'mgceq',
                },
            },
        'output_format': {
            'name'        : 'output_format',
            'description' : 'Output format (table or chart)',
            'in'          : 'query',
            'schema'      : {
                'type'   : 'string',
                'default': 'table',
                },
            },
        'max_items': {
            'name'        : 'max_items',
            'description' : 'Maximum number of items',
            'in'          : 'query',
            'schema'      : {
                'type'   : 'int',
                'default': '20',
                },
            },
        'cummulative': {
            'name'        : 'cummulative',
            'description' : 'Whether series should be cummulative',
            'in'          : 'query',
            'schema'      : {
                'type'   : 'boolean',
                'default': False,
                }
            },
        'increase': {
            'name'        : 'increase',
            'description' : 'Whether result should include increase rate since last period. Will only report increases if the dataset coverage fully includes the last period.',
            'in'          : 'query',
            'schema'      : {
                'type'   : 'boolean',
                'default': False,
                }
            },
        'rate': {
            'name'        : 'rate',
            'description' : 'Whether result should include temporal rates',
            'in'          : 'query',
            'schema'      : {
                'type'   : 'boolean',
                'default': False,
                }
            },
        'rate_unit': {
            'name'        : 'rate_unit',
            'description' : 'The timescale to calculate rates; defaults the value of temporal_unit',
            'in'          : 'query',
            'schema'      : {
                'type'   : 'string',
                'default': 'day',
                }
            },
        'territory_area': {
            'name'        : 'territory_area',
            'description' : 'Whether result should include totals related to the aggregated area ofall territories that matches the selection',
            'in'          : 'query',
            'schema'      : {
                'type'   : 'boolean',
                'default': False,
                }
            },

        #
        # Other parameters that might be send by the frontend or by the API
        # gateway but currently are of no use for the backend. They're listed
        # here to they can be removed by strip_params()
        #

        'apikey': {
                'name': 'apikey',
                'schema'     : {
                    'type'   : 'string',
                    'default': '',
                    }
                },
        'controls': {
                'name': 'controls',
                'schema'     : {
                    'type'   : 'boolean',
                    'default': False,
                    }
                },
        'buffers': {
                'name': 'buffers',
                'schema'     : {
                    'type'   : 'boolean',
                    'default': False,
                    }
                },
        'relative_dates': {
                'name': 'relative_dates',
                'schema'     : {
                    'type'   : 'boolean',
                    'default': False,
                    }
                },
        }

# Spec
spec = APISpec(**spec_definition)
spec.tag(tags)

def build_webargs(action):
    """Define params' webargs according to the action's docstring"""

    webargs = {}

    # Build webargs using webargs API
    # See https://github.com/marshmallow-code/webargs
    #     https://github.com/marshmallow-code/webargs/blob/dev/examples/flaskrestful_example.py
    #     https://stackoverflow.com/questions/30779584/flask-restful-passing-parameters-to-get-request#30779996
    for item in parameters[action]:
        if item['schema']['type'] == 'list':
            webargs[item['name']] = fields.List(fields.Str())
        else:
            webargs[item['name']] = getattr(fields, item['schema']['type'].title())()

    # Adds additional apikey field
    # This is useful so the API frontend (eg. Kong) can use and forward an apikey argument
    webargs['apikey'] = fields.Str()

    return webargs

def parse_action(action,
        function,
        exclude_params = [],
        custom_notes = None,
        include_params = [],
        tags = []
        ):
    """Docstring and signature parsing with some introspection"""

    docstrings[action] = parse(function.__doc__)
    signatures[action] = signature(function)
    parameters[action] = [
            {
                'name'        : item.arg_name,
                'description' : item.description,
                'in'          : 'query',
                'schema'      : {
                    'type'   : str(type(
                        signatures[action].parameters[item.arg_name].default
                        )).replace("<class '", '').replace("'>", ''),
                    'default': signatures[action].parameters[item.arg_name].default,
                    }
                } for item in docstrings[action].params if item.arg_name not in exclude_params
            ]

    parameters[action].extend([ common_params[item] for item in include_params ])

    webargs[action]    = build_webargs(action)
    operations[action] = {
                            'tags'      : tags,
                            'parameters': parameters[action],
                            'responses': {
                                "200":
                                { "description": action,
                                  "content":
                                        { "application/json":
                                            { }
                                            },
                                        },
                                    },
                         }

    # Add action into spec
    spec.path(
            path="/" + action,
            #parameters=openapi['params'][action],
            operations={
                'get': operations[action]
                },
            )

    return

def strip_params(params):
    """
    Strip params that are not present in query methods

    Some parameters are available in the API but are not in the Query
    class' method signatures' and should be removed.
    """

    # Strip common additional parameters
    for item in common_params:
        name = common_params[item]['name']

        if name in params:
            del params[name]

    return params

def default_params(params):
    """Handle defaults for the included params"""

    for item in common_params:
        name   = common_params[item]['name']
        schema = common_params[item]['schema']

        if name not in params:
            if schema['type'] == 'int':
                params[name] = int(schema['default'])
            else:
                params[name] = schema['default']

    return params

def parse_params(params):
    """
    Additional param parsing

    This function process it's input parameters and returns two dictionaries:
    one ready for the query method and another for the postprocessing
    functions.
    """

    # Verbosity
    #params['verbose'] = True

    # Handle additional arguments and copy to a new dict
    args = params.copy()
    args = strip_params(args)

    # Ensure JSON output
    args['json'] = True

    # Always use the smart area optimization
    args['smart_area'] = True

    # Add default params
    params = default_params(params)

    # Return two dicts: one ready for the query function and another
    # for the postprocessing
    return ( args, params )

def get_default_value(action, param):
    for item in parameters[action]:
        if item['name'] == param:
            return item['schema']['default']
