#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Helper API functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from lib.params import buffers

def territory_name(name, type):
    buff = re.search(r"_(B[1-9])$", type)

    if buff is not None:
        name += ' (' + buffers[buff.group(1)] + ')'

    return name

def selection_name(item):
    name = [ ]

    if 'name_territory' in item and 'territory_type' in item:
        name.append(territory_name(item['name_territory'], item['territory_type']))
    elif 'name_territory' in item:
        name.append(item['name_territory'])
    elif 'territory_type' in item:
        name.append(item['territory_type'])

    if 'name_uf' in item:
        name.append(item['name_uf'])

    if 'name_mun' in item:
        name.append(item['name_mun'])

    if 'basin' in item:
        name.append(item['basin'])

    if len(name) > 0:
        name = ' - '.join(name)
    else:
        name = ''

    return name

def area_unit(value, unit):
    if unit == 'ha':
        return value

    if unit == 'km2':
        area = value / 100

        return round(float(area), 2)

    # CBF/FIFA World Cup standard
    # See https://www.cbf.com.br/futebol-brasileiro/noticias/campeonato-brasileiro/projeto-gramados-cbf-padroniza-campos-em-105-x-68
    if unit == 'soccer':
        hectare_in_m2      = int(10E4)
        soccer_field_in_m2 = int(105 * 68)
        area               = (value * hectare_in_m2) / soccer_field_in_m2

        return round(float(area), 2)

    # See doi:10.1023/a:1024593414624
    # Estimate might work only for the Amazon biome
    # Using the average between 400 and 750 adult trees per hectare
    if unit == 'trees':
        area = value * 575

        return round(float(area), 2)

def carbon_emission_unit(value, unit):
    # Tons of equivalent carbon
    if unit == 'mgceq':
        return value

    # Tons of equivalent carbon dioxide
    if unit == 'mgco2eq':
        # This assumes that each equivalent carbon combines with two oxygen molecules.
        #
        # The atomic mass of carbon (C) is 12 and oxygen (O) is 16.
        # So each Ce (12 atomic weights) combines with two oxygens (32 atomic weights in total).
        #
        # This gives the following ration:
        #
        # Before = 12 atomic weights.
        # After  = 44 atomic weights.
        # After/before ratio of ~ 3.7
        #
        # Then, to convert mgceq to MgCO2 is just a matter of multiplying by this ration
        # which represents the increase in weight of the carbon lost to the atmosphere
        # when combined with oxygen.
        return round(float(value) * 3.7, 2)

    # Megatons of equivalent carbon
    if unit == 'mtceq':
        return round(float(value / (1000*1000)), 2)

    # Megatons of equivalent carbon dioxide
    if unit == 'mtco2eq':
        return round(float(carbon_emission_unit(value, 'mgco2eq') / (1000*1000)), 2)

    # Gigatons of equivalent carbon
    if unit == 'gtceq':
        return round(float(value / (1000*1000*1000)), 2)

    # Gigatons of equivalent carbon dioxide
    if unit == 'gtco2eq':
        return round(float(carbon_emission_unit(value, 'mgco2eq') / (1000*1000*1000)), 2)

def previous_interval(params):
    """Determine the previous interval"""

    # Import datetime library
    import datetime
    import lib.api.config

    # Ensure a date_initial param
    if 'date_initial' not in params:
        params['date_initial'] = lib.api.config.get_default_value('events', 'date_initial')

    # Ensure a date_final param
    if 'date_final' not in params:
        params['date_final'] = lib.api.config.get_default_value('events', 'date_final')

    # Calculate the interval between final and initial dates
    #initial = datetime.date(*tuple([ int(x) for x in params['date_initial'].split('-') ]))
    #final   = datetime.date(*tuple([ int(x) for x in params['date_final'].split('-')   ]))
    initial  = datetime.date.fromisoformat(params['date_initial'])
    final    = datetime.date.fromisoformat(params['date_final'])
    interval = final - initial
    one_day  = datetime.timedelta(days=1)

    # Set the new final date
    date_final = str(initial - one_day)

    # Calculate the new initial date
    date_initial = str(initial - interval - one_day)

    return [ date_initial, date_final ]
