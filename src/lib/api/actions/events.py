#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Event API functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.api.helpers import selection_name, area_unit, carbon_emission_unit

from webargs.flaskparser  import use_args
from query                import Query
from lib.api.base         import BaseApi

import lib.api.config

# Params
params = {
        'exclude_params': [ 'verbose', 'smart_area' ],
        'include_params': [ 'area_unit', 'carbon_emission_unit', ]
        }

# Action parsing
lib.api.config.parse_action('events', Query.events,
        tags           = [ 'events' ],
        exclude_params = params['exclude_params'],
        include_params = params['include_params'],
        )

class GetEvents(BaseApi):
    """Event query: pressures/menaces located in a given space-time definition"""

    @use_args(lib.api.config.webargs['events'], location='query')
    def get(self, params):
        """Get pressure/menace events"""

        return self.getter(params)

    def getter(self, params):
        """Get events"""

        # Handle query and post-processing params
        args, params = lib.api.config.parse_params(params)

        # Instantiate
        inquiry = Query()

        # Dispatch
        result = self.cache_get_or_set('events', inquiry.events, **args)

        # Process
        args   = { 'result': result, 'params': params }
        output = self.cache_get_or_set('misc', self.process, **args)

        # Teardown
        inquiry.close()

        return output

    def process(self, result, params):
        """Process events result set. """

        if result != False and result['data'] != None:
            # Format for table display
            for key, item in enumerate(result['data']):
                name                        = selection_name(item)
                result['data'][key]['name'] = name
                area_field                  = 'area_' + params['area_unit']
                carbon_field                = 'carbon_emission' + params['carbon_emission_unit']

                if 'area_ha' in result['data'][key] and params['area_unit'] != 'ha':
                    area_ha                         = result['data'][key]['area_ha']
                    result['data'][key][area_field] = area_unit(area_ha, params['area_unit'])

                    del result['data'][key]['area_ha']

                if 'carbon_emission_mgceq' in result['data'][key] and params['carbon_emission_unit'] != 'mgceq':
                    carbon_emission_mgceq             = result['data'][key]['carbon_emission_mgceq']
                    result['data'][key][carbon_field] = carbon_emission_unit(carbon_emission_mgceq, params['carbon_emission_unit'])

                    del result['data'][key]['carbon_emission_mgceq']

        return result
