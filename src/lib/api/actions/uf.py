#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ARP functions.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.config     import config
from lib.api.sisarp import Sisarp
from lib.api.base   import BaseApi

import lib.api.config

# Parse action
lib.api.config.parse_action('uf',  Sisarp.uf,  tags = [ 'territories' ])

class GetUf(BaseApi):
    def __init__(self):
        self.sisarp = Sisarp()

    def get(self):
        """List UFs"""

        return self.sisarp.get('uf')
