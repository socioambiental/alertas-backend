#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Ranking API functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.api.helpers import selection_name, area_unit, carbon_emission_unit

from webargs.flaskparser  import use_args
from query                import Query
from lib.api.base         import BaseApi

import lib.api.config

# Parse the ranking query
lib.api.config.parse_action('ranking', Query.events,
        tags           = [ 'events' ],
        custom_notes   = 'Pressures/menaces ranking given space-time definition',
        exclude_params = [ 'verbose',      'geojson',
            'output_geometry_aggregated', 'output_geometry_disaggregated',
            'by_timeperiod', 'temporal_unit',
            #'id_territory', 'name_territory',
            'smart_area' ],
        include_params = [
            'output_format',
            'area_unit',
            'carbon_emission_unit',
            'max_items',
            ]
        )

class GetRanking(BaseApi):
    """Building pressures/menaces ranking given space-time definition"""

    @use_args(lib.api.config.webargs['ranking'], location='query')
    def get(self, params):
        """Build ranking"""

        return self.getter(params)

    def getter(self, params):
        """Build ranking"""

        # Handle query and post-processing params
        args, params = lib.api.config.parse_params(params)

        # Set custom defaults
        args['by_timeperiod'] = False

        # Instantiate
        inquiry = Query()

        # Dispatch
        #result = inquiry.events(**params)
        result  = self.cache_get_or_set('events', inquiry.events, **args)

        # Process
        args   = { 'result': result, 'params': params }
        output = self.cache_get_or_set('misc', self.process, **args)

        # Teardown
        inquiry.close()

        return output

    def process(self, result, params):
        ranking               = None
        meta                  = {}
        max_items             = params['max_items']
        area_field            = 'area_'            + params['area_unit']
        carbon_emission_field = 'carbon_emission_' + params['carbon_emission_unit']
        """Process ranking result set. """

        # Populate
        if params['output_format'] == 'chart':
            ranking = {
                        'events' : {
                            'categories': [],
                            'chart'     : [],
                        },
                        area_field: {
                            'categories': [],
                            'chart'     : [],
                        },
                    }

            if result != False and result['data'] != None:
                meta = result['meta']

                # Format using highcharts data series convention
                for item in result['data']:
                    name = selection_name(item)

                    ranking['events']['chart'].append([  name, item['events'] ])

                    if 'area_ha' in item:
                        area = area_unit(item['area_ha'], params['area_unit'])

                        if area_field != 'ha':
                            del item['area_ha']

                        ranking[area_field]['chart'].append([ name, round(float(area), 2) ])

                    if 'carbon_emission_mgceq' in item:
                        if carbon_emission_field not in ranking:
                            ranking[carbon_emission_field] = {
                                    'categories': [],
                                    'chart'     : [],
                                    }

                        carbon_emission = \
                                carbon_emission_unit(item['carbon_emission_mgceq'], params['carbon_emission_unit'])

                        if area_field != 'mgceq':
                            del item['carbon_emission_mgceq']

                        ranking[carbon_emission_field]['chart'].append([ name, round(float(carbon_emission), 2) ])

                # Sort by value (second item in each data point)
                from operator import itemgetter
                ranking['events']['chart']   = sorted(ranking['events']['chart'],   key=itemgetter(1), reverse=True)
                ranking[area_field]['chart'] = sorted(ranking[area_field]['chart'], key=itemgetter(1), reverse=True)

                if carbon_emission_field in ranking:
                    ranking[carbon_emission_field]['chart'] = \
                            sorted(ranking[carbon_emission_field]['chart'], key=itemgetter(1), reverse=True)

                # Slice
                ranking['events']['chart']   = ranking['events']['chart'][0:max_items]
                ranking[area_field]['chart'] = ranking[area_field]['chart'][0:max_items]

                # Build categories
                for measure in [ 'events', area_field, carbon_emission_field ]:
                    if measure in ranking:
                        for item in ranking[measure]['chart']:
                            ranking[measure]['categories'].append(item[0])

        else:
            if result != False and result['data'] != None:
                meta = result['meta']

                # Format for table display
                for key, item in enumerate(result['data']):
                    name = selection_name(item)

                    result['data'][key]['name'] = name

                    if 'area_ha' in result['data'][key] and params['area_unit'] != 'ha':
                        area_ha                         = result['data'][key]['area_ha']
                        result['data'][key][area_field] = area_unit(area_ha, params['area_unit'])

                        del result['data'][key]['area_ha']

                    if 'carbon_emission_mgceq' in result['data'][key] and params['carbon_emission_unit'] != 'mgceq':
                        carbon_emission_mgceq                      = result['data'][key]['carbon_emission_mgceq']
                        result['data'][key][carbon_emission_field] = carbon_emission_unit(carbon_emission_mgceq, params['carbon_emission_unit'])

                        del result['data'][key]['carbon_emission_mgceq']

                ranking = result['data']

        ranking = [ ] if ranking is None else ranking

        return {
                 'data': ranking,
                 'meta': meta,
               }
