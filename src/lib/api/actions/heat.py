#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Heat API functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from webargs.flaskparser  import use_args
from query                import Query
from lib.api.base         import BaseApi

import lib.cache.manager
import lib.api.config

# Parse the heat query
lib.api.config.parse_action('heat', Query.events,
        tags           = [ 'events' ],
        custom_notes   = 'Pressures/menaces heatmap given space-time definition',
        exclude_params = [
            'verbose', 'geojson', 'centroid'
            'output_geometry_aggregated', 'output_geometry_disaggregated',
            'by_territory', 'by_timeperiod',
            'name_territory',
            'by_apt', 'by_uf', 'by_mun', 'smart_area',
            ]
        )

class GetHeat(BaseApi):
    """Building pressures/menaces heat given space-time definition"""

    @use_args(lib.api.config.webargs['heat'], location='query')
    def get(self, params):
        """Build heat"""

        return self.getter(params)

    def getter(self, params):
        """Build heat"""

        # Handle query and post-processing params
        args, params = lib.api.config.parse_params(params)

        # Set custom defaults
        args['by_timeperiod'] = True
        args['centroid']      = True

        # Instantiate
        inquiry = Query()

        # Dispatch
        #result = inquiry.events(**params)
        result  = self.cache_get_or_set('events', inquiry.events, **args)

        # Process
        args   = { 'result': result, 'params': params }
        output = self.cache_get_or_set('misc', self.process, **args)

        # Teardown
        inquiry.close()

        return output

    def process(self, result, params):
        """Process heat result set. """

        output = None

        if result != False and result['data'] != None:
            heatmap = [ ]
            max     = 0

            # Build heatmap elements
            for item in result['data']:
                # Item is in "POINT(x, y)" format
                # We need it as [y, x, intensity], i.e, the Leaflet.heat format
                #
                # See https://github.com/Leaflet/Leaflet.heat
                if 'centroid' in item:
                    lon, lat = item['centroid'].split(' ')
                    lon      = lon[6:]
                    lat      = lat[0:-1]

                    if 'area_hea' in item:
                        # Determine max intensity
                        if item['area_ha'] > max:
                            max = item['area_ha']

                        # Event intensity according to it's area
                        intensity = item['area_ha']
                    else:
                        # Assuming every event has a constant "1" intensity
                        intensity = 1
                        max       = 1

                    # Append heat point into the dataset
                    heatmap.append([ lat, lon, intensity ])

            output = {
                    'data': {
                            'heatmap'     : heatmap,
                            'maxIntensity': max,
                        },
                     'meta': result['meta']
                    }

        return output
