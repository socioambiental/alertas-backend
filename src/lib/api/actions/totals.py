#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Totals query functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.api.helpers import area_unit, carbon_emission_unit, previous_interval

from webargs.flaskparser  import use_args
from query                import Query
from lib.api.base         import BaseApi

import lib.api.config

# Params
params = {
        'exclude_params': [ 'verbose', 'geojson',
            'output_geometry_aggregated', 'output_geometry_disaggregated',
            'by_territory', 'by_timeperiod',
            'name_territory',
            'by_apt', 'by_uf', 'by_mun', 'smart_area' ],
        'include_params': [
            'area_unit',
            'carbon_emission_unit',
            'increase',
            'rate',
            'rate_unit',
            'territory_area',
            ]
        }

# Parse the totals query
lib.api.config.parse_action('totals', Query.events,
        tags           = [ 'events' ],
        custom_notes   = 'Pressures/menaces totals given space-time definition',
        exclude_params = params['exclude_params'],
        include_params = params['include_params'],
        )

class GetTotals(BaseApi):
    """Building pressures/menaces totals given space-time definition"""

    @use_args(lib.api.config.webargs['totals'], location='query')
    def get(self, params):
        """Build totals"""

        return self.getter(params)

    def getter(self, params):
        """Build totals"""

        # Get total events
        result = self.get_totals(params)

        # Simply return the result in a query without increase calculation
        if 'increase' not in params or not params['increase']:
            return result

        if result['data']['events'] > 0:
            args   = { 'result': result, 'params': params }
            result = self.cache_get_or_set('misc', self.get_increase, **args)

        return result

    def get_totals(self, params):
        """Compute total events from a given param set."""

        # Handle query and post-processing params
        args, params = lib.api.config.parse_params(params)

        # Set custom defaults
        args['by_timeperiod'] = True

        # Instantiate
        inquiry = Query()

        # Dispatch
        #result = inquiry.events(**params)
        result  = self.cache_get_or_set('events', inquiry.events, **args)

        if 'territory_area' in params and params['territory_area'] == True:
            result['extra'] = {
                    'territory_area_ha': self.cache_get_or_set('events', inquiry.area, **args),
                    }

        # Process
        args   = { 'result': result, 'params': params }
        output = self.cache_get_or_set('misc', self.process, **args)

        # Teardown
        inquiry.close()

        return output

    def is_date_range_available(self, params):
        import datetime
        from lib.metadata.events import EventMetadataManager
        from lib.api.config      import get_default_value

        # Event metadata
        metadata_event = EventMetadataManager()
        events         = metadata_event.all()

        # Ensure an event type
        if 'event_type' not in params:
            params['event_type'] = get_default_value('totals', 'event_type')

        # Check if the event is available
        if params['event_type'] not in events:
            return False

        # Convert dates for comparison
        initial = datetime.date.fromisoformat(params['date_initial'])
        final   = datetime.date.fromisoformat(params['date_final'])
        oldest  = datetime.date.fromisoformat(events[params['event_type']]['oldest_event'])
        newest  = datetime.date.fromisoformat(events[params['event_type']]['newest_event'])

        # Initial date must be within in the available event range
        if (initial < oldest):
            return False

        # Final date must be within the available event range
        if (final > newest):
            return False

        return True

    def get_increase(self, result, params):
        """Compute total increase since last period."""

        # Get previous interval
        #[ params['date_initial'], params['date_final'] ] = previous_interval(params)
        [ params['date_initial'], params['date_final'] ] = previous_interval(result['meta']['args'])

        if not self.is_date_range_available(params):
            return result

        # Include the previous interval in the metadata
        result['meta']['extra'] = {
                'date_initial_previous': params['date_initial'],
                'date_final_previous'  : params['date_final'],
                }

        # Get the total events from the previous period
        result_previous = self.get_totals(params)

        return self.process_increase(result, result_previous)

    def process(self, result, params):
        """Process totals result set. """

        area_field            = 'area_' + params['area_unit']
        carbon_emission_field = 'carbon_emission_' + params['carbon_emission_unit']

        # Totals dataset
        totals = { 'events': 0, area_field: 0 }
        meta   = {}

        # Populate
        if result != False and result['data'] != None:
            meta = result['meta']

            # Format using highcharts data totals convention
            for item in result['data']:
                totals['events']  += int(item['events'])

                if 'area_ha' in item:
                    totals[area_field] += float(item['area_ha'])

                if 'carbon_emission_mgceq' in item:
                    if 'carbon_emission_mgceq' not in totals:
                        totals['carbon_emission_mgceq'] = 0

                    totals['carbon_emission_mgceq'] += float(item['carbon_emission_mgceq'])

            # Round
            totals[area_field] = round(totals[area_field], 2)

            if 'carbon_emission_mgceq' in totals:
                totals['carbon_emission_mgceq'] = round(totals['carbon_emission_mgceq'], 2)

            # Convert area unit
            if params['area_unit'] != 'ha':
                totals[area_field] = area_unit(totals[area_field], params['area_unit'])

            # Convert carbon unit
            if params['carbon_emission_unit'] != 'mgceq':
                totals[carbon_emission_field] = \
                        carbon_emission_unit(totals['carbon_emission_mgceq'], params['carbon_emission_unit'])

                del totals['carbon_emission_mgceq']

            # Average event area
            if area_field in totals       and \
               totals[area_field] != None and \
               totals[area_field]  > 0    and \
               totals['events'] > 0:
                totals['events_average_area_' + params['area_unit']] = round(totals[area_field] / totals['events'], 2)

            # Process rates
            if 'rate' in params and params['rate'] == True:
                if 'events' in totals:
                    totals['events_rate'] = self.process_rate(totals['events'], result, params)

                if area_field in totals:
                    totals[area_field + '_rate'] = self.process_rate(totals[area_field], result, params)

                if carbon_emission_field in totals:
                    totals[carbon_emission_field + '_rate'] = \
                            self.process_rate(totals[carbon_emission_field], result, params)

            # Process extra info
            if 'extra' in result:
                if 'territory_area_ha' in result['extra'] and result['extra']['territory_area_ha'] > 0:
                    territory_area_field = 'territory_area_' + params['area_unit']

                    # First convert to the area_unit in use
                    if params['area_unit'] != 'ha':
                        result['extra'][territory_area_field] = \
                                area_unit(result['extra']['territory_area_ha'], params['area_unit'])

                        del result['extra']['territory_area_ha']

                    # Then move the value to the totals
                    totals[territory_area_field] = result['extra'][territory_area_field]
                    del result['extra'][territory_area_field]

                    # Events per territory area
                    totals['events_per_' + territory_area_field] = \
                            round(totals['events'] / totals[territory_area_field], 2)

                    # Event area per territory area
                    if area_field in totals:
                        totals['percent_area_' + params['area_unit'] + '_per_' + territory_area_field] = \
                                round((totals[area_field] / totals[territory_area_field]) * 100, 2)

                    # Carbon emission per territory area
                    if carbon_emission_field in totals:
                        totals[carbon_emission_field + '_per_' + territory_area_field] = \
                                round(totals[carbon_emission_field] / totals[territory_area_field], 2)

        totals = [ ] if totals is None else totals

        return {
                 'data': totals,
                 'meta': meta,
               }

    @staticmethod
    def process_rate(value, result, params):
        # Parse
        if 'rate_unit' in params:
            unit = params['rate_unit'].lower()
        elif 'temporal_unit' in params:
            unit = params['temporal_unit'].lower()
        else:
            unit = 'day'

        import datetime

        # Deltas
        deltas = {
                'minute' : datetime.timedelta(seconds=60),
                'hour'   : datetime.timedelta(hours=1),
                'day'    : datetime.timedelta(days=1),
                'week'   : datetime.timedelta(weeks=1),
                'month'  : datetime.timedelta(days=30),
                'quarter': datetime.timedelta(days=122),
                'year'   : datetime.timedelta(days=365),
                }

        # Validate
        if unit not in deltas.keys():
            return False

        # Get interval
        initial  = datetime.date.fromisoformat(result['meta']['args']['date_initial'])
        final    = datetime.date.fromisoformat(result['meta']['args']['date_final'])
        interval = final - initial

        # Determine total time
        total = interval / deltas[unit]

        # Determine the rate
        rate = round(float(value) / total, 2)

        return rate

    @staticmethod
    def process_increase(result, result_previous):
        """Process increase since the previous period"""

        # Determine values for calculation
        items = list(result['data'].keys())

        # Calculate increase in the original result set
        for item in items:
            if result != False and result_previous != False and \
                float(result['data'][item]) != 0:
                result['data'][item + '_increase'] = round(
                        float(
                            (result['data'][item] - result_previous['data'][item]) / result['data'][item]),
                        2)

        return result
