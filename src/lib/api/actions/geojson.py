#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# GeoJSON API functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from webargs.flaskparser  import use_args
from query                import Query
from lib.api.base         import BaseApi

import lib.api.config

from lib.api.actions.events import params as events_params

params = dict(events_params)
params['exclude_params'].append('output_geometry_aggregated')
params['exclude_params'].append('output_geometry_disaggregated')
params['exclude_params'].append('geojson')

# Parse the events query
lib.api.config.parse_action('geojson', Query.events,
        tags           = [ 'events' ],
        exclude_params = params['exclude_params'],
        include_params = params['include_params'],
        )

class GetGeoJSON(BaseApi):
    """
    Event query: pressures/menaces located in a given space-time definition in GeoJSON format
    """

    @use_args(lib.api.config.webargs['events'], location='query')
    def get(self, params):
        """Get pressure/menace events in GeoJSON"""

        return self.getter(params)

    def getter(self, params):
        """Get GeoJSON"""

        # Handle query and post-processing params
        args, params = lib.api.config.parse_params(params)

        # Set custom defaults
        args['output_geometry_aggregated'] = True
        args['geojson']                    = True

        # Instantiate
        inquiry = Query()

        # Dispatch
        #result = inquiry.events(**params)
        result  = self.cache_get_or_set('events', inquiry.events, **args)

        # Process
        args   = { 'result': result, 'params': params }
        output = self.cache_get_or_set('misc', self.process, **args)

        # Teardown
        inquiry.close()

        return output

    def process(self, result, params):
        """Process GeoJSON result set. """

        import json

        geojson = {
                'type'    : 'FeatureCollection',
                'features': [],
                }

        if result != False and result['data'] != None:
            # Format for table display
            for item in result['data']:
                if 'geojson' not in item:
                    continue

                element = {
                        'type'      : 'Feature',
                        'geometry'  : json.loads(item['geojson']),
                        'properties': {},
                        }

                for prop in item:
                    if prop != 'geojson':
                        element['properties'][prop] = item[prop]

                geojson['features'].append(element)

        return geojson
