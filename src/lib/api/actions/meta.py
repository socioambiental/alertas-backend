#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Metadata API functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from webargs.flaskparser  import use_args
from lib.metadata.info    import get_metadata
from lib.api.base         import BaseApi

import lib.api.config

# Parse the meta query
lib.api.config.parse_action('meta', get_metadata, tags = [ 'meta' ])

class GetMeta(BaseApi):
    @use_args(lib.api.config.webargs['meta'], location='query')
    def get(self, params):
        """Retrieve metadata"""

        return self.getter(params)

    def getter(self, params):
        """Retrieve metadata"""

        # Dispatch
        output = self.cache_get_or_set('misc', get_metadata)

        # Process
        #from lib.api.meta import process_meta
        #args   = { 'result': result, 'params': params }
        #output = self.cache_get_or_set('misc', process_meta, **args)

        return output
