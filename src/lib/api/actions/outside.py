#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Outside ARPs API functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.api.helpers import previous_interval

from webargs.flaskparser  import use_args
from query                import Query
from lib.api.base         import BaseApi

import lib.api.config

from lib.api.actions.totals import params as totals_params, GetTotals

# Params
params = dict(totals_params)

# Remove 'territory_area' param since the "outside" method does not have yeat
# have a way to determine the areas of territories that are outside protected
# areas
del params['include_params'][params['include_params'].index('territory_area')]

# Parse the totals query
lib.api.config.parse_action('outside', Query.events,
        tags           = [ 'events' ],
        custom_notes   = 'Pressures/menaces totals outside protected areas given space-time definition',
        exclude_params = params['exclude_params'],
        include_params = params['include_params'],
        )

class GetOutside(BaseApi):
    """Builds pressures/menaces totals outside protected areas given space-time definition"""

    def __init__(self):
        super().__init__()

        self.totals                = GetTotals()
        self.totals.cache_strategy = self.cache_strategy

    @use_args(lib.api.config.webargs['totals'], location='query')
    def get(self, params):
        """Build totals outside protected areas"""

        return self.getter(params)

    def getter(self, params):
        """Build totals"""

        # Get total events
        result = self.get_outside(params)

        # Check for non-imported events
        if result == False:
            return False

        # Simply return the result in a query without increase calculation
        if 'increase' not in params or not params['increase']:
            return result

        if result['data']['events'] > 0:
            args   = { 'result': result, 'params': params }
            result = self.cache_get_or_set('misc', self.get_increase, **args)

        return result

    def get_outside(self, params):
        """Build totals outside protected areas"""

        # Set custom defaults
        params['territory_area'] = False

        # All
        params['territory_types'] = [ 'UF' ]
        all                       = self.totals.get_totals(params)

        # Inside Protected Areas
        params['territory_types'] = [ 'TI', 'UCF', 'UCE', 'APAF', 'APAE', 'QUI', ]
        inside                    = self.totals.get_totals(params)

        outside                   = { 'data': {} }

        # Check for non-imported events
        if all['data']['events'] == 0:
            return False

        # Outside
        for item in all['data']:
            outside['data'][item] = round(all['data'][item] - inside['data'][item], 2)

        # Include metadata
        outside['meta'] = all['meta']

        if 'args' in outside['meta']:
            del outside['meta']['args']['territory_types']

        return outside

    def get_increase(self, result, params):
        """Compute total increase since last period."""

        # Get previous interval
        #[ params['date_initial'], params['date_final'] ] = previous_interval(params)
        [ params['date_initial'], params['date_final'] ] = previous_interval(result['meta']['args'])

        if not self.totals.is_date_range_available(params):
            return result

        # Include the previous interval in the metadata
        result['meta']['extra'] = {
                'date_initial_previous': params['date_initial'],
                'date_final_previous'  : params['date_final'],
                }

        # Get the total events from the previous period
        result_previous = self.get_outside(params)

        return self.totals.process_increase(result, result_previous)
