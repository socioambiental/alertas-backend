#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Series API functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.api.helpers import selection_name, area_unit, carbon_emission_unit

from webargs.flaskparser  import use_args
from query                import Query
from lib.api.base         import BaseApi

import lib.api.config

# Parse the series query
lib.api.config.parse_action('series', Query.events,
        tags           = [ 'events' ],
        custom_notes   = 'Pressures/menaces series given space-time definition',
        exclude_params = [ 'verbose',      'geojson',
            'output_geometry_aggregated', 'output_geometry_disaggregated',
            'by_timeperiod',
            'name_territory',
            'smart_area' ],
        include_params = [
            'output_format',
            'area_unit',
            'carbon_emission_unit',
            'cummulative',
            ]
        )

class GetSeries(BaseApi):
    """Building pressures/menaces time series given space-time definition"""

    @use_args(lib.api.config.webargs['series'], location='query')
    def get(self, params):
        """Build series"""

        return self.getter(params)

    def getter(self, params):
        """Build series"""

        # Ensure a temporal unit
        if 'temporal_unit' not in params:
            params['temporal_unit'] = 'month'

        # Handle query and post-processing params
        args, params = lib.api.config.parse_params(params)

        # Set custom defaults
        args['by_timeperiod'] = True

        # Instantiate
        inquiry = Query()

        # Dispatch
        #result = inquiry.events(**params)
        result  = self.cache_get_or_set('events', inquiry.events, **args)

        # Process
        args   = { 'result': result, 'params': params }
        output = self.cache_get_or_set('misc', self.process, **args)

        # Teardown
        inquiry.close()

        return output

    def process(self, result, params):
        """Process series result set. """

        series                = None
        meta                  = {}
        area_field            = 'area_' + params['area_unit']
        carbon_emission_field = 'carbon_emission_' + params['carbon_emission_unit']

        # Populate
        if result != False and result['data'] != None:
            meta = result['meta']

            # Series dataset
            series = {}

            # Format using highcharts data series convention
            for item in result['data']:
                # Convert the date fields, assuming all dates are relative to UTC
                import datetime
                #date = item[params['temporal_unit']].split('-')
                date = item['date'].split('-')
                time = datetime.datetime(int(date[0]), int(date[1]), int(date[2]), tzinfo=datetime.timezone.utc)
                time = int(time.timestamp() * 1000) # milliseconds

                # Format name
                name = selection_name(item)

                # Determine single identifier
                #if 't_id_orig' in item and 'territory_type' in item:
                #    id = str(item['territory_type']) + '-' + str(item['t_id_orig'])
                #elif 't_id_orig' in item:
                #    #id = name + ' - ' + str(item['t_id_orig'])
                #    id = str(item['t_id_orig'])
                #else:
                #    id = name
                id = name + ' - ID ' + str(item['t_id_orig']) if 't_id_orig' in item else name

                for measure in [ 'events', 'area_ha', 'carbon_emission_mgceq' ]:
                    if measure in item:
                        # Handle area conversion
                        if measure == 'area_ha':
                            key   = area_field
                            value = area_unit(item[measure], params['area_unit'])
                        # Handle c_emission conversion
                        elif measure == 'carbon_emission_mgceq':
                            key   = carbon_emission_field
                            value = carbon_emission_unit(item[measure], params['carbon_emission_unit'])
                        # Other values
                        else:
                            key   = measure
                            value = item[measure]

                        # Initialize series if needed
                        if key not in series:
                            series[key] = {
                                    'chart': {},
                                    }

                        # Initialize dataset if needed
                        if id not in series[key]['chart']:
                            series[key]['chart'][id] = { 'name': name, 'data': [] }

                        # Append items
                        series[key]['chart'][id]['data'].append([ time, value ])

            from operator import itemgetter

            for measure in [ 'events', area_field, carbon_emission_field ]:
                if measure in series:
                    for item in series[measure]['chart']:
                        # Sort by date (first item in each data point)
                        series[measure]['chart'][item]['data'] = sorted(series[measure]['chart'][item]['data'], key=itemgetter(0))

                        # Sum up values
                        if params['cummulative']:
                            for n in range(1, len(series[measure]['chart'][item]['data'])):
                                series[measure]['chart'][item]['data'][n][1] += series[measure]['chart'][item]['data'][n - 1][1]

        series = [ ] if series is None else series

        return {
                 'data': series,
                 'meta': meta,
               }
