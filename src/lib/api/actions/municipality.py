#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Municipality functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from webargs.flaskparser  import use_args
from query                import Query
from lib.api.base         import BaseApi

import lib.api.config

# Parse the municipality query
lib.api.config.parse_action('municipality', Query.municipality, tags = [ 'territories' ])

# Using a custom implementation instead of SisARP's because we need
# filtering municipalities both by UF and by Legal Amazon.
class GetMunicipality(BaseApi):
    @use_args(lib.api.config.webargs['municipality'], location='query')
    def get(self, params):
        """List municipalities"""

        return self.getter(params)

    def getter(self, params):
        """List municipalities"""

        if 'uf' in params:
            uf = params['uf']
        else:
            #params['uf'] = uf = 0
            params['uf']  = uf = []

        # Instantiate
        inquiry = Query()

        # Dispatch
        #result = inquiry.events(**params)
        result  = self.cache_get_or_set('misc', inquiry.municipality, uf)

        # Process
        args   = { 'result': result, 'params': params }
        output = self.cache_get_or_set('misc', self.process, **args)

        # Teardown
        inquiry.close()

        return output

    def process(self, result, params):
        """Process municipality result set. """

        # Insert the first empty item to allow cleaning the selection in the UX
        # This is useful for single (i.e, non-multiple) select boxes
        #result['data'].insert(0, { 'text': '-', 'value': 0 })

        return result

#
# SisARP implementation
#

#from lib.api.sisarp import Sisarp
#
## Setup a Sisarp instance
#sisarp = Sisarp()
#
#lib.api.config.parse_action('municipality', sisarp.municipality, tags = [ 'territories' ])
#
#class GetMunicipality(BaseApi):
#    @use_args(lib.api.config.webargs['municipality'], location='query')
#    def get(self, params):
#        """List municipalities"""
#
#        if 'uf' in params:
#            uf = params['uf']
#            del params['uf']
#        else:
#            uf = ''
#
#        return sisarp.get('municipality', uf)
