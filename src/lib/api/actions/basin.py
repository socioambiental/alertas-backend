#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Basin functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from webargs.flaskparser  import use_args
from query                import Query
from lib.api.base         import BaseApi

import lib.api.config

# Action parsing
lib.api.config.parse_action('basin', Query.basin, tags = [ 'territories' ])

# Using a custom implementation instead of SisARP's API for compatibility with
# the spatial dataset which allows filtering basins intersecting the Legal Amazon,
# but it is a dataset that currently does not share a basin ID with SisARP.
#
# In the SisARP API, there's a (maybe persistent) ID for basins
# that could be used in the future:
#
# See https://sisarp.socioambiental.org/docs/api/v2/#/Bacia/get_bacia
class GetBasin(BaseApi):
    @use_args(lib.api.config.webargs['basin'], location='query')
    def get(self, params):
        """List basins"""

        return self.getter(params)

    def process(self, result, params):
        """Process basin result set. """

        # Insert the first empty item to allow cleaning the selection in the UX
        # This is useful for single (i.e, non-multiple) select boxes
        #result['data'].insert(0, { 'text': '-', 'value': '' })

        return result

    def getter(self, params):
        """List basins"""

        # Instantiate
        inquiry = Query()

        # Dispatch
        #result = inquiry.events(**params)
        result  = self.cache_get_or_set('misc', inquiry.basin)

        # Process
        args   = { 'result': result, 'params': params }
        output = self.cache_get_or_set('misc', self.process, **args)

        # Teardown
        inquiry.close()

        return output
