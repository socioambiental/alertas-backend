#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ARP functions.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.config     import config
from lib.api.sisarp import Sisarp

from webargs.flaskparser  import use_args
from lib.api.base         import BaseApi

import lib.api.config

# Parse action
lib.api.config.parse_action('arp', Sisarp.arp, tags = [ 'territories' ])

class GetArp(BaseApi):
    def __init__(self):
        self.sisarp = Sisarp()

    @use_args(lib.api.config.webargs['arp'], location='query')
    def get(self, params):
        """List Protected Areas"""

        if 'types' in params:
            types = params['types']
            del params['types']
        else:
            types = []

        if 'detailed' in params:
            detailed = params['detailed']
            del params['detailed']
        else:
            detailed = False

        return self.sisarp.get('arp', types, detailed)
