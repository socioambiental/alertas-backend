#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# News functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.api.rest import fetch, RestClient

from webargs.flaskparser  import use_args
from lib.api.base         import BaseApi

import lib.api.config

class News(RestClient):
    """Interaction with news upstream API"""

    def url(self, action = 'list', query_string = ''):
        if action == 'list':
            return 'https://www.socioambiental.org/pt-br/noticias-socioambientais/alertas-mais/json/?' + query_string

        return ''

    def list(self, page = 0):
        """
        Retrieve news from upstream ISA news endpoint.

        :type page : int, optional
        :param page: Page number. Defaults to zero.

        :rtype: dict
        :return: Dict of news content.
        """

        url = self.url('list', 'page=' + str(page))

        response = fetch(url)

        if response is False:
            return False

        if 'nodes' not in response:
            return False

        items = []
        nids  = []

        # Build list
        for item in response['nodes']:
            # Remove duplicates
            if item['node']['nid'] in nids:
                continue
            else:
                nids.append(item['node']['nid'])

            items.append(item['node'])

        #output = { 'data': response['nodes'], 'meta': response['pager'] }
        output = { 'data': { 'headlines': items, 'pager': response['pager'] }, 'meta': {} }

        return output

# Parse News queries
lib.api.config.parse_action('news', News.list, tags = [ 'news' ])

class GetNews(BaseApi):
    def __init__(self):
        self.news = News()

    @use_args(lib.api.config.webargs['news'], location='query')
    def get(self, params):
        """Get news"""

        return self.getter(params)

    def getter(self, params):
        """Get news"""

        if 'page' in params:
            page = params['page']
            del params['page']
        else:
            page = 0

        return self.news.get('list', page)
