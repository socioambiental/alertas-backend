#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# SisARP functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.config   import config
from lib.api.rest import fetch, RestClient

class Sisarp(RestClient):
    """Interaction with SisArp API"""

    def url(self, action, query_string = ''):
        return config['sisarp_api']['url'] + '/' + action + '?' + query_string + '&apikey=' + config['sisarp_api']['apikey']

    def check_arp_type(self, arp, types):
        """Check if a given ARP belongs to one of the given types"""

        for arp_type in types:
            if   arp_type == 'TI'   and arp['tipo'] == 'TI':
                 return True
            #elif arp_type == 'TQ'  and arp['tipo'] == 'QU':
            #     return True
            elif arp_type == 'UCF'  and arp['tipo'] == 'UC' and arp['instancia_responsavel'] == 'Federal':
                 return True
            elif arp_type == 'UCE'  and arp['tipo'] == 'UC' and arp['instancia_responsavel'] == 'Estadual':
                 return True
            elif arp_type == 'APAF' and arp['tipo'] == 'UC' and arp['instancia_responsavel'] == 'Federal'  and arp['categoria'] == 'APA':
                 return True
            elif arp_type == 'APAE' and arp['tipo'] == 'UC' and arp['instancia_responsavel'] == 'Estadual' and arp['categoria'] == 'APA':
                 return True

        return False

    def arp_url(self, arp_data, test = False):
        if 'href' not in arp_data or 'tipo' not in arp_data or 'id' not in arp_data:
            return ''

        if arp_data['tipo'] == 'TI':
            href = 'https://terrasindigenas.org.br/pt-br/terras-indigenas/' + str(arp_data['id'])
        elif arp_data['tipo'] == 'UC':
            href= 'https://uc.socioambiental.org/pt-br/arp/' + str(arp_data['id'])
        else:
            #href = return arp_data['href']
            href = ''

        # Test the URL
        if href != '' and test != False:
            response = fetch(href, False)

            if response is False:
                href = ''

        return href

    def arp(self, types = [ ], detailed = False):
        """
        Retrieve protected areas from SisARP API

        :type types : list, optional
        :param types: Territory types, similar to the events action

        :type detailed : boolean, optional
        :param detailed: Whether the output should include detailed information

        :rtype: list
        :return: ARPs satisfying the given conditions or
                 False when an error happens
        """

        # Restrict to Legal Amazon (jurisdicao=1)
        url      = self.url('arp', 'jurisdicao=1')
        response = fetch(url)

        if response is False:
            return False

        output = { 'data': [], 'meta': response['meta'] }

        del response['meta']['apikey']
        del response['meta']['endpoint']

        # Insert the first empty item to allow cleaning the selection in the UX
        # This is useful for single (i.e, non-multiple) select boxes
        #output['data'].append({
        #                        'text' : '-',
        #                        'value': 0,
        #                      })

        for item in response['data']:
            include = True

            # Exclude currently unavaibale types in the system
            # Handle TQ case
            if item['tipo'] == 'QU':
                continue

            # Filter results according to specified types
            if (len(types) > 0):
                include = self.check_arp_type(item, types)

            if include == True:
                if detailed == True:
                    info         = dict(item)
                    info['href'] = self.arp_url(info, False)

                else:
                    info = {}

                # Default info for select boxes
                info['text']  = item['categoria'] + ' ' + item['nome_arp']
                info['value'] = item['id']

                output['data'].append(info)

        return output

    def uf(self):
        """Retrieve UF data from SisARP API"""

        url      = self.url('uf')
        response = fetch(url)

        if response is False:
            return False

        output = { 'data': [], 'meta': response['meta'] }

        del response['meta']['apikey']
        del response['meta']['endpoint']

        # Insert the first empty item to allow cleaning the selection in the UX
        # This is useful for single (i.e, non-multiple) select boxes
        #output['data'].append({
        #                        'text' : '-',
        #                        'value': 0,
        #                      })

        from lib.params import legal_amazon_states

        for item in response['data']:
            # Discard items outside the Legal Amazon
            if item['nome'] not in legal_amazon_states.values():
                continue

            output['data'].append({
                                 'text' : item['nome'],
                                 'value': item['id'],
                               })

        return output

    def municipality(self, uf = ''):
        """
        Retrieve municipality data from SisARP API

        :type  uf: string, optional
        :param uf: Restrict to the given Federal Unit state code.
                   Code can be given either a number or acronym.
        """

        url      = self.url('municipio')
        response = fetch(url)

        if response is False:
            return False

        output = { 'data': [], 'meta': response['meta'] }

        del response['meta']['apikey']
        del response['meta']['endpoint']

        output['data'].append({
                                'text' : '-',
                                'value': 0,
                              })

        for item in response['data']:
            # Discard items whose either UF or IBGE code does not match
            if uf != '' and (item['uf'] != uf and str(item['code_ibge'])[0:2] != uf):
                continue

            output['data'].append({
                                 'text' : item['nome_municipio'] + ' (' + item['uf'] + ')',
                                 'value': item['code_ibge'],
                               })

        return output
