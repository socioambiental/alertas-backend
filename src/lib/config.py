#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# DETERpy config loader.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import configparser
from lib.params import config_path

# Set config file name
config_file = os.path.join(config_path, 'config.ini')

# Check config file
if not os.path.exists(config_file):
    raise FileNotFoundError('File not found: ' + config_file + \
                            '. Please configure it first by copying/editing ' +  config_file + '.dist')
    exit(1)

# Load config
config = configparser.ConfigParser()
config.read(config_file)
