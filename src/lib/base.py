#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Base class.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import fasteners
from lib.logger           import logger
from lib.params           import lock_path
from lib.metadata.imports import ImportMetadataManager

class Base:
    def __init__(self, config, args = {}):
        pass

    def set_lockfile(self, dataset='', lockfile_template='.{prefix}-{collection}'):
        try:
            date = self.date
        except AttributeError:
            date = ''

        lockfile = lockfile_template.format(
                collection=self.config.collection.lower(),
                dataset=dataset,
                date=date,
                prefix=self.lock_prefix,
                )

        self.lockfile = os.path.join(lock_path, lockfile)

        logger('info', 'Setting lock file {lockfile}.'.format(lockfile=lockfile))

        self.lock = fasteners.InterProcessLock(self.lockfile)
        lock      = self.lock.acquire(blocking=False)

        return lock

    def release_lockfile(self):
        self.lock.release()

        # Erase the lockfile. That might generate race conditions, so it's
        # better to leave that commented out.
        #os.remove(self.lockfile)

    def check_dependencies(self):
        if 'dependencies' in dir(self.config) and len(self.config.dependencies) > 0:
            metadata = ImportMetadataManager()

            for dependency in self.config.dependencies:
                collection  = metadata.collection(dependency)
                message     = 'Importer {name} requirement not satisfied: {dependency}. '
                message    += 'Please import if first or use --force to ignore checking.'

                if collection == None:
                    if 'force' not in self.args or self.args['force'] is False:
                        logger('error',
                                message.format(
                                        name       = self.config.name,
                                        dependency = dependency,
                                        )
                                )

                        return False

            logger('info', 'Dependencies check: all satisfied: {deps}'.format(
                deps=', '.join(self.config.dependencies)))

def lock(run):
    """Lockfile decorator"""

    def decorated(self):
        # Set lockfile
        lock = self.set_lockfile()

        if not lock:
            logger('error', 'Locked by another process.')

            # Execute the close method if available
            try:
                self.close()
            except AttributeError:
                #return False
                return

            # Return an error
            #return False

            # Return gracefully, avoiding a re-queue in Airflow in case an
            # importer is already running
            return

        if run(self) is False:
            return False

        self.release_lockfile()

    return decorated

def check_dependencies(run):
    """Dependency check decorator"""

    def decorated(self):
        # Check dependencies
        if self.check_dependencies() is False:
            return False

        if run(self) is False:
            return False

    return decorated
