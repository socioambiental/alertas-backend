#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Helper query functions.
# Uses code from https://github.com/pramsey/minimal-mvt
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#def buildTile(x, y, z):
#    """
#    Build a tile definition.
#
#    Adapted from https://github.com/pramsey/minimal-mvt/blob/master/minimal-mvt.py
#
#    """
#
#    return {
#            'zoom':   int(x),
#            'x':      int(y),
#            'y':      int(z),
#            }

from datetime   import datetime
from lib.logger import logger
from lib.params import srid

def tileIsValid(x, y, z):
    """
    Checks if a tile definition is valid.

    Adapted from https://github.com/pramsey/minimal-mvt/blob/master/minimal-mvt.py

    """

    size = 2 ** z

    # Do the tile x/y coordinates make sense at this zoom level?
    if x >= size or y >= size:
        return False

    if x < 0 or y < 0:
        return False

    return True

def tileToEnvelope(x, y, z, densify = 4):
    """
    Calculate envelope in "Spherical Mercator" (https://epsg.io/3857)

    Adapted from https://github.com/pramsey/minimal-mvt/blob/master/minimal-mvt.py

    """

    # Width of world in EPSG:3857
    worldMercMax  = 20037508.3427892
    worldMercMin  = -1 * worldMercMax
    worldMercSize = worldMercMax - worldMercMin

    # Width in tiles
    worldTileSize = 2 ** z

    # Tile width in EPSG:3857
    tileMercSize = worldMercSize / worldTileSize

    # Calculate geographic bounds from tile coordinates
    # XYZ tile coordinates are in "image space" so origin is
    # top-left, not bottom right
    xmin = worldMercMin + tileMercSize *  x
    xmax = worldMercMin + tileMercSize * (x + 1)
    ymin = worldMercMax - tileMercSize * (y + 1)
    ymax = worldMercMax - tileMercSize * (y)

    # Densify the edges a little so the envelope can be
    # safely converted to other coordinate systems.
    if densify > 0:
        seg_size = (xmax - xmin) / densify

    return [ xmin, ymin, xmax, ymax, seg_size ]

def build_query(query_select, query_from, query_where, query_group, query_order, query_with = []):
    query = "SELECT " + ", ".join(query_select) + query_from

    if len(query_where) != 0:
        query = query + " WHERE " + " AND ".join(query_where)

    if len(query_group) != 0:
        query = query + " GROUP BY " + ", ".join(query_group)

    if len(query_order) != 0:
        query = query + " ORDER BY " + ", ".join(query_order)

    if len(query_with) != 0:
        query = 'WITH ' + ','.join(query_with) + ' ' + query

    return query

def build_territories_query(
        territory_type_index = -1,
        territory_types      = [],
        id_territory         = [],
        id_uf                = [],
        id_mun               = [],
        name_basin           = [],
        output               = 'mvt'):
    """Build a query for territories"""

    # Initialize query
    query_select = []; query_where = []; query_group = []; query_order = []; query_from = ""

    #
    # Query composition: WITH
    #

    query_with = [ ]

    if len(id_uf) > 0:
        query_with.append("uf_filter AS (SELECT id_orig, geom FROM base.territory WHERE TYPE = 'UF' AND (id_orig={where_id_uf}))")

    if len(id_mun) > 0:
        query_with.append("mun_filter AS (SELECT id_orig, geom FROM base.territory WHERE TYPE = 'MUN' AND (id_orig={where_id_mun}))")

    if len(name_basin) > 0:
        query_with.append("basin_filter AS (SELECT bacia, shape AS geom FROM base.basins_lvl2_ana WHERE (bacia={where_name_basin}))")

    #
    # Query composition: SELECT
    #

    if output == 'mvt':
        query_select.append("ST_AsMVTGeom(ST_Transform(t.geom, 3857), bounds.b2d) AS geom")
    elif output == 'area_ha':
        query_select.append("SUM(ST_Area(geom::geography)::numeric/10000) AS area_ha")
    else:
        query_select.append("t.geom AS geom")

    if output != 'area_ha':
        query_select.append("t.id_orig AS id")
        query_select.append("t.type    AS type")
        query_select.append("t.name    AS name")

    #
    # Query composition: FROM
    #

    query_from  = " FROM base.territory t"

    # To allow restriction of territories only in the Legal Amazon
    query_from += ", base.bla_base250 a "

    if len(id_uf) > 0:
        query_from += ", uf_filter "

    if len(id_mun) > 0:
        query_from += ", mun_filter "

    if len(name_basin) > 0:
        query_from += ", basin_filter "

    # MVT
    if output == 'mvt':
        query_from += ", bounds"

    #
    # Query composition: WHERE
    #

    if territory_type_index >= 0:
        query_where.append("(t.type={territory_type_" + str(territory_type_index) + "})")
    elif len(territory_types) > 0:
        query_where.append("(t.type={where_territory_types})")

    if len(id_territory) > 0:
        query_where.append("(t.id_orig={where_id_territory})")

    if len(id_uf) > 0:
        query_where.append("ST_Intersects(t.geom, uf_filter.geom)")

    if len(id_mun) > 0:
        query_where.append("ST_Intersects(t.geom, mun_filter.geom)")

    if len(name_basin) > 0:
        query_where.append("ST_Intersects(t.geom, basin_filter.geom)")

    # Restrict to territories in the Legal Amazon
    # The && operator is faster but includes territories outside the boundary
    #query_where.append("t.geom && a.shape")
    query_where.append("ST_intersects(t.geom, a.shape)")

    # MVT
    if output == 'mvt':
        query_where.append("ST_Intersects(t.geom, ST_Transform(bounds.geom, {srid}))")

    #
    # Build query
    #

    query = build_query(query_select, query_from, query_where, query_group, query_order, query_with)

    return query

def mvt_query_envelope(queries):
    """
    Envelope multiples queries for MVT output, each one in a separate layer
    Thanks https://github.com/pramsey/minimal-mvt/blob/master/minimal-mvt.py
    See    https://postgis.net/docs/ST_AsMVT.html

    Beware that multiple queries seems way slower than single queries.
    """

    # Bounds
    bounds = "ST_Segmentize(ST_MakeEnvelope({xmin}, {ymin}, {xmax}, {ymax}, 3857), {seg_size})"
    sql    = """
        WITH
        bounds AS (
                   SELECT {bounds} AS geom,
                          {bounds}::box2d AS b2d
        ),
    """.format(bounds=bounds)

    # Queries
    sql_queries = [ ]
    for layer, query in queries.items():
        sql_queries.append("    {layer} AS ({query})".format(layer=layer, query=query))

    sql += ', '.join(sql_queries)

    # Query composition: SELECT
    sql += " SELECT "

    query_select = [ ]
    for layer, query in queries.items():
        query_select.append("ST_AsMVT({layer}.*, '{layer}')".format(layer=layer))

    sql += ' || '.join(query_select)

    # Query composition: FROM
    sql += " FROM "

    query_from = []
    for layer, query in queries.items():
        query_from.append(layer)

    sql += ', '.join(query_from)

    return sql

def log_query(query, verbose):
    if verbose:
        logger('info', "Query is:")
        logger('info', query)
        logger('info', "Submitting query...")

def log_elapsed(init_time, verbose):
    elapsed = str((datetime.now() - init_time))

    if verbose:
        logger('info', "Elapsed time: " + elapsed)

    return elapsed
