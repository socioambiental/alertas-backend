#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Logging functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import logging
import logging.config
#import traceback

from concurrent_log_handler import ConcurrentRotatingFileHandler
from lib.params             import base_path

# Config file alternatives
config      = os.path.join(base_path, 'config', 'logging.ini')
config_dist = os.path.join(base_path, 'config', 'logging.ini.dist')

# Log file name
logfile = os.path.join(base_path, 'log', 'main.log')

# Avoid Windows path issues
# See https://lerner.co.il/2018/07/24/avoiding-windows-backslash-problems-with-pythons-raw-strings/
#     https://stackoverflow.com/questions/11924706/how-to-get-rid-of-double-backslash-in-python-windows-file-path-string#11924842
logfilename = r'{}'.format(logfile)

# For fileConfig defaults, see
# https://stackoverflow.com/questions/13649664/how-to-use-logging-with-pythons-fileconfig-and-configure-the-logfile-filename#20243458
if os.path.exists(config):
    logging.config.fileConfig(config, defaults={'logfilename': logfilename})
else:
    logging.config.fileConfig(config_dist, defaults={'logfilename': logfilename})

def logger(level, message, *args, **kwargs):
    if level == 'exception':
        logging.exception(message, *args, **kwargs)

    else:
        getattr(logging, level)(message)

        #if level == 'error':
        #    gettattr(logging, level)(traceback.format_exc())
