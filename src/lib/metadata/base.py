#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Metadata-related functionality.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import time
import random
from psycopg2                import Error, OperationalError
from psycopg2.extensions     import QueryCanceledError
from psycopg2.errors         import UniqueViolation
from lib.database.sqlalchemy import create_engine, start_session
from lib.logger              import logger
from lib.database.timeout    import retry_wait

class BaseMetadata():
    """Base class for metadata handling"""

    def __init__(self):
        # Setup database engine
        self.engine = create_engine()

        # Register model and start session
        self.model   = self.create_model()
        self.session = start_session(self.engine, lock_timeout=True)

    def close(self):
        self.session.close()

        # See https://docs.sqlalchemy.org/en/14/core/connections.html#engine-disposal
        self.engine.dispose()

    def create_model(self):
        """
        Create the metadata database model for events.

        """

        if 'model_definition' not in dir(self):
            return None

        # Ensure the database has the modeled tables and cols
        self.model_definition.metadata.create_all(self.engine)

        return self.model_definition

    def execute(self, *args, **kwargs):
        """
        Wrapper to self.session.execute() handling timeouts and other
        operational errors.

        """

        try:
            return self.session.execute(*args, **kwargs)
        except (OperationalError, QueryCanceledError) as e:
            retry_wait()

            return self.execute(*args, **kwargs)

    def query(self, *args, **kwargs):
        """
        Wrapper to self.session.query() handling timeouts and other
        operational errors.

        """

        try:
            return self.session.query(*args, **kwargs)
        except (OperationalError, QueryCanceledError) as e:
            retry_wait()

            return self.query(*args, **kwargs)

    def commit(self):
        """
        Wrapper to self.session.commit() handling timeouts and other
        operational errors.

        """

        try:
            return self.session.commit()
        except (OperationalError, QueryCanceledError) as e:
            retry_wait()

            return self.commit()

    def concurrent_insert(self, record):
        """
        Ensure data insertion in cases a table is locked or if
        the generated row ID is duplicated in case of a race condition
        between concurrent importers.

        Maybe could be implemented using Contextual/Thread-local Sessions
        https://docs.sqlalchemy.org/en/14/orm/contextual.html

        Approach using direct database lock (which does not work):
        See https://stackoverflow.com/questions/37331646/sqlalchemy-explicit-locking-of-postgresql-table#37396201

        Problem description:
        https://stackoverflow.com/questions/54351783/duplicate-key-value-violates-unique-constraint-postgres-error-when-trying-to-c

        See also:
        https://docs.sqlalchemy.org/en/14/orm/session_transaction.html
        https://stackoverflow.com/questions/21653319/sqlalchemy-concurrency-update-issue#21695216

        """

        attempts = 0

        try:
            #nested = self.session.begin_nested()
            #self.session.execute('LOCK TABLE meta.import;')

            self.session.add(record)
            #nested.commit()

            self.commit()

        except UniqueViolation as e:
            self.session.rollback()

            if attempts >= 20:
                logger('exception', e)
                raise Exception('Error inserting info into metadata table')

            attempts += 1

            # A random wait period decreases the chance that calls to
            # concurrent_insert() from different processes will overlap
            wait = random.randint(1, 5)

            logger('warn',
                    'Attempt to insert on metadata table failed, retrying in {wait} seconds...'.format(wait=wait))

            # Try again after a while
            time.sleep(wait)

            self.concurrent_insert(record)

        except Error as e:
            logger('exception', e)
            raise Exception('Error inserting info into metadata table')
