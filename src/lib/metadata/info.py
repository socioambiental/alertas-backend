#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Metadata-related functionality.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.metadata.imports     import ImportMetadataManager
from lib.metadata.exports     import ExportMetadataManager
from lib.metadata.events      import EventMetadataManager
from lib.metadata.territories import TerritoryMetadataManager

def get_metadata():
    """
    Get metadata information on past imports and available events.

    :rtype: dict
    :return: Dictionary with events and import information.

    """

    # Instantiate
    metadata_import    = ImportMetadataManager()
    metadata_export    = ExportMetadataManager()
    metadata_event     = EventMetadataManager()
    metadata_territory = TerritoryMetadataManager()

    # Get data
    meta = {
            'data' : {
                'events'      : metadata_event.all(),
                'territories' : metadata_territory.all(),
                'imports'     : metadata_import.completed(),
                'exports'     : metadata_export.latest(),
                },
            'meta': {},
            }

    # Cleanup
    metadata_import.close()
    metadata_export.close()
    metadata_event.close()
    metadata_territory.close()

    return meta

def clear_metadata_cache():
    from lib.cache.manager import delete

    delete('misc', get_metadata)
