#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Metadata-related functionality.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from lib.metadata.imports  import ImportMetadataManager
from lib.metadata.exports  import ExportMetadataManager
from lib.metadata.events   import EventMetadataManager
from lib.params            import pipeline_next_phases
from lib.logger            import logger
from lib.cache.manager     import clear
#from lib.metadata.info    import clear_metadata_cache

#
# Helpers
#

def should_save_event_metadata(id, phase, config):
    """Check if should save event metadata in the last phase"""

    if 'type' not in dir(config) or config.type == 'event':
        next = pipeline_next_phases[config.pipeline][phase]

        if len(next) == 0:
            return True

    return False

#
# Decorators
#

#def truncate_import_metadata(run):
#    """Decorator allowing metadata truncation during load phase. """
#
#    def decorated(self):
#        if self.args['force'] != True:
#            metatada_manager = ImportMetadataManager()
#
#            # Delete import metadata
#            metadata_import.delete(self.id, self.phase, self.config)
#
#            # Delete event metadata
#            if 'type' not in dir(config) or config.type == 'event':
#               metadata_event = EventMetadataManager()
#
#               metadata_event.delete(config)
#
#        if run(self) is False:
#            return False
#
#    return decorated

def check_import_metadata(method):
    """
    Decorator that checks for metadata, checking if the import phase should
    run.

    """

    def decorated(self, *args, **kwargs):
        metadata_import = ImportMetadataManager()

        if self.args['force'] != True:
            status = metadata_import.check(self.id, self.phase, self.config)

            if status == 'missing_previous':
                # Return False if there's no previous phase with the
                # same name and import_id
                return False

            elif status == 'already_processed':
                # Return True if there's already an entry with the same name,
                # phase and import_id, i.e, the phase was already executed
                logger('info', 'Skipping run.')

                return True

        else:
            # If 'force' config is present, delete the metadata
            metadata_import.reset(self.id, self.phase, self.config)

        # Execute the phase
        if method(self, *args, **kwargs) is False:
            return False

    return decorated

def save_import_metadata(method):
    """Decorator allowing an importer phase to save metadata."""

    def decorated(self, *args, **kwargs):
        metadata_import = ImportMetadataManager()

        if method(self, *args, **kwargs) is False:
            return False

        # Upsert import record
        metadata_import.upsert(self.id, self.phase, self.config)

        if should_save_event_metadata(self.id, self.phase, self.config):
            metadata_event = EventMetadataManager()

            metadata_event.upsert(self.config)

        else:
            # Here we could save territory metadata, but that could
            # be simply queried, see lib.metadata.territory for details.
            pass

        # Cleanup metadata cache, ensuring that the API will refresh this
        # information automatically upon the next request
        #logger('info', 'Clearing metadata cache...')
        #clear_metadata_cache()

        # Clear all caches after an import has concluded
        #logger('info', 'Clearing all caches...')
        #clear()

        # Clear the misc cache after an import has concluded
        logger('info', 'Clearing the misc cache...')
        clear([ 'misc' ])

    return decorated
