#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Metadata-related functionality.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import Table, Column, Integer, String, Date
from sqlalchemy                 import UniqueConstraint
from lib.metadata.base          import BaseMetadata
from query                      import Query

Base = declarative_base()

class EventMetadata(Base):
    """Data model for event metadata"""

    __tablename__  = 'event'
    __table_args__ = ( UniqueConstraint('type'), { 'schema': 'meta' })
    id             = Column(Integer, primary_key=True)
    type           = Column(String)
    classes        = Column(String)
    total_events   = Column(Integer)
    oldest_event   = Column(Date)
    newest_event   = Column(Date)
    area_ha        = Column(Integer)
    geometry_type  = Column(String)

class EventMetadataManager(BaseMetadata):
    def __init__(self):
        self.model_definition = EventMetadata

        super().__init__()

    def get_data(self, config):
        import json

        results    = {}
        statements = {
                'oldest' : "SELECT date        FROM event_processed.event WHERE type = :type ORDER BY date  ASC LIMIT 1",
                'newest' : "SELECT date        FROM event_processed.event WHERE type = :type ORDER BY date DESC LIMIT 1",
                'total'  : "SELECT COUNT(type) FROM event_processed.event WHERE type = :type",
                }

        # Check if the event type is poligonal
        query  = Query()
        is_pol = query.is_polygon(config.collection)

        # Poligonal events also have an additional area calculation
        if is_pol:
            statements['area_ha'] = "SELECT SUM(ST_Area(geom::geography)::numeric/10000) FROM event_processed.event WHERE type = :type"
        else:
            statements['area_ha'] = "SELECT 0 as sum"

        for key, statement in statements.items():
            results[key] = self.execute(statement, { 'type': config.collection }).scalar()

        return {
                'type'          : config.collection,
                'oldest'        : results['oldest'],
                'newest'        : results['newest'],
                'total'         : results['total'],
                'area_ha'       : round(float(results['area_ha']), 2),
                'geometry_type' : 'area' if is_pol else 'point',
                'classes'       : json.dumps(config.classes),
                }

    #
    # CRUD
    #

    def insert(self, config):
        data   = self.get_data(config)
        record = self.model(
                type          = config.collection,
                classes       = data['classes'],
                total_events  = data['total'],
                oldest_event  = data['oldest'],
                newest_event  = data['newest'],
                area_ha       = data['area_ha'],
                geometry_type = data['geometry_type'],
                )

        self.concurrent_insert(record)

    def update(self, config):
        query = self.query(self.model).filter(
                self.model.type == config.collection,
                )

        record               = query.one()
        data                 = self.get_data(config)
        record.classes       = data['classes'],
        record.total_events  = data['total'],
        record.oldest_event  = data['oldest']
        record.newest_event  = data['newest']
        record.area_ha       = data['area_ha']
        record.geometry_type = data['geometry_type']

        self.commit()

    def delete(self, config):
        query = self.query(self.model).filter(
                self.model.type == config.collection,
                )

        record = query.delete()

        self.commit()

    def upsert(self, config):
        query = self.query(self.model).filter(
                self.model.type == config.collection,
                )

        record = query.first()

        if record == None:
            self.insert(config)
        else:
            self.update(config)

    def all(self):
        from lib.database.sqlalchemy import rows_to_dict
        import json

        query   = self.query(self.model).order_by(self.model.type)
        results = rows_to_dict(query.all(), 'type')

        # Unserialize the classes object
        for item in results.keys():
            results[item]['classes'] = json.loads(results[item]['classes'])

        return results
