#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Metadata-related functionality.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import datetime
import json
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import Table, Column, Integer, String, Date, Boolean
from sqlalchemy                 import UniqueConstraint
from lib.logger                 import logger
from lib.params                 import pipeline_previous_phases, pipeline_next_phases
from lib.metadata.base          import BaseMetadata

Base = declarative_base()

class ImportMetadata(Base):
    """The importer metadata model."""

    # Metadata available at all phases:
    #
    # self.id
    # self.phase
    # self.config.name
    # self.config.collection
    __tablename__  = 'import'
    __table_args__ = ( UniqueConstraint('name', 'import_id'), { 'schema': 'meta' })
    id             = Column(Integer, primary_key=True)
    name           = Column(String)
    collection     = Column(String)
    import_id      = Column(String)
    phase          = Column(String)
    phase_date     = Column(Date)
    is_last_phase  = Column(Boolean)
    meta           = Column(String)
    archive        = Column(String)
    #download_id   = Column(String)
    #dataset_date  = Column(Date)
    #start_date    = Column(Date)
    #end_date      = Column(Date)
    #filename      = Column(String)
    #filehash      = Column(String)
    #filesize      = Column(Integer)
    #entries       = Column(Integer)

class ImportMetadataManager(BaseMetadata):
    def __init__(self):
        self.model_definition = ImportMetadata

        super().__init__()

        # Pipeline
        self.previous = pipeline_previous_phases
        self.next     = pipeline_next_phases

    def check(self, id, phase, config):
        """
        Check if the current or previous phases were already processed
        given an import id, a phase and an import config.

        """

        previous = self.previous[config.pipeline][phase]
        next     = self.next[config.pipeline][phase]
        query    = self.query(self.model).filter(
                   self.model.name      == config.name,
                   self.model.import_id == id,
                   )

        record = query.first()

        if record == None:
            if previous == '':
                logger('info', 'Metadata check: needs processing for phase ' + \
                        phase + ' for importer ' + config.name + ' and import ID ' + id)

                return 'needs_processing'
            else:
                logger('error', 'Metadata check: missing previous phase ' + \
                        str(previous) + ' for importer ' + config.name + ' and import ID ' + id)

                return 'missing_previous'
        else:
            if record.phase == phase or record.phase in next:
                logger('info', 'Metadata check: already processed phase ' + \
                        phase + ' for importer ' + config.name + ' and import ID ' + id + \
                        '. Use --force to ignore metadata checks.')

                return 'already_processed'
            elif record.phase != previous:
                logger('error', 'Metadata check: previous phase is not ' + \
                        str(previous) + ' for importer ' + config.name + ' and import ID ' + id)

                return 'missing_previous'
            else:
                logger('info', 'Metadata check: needs processing for phase ' + \
                        phase + ' for importer ' + config.name + ' and import ID ' + id)

                return 'needs_processing'

    def is_last_phase(self, phase, config):
        return True if len(self.next[config.pipeline][phase]) == 0 else False

    #
    # CRUD
    #

    def insert(self, id, phase, config):
        record = self.model(
                name          = config.name,
                collection    = config.collection,
                meta          = json.dumps(config.metadata),
                archive       = json.dumps(config.archive),
                import_id     = id,
                phase         = phase,
                phase_date    = datetime.datetime.now(),
                is_last_phase = self.is_last_phase(phase, config)
                )

        self.concurrent_insert(record)

    def update(self, id, phase, config):
        query = self.query(self.model).filter(
                self.model.name      == config.name,
                self.model.import_id == id,
                )

        record               = query.one()
        record.collection    = config.collection
        record.meta          = json.dumps(config.metadata),
        record.archive       = json.dumps(config.archive),
        record.phase         = phase
        record.phase_date    = datetime.datetime.now()
        record.is_last_phase = self.is_last_phase(phase, config)

        self.commit()

    def delete(self, id, phase, config):
        query = self.query(self.model).filter(
                self.model.name      == config.name,
                self.model.import_id == id,
                )

        record = query.delete()

        self.commit()

    def reset(self, id, phase, config):
        query = self.query(self.model).filter(
                self.model.name      == config.name,
                self.model.import_id == id,
                )

        record = query.first()

        if record != None:
            record.phase      = ''
            record.phase_date = datetime.datetime.now()

            self.commit()

    def upsert(self, id, phase, config):
        query = self.query(self.model).filter(
                self.model.name      == config.name,
                self.model.import_id == id,
                )

        record = query.first()

        if record == None:
            self.insert(id, phase, config)
        else:
            self.update(id, phase, config)

    #
    # Misc queries
    #

    def all(self):
        from lib.database.sqlalchemy import rows_to_dict

        query = self.query(self.model)

        return rows_to_dict(query.all(), 'name')

    def completed(self, latest_only = True, short = True):
        # ORM approach
        #
        #from lib.database.sqlalchemy import rows_to_dict
        #
        #query = self.query(self.model).filter(
        #        self.model.is_last_phase == True
        #        )
        #
        ## This should be changed when sqlalchemy is upgraded to 2.x
        ## https://docs.sqlalchemy.org/en/14/changelog/migration_20.html#migration-20-query-distinct
        ## https://docs.sqlalchemy.org/en/14/changelog/migration_20.html#using-distinct-with-additional-columns-but-only-select-the-entity
        #if latest_only:
        #    query.distinct(self.model.name)
        #
        #query.order_by(self.model.name.desc(), self.model.phase_date.desc())
        #
        #results = rows_to_dict(query.all(), 'name')

        # SQL approach, with does not suffer from weird DISTINCT ON behavior
        distinct  = 'DISTINCT ON (name)' if latest_only else ''
        statement = """SELECT {distinct} name, id, collection,
                       import_id, phase, phase_date, is_last_phase, meta, archive
                       FROM meta.import WHERE is_last_phase = TRUE
                       ORDER BY name, phase_date DESC;""".format(
                               distinct=distinct,
                               )

        query   = self.execute(statement).all()
        results = {}

        for item in query:
            results[item.name] = item._asdict()

        for item in results.keys():
            # Remove unwanted fields
            if short:
                del results[item]['id']
                del results[item]['phase']
                del results[item]['is_last_phase']

            # Convert date fields
            results[item]['phase_date'] = results[item]['phase_date'].strftime('%Y-%m-%d')

            # Unserialize some fields
            if 'meta' in results[item]:
                if results[item]['meta'] != None and results[item]['meta'] != 'None':
                    results[item]['meta'] = json.loads(results[item]['meta'])

            if 'archive' in results[item]:
                if results[item]['archive'] != None and results[item]['archive'] != 'None':
                    results[item]['archive'] = json.loads(results[item]['archive'])

        return results

    def collection(self, collection):
        """Query available collections."""

        query = self.query(self.model).filter(
                self.model.collection    == collection,
                self.model.is_last_phase == True
                )

        results = query.first()

        return results
