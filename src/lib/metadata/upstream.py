#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Metadata-related functionality.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from lib.api.rest import fetch
from lib.config   import config
from lib.logger   import logger

def get_download_id(export):
    """Determine the download_id from the canonical upstream API"""

    if 'upstream' not in config or \
       'apiurl'   not in config['upstream'] or \
       'apikey'   not in config['upstream']:
           return False

    meta_url = os.path.join(config['upstream']['apiurl'], 'meta?apikey={apikey}')
    apikey   = config['upstream']['apikey']
    url      = meta_url.format(apikey=apikey)

    logger('info', 'Trying to get download_id from ' + meta_url + '...')

    response = fetch(url)

    if response is False or 'data' not in response:
        return False

    if 'exports' not in response['data']:
        return False

    if export not in response['data']['exports']:
        return False

    download_id = response['data']['exports'][export]['export_id']

    logger('info', 'Download ID set to ' + download_id + '.')

    return download_id
