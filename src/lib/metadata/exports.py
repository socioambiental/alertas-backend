#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Metadata-related functionality.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import datetime
import json
from pathlib    import Path
from lib.params import base_path, exports_path

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import Table, Column, Integer, String, Date, Boolean
from sqlalchemy                 import UniqueConstraint
from lib.logger                 import logger
from lib.metadata.base          import BaseMetadata

Base = declarative_base()

class ExportMetadata(Base):
    """The exporter metadata model."""

    __tablename__  = 'export'
    __table_args__ = ( UniqueConstraint('name', 'export_id', 'operation'), { 'schema': 'meta' })
    id             = Column(Integer, primary_key=True)
    name           = Column(String)
    operation      = Column(String)
    collection     = Column(String)
    export_id      = Column(String)
    export_date    = Column(Date)
    meta           = Column(String)
    type           = Column(String)
    prefix         = Column(String)
    params         = Column(String)
    #filehash      = Column(String)
    #filesize      = Column(Integer)

class ExportMetadataManager(BaseMetadata):
    def __init__(self):
        self.model_definition = ExportMetadata

        super().__init__()

    def check(self, id, operation, config, operations):
        """
        Check if a given export operation was already processed.

        """

        query = self.query(self.model).filter(
                self.model.name      == config.name,
                self.model.export_id == id,
                self.model.operation == operation,
                )

        record = query.first()

        if record == None:
            logger('info', 'Metadata check: needs processing for operation ' + \
                    operation + ' for exporter ' + config.name + ' and ID ' + id)

            return 'needs_processing'
        else:
            logger('info', 'Metadata check: already processed operation ' + \
                    operation + ' for exporter ' + config.name + ' and ID ' + id + \
                    '. Use --force to ignore metadata checks.')

            return 'already_processed'

    #
    # CRUD
    #

    def insert(self, id, operation, config, operations):
        record = self.model(
                name        = config.name,
                operation   = operation,
                collection  = config.collection,
                export_id   = id,
                export_date = datetime.datetime.now(),
                meta        = json.dumps(config.metadata),
                type        = operations[operation]['type'],
                prefix      = operations[operation]['prefix'],
                params      = operations[operation]['params'],
                )

        self.concurrent_insert(record)

    def update(self, id, operation, config, operations):
        query = self.query(self.model).filter(
                self.model.name      == config.name,
                self.model.export_id == id,
                self.model.operation == operation,
                )

        record             = query.one()
        record.collection  = config.collection
        record.export_date = datetime.datetime.now()
        record.meta        = json.dumps(config.metadata),
        record.type        = operations[operation]['type'],
        record.prefix      = operations[operation]['prefix'],
        record.params      = operations[operation]['params'],

        self.commit()

    def delete(self, id, operation, config, operations):
        query = self.query(self.model).filter(
                self.model.name      == config.name,
                self.model.export_id == id,
                self.model.operation == operation,
                )

        record = query.delete()

        self.commit()

    def reset(self, id, operation, config, operations):
        query = self.query(self.model).filter(
                self.model.name      == config.name,
                self.model.export_id == id,
                self.model.operation == operation,
                )

        record = query.first()

        if record != None:
            record.export_date = datetime.datetime.now()

            self.commit()

    def upsert(self, id, operation, config, operations):
        query = self.query(self.model).filter(
                self.model.name      == config.name,
                self.model.export_id == id,
                self.model.operation == operation,
                )

        record = query.first()

        if record == None:
            self.insert(id, operation, config, operations)
        else:
            self.update(id, operation, config, operations)

    #
    # Misc queries
    #

    @staticmethod
    def build_filepath(results, item):
        import posixpath

        folder = results[item]['type'] + 's'

        if results[item]['type'] == 'shapefile':
            extension = '.zip'
        elif results[item]['type'] == 'geotiff':
            extension = '.tiff'
            folder    = 'rasters'
        elif results[item]['type'] == 'schema' or results[item]['type'] == 'table':
            extension = '.sql.bz2'
        else:
            extension = '.bz2'

        filename = results[item]['prefix'] + '-' + results[item]['export_id'] + extension
        path     = posixpath.join('data/exports',  folder, filename)

        return path

    @staticmethod
    def build_url(results, item):
        import urllib.parse

        from lib.config import config as global_config

        if 'frontend' in global_config and 'url' in global_config['frontend']:
            base_url = global_config['frontend']['url']
        else:
            base_url = '/'

        path = ExportMetadataManager.build_filepath(results, item)
        url  = urllib.parse.urljoin(base_url, path)

        return url

    @staticmethod
    def file_size(results, item):
        path = ExportMetadataManager.build_filepath(results, item)
        path = os.path.join(base_path, path)

        if not os.path.exists(path):
            return ''

        size = os.path.getsize(path)

        from lib.utils import humansize

        return humansize(size)

    def all(self):
        # Simply list the exports path
        #if os.path.exists(exports_path):
        #    return [ os.path.join(path.relative_to(exports_path)) for path in Path(exports_path).rglob('*.zip') if path.is_file() ]

        from lib.database.sqlalchemy import rows_to_dict

        query = self.query(self.model)

        return rows_to_dict(query.all(), 'name')

    def latest(self, short = True):
        statement = """SELECT DISTINCT ON (name, operation) name, id, collection,
                       operation, export_id, export_date, type, prefix, params, meta
                       FROM meta.export
                       ORDER BY name, operation, export_date DESC;"""

        query   = self.execute(statement).all()
        results = {}

        for item in query:
            results[item.name + '_' + item.operation] = item._asdict()

        for item in results.keys():
            # Remove unwanted fields
            if short:
                del results[item]['id']

            # Add the file size field
            results[item]['size'] = self.file_size(results, item)

            # Add the file URL field
            results[item]['url'] = self.build_url(results, item)

            # Convert date fields
            results[item]['export_date'] = results[item]['export_date'].strftime('%Y-%m-%d')

            # Unserialize some fields
            if 'meta' in results[item]:
                if results[item]['meta'] != None and results[item]['meta'] != 'None':
                    results[item]['meta'] = json.loads(results[item]['meta'])

        return results
