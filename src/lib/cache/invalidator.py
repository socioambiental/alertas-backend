#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Custom cache invalidator.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import time

from dogpile.cache.region import RegionInvalidationStrategy
from lib.cache.base       import key

class Invalidator(RegionInvalidationStrategy):
    """Custom cache invalidation strategy

       A custom invalidation strategy is needed to store the invalidation
       timestamp in the cache, so cache clear operations propagate to other
       processes relying on the same cache region.

       See https://dogpilecache.sqlalchemy.org/en/latest/api.html#dogpile.cache.region.CacheRegion.invalidate

    """

    def __init__(self, region):
        self.region            = region
        self.key_soft          = key(self.region.name, self._creator_soft)
        self.key_hard          = key(self.region.name, self._creator_hard)
        self._soft_invalidated = None
        self._hard_invalidated = None

    def _creator_soft(self, reset=False):
        if reset:
            return None

        return time.time()

    def _creator_hard(self, reset=False):
        if reset:
            return None

        return time.time()

    def _set_soft_timestamp(self, *args, **kwargs):
        self._soft_invalidated = self._creator_soft(*args, **kwargs)

        self.region.set(key=self.key_soft, value=self._soft_invalidated)

    def _set_hard_timestamp(self, *args, **kwargs):
        self._hard_invalidated = self._creator_hard(*args, **kwargs)

        self.region.set(key=self.key_hard, value=self._hard_invalidated)

    def _get_soft_timestamp(self, *args, **kwargs):
        # Ignore expiration to avoid an infinite recursion
        value                  = self.region.get(key=self.key_soft, ignore_expiration=True)
        self._soft_invalidated = value if value != False else None

    def _get_hard_timestamp(self, *args, **kwargs):
        # Ignore expiration to avoid an infinite recursion
        value                  = self.region.get(key=self.key_hard, ignore_expiration=True)
        self._hard_invalidated = value if value != False else None

    # Update timestamps, as they might be changed by other processes
    def _update_timestamps(self):
        self._get_soft_timestamp()
        self._get_hard_timestamp()

    def invalidate(self, hard=None):
        if hard:
            # Clean the old value
            #self.region.delete(self.key_hard)

            self._set_soft_timestamp(reset=True)
            self._set_hard_timestamp()
        else:
            # Clean the old value
            #self.region.delete(self.key_soft)

            self._set_soft_timestamp()
            self._set_hard_timestamp(reset=True)

    def is_invalidated(self, timestamp):
        self._update_timestamps()

        return ((self._soft_invalidated and
                 timestamp < self._soft_invalidated) or
                (self._hard_invalidated and
                 timestamp < self._hard_invalidated))

    def was_hard_invalidated(self):
        self._update_timestamps()

        return bool(self._hard_invalidated)

    def is_hard_invalidated(self, timestamp):
        self._update_timestamps()

        return (self._hard_invalidated and
                timestamp < self._hard_invalidated)

    def was_soft_invalidated(self):
        self._update_timestamps()

        return bool(self._soft_invalidated)

    def is_soft_invalidated(self, timestamp):
        self._update_timestamps()

        return (self._soft_invalidated and
                timestamp < self._soft_invalidated)
