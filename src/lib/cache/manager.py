#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Cache manager.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import time

from lib.config        import config
from lib.logger        import logger
from lib.cache.base    import key
from lib.cache.regions import regions, host, redis_dbs

def get(region, creator, *args, **kwargs):
    """Wrapper for calling a function using dogpile.cache.get_or_create"""

    id = key(region, creator, *args, **kwargs)

    return regions[region].get_or_create(key=id, creator=creator, creator_args=(args, kwargs))

def set(region, creator, *args, **kwargs):
    """Wrapper for calling a function using dogpile.cache.set"""

    id     = key(region, creator, *args, **kwargs)
    result = creator(*args, **kwargs)

    regions[region].set(key=id, value=result)

    return result

def delete(region, creator, *args, **kwargs):
    id = key(region, creator, *args, **kwargs)

    regions[region].delete(id)

def clear(which_regions=regions.keys()):
    """Clear cache regions"""

    # Ensure execution only on non-Windows systems to avoid
    # dogpile.cache issues
    if os.name != 'nt':
        for region in which_regions:
            regions[region].invalidate()

def purge():
    """Purge cache"""

    if config['cache']['type'] != 'redis':
        logger('error', 'Cache purge implemented currently only for Redis.')
        exit(1)

    # Additional requirement
    import redis

    # The available databases
    dbs = list(dict.fromkeys(redis_dbs.values()))

    # Need to do this for each db
    for db in dbs:
        logger('info', 'Flushing Redis db ' + str(db) + '...')
        r = redis.Redis(host=host, db=db)
        r.flushdb()

def refresh():
    # First the cache should be cleared
    # That might not be a good idea since it leaves a bare cache
    # while this procedure computes expensive operations.
    #
    # Better to just refresh it instead.
    #clear()

    # Additional requirements
    from benchmark       import Benchmark
    from lib.api.refresh import RefreshApi

    # Use the benchmarking tool to pre-fill the events cache
    #benchmarking = Benchmark()
    #benchmarking.run(caching = True)

    # Refresh cache with some common API calls
    refresh_api = RefreshApi()
    refresh_api.run()
