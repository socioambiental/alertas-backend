#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Cache regions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os

from lib.config            import config
from dogpile.cache         import make_region
from lib.cache.invalidator import Invalidator

# Parameters
dirname    = os.path.dirname(os.path.abspath(__file__))
expiration = int(config['cache']['expiration']) if 'expiration' in config['cache'] else 86400
host       = config['cache']['host']            if 'host'       in config['cache'] else 'localhost'
redis_dbs  = {
        'events': 0,
        'tiles' : 0,
        'misc'  : 1,
        }

# Cache regions
regions = {
        # Cache region for event-related data
        'events': make_region(name='events'),

        # Cache region for tile-related data
        'tiles': make_region(name='tiles'),

        # Miscelaneous cache
        'misc': make_region(name='misc'),
        }

# Backend cache config
if config['cache']['type'] == 'dbm':
    for region in regions:
        regions[region].configure('dogpile.cache.dbm',
                expiration_time    = expiration,
                region_invalidator = Invalidator(regions[region]),
                arguments          = {
                    "filename": dirname + os.sep + ".." + os.sep + ".." + os.sep + 'cache' + os.sep + region + '.dbm'
                    }
                )

elif config['cache']['type'] == 'redis':
    for region in regions:

        # Events and tiles share the same cache database
        #if region == 'events' or region == 'tiles':
        #    db = 0
        #else:
        #    db = 1

        regions[region].configure('dogpile.cache.redis',
                expiration_time    = expiration,
                region_invalidator = Invalidator(regions[region]),
                arguments          = {
                    'host'                 : host,
                    'port'                 : 6379,
                    'db'                   : redis_dbs[region],
                    'redis_expiration_time': expiration,
                    'distributed_lock     ': True,
                    'thread_local_lock'    : False
                    }
                )

elif config['cache']['type'] == 'memcache':
    for region in regions:
        regions[region].configure('dogpile.cache.bmemcached',
                expiration_time    = expiration,
                region_invalidator = Invalidator(regions[region]),
                arguments          = {
                    'url':["127.0.0.1"],
                    }
                ),

else:
    # Fallback cache
    cache = { 'events', 'tiles', 'misc' }

    for region in regions:
        regions[region].configure('dogpile.cache.memory',
                expiration_time    = expiration,
                region_invalidator = Invalidator(regions[region]),
                arguments          = {
                    "cache_dict": cache['region'],
                    },
                )
