#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Psycopg2 functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import psycopg2
from lib.config import config
from lib.params import srid, sql_path
from lib.logger import logger

def create_connection(config_section = 'main_db', readonly = False, autocommit = False,
        lock_timeout = False):
    """
    Create a database connection using psycopg2
    """

    try:
        conn = psycopg2.connect(host=config[config_section]['host'],
                                port=config[config_section]['port'],
                                user=config[config_section]['user'],
                                password=config[config_section]['password'],
                                dbname=config[config_section]['db'])

        # For importer execution (batch mode), ensure that the connection
        # is not in autocommit mode so custom SQLs might define their own
        # transaction sequences.
        #
        # For long lived scripts (such as API and Tileserver), either ensure to
        # terminate a transaction as soon as possible or use an autocommit
        # connection.
        #
        # See https://www.psycopg.org/docs/connection.html#connection.autocommit
        #     https://www.psycopg.org/docs/usage.html#transactions-control
        #     https://www.psycopg.org/docs/connection.html
        #     https://stackoverflow.com/questions/36760285/how-to-prevent-psycopg2-locking-a-table#36929756
        conn.set_session(readonly=readonly, autocommit=autocommit)

        if lock_timeout:
            from lib.database.timeout import query_lock_timeout

            cur = conn.cursor()

            cur.execute(query_lock_timeout)

            if not autocommit:
                conn.commit()

    except psycopg2.Error as e:
        logger('exception', e)
        #conn.close()
        #return False
        exit(1)

    return conn

def execute_sql_file(file_name, config_section = 'main_db', params = None):
    """
    Execute the contents of an SQL file with a specific database configuration

    This ensures that statements are made with an specific user which might not
    be the same as the current user used in a class instance.
    """

    if not os.path.exists(file_name):
        raise FileNotFoundError('File not found: ' + file_name)
        exit(1)

    content = open(file_name, "r").read()
    conn    = create_connection(config_section)
    cur     = conn.cursor()

    if params is None:
        params = {
                'srid': srid,
                }

    try:
        cur.execute(content.format(**params))
        conn.commit()
        cur.close()
        conn.close()

    except psycopg2.Error as e:
        logger('exception', e)
        conn.close()
        #return False
        exit(1)

    cur.close()
    conn.close()

def create_functions():
    """
    Create custom PostgreSQL functions

    This function needs to be executed using the regular user
    Otherwise it would require additional "GRANT USAGE ON" to each function
    """

    execute_sql_file(os.path.join(sql_path, "bootstrap", "create_spatial_functions.sql"))

def create_schemas():
    """Create the required database schemas"""

    execute_sql_file(os.path.join(sql_path, "bootstrap", "create_schemas.sql"), 'main_db_admin', {
        'base_schema'           : 'base',
        'event_canonical_schema': 'event_canonical',
        'event_processed_schema': 'event_processed',
        'metadata_schema'       : 'meta',
        'db_user'               : config['main_db']['user'],
        'db_user_query'         : config['main_db_query']['user'],
        })

def create_event_tables():
    """Create the event tables"""

    execute_sql_file(os.path.join(sql_path, "bootstrap", "create_event_tables.sql"))
