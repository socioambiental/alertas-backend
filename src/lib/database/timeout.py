#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Database timeout handling.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.config import config

def retry_wait():
    # In the case of an operation error such as a lock timeout, wait
    # some more time and try again

    logger('warn', 'Received and operational/query cancel error. Waiting for retry...')

    if 'database' in config and 'retry_wait' in config['database']:
        retry_wait = int(config['database']['retry_wait'])
    else:
        retry_wait = 5

    import time

    time.sleep(retry_wait)

#
# Database lock timeout
#

lock_timeout = ''

if 'database' in config and 'lock_timeout' in config['database']:
    lock_timeout = config['database']['lock_timeout']
else:
    lock_timeout = '20s'

#
# Database lock_timeout query
#
# See https://stackoverflow.com/questions/20963450/controlling-duration-of-postgresql-lock-waits#20963803
#     https://www.citusdata.com/blog/2018/02/22/seven-tips-for-dealing-with-postgres-locks/
#     https://www.postgresql.org/docs/current/runtime-config-client.html#GUC-LOCK-TIMEOUT

query_lock_timeout = "SET lock_timeout TO '{lock_timeout}';".format(lock_timeout=lock_timeout)
