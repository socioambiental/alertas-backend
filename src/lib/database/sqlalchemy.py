#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# SQL Alchemy functions.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import psycopg2
from lib.config import config
from lib.logger import logger

def create_engine(config_section = 'main_db'):
    """Create the database connection engine using SQL Alchemy"""

    # Get database credentials from ~/.pgpass
    #import pgpasslib
    #config['main_db']['password'] = pgpasslib.getpass(config[config_section]['host'],
    #                                                  config[config_section]['port'],
    #                                                  config[config_section]['db'],
    #                                                  config[config_section]['user'])

    from sqlalchemy import create_engine

    db_params = {
            'host'   : config[config_section]['host'],
            'port'   : config[config_section]['port'],
            'user'   : config[config_section]['user'],
            'passwd' : config[config_section]['password'],
            'db'     : config[config_section]['db'],
            #'echo'  : True,
            }

    # Create the database engine
    try:
        engine = create_engine(
                'postgresql://{user}:{passwd}@{host}:{port}/{db}'.format(**db_params),
                #isolation_level='AUTOCOMMIT',
                )

    except psycopg2.Error as e:
        logger('exception', e)
        #return False
        exit(1)

    return engine

def rows_to_dict(rows, key):
    return { row[key] : row for row in rows_to_list(rows) }

def rows_to_list(rows):
    return [ row_to_dict(row) for row in rows ]

# Thanks https://stackoverflow.com/questions/1958219/convert-sqlalchemy-row-object-to-python-dict
def row_to_dict(row):
    d = {}

    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))

    return d

def start_session(engine, lock_timeout = False):
    """Start a database session."""

    from sqlalchemy.orm import sessionmaker

    try:
        Session = sessionmaker(bind=engine)
        session = Session()

        if lock_timeout:
            from lib.database.timeout import query_lock_timeout

            session.execute(query_lock_timeout)
            session.commit()

    except psycopg2.Error as e:
        logger('exception', e)
        #return False
        exit(1)

    return session
