#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Psycopg2 decorators.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import psycopg2
from datetime              import datetime
from lib.logger            import logger
from lib.database.psycopg2 import create_connection
from lib.database.psycopg2 import create_functions, create_schemas, create_event_tables

def connect(
        config_section = 'main_db', cursor_factory = None,
        readonly = False, autocommit = False, lock_timeout=False
        ):
    """Decorator to create the database connection using psycopg2"""

    def decorator(init):
        def decorated(self, config = {}, args = {}):
            # Database instance
            self.conn = create_connection(config_section, readonly, autocommit, lock_timeout)
            self.cur  = self.conn.cursor(cursor_factory=cursor_factory)

            init(self, config, args)

        return decorated

    return decorator

def bootstrap(run):
    """Bootstrap decorator to initialize the database"""

    def decorated(self):
        logger('info', "Initializing database...")

        try:
            create_functions()
            create_schemas()
            create_event_tables()
        except psycopg2.Error as e:
            logger('exception', e)
            self.close()
            return False

        logger('info', "Done.")

        return run(self)

    return decorated

def commit(run):
    """Database commit decorator"""

    def decorated(self):
        # Init time
        init_time = datetime.now()

        if run(self) is False:
            return False

        # Closing time
        try:
            self.conn.commit()
        except psycopg2.Error as e:
            logger('exception', e)
            self.close()
            return False

        logger('info', "Elapsed time: " + str((datetime.now() - init_time)))

        self.close()

    return decorated

def teardown(close):
    """Decorator to close the database connection"""

    def decorated(self):
        close(self)

        self.cur.close()
        self.conn.close()

    return decorated
