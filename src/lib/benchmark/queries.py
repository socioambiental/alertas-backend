#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Benchmark query parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Query parameters hyperspace definition
#

param_spaces = {
        '0': {
            'territory_types'           : [['UCF', 'TI', ]],
            'event_class'               : [ 1 ],
            'event_type'                : ['deter'],
            'output_geometry_aggregated': [False],
            'geojson'                   : [False, True],
            'group_by'                  : [ [], [ 'territory' ], [ 'territory_type' ], [ 'uf' ], [ 'mun' ],
                [ 'territory', 'territory_type', 'uf', 'mun' ],
                [ 'territory', 'uf', 'mun' ],
                [ 'territory_type', 'uf', 'mun' ],
                [ 'territory_type', 'uf' ],
                [ 'territory_type', 'mun' ],
                ],
            'name_uf'                   : [[""]],
            'name_mun'                  : [[""]],
            'id_territory'              : [[3585],[590],[4336]],
            'temporal_unit'             : ['year', 'month', 'day'],
            'date_initial'              : ['2018-01-01'],
            'date_final'                : ['2020-01-01'],
            'verbose'                   : [True]
            },

        '1': {
            'territory_types'            : [ [], ['UCF'],  ['UCF', 'TI'], ['UCF', 'TI', ]],
            'event_class'               : [ 1 ],
            'event_type'                : ['deter'],
            'output_geometry_aggregated': [False],
            'geojson'                   : [False, True],
            'group_by'                  : [ [], [ 'territory' ], [ 'territory_type' ], [ 'uf' ], [ 'mun' ],
                [ 'territory', 'territory_type', 'uf', 'mun' ],
                [ 'territory', 'uf', 'mun' ],
                [ 'territory_type', 'uf', 'mun' ],
                [ 'territory_type', 'uf' ],
                [ 'territory_type', 'mun' ],
                ],
            'name_uf'                   : [[''],['RO']],
            'name_mun'                  : [[''],['Feliz Natal']],
            #'id_territory'             : [0,3585,590,4336],
            'temporal_unit'             : ['year','month','day'],
            'date_initial'              : ['2018-01-01'],
            'date_final'                : ['2020-01-01'],
            'verbose'                   : [True]
            },
}

