#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Default parameters.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os

#
# Paths
#

src_path       = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir) + os.sep
sql_path       = os.path.join(src_path,   'sql')
base_path      = os.path.join(src_path,   os.pardir)         + os.sep
data_path      = os.path.join(base_path, 'data')             + os.sep
config_path    = os.path.join(base_path, 'config')           + os.sep
imports_path   = os.path.join(data_path, 'imports')          + os.sep
exports_path   = os.path.join(data_path, 'exports')          + os.sep
importers_path = os.path.join(base_path, 'src', 'importers') + os.sep
exporters_path = os.path.join(base_path, 'src', 'exporters') + os.sep
lock_path      = os.path.join(base_path, 'lock')             + os.sep
bin_path       = os.path.join(base_path, 'bin')              + os.sep

# The old imports path
downloads_path = os.path.join(base_path, 'downloads')        + os.sep

#
# Default database parameters
#

#db_info      = {
#                 'host'     : 'localhost',
#                 'port'     : 5432,
#                 'user'     : 'alertas_admin',
#                 'password' : 'alertas'
#               }

#
# GIS
#

# See https://epsg.io/4674
srid = 4674

legal_amazon_states = {
                        'AC': "Acre",
                        'AP': "Amapá",
                        "AM": "Amazonas",
                        "PA": "Pará",
                        "RO": "Rondônia",
                        "RR": "Roraima",
                        "TO": "Tocantins",
                        "MT": "Mato Grosso",
                        "MA": "Maranhão",
                      }

#
# Buffers
#

buffers = {
            'B1': 'Buffer 0-3km',
            'B2': 'Buffer 0-10km',
            'B3': 'Buffer 3-10km',
          }

#
# Pipeline
#

# Phases
pipeline_phases = {
        'el':  [ 'extract', 'load' ],
        'lt':  [ 'load', 'transform' ],
        'elt': [ 'extract', 'load', 'transform' ],
        'etl': [ 'extract', 'transform', 'load' ],
        }

# Previous phases
pipeline_previous_phases = {
        'el': {
            'extract'  : '',
            'load'     : 'extract',
            },
        'lt': {
            'load'     : '',
            'transform': 'load',
            },
        'elt': {
            'extract'  : '',
            'load'     : 'extract',
            'transform': 'load',
            },
        'etl': {
            'extract'  : '',
            'transform': 'extract',
            'load'     : 'transform',
            },
        }

# Next phases
pipeline_next_phases = {
        'el': {
            'extract'  : [ 'load'              ],
            'load'     : [                     ],
            },
        'lt': {
            'load'     : [ 'transform'         ],
            'transform': [                     ],
            },
        'elt': {
            'extract'  : [ 'load', 'transform' ],
            'load'     : [ 'transform'         ],
            'transform': [                     ],
            },
        'etl': {
            'extract'  : [ 'transform', 'load' ],
            'transform': [ 'load'              ],
            'load'     : [                     ],
            },
        }

#
# Misc
#

copyright_year   = '2021'
copyright_holder = 'Instituto Socioambiental'

def copyright_notice(program):
    copyright_notice = (
                        "{program} Copyright (C) {copyright_year} {copyright_holder}\n\n"
                        "  This program comes with ABSOLUTELY NO WARRANTY;\n"
                        "  This is free software, and you are welcome to redistribute it under\n"
                        "  certain conditions; see LICENSE for details."
                        ).format(
                                 program=program,
                                 copyright_year=copyright_year,
                                 copyright_holder=copyright_holder
                                )

    return copyright_notice
