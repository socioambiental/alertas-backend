#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import argparse
from importlib  import import_module
from lib.params import exporters_path
from lib.params import copyright_notice
from lib.logger import logger
from lib.utils  import to_camel_case

# Exporters list
exporters = os.listdir(exporters_path)

# Current folder
dirname = os.path.dirname(__file__)

class Exporter:
    """Process exporters"""

    def __init__(self, args):
        """
        Class initialization.

        Instantiate class properties and run basic checks.

        :param args dict: Configuration arguments.
        """

        if args.exporter is not None:
            self.exporters = [ args.exporter ]
        else:
            self.exporters = exporters

        # Currently there's only one exporter phase
        #if args.phase is not None:
        #    self.phases = [ args.phase ]
        #else:
        #    self.phases = None
        self.phases = [ 'export' ]

        # Configure date
        if 'date' in args and args.date != None:
            self.date = args.date
        else:
            self.date = None

        self.abort_on_error = args.abort_on_error
        self.force          = args.force
        self.progress       = args.progress

    def run(self):
        """Run exporters"""

        for exporter in self.exporters:
            package_name = 'exporters.' + exporter
            config_name  = package_name + '.config'
            config       = import_module(config_name)

            # Defaults to an ELT pipeline
            if 'pipeline' not in dir(config):
                config.pipeline = 'elt'

            if self.phases == None:
                self.phases = pipeline_phases[config.pipeline]

            for phase in self.phases:
                if os.path.exists(os.path.join(dirname, 'exporters', exporter, phase + '.py')):
                    # Import the appropriate module
                    # Use import_module() in favour of __import__
                    # See https://docs.python.org/3.8/library/importlib.html#importlib.import_module
                    #     https://stackoverflow.com/questions/9806963/how-to-use-the-import-function-to-import-a-name-from-a-submodule#37308413
                    module_name = package_name + '.' + phase
                    class_name  = to_camel_case(exporter) + to_camel_case(phase)
                    #module     = __import__(module_name, fromlist=[None])
                    module      = import_module(module_name)
                    args        = { 'date': self.date, 'force': self.force, 'progress': self.progress, }

                    # Get the appropriate class
                    module_class = getattr(module, class_name)

                    # Instantiate
                    instance = module_class(config, args)

                    if instance is False:
                        logger('error', 'Error initializing {instance}'.format(instance=module_name))

                        if self.abort_on_error:
                            exit(1)
                        else:
                            break

                    logger('info', 'Processing phase {phase} from exporter {exporter}'.format(
                        phase=phase, exporter=exporter))

                    # Try to run the phase
                    # On error, skip further phases of the current exporter
                    try:
                        if instance.run() is False:
                            if self.abort_on_error:
                                logger('error', 'Error processing {phase} from {exporter}, aborting'.format(
                                    phase=phase, exporter=exporter))
                                exit(1)
                            else:
                                break
                    except Exception as e:
                        logger('exception', e)

                        if self.abort_on_error:
                            exit(1)
                        else:
                            break

def cmdline():
    """
    Evalutate the command line.

    :return: Command line arguments.
    """

    basename = os.path.basename(__file__)
    notice   = copyright_notice(basename)

    # Parse CLI
    #examples  = "Examples:\n\t" + basename + " --no-progress \n"
    examples  = "Examples:\n\t" + basename + " --phase load deter\n"

    epilog = examples + "\n\n" + notice
    parser = argparse.ArgumentParser(description='Run an exporter',
                                     epilog=epilog,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,)

    parser.add_argument('exporter', nargs='?',
            help='Run only a given exporter. Available exporters: ' + \
                    ' '.join(exporters) + '. Default: all exporters.')

    parser.add_argument('date', nargs='?', help='Date in the Ymd (year month day) format ' + \
            'for the extract and load phases. Support for arbitrary dates varies for each exporter. ' + \
            'Default value will be guesses also depending on the exporter. Some exporters might just ignore this parameter entirely.')

    #parser.add_argument('--phase',
    #        help='Run only the specific phase. Possible phases (availability depends on the selected exporter): ' + \
    #                ' '.join(pipeline_phases['elt'])  + '. Default: all phases.')

    parser.add_argument('--abort-on-error', dest='abort_on_error', action='store_true',
                        help='Abort on the first import error.')

    parser.add_argument('--do-not-abort-on-error', dest='abort_on_error', action='store_false',
                        help='Do not abort on errors.')

    parser.add_argument('--progress', dest='progress', action='store_true',
                        help='Enable progress bar for exporters/phases that supports it.')

    parser.add_argument('--no-progress',   dest='progress', action='store_false',
                        help='Disable progress bar.')

    parser.add_argument('--force', dest='force', action='store_true',
                        help='Force importing, skipping metadata checks.')

    # Add default values and get args
    parser.set_defaults(progress=True)
    parser.set_defaults(force=False)
    parser.set_defaults(abort_on_error=True)
    args = parser.parse_args()

    # Print copyright notice
    #print(notice + "\n")

    return args

if __name__ == "__main__":
    args = cmdline()

    exporter = Exporter(args)
    exporter.run()
