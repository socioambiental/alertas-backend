-- Set search path to include the wanted schemas in order
SET search_path to event_canonical,public;

-- Ensure the temporary table exists in any case so a further SELECT
-- does not fail.
CREATE TABLE IF NOT EXISTS {e_type}_canonical_old ({date_field} date);

-- Insert newer events that have been included by any daily imports but aren't
-- present in the Arquive package, avoiding any gaps in the dataset
--
-- This script may be called multiple times but only in the FIRST run that date
-- from the old table is inserted. After this first run, the old table is
-- deleted so subsequent runs doesn't produce duplicate data.
INSERT INTO {e_type}_canonical (latitude,longitude,scan,track,acq_date,acq_time,satellite,instrument,confidence,version,frp,daynight,geom)
    SELECT latitude,longitude,scan,track,acq_date,acq_time,satellite,instrument,confidence,version,frp,daynight,geom
        FROM {e_type}_canonical_old f
        WHERE f.{date_field} > '{latest}'::DATE;

-- Deletes the old table
DROP TABLE IF EXISTS "{e_type}_canonical_old";
DROP INDEX IF EXISTS "idx_{e_type}_canonical_geom_old";
