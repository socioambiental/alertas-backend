SET search_path to event_canonical,public;

-- Create table if it doesn't exist
-- This is not needed since the loading phase already ensures the model exists
--CREATE TABLE IF NOT EXISTS {e_type}_canonical (
--    id         BIGSERIAL NOT NULL,
--    latitude   double precision,
--    longitude  double precision,
--    scan       double precision,
--    track      double precision,
--    acq_date   date,
--    acq_time   character varying COLLATE pg_catalog."default",
--    satellite  character varying COLLATE pg_catalog."default",
--    instrument character varying COLLATE pg_catalog."default",
--    confidence character varying COLLATE pg_catalog."default",
--    version    character varying COLLATE pg_catalog."default",
--    frp double precision,
--    daynight   character varying COLLATE pg_catalog."default",
--    geom       geometry(Point),
--    CONSTRAINT {e_type}_canonical_pkey PRIMARY KEY (id)
--);

-- The index might not be created along with the data model, so ensure it exists
CREATE INDEX IF NOT EXISTS idx_{e_type}_canonical_geom ON {e_type}_canonical USING gist(geom);

-- Load arquive fire hotspot data on canonical table
INSERT INTO {e_type}_canonical (latitude,longitude,scan,track,acq_date,acq_time,satellite,instrument,confidence,version,frp,daynight,geom)
    SELECT latitude,longitude,scan,track,acq_date,acq_time,satellite,instrument,confidence,version,frp,daynight,geom
        FROM "{input_table}" f
        JOIN base.BLA_BASE250_wgs84 bla ON ST_WITHIN(f.geom,bla.shape);

-- Deletes previous work table
DROP TABLE IF EXISTS "{input_table}";
