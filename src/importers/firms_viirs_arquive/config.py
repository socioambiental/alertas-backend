#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# FirmsViirs Arquive default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic configuration
name         = 'firms_viirs_arquive'
collection   = 'firms_viirs'
description  = 'Imports VIIRS fire data archive downloads'
dependencies = [ 'isa', 'biomass', 'land_cover' ]

# Available datasets, location and filename params
download_id = 239607
datasets    = [ 'arquive' ]
base_url    = 'https://firms.modaps.eosdis.nasa.gov/data/download/DL_FIRE_SV-C2_{download_id}.zip'
base_prefix = 'firms_viirs-'
shapefiles  = ['fire_nrt_SV-C2_{id}.shp', 'fire_archive_SV-C2_{id}.shp']

# How to deal with new data
update_mode = 'truncate_old_data'
date_field  = 'acq_date'

# Date pattern in the dataset file name
#date_pattern = r"([0-9]{4})([a-zA-Z][a-z]{2})([0-9]{2})"

# Hardcoded month table as used in the dataset file name
# months = {
#            'Jan': '01',
#            'Fev': '02',
#            'Mar': '03',
#            'Abr': '04',
#            'Mai': '05',
#            'Jun': '06',
#            'Jul': '07',
#            'Ago': '08',
#            'Set': '09',
#            'Out': '10',
#            'Nov': '11',
#            'Dez': '12'
#          }

# Class information in the final converted dataset, after transform stage
from importers.firms_viirs.config import classes

# Metadata
# Currently FIRMS VIIRS disclaimers are making no difference between NRT and Standard processing
from importers.firms_viirs.config import metadata

# Archive management
from importers.firms_viirs.config import archive
