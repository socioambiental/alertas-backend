#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# FirmsViirs database model.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import Table, Column, Integer, String, Date, Float
from geoalchemy2                import Geometry
#from sqlalchemy                import UniqueConstraint
#from sqlalchemy                import func, Index

Base = declarative_base()

class FirmsViirsCanonical(Base):
    """The FirmsViirs canonic model."""

    __tablename__  = 'firms_viirs_canonical'
    __table_args__ = ({ 'schema': 'event_canonical'},)
    id             = Column(Integer, primary_key=True)
    latitude       = Column(Float)
    longitude      = Column(Float)
    #bright_ti4    = Column(Float)
    scan           = Column(Float)
    track          = Column(Float)
    acq_date       = Column(Date)
    acq_time       = Column(String)
    satellite      = Column(String)
    instrument     = Column(String)
    confidence     = Column(String)
    version        = Column(String)
    #bright_ti5    = Column(Float)
    frp            = Column(Float)
    daynight       = Column(String)
    geom           = Column(Geometry('POINT'))
