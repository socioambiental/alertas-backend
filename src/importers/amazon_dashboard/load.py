#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Amazon Dashboard dataset loader.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import datetime
from lib.importer.load.shapely import ShapelyLoad

class AmazonDashboardLoad(ShapelyLoad):
    """Process an Amazon Dashboard shapefile."""

    def create_model(self):
        """Create the database model."""

        # Import model definition
        from .datamodel import AmazonDashboardCanonical

        # Ensure the database has the modeled tables and cols
        AmazonDashboardCanonical.metadata.create_all(self.engine)

        return AmazonDashboardCanonical

    def get_total_records(self):
        return len(self.shape.shapes())

    def add_record(self, record, session, Model):
        """
        Add a single shape record into the database.

        :param record  object: Shapefile record (pyshp).
        :param session object: Database session.
        :param Model   object: Model.
        """

        from geoalchemy2.shape import to_shape,from_shape
        from shapely.geometry  import Polygon,shape
        #from geoalchemy2      import Geometry

        # Some debugging
        #print(record.shape.__geo_interface__)
        #print(record.record)
        #print(record.shape.__geo_interface__['type'])

        year = self.get_year()

        # Since recent changes (2021) in the Amazon Dashboard dataset,
        # seems like the latest dump has only the current year of events
        # and has not the "start_date" and "last_date" fields anymore.
        #
        # To compute the start and end date we need to use start_DOY
        # and last_DOY.
        if year >= 2021:
            new_year    = datetime.date(year, 1, 1)
            start_doy   = int(record.record[15])
            start_delta = datetime.timedelta(start_doy - 1)
            start_date  = new_year + start_delta
            last_doy    = int(record.record[16])
            last_delta  = datetime.timedelta(last_doy - 1)
            last_date   = new_year + last_delta

            # Put shape record attributes into the event structure
            record = Model(
                    cluster_ID = record.record[0],
                    fire_type  = record.record[1],
                    confidence = record.record[2],
                    deforestat = record.record[3],
                    tree_cover = record.record[4],
                    biomass    = record.record[5],
                    frp        = record.record[6],
                    fire_count = record.record[7],
                    size       = record.record[8],
                    persistenc = record.record[9],
                    daytime    = record.record[10],
                    progressio = record.record[11],
                    biome      = record.record[12],
                    #region    = record.record[13],
                    protected  = record.record[14],
                    start_DOY  = start_doy,
                    last_DOY   = last_doy,
                    is_new     = record.record[17],
                    is_active  = record.record[18],
                    start_date = start_date,
                    last_date  = last_date,
                    #geom      = Polygon(tuple(record.shape.points)).wkt,
                    geom       = shape(record.shape.__geo_interface__).wkt,
                    )
        else:
            # Put shape record attributes into the event structure
            record = Model(
                    cluster_ID = record.record[0],
                    fire_type  = record.record[1],
                    confidence = record.record[2],
                    deforestat = record.record[3],
                    tree_cover = record.record[4],
                    biomass    = record.record[5],
                    frp        = record.record[6],
                    fire_count = record.record[7],
                    size       = record.record[8],
                    persistenc = record.record[9],
                    daytime    = record.record[10],
                    progressio = record.record[11],
                    biome      = record.record[12],
                    country    = record.record[13],
                    state      = record.record[14],
                    protected  = record.record[15],
                    start_DOY  = record.record[16],
                    last_DOY   = record.record[17],
                    is_new     = record.record[18],
                    is_active  = record.record[19],
                    last_date  = record.record[20],
                    start_date = record.record[21],
                    #geom      = Polygon(tuple(record.shape.points)).wkt,
                    geom       = shape(record.shape.__geo_interface__).wkt,
                    )

        session.add(record)
