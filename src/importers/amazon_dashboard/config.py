#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Amazon Dashboard default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic configuration
name         = 'amazon_dashboard'
collection   = 'amazon_dashboard'
description  = 'Imports fire data from Amazon Dashboard / GFED shapefile downloads'
dependencies = [ 'isa', 'biomass', 'land_cover' ]

# Available datasets, location and filename params
datasets    = [ 'main' ]
base_prefix = 'amazon_dashboard_data_'
base_suffix = ''
shapefiles  = ['fire_atlas_events_{id}.shp']

# How to deal with new data
#
# Do not acumulate data, but remove only the current dataset year
# as each dataset file has only the current year.
#
# Then, sequential daily runs from the same year needs to remove
# previous data from the same year while preserving data from
# past years.
update_mode = 'truncate_dataset_year'

# The base URL
#base_url = 'https://www.globalfiredata.org/data/amazonDashboard/amazon_dashboard_data_{date}.zip'
#base_url = 'https://globalfiredata.org/data/amazonDashboard/amazon_dashboard_data.zip'
#base_url = 'https://globalfiredata.org/pages/pt/amazon-dashboard/'
#base_url = 'https://www.globalfiredata.org/tools/amazonDashboard.html'
base_url  = 'https://www.globalfiredata.org/tools/js/amazonDashboard_cronjob.js'

# Date pattern in the dataset file name
date_pattern = r"[0-9]{8}"

# Pattern used to extract the latest dataset date from the Javascript base_url
last_updated_pattern = r'let zipLastUpdated = "[0-9]{8}"'

# Zip file URL
zip_url         = 'https://globalfiredata.org/data/amazonDashboard/amazon_dashboard_data_{date}.zip'
zip_url_archive = 'https://globalfiredata.org/data/amazonDashboard/amazon_dashboard_data.zip'

# HTML element ID where the link latest download link is located
# to be used with the HTML base_url
#link_id     = 'gfed-ad-zipfile'

# Class information in the final converted dataset, after transform stage
classes = {
            1: {
                'name': 'savanna_grassland_fire',
                'subclasses': {
                    1: 'fire_cluster'
                    },
                'emission_factor': 0.57
                },
            2: {
                'name': 'small_clearing_and_agriculture_fire',
                'subclasses': {
                    1: 'fire_cluster'
                    },
                'emission_factor': 0.57
                },
            3: {
                'name': 'understory_fire',
                'subclasses': {
                    1: 'fire_cluster'
                    },
                'emission_factor': 0.57
                },
            4: {
                'name': 'deforestation_fire',
                'subclasses': {
                    1: 'fire_cluster'
                    },
                'emission_factor': 0.57
                }
          }

# Metadata
metadata = {
        'name'           : 'Amazon Dashboard',
        'description'    : {
            'en'   : 'The Amazon Dashboard tracks individual fires in the Amazon region using a new approach to cluster and classify VIIRS active fire detections by fire type. Based on the fire location, intensity, duration, and spread rate, each individual event is classified as a deforestation fire, understory forest fire, small clearing & agricultural fire, or savanna fire. The data and summary figures are updated daily using the combined active fire detections from the VIIRS instruments on the Suomi-NPP and NOAA-20 satellites.',
            'pt-br': 'O Amazon Dashboard rastreia incêndios individuais na região amazônica usando uma nova abordagem para agrupar e classificar as detecções de incêndio ativas (VIIRS) por tipo de incêndio. Com base na localização, intensidade, duração e taxa de propagação do fogo, cada evento individual pode ser classificado como: incêndio de desmatamento, incêndio florestal de sub-bosque, pequena clareira e incêndio agrícola, ou incêndio na savana.',
            'es'   : 'El Amazon Dashboard rastrea los incendios individuales en la región Amazónica utilizando un nuevo enfoque para agrupar y clasificar las detecciones de incendios activas de VIIRS por tipo de incendio. Según la ubicación, la intensidad, la duración y la tasa de propagación del incendio, cada evento individual se clasifica como incendio de deforestación, incendio de bosque en el sotobosque, incendio de pequeños claros y agrícolas o incendio de sabana. Los datos y las cifras resumidas se actualizan diariamente utilizando las detecciones de incendios activas combinadas de los instrumentos VIIRS en los satélites Suomi-NPP y NOAA-20. ',
            },
        'url'            : 'https://globalfiredata.org/pages/pt/amazon-dashboard/',
        'methodology_url': 'https://essd.copernicus.org/articles/11/529/2019/',
        'license_type'   : 'custom',
        'license_url'    : None,
        'license_text': {
            'en'   : '',
            'pt-br': '',
            'es'   : '',
            },
        'citation_doi': '10.5194/essd-11-529-2019',
        'citation'    : {
            'en'   : 'Data from the Amazon Dashboard are provided by NASA, Cardiff University, and UC-Irvine, with support from the NASA SERVIR Program',
            'pt-br': 'Dados do Amazon Dashboard são providor pela NASA, Universidade de Cardiff a UC-Irvine, com o apoio do Programa NASA SERVIR',
            'es'   : 'Datos de Amazon Dashboard son proporcionados por la NASA, la Universidad de Cardiff y UC-Irvine, con el apoyo del Programa SERVIR de la NASA',
            },
        }

# Archive management
archive = {
        'redistribute': False, # Download is available and encouraged, but there is no redistribution license
        'safeguard'   : False,
        }
