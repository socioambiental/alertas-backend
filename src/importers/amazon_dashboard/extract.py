#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Amazon Dashboard dataset extractor.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import requests
import re
import datetime
#from bs4                     import BeautifulSoup
from lib.importer.extract.zip import ZipExtract
from lib.params               import imports_path
from lib.logger               import logger
from .config                  import base_url
from .config                  import date_pattern, last_updated_pattern, zip_url, zip_url_archive, base_prefix, base_suffix
#from .config                 import link_id

class AmazonDashboardExtract(ZipExtract):
    """Downloads an Amazon Dashboard dataset."""

    def build_url(self, dataset):
        """Determine download url by scraping Amazon Dashboard site"""

        # An arbitrary date was specified
        if 'date' in self.args and self.args['date'] != None:
            self.id = str(self.args['date'])

            # Check if the year is the current or past
            #
            # This is required because Amazon Dashboard currently have to
            # patterns for the current year's data URL and for the archived
            # data
            today        = datetime.datetime.today()
            year         = today.strftime('%Y')
            dataset_year = self.id[0:4]
            real_zip_url = zip_url if year == dataset_year else zip_url_archive

            # Set the downloaded filename path prefix
            url                  = real_zip_url.format(date=self.id)
            filename             = base_prefix + self.id + base_suffix + '.zip'
            self.output[dataset] = os.path.join(imports_path,
                     self.config.name.lower(),
                     'packaged', dataset, filename)

            logger('info', 'Selected date is ' + self.id + '.')

            return url

        logger('info', 'Trying to get the latest dataset date from ' + base_url + '...')

        # No arbitrary date was specified, so try to determine latest from upstream
        try:
            with requests.get(base_url) as r:
                # Raise exceptions when they happen
                r.raise_for_status()

                # Beautiful Soup implementation when using the dashboard HTML location as base_url
                #soup = BeautifulSoup(r.text, 'html.parser')
                #link = soup.find(id=link_id)
                #url  = link.get('href')

                # Parse latest data from the Javascript cronjob.js
                search_latest = re.search(last_updated_pattern, r.text)

                if search_latest:
                    latest      = search_latest.group(0)
                    search_date = re.search(date_pattern, latest)

                    if search_date:
                        self.id = search_date.group(0)
                        url       = zip_url.format(date=self.id)
                        filename  = base_prefix + self.id + base_suffix + '.zip'

                        # Set the downloaded filename path prefix
                        self.output[dataset] = os.path.join(imports_path,
                                 self.config.name.lower(),
                                 'packaged', dataset, filename)

                        logger('info', 'Latest is ' + self.id + '.')

                        return url
                    else:
                        return False
                else:
                    return False

        except requests.ConnectionError as e:
            logger('exception', e)
            return False
            #exit(1)

        except requests.exceptions.HTTPError as e:
            logger('exception', e)
            return False
            #exit(1)

        except requests.exceptions.Timeout as e:
            logger('exception', e)
            return False
            #exit(1)

        except requests.exceptions.TooManyRedirects as e:
            logger('exception', e)
            return False
            #exit(1)

    def process_headers_info(self, dataset, r):
        """
        Process HTTP Headers from a dataset request.
        In the case of the Amazon Dashboard we simply do nothing
        as the file name and date are determined by build_url().

        :param dataset str: Dataset name.
        :param r object: Requests object from dataset endpoint.
        """

        return
