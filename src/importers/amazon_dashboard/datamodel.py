#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Amazon Dashboard database model.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import Table, Column, Integer, String, Date, Float
from geoalchemy2                import Geometry
#from sqlalchemy                import UniqueConstraint
#from sqlalchemy                import func, Index

Base = declarative_base()

class AmazonDashboardCanonical(Base):
    """The Amazon dashboard fire events canonic model."""

    __tablename__  = 'amazon_dashboard_canonical'
    __table_args__ = ({ 'schema': 'event_canonical'},)
    id             = Column(Integer, primary_key=True)
    # Cluster ID is not unique among different datasets (eg. 20201231 and 20210506)
    #cluster_ID    = Column(Integer, primary_key=True)
    cluster_ID     = Column(Integer)
    fire_type      = Column(Integer)
    confidence     = Column(Integer)
    deforestat     = Column(Float)
    tree_cover     = Column(Float)
    biomass        = Column(Float)
    frp            = Column(Float)
    fire_count     = Column(Float)
    size           = Column(Float)
    persistenc     = Column(Float)
    daytime        = Column(Float)
    progressio     = Column(Float)
    biome          = Column(Integer)
    country        = Column(Float)
    state          = Column(Float)
    #region        = Column(Float)
    protected      = Column(Float)
    start_DOY      = Column(Integer)
    last_DOY       = Column(Integer)
    is_new         = Column(Integer)
    is_active      = Column(Integer)
    last_date      = Column(Date)
    start_date     = Column(Date)
    geom           = Column(Geometry('GEOMETRY'))
