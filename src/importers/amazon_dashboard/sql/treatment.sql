-- Overlap treatment of Amazon Dashboard dataset

-- Set search path to include the wanted schemas in order
SET search_path to event_processed,public;

--
-- BEGIN transaction for insertion
--
BEGIN;

-- Do not create a partition right now to avoid a lock in the event table in
-- case of simultaneous imports
--DROP TABLE IF EXISTS event_{e_type};
--CREATE TABLE event_{e_type} PARTITION OF event
--    FOR VALUES IN ('{e_type}');

-- Deletes any previous work table
DROP TABLE IF EXISTS event_{e_type}_new;

-- Inserts previously treated records on the event table
--SELECT Populate_Geometry_Columns('public.event'::regclass);
--CREATE TABLE event_{e_type} AS
CREATE TABLE event_{e_type}_new (LIKE event INCLUDING DEFAULTS INCLUDING CONSTRAINTS);
INSERT INTO event_{e_type}_new (orig_id,type,class_1,class_2,date,coduf,uf,codmun,mun,geom)
  SELECT a.orig_id AS orig_id,
    a.type AS type, a.class_1 AS class_1, a.class_2 AS class_2,
    a.date AS date, mun.cod_uf AS coduf, mun.siglauf AS uf, mun.cod_mun AS codmun,
    mun.nome AS mun, geom AS geom
  FROM event_canonical.{e_type}_tmp a
  JOIN base.MUN_base250 mun ON st_within(ST_Centroid(a.geom),mun.shape);

DROP TABLE event_canonical.{e_type}_tmp;

--
-- COMMIT transaction for insertion
--
COMMIT;
