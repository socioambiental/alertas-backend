#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ISA default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic configuration
name         = 'prodes_acumulado_dblink'
collection   = 'prodes_acumulado'
description  = 'Imports PRODES accumulated deforestation using a direct dblink connection with ISA internal database'
dependencies = [ 'isa', 'biomass', 'land_cover' ]
pipeline     = 'lt'

# The "prodes_acumulado" "event" type is a pseudo-event in the sense that
# it does not represent an actual event that might have taken place somewhere
# in space-time.
#
# Instead, it contains data that represents the accumulated deforestation
# until the year 2007, which _in theory_ means an hypothetical accumulated
# deforestation since the "year" minus infinity until the year 2007 from
# the Gregorian calendar.
#
# This data is then used to compute the _total_ deforestation in a territory
# selection since minus infinity to the desired year by summing up data from
# this event type with the regular PRODES event dataset.
accumulated_until = 2007

# Available datasets, location and filename params
download_id = accumulated_until

# Class information in the final converted dataset, after transform stage
from importers.prodes_dblink.config import classes

# Metadata
from importers.deter.config import metadata as deter_metadata
metadata                = dict(deter_metadata)
metadata['name']        = 'PRODES Acumulado'
metadata['description'] = {
            'en'   : 'PRODES/INPE Accumulated Deforestation',
            'pt-br': 'Desmatamento acumulado PRODES/INPE',
            'es'   : 'Desmatamiento acumulado PRODES/INPE',
        }

# Archive management
from importers.deter.config import archive
