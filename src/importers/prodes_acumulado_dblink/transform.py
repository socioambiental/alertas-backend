#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# PRODES dataset transformer.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from lib.database.decorators.psycopg2 import connect
from lib.importer.transform.sql       import SqlTransform
from lib.params                       import srid, importers_path
from .config                          import classes

class ProdesAcumuladoDblinkTransform(SqlTransform):
    @connect()
    def __init__(self, config, args = {}):
        self.dirname = os.path.dirname(__file__)

        super().__init__(config, args)

        # Needed for carbon emissions calculation
        self.classes = classes

        # Re-use PRODES' SQL
        self.sql_local = os.path.join(importers_path, "prodes", "sql") + os.sep

        self.operations['transform'] = {
                'info'            : 'Initial processing of {collection} dataset',
                'sql'             : open(self.sql_local + "transform.sql", "r").read(),
                'pivot_partition' : 'event',
                'params'          : {
                        'srid'                   : srid,
                        'e_type'                 : self.config.collection,
                        'canonical_union_clauses': '',
                    },
                }

        self.operations['compute'] = {
                'info' : 'Intersecting {collection} datasets with territories',
                'sql'  : open(self.sql_global + "compute_event_territory_table.sql", "r").read(),
                }

        self.operations['process'] = {
                'info'            : 'Processing {collection} intersection with territories',
                'sql'             : open(self.sql_global + "process_event_territory_table.sql", "r").read(),
                'pivot_partition' : 'event_territory',
                }

        self.operations['carbon_emissions'] = {
               'info': 'Computing associated carbon emissions',
               'sql' : open(self.sql_global + "compute_carbon_emissions.sql", "r").read(),
               }
