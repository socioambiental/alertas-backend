-- Overlap treatment of {e_type} dataset

-- Set search path to include the wanted schemas in order
SET search_path to event_processed,public;

--
-- BEGIN transaction for computation
--
BEGIN;

-- Deletes any previous work table
DROP TABLE IF EXISTS {e_type}_tmp2;

-- Creates {e_type}_tmp2 table with original ({e_type}_tmp) polygons treated
CREATE TABLE {e_type}_tmp2 AS
  SELECT orig_id, type, view_date,class_1,class_2, COALESCE(
    safe_diff(geom,
      (SELECT st_union(b.geom)
       FROM event_canonical.{e_type}_tmp b
       WHERE
        (a.class_1=1 or a.class_1=2) AND (b.class_1=1 or b.class_1=2)
        AND a.id!=b.id
        AND a.UF = b.UF
        --AND safe_intersects(a.geom,b.geom)
        AND (a.geom && b.geom)
        AND a.view_date<b.view_date
      )
    ),a.geom)
     AS geom
FROM event_canonical.{e_type}_tmp a;

-- Optimizes the {e_type}_tmp table
SELECT UpdateGeometrySRID('{e_type}_tmp2', 'geom', {srid});
--SELECT Populate_Geometry_Columns('public.{e_type}_tmp2'::regclass);
ALTER TABLE {e_type}_tmp2 ADD ID SERIAL PRIMARY KEY;
CREATE INDEX sidx_{e_type}_tmp2_geom
    ON {e_type}_tmp2 USING gist
    (geom)
    TABLESPACE pg_default;

--
-- COMMIT transaction for computation
--
COMMIT;

--
-- BEGIN transaction for insertion
--
BEGIN;

-- Do not create a partition right now to avoid a lock in the event table in
-- case of simultaneous imports
--DROP TABLE IF EXISTS event_{e_type};
--CREATE TABLE event_{e_type} PARTITION OF event
--    FOR VALUES IN ('{e_type}');

-- Deletes any previous work table
DROP TABLE IF EXISTS event_{e_type}_new;

--SELECT Populate_Geometry_Columns('public.event'::regclass);
--CREATE TABLE event_{e_type} AS
CREATE TABLE event_{e_type}_new (LIKE event INCLUDING DEFAULTS INCLUDING CONSTRAINTS);
INSERT INTO event_{e_type}_new (orig_id,type,class_1,class_2,date,coduf,uf,codmun,mun,geom)
  SELECT a.orig_id AS orig_id, a.type AS type, a.class_1 AS class_1, a.class_2 AS class_2,
         a.view_date AS date, mun.cod_uf AS coduf, mun.siglauf AS uf, mun.cod_mun AS
         codmun, mun.nome AS mun, a.geom as geom
  FROM {e_type}_tmp2 a
  JOIN base.MUN_base250 mun ON st_within(ST_Centroid(a.geom),mun.shape);

-- Deletes work tables
DROP TABLE IF EXISTS event_canonical.{e_type}_tmp, {e_type}_tmp2;

--
-- COMMIT transaction for insertion
--
COMMIT;
