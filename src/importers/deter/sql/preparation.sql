-- Initial preparation of {e_type} dataset

-- Set search path to include the wanted schemas in order
SET search_path to event_canonical,public;

-- Deletes any previous work table
DROP TABLE IF EXISTS {e_type}_tmp;

-- Creates {e_type}_tmp table with the original polygons, recategorized and geometricaly optimized
CREATE TABLE {e_type}_tmp AS
    WITH e_explosed_clean AS (
        WITH e_explosed AS (
            SELECT id AS orig_id, view_date, uf,
                CASE
                  WHEN classname='DESMATAMENTO_CR'      OR classname='DESMATAMENTO_VEG' OR classname='aviso'  THEN 1
                  WHEN classname='MINERACAO'                                                                  THEN 2
                  WHEN classname='CICATRIZ_DE_QUEIMADA'                                                       THEN 3
                  WHEN classname='CORTE_SELETIVO'       OR classname='CS_DESORDENADO'
                    OR classname='CS_GEOMETRICO'        OR classname='DEGRADACAO'                             THEN 4
                END class_1,
                CASE
                   WHEN classname='aviso' THEN 'DESMATAMENTO_CR'
                   ELSE classname
                END class_2,
                st_makevalid((st_dump(geom)).geom) as geom
            FROM {e_type}_canonical)
        SELECT * from e_explosed WHERE st_area(geom::geography)>10 and st_geometrytype(geom)='ST_Polygon')
    SELECT orig_id, class_1, class_2, view_date,uf, st_buffer(st_snaptogrid(geom, 0.00001),0) as geom
    FROM e_explosed_clean;

-- Populate Geometry columns
ALTER TABLE {e_type}_tmp ADD COLUMN type varchar;
UPDATE {e_type}_tmp SET type='{e_type}';
--SELECT Populate_Geometry_Columns('public.{e_type}_tmp'::regclass);

-- Creates Indexes
ALTER TABLE {e_type}_tmp ADD ID SERIAL PRIMARY KEY;
CREATE INDEX sidx_{e_type}_tmp_geom
    ON {e_type}_tmp USING gist
    (geom)
    TABLESPACE pg_default;
ANALYZE {e_type}_tmp;

-- Change the storage type
ALTER TABLE {e_type}_tmp
  ALTER COLUMN geom
  SET STORAGE EXTERNAL;

-- Force the column to rewrite
UPDATE {e_type}_tmp SET geom = ST_SetSRID(geom, {srid});
