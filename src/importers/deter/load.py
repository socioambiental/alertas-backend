#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# DETER dataset loader.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.importer.load.shapely import ShapelyLoad

class DeterLoad(ShapelyLoad):
    """Process a DETER shapefile."""

    def create_model(self):
        """Create the database model."""

        # Import model definition
        from .datamodel import DeterCanonical

        # Ensure the database has the modeled tables and cols
        DeterCanonical.metadata.create_all(self.engine)

        return DeterCanonical

    def add_record(self, record, session, Model):
        """
        Add a single shape record into the database.

        :param record  object: Shapefile record (pyshp).
        :param session object: Database session.
        :param Model   object: Model.
        """

        from shapely.geometry import Polygon, shape

        # Put shape record attributes into the event structure
        record = Model(
                classname  = record.record[0],
                quadrant   = record.record[1],
                path_row   = record.record[2],
                view_date  = record.record[3],
                sensor     = record.record[4],
                satellite  = record.record[5],
                areauckm   = record.record[6],
                uc         = record.record[7],
                areamunkm  = record.record[8],
                municipali = record.record[9],
                uf         = record.record[10],
                #geom      = Polygon(tuple(record.shape.points)).wkt,
                geom       = shape(record.shape.__geo_interface__).wkt,
                )

        session.add(record)
