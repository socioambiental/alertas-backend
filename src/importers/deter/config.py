#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# DETER default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic configuration
name         = 'deter'
collection   = 'deter'
description  = 'Imports DETER data from TerraBrasilis shapefile downloads'
dependencies = [ 'isa', 'biomass', 'land_cover' ]

# Available datasets, location and filename params
#datasets   = [ 'amz', 'cerrado' ]
datasets    = [ 'amz' ]
base_url    = 'http://terrabrasilis.dpi.inpe.br/file-delivery/download/deter-{dataset}/shape'
base_prefix = 'deter-'
base_suffix = '-public-'
shapefiles  = ['deter_public.shp']

# How to deal with new data
update_mode = 'truncate'

# Date pattern in the dataset file name
date_pattern = r"([0-9]{4})([a-zA-Z][a-z]{2})([0-9]{2})"

# Hardcoded month table as used in the dataset file name
months = {
           'Jan': '01',
           'Fev': '02',
           'Mar': '03',
           'Abr': '04',
           'Mai': '05',
           'Jun': '06',
           'Jul': '07',
           'Ago': '08',
           'Set': '09',
           'Out': '10',
           'Nov': '11',
           'Dez': '12'
         }

# Class information in the final converted dataset, after transform stage
classes = {
            1: {
                'name': 'deforestation',
                'subclasses': {
                    1: 'with_exposed_terrain',
                    2: 'with_vegetation'
                    },
                'emission_factor': 1
                },

            2: {
                'name': 'mining',
                'subclasses': {
                    1: 'mined_area'
                    },
                'emission_factor': 1
                },
            3: {
                'name': 'burned',
                'subclasses': {
                    1: 'burning_scar',
                    },
                'emission_factor': 0.57
                },

            4: {
                'name': 'degradation',
                'subclasses': {
                    1: 'degradation',
                    2: 'selective_logging_unordered',
                    3: 'selective_logging_geometric',
                    },
                'emission_factor': 0.35
                },
          }

# Metadata
metadata = {
        'name'       : 'DETER',
        'description': {
            'en'   : 'DETER/INPE Alerts',
            'pt-br': 'Alertas DETER/INPE',
            'es'   : 'Alertas DETER/INPE',
            },
        'url'            : 'http://terrabrasilis.dpi.inpe.br',
        'methodology_url': 'http://www.obt.inpe.br/OBT/assuntos/programas/amazonia/prodes/pdfs/Metodologia_Prodes_Deter_revisada.pdf',
        'license_type'   : 'CC BY-SA 4.0',
        'license_url'    : 'https://creativecommons.org/licenses/by-sa/4.0/',
        'license_text': {
            'en'   : 'The work, Monitoring Program of the Amazon and other Biomes of NATIONAL INSTITUTE FOR SPACE RESEARCH, EARTH OBSERVATION GENERAL COORDINATION, is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.',
            'pt-br': 'O trabalho Programa de monitoramento da Amazônia e demais biomas. de INSTITUTO NACIONAL DE PESQUISAS ESPACIAIS. COORDENAÇÃO GERAL DE OBSERVAÇÃO DA TERRA. está licenciado com uma Licença Creative Commons – Atribuição-CompartilhaIgual 4.0 Internacional.',
            'es'   : 'La obra Programa de Monitoreo de la Amazonía y otros Biomas del INSTITUTO NACIONAL DE INVESTIGACIÓN ESPACIAL, COORDINACIÓN GENERAL DE OBSERVACIÓN DE LA TIERRA, se encuentra bajo una licencia Creative Commons Attribution-ShareAlike 4.0 International License.',
            },
        'citation_doi': '10.3390/ijgi8110513',
        'citation'    : {
            'en'   : 'Assis, L. F. F. G.; Ferreira, K. R.; Vinhas, L.; Maurano, L.; Almeida, C.; Carvalho, A.; Rodrigues, J.; Maciel, A.; Camargo, C. TerraBrasilis: A Spatial Data Analytics Infrastructure for Large-Scale Thematic Mapping. ISPRS International Journal of Geo-Information. 8, 513, 2019. DOI: 10.3390/ijgi8110513 ',
            'pt-br': 'Assis, L. F. F. G.; Ferreira, K. R.; Vinhas, L.; Maurano, L.; Almeida, C.; Carvalho, A.; Rodrigues, J.; Maciel, A.; Camargo, C. TerraBrasilis: A Spatial Data Analytics Infrastructure for Large-Scale Thematic Mapping. ISPRS International Journal of Geo-Information. 8, 513, 2019. DOI: 10.3390/ijgi8110513',
            'es'   : 'Assis, L. F. F. G.; Ferreira, K. R.; Vinhas, L.; Maurano, L.; Almeida, C.; Carvalho, A.; Rodrigues, J.; Maciel, A.; Camargo, C. TerraBrasilis: A Spatial Data Analytics Infrastructure for Large-Scale Thematic Mapping. ISPRS International Journal of Geo-Information. 8, 513, 2019. DOI: 10.3390/ijgi8110513 ',
            },
        }

# Archive management
archive = {
        'redistribute': True,
        'safeguard'   : True,
        }
