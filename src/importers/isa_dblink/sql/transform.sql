-- Set search path to include the wanted schemas in order
SET search_path to base,public;

--
-- BEGIN transaction for insertion
--
BEGIN;

-- Remove any existing base.territory_tmp table
DROP TABLE IF EXISTS base.territory_tmp;

-- Create the base.territory_tmp table
-- Use a temporary table to avoid locking on the main territory table in case
-- of Tileserver or API requests during imports
CREATE TABLE base.territory_tmp (
    id_orig INTEGER,
    type    VARCHAR,
    name    VARCHAR,
    geom    geometry);

-- Update geometry
SELECT UpdateGeometrySRID('base','territory_tmp', 'geom', {srid});

-- Create indexes
CREATE INDEX sidx_territory_tmp_geom ON base.territory_tmp USING gist (geom) TABLESPACE pg_default;
CREATE INDEX sidx_territory_tmp_type ON base.territory_tmp            (type) TABLESPACE pg_default;

-- Insert UFs and MUNs
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT geocodigo, 'UF',   sigla, shape FROM base.UFS_base250;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT cod_mun,   'MUN',  nome,  shape FROM base.MUN_base250;

-- Insert protected areas
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp,    'TI',   nome,  shape FROM base.TIS_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp,    'UCE',  nome,  shape FROM base.UCE_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp,    'UCF',  nome,  shape FROM base.UCF_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp,    'APAE', nome,  shape FROM base.APAE_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp,    'APAF', nome,  shape FROM base.APAF_monit;
--INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp,  'TQ',   nome,  shape FROM base.TQS_monit;

-- Insere buffers 0-3km
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'TI_B1',   nome, safe_diff(ST_SetSRID(st_buffer(shape::geography,  3000)::geometry, {srid}), shape) FROM base.TIS_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'UCE_B1',  nome, safe_diff(ST_SetSRID(st_buffer(shape::geography,  3000)::geometry, {srid}), shape) FROM base.UCE_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'UCF_B1',  nome, safe_diff(ST_SetSRID(st_buffer(shape::geography,  3000)::geometry, {srid}), shape) FROM base.UCF_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'APAE_B1', nome, safe_diff(ST_SetSRID(st_buffer(shape::geography,  3000)::geometry, {srid}), shape) FROM base.APAE_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'APAF_B1', nome, safe_diff(ST_SetSRID(st_buffer(shape::geography,  3000)::geometry, {srid}), shape) FROM base.APAF_monit;
--INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'TQ_B1' ,  nome, safe_diff(ST_SetSRID(st_buffer(shape::geography,  3000)::geometry, {srid}), shape) FROM base.TQS_monit;

-- Insere buffers 0-10km
INSERT INTO territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'TI_B2',   nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), shape) FROM base.TIS_monit;
INSERT INTO territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'UCE_B2',  nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), shape) FROM base.UCE_monit;
INSERT INTO territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'UCF_B2',  nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), shape) FROM base.UCF_monit;
INSERT INTO territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'APAE_B2', nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), shape) FROM base.APAE_monit;
INSERT INTO territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'APAF_B2', nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), shape) FROM base.APAF_monit;
--INSERT INTO territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'TQ_B2',   nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), shape) FROM base.TQS_monit;

-- Insere buffers 3-10km
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'TI_B3',   nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), ST_SetSRID(st_buffer(shape::geography, 3000)::geometry, {srid})) FROM base.TIS_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'UCE_B3',  nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), ST_SetSRID(st_buffer(shape::geography, 3000)::geometry, {srid})) FROM base.UCE_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'UCF_B3',  nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), ST_SetSRID(st_buffer(shape::geography, 3000)::geometry, {srid})) FROM base.UCF_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'APAE_B3', nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), ST_SetSRID(st_buffer(shape::geography, 3000)::geometry, {srid})) FROM base.APAE_monit;
INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'APAF_B3', nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), ST_SetSRID(st_buffer(shape::geography, 3000)::geometry, {srid})) FROM base.APAF_monit;
--INSERT INTO base.territory_tmp (id_orig, type, name, geom) SELECT id_arp, 'TQ_B3',   nome, safe_diff(ST_SetSRID(st_buffer(shape::geography, 10000)::geometry, {srid}), ST_SetSRID(st_buffer(shape::geography, 3000)::geometry, {srid})) FROM base.TQS_monit;

-- Add primary key
ALTER TABLE base.territory_tmp ADD id SERIAL PRIMARY KEY;

-- Run ANALYZE whenever a bulk loading takes place
-- See https://www.2ndquadrant.com/en/blog/postgresql-vacuum-and-analyze-best-practice-tips/
--     https://andreigridnev.com/blog/2016-04-01-analyze-reindex-vacuum-in-postgresql/
--     https://wiki.postgresql.org/wiki/Introduction_to_VACUUM,_ANALYZE,_EXPLAIN,_and_COUNT
ANALYZE base.territory_tmp;

--
-- COMMIT transaction for insertion
--
COMMIT;

--
-- BEGIN transaction for replacement
--
BEGIN;

-- Remove any existing base.territory table
DROP TABLE IF EXISTS base.territory;

-- Put the new table in place
ALTER TABLE territory_tmp RENAME TO territory;

-- Rename indexes
ALTER INDEX IF EXISTS sidx_territory_tmp_geom RENAME TO sidx_territory_geom;
ALTER INDEX IF EXISTS sidx_territory_tmp_type RENAME TO sidx_territory_type;

--GRANT SELECT ON {db}.territory TO {main_db_user}; -- PostgreSQL 13 onwards?
GRANT SELECT ON base.territory TO {main_db_user};
GRANT SELECT ON base.territory TO {main_db_query};

--
-- COMMIT transaction for replacement
--
COMMIT;
