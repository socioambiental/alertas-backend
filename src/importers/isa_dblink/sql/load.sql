-- Set search path to include the wanted schemas in order
SET search_path to base,public;

-- Remove any existing servers
DROP SERVER IF EXISTS server_monitoramento CASCADE;
DROP SERVER IF EXISTS server_tematicos     CASCADE;
DROP SERVER IF EXISTS server_analises      CASCADE;
DROP SERVER IF EXISTS server_ibge          CASCADE;

-- Remove any existing base tables
DROP TABLE IF EXISTS base.TIS_monit;
DROP TABLE IF EXISTS base.UCF_monit;
DROP TABLE IF EXISTS base.UCE_monit;
DROP TABLE IF EXISTS base.APAF_monit;
DROP TABLE IF EXISTS base.APAE_monit;
DROP TABLE IF EXISTS base.TQS_monit;
DROP TABLE IF EXISTS base.UFS_base250;
DROP TABLE IF EXISTS base.MUN_base250;
DROP TABLE IF EXISTS base.BLA_base250;
DROP TABLE IF EXISTS base.BASINS_lvl2_ana;

-- Create servers
CREATE SERVER IF NOT EXISTS server_monitoramento FOREIGN DATA WRAPPER dblink_fdw OPTIONS (host '{monitoramento_host}', dbname '{monitoramento_db}', port '{monitoramento_port}');
CREATE SERVER IF NOT EXISTS server_tematicos     FOREIGN DATA WRAPPER dblink_fdw OPTIONS (host '{tematicos_host}',     dbname '{tematicos_db}',     port '{tematicos_port}');
CREATE SERVER IF NOT EXISTS server_analises      FOREIGN DATA WRAPPER dblink_fdw OPTIONS (host '{analises_host}',      dbname '{analises_db}',      port '{analises_port}');
CREATE SERVER IF NOT EXISTS server_ibge          FOREIGN DATA WRAPPER dblink_fdw OPTIONS (host '{ibge_host}',          dbname '{ibge_db}',          port '{ibge_port}');

-- Create user mappings
CREATE USER MAPPING FOR {main_db_admin} SERVER server_monitoramento OPTIONS (user '{monitoramento_user}', password '{monitoramento_password}');
CREATE USER MAPPING FOR {main_db_admin} SERVER server_tematicos     OPTIONS (user '{tematicos_user}',     password '{tematicos_password}');
CREATE USER MAPPING FOR {main_db_admin} SERVER server_analises      OPTIONS (user '{analises_user}',      password '{analises_password}');
CREATE USER MAPPING FOR {main_db_admin} SERVER server_ibge          OPTIONS (user '{ibge_user}',          password '{ibge_password}');

-- Set permissions
GRANT USAGE ON FOREIGN SERVER server_ibge          TO {main_db_admin};
GRANT USAGE ON FOREIGN SERVER server_tematicos     TO {main_db_admin};
GRANT USAGE ON FOREIGN SERVER server_analises      TO {main_db_admin};
GRANT USAGE ON FOREIGN SERVER server_monitoramento TO {main_db_admin};

-- Setup connections
SELECT dblink_connect('conn_db_link_monitoramento', 'server_monitoramento');
SELECT dblink_connect('conn_db_link_tematicos',     'server_tematicos');
SELECT dblink_connect('conn_db_link_analises',      'server_analises');
SELECT dblink_connect('conn_db_link_ibge',          'server_ibge');

CREATE TABLE base.TIS_monit AS
  SELECT * FROM dblink('conn_db_link_monitoramento', 'SELECT id_arp,nome_ti,shape FROM isa.areaindi_isa_pl WHERE critica != 4') AS
      TIS(id_arp int, nome varchar, shape geometry);

CREATE TABLE base.UCF_monit AS
  SELECT * FROM dblink('conn_db_link_monitoramento', 'SELECT id_arp,nome_uc,uso,shape FROM isa.Areacons_isa_pl WHERE critica != 4 AND left(nome_uc,3)!=''APA''') AS
      UCF_monit (id_arp int, nome varchar, uso varchar, shape geometry);

CREATE TABLE base.UCE_monit AS
  SELECT * FROM dblink('conn_db_link_monitoramento', 'SELECT id_arp,nome_uc,uso,shape FROM isa.Consest_isa_pl WHERE critica != 4 AND left(nome_uc,3)!=''APA''') AS
      UCE_monit (id_arp int, nome varchar, uso varchar, shape geometry);

CREATE TABLE base.APAF_monit AS
  SELECT * FROM dblink('conn_db_link_monitoramento', 'SELECT id_arp,nome_uc,uso,shape FROM isa.Areacons_isa_pl WHERE critica != 4 AND left(nome_uc,3)=''APA''') AS
      APAF_monit (id_arp int, nome varchar, uso varchar, shape geometry);

CREATE TABLE base.APAE_monit AS
  SELECT * FROM dblink('conn_db_link_monitoramento', 'SELECT id_arp,nome_uc,uso,shape FROM isa.Consest_isa_pl WHERE critica != 4 AND left(nome_uc,3)=''APA''') AS
      APAE_monit (id_arp int, nome varchar, uso varchar, shape geometry);

--CREATE TABLE base.TQS_monit AS
--  SELECT * FROM dblink('conn_db_link_monitoramento', 'SELECT id_arp,nome_qui,shape FROM isa.tquilombos_isa_pl WHERE critica != 4') AS
--      TQS_monit (id_arp int, nome varchar, shape geometry);

CREATE TABLE UFS_base250 AS
     SELECT * FROM dblink('conn_db_link_ibge', 'SELECT geocodigo, nome, sigla, ST_Force_2D(shape) FROM isa.LIM_Unidade_Federacao_A') AS UFS_base250 (geocodigo int, nome varchar, sigla varchar, shape geometry);

CREATE TABLE base.MUN_base250 AS
   SELECT * FROM dblink('conn_db_link_ibge', 'SELECT geocodigo, cod_uf, nome, siglauf, amaleg, ST_Force_2D(shape) FROM isa.LIM_municipios_a_md2017')
   AS MUN_base250 (cod_mun int, cod_uf int, nome varchar, siglauf varchar, amaleg smallint, shape geometry);

CREATE TABLE base.BLA_base250 AS
   SELECT * FROM dblink('conn_db_link_tematicos', 'SELECT amaleg, ST_Force_2D(shape) FROM isa.amaleg_sivam_pl_250')
   AS BLA_base250 (amaleg varchar, shape geometry);

CREATE TABLE base.BASINS_lvl2_ana AS
   SELECT * FROM dblink('conn_db_link_analises', 'SELECT objectid, nivel1, nivel2, rg_hidro, bacia, ST_Force_2D(shape) FROM isa.bacias_ana')
   AS BASINS_lvl2_ana (objectid int, nivel1 varchar, nivel2 varchar, rg_hidro varchar, bacia varchar, shape geometry);

-- Disconnect
SELECT dblink_disconnect('conn_db_link_monitoramento');
SELECT dblink_disconnect('conn_db_link_tematicos');
SELECT dblink_disconnect('conn_db_link_analises');
SELECT dblink_disconnect('conn_db_link_ibge');

-- Update geometries
SELECT UpdateGeometrySRID('base', 'tis_monit',       'shape', {srid});
SELECT UpdateGeometrySRID('base', 'ucf_monit',       'shape', {srid});
SELECT UpdateGeometrySRID('base', 'uce_monit',       'shape', {srid});
SELECT UpdateGeometrySRID('base', 'apaf_monit',      'shape', {srid});
SELECT UpdateGeometrySRID('base', 'apae_monit',      'shape', {srid});
--SELECT UpdateGeometrySRID('base', 'tqs_monit',     'shape', {srid});
SELECT UpdateGeometrySRID('base', 'ufs_base250',     'shape', {srid});
SELECT UpdateGeometrySRID('base', 'mun_base250',     'shape', {srid});
SELECT UpdateGeometrySRID('base', 'bla_base250',     'shape', {srid});
SELECT UpdateGeometrySRID('base', 'basins_lvl2_ana', 'shape', {srid});

-- Add primary keys
ALTER TABLE base.TIS_monit        ADD COLUMN ID SERIAL PRIMARY KEY;
ALTER TABLE base.UCE_monit        ADD COLUMN ID SERIAL PRIMARY KEY;
ALTER TABLE base.UCF_monit        ADD COLUMN ID SERIAL PRIMARY KEY;
ALTER TABLE base.APAE_monit       ADD COLUMN ID SERIAL PRIMARY KEY;
ALTER TABLE base.APAF_monit       ADD COLUMN ID SERIAL PRIMARY KEY;
--ALTER TABLE base.TQS_monit      ADD COLUMN ID SERIAL PRIMARY KEY;
ALTER TABLE base.UFS_base250      ADD COLUMN ID SERIAL PRIMARY KEY;
ALTER TABLE base.MUN_base250      ADD COLUMN ID SERIAL PRIMARY KEY;
ALTER TABLE base.BLA_base250      ADD COLUMN ID SERIAL PRIMARY KEY;
ALTER TABLE base.BASINS_lvl2_ana  ADD COLUMN ID SERIAL PRIMARY KEY;

-- Create indexes
CREATE INDEX sidx_TIS_monit_geom       ON base.TIS_monit       USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_UCE_monit_geom       ON base.UCE_monit       USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_UCF_monit_geom       ON base.UCF_monit       USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_APAE_monit_geom      ON base.APAE_monit      USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_APAF_monit_geom      ON base.APAF_monit      USING gist (shape) TABLESPACE pg_default;
--CREATE INDEX sidx_TQS_monit_geom     ON base.TQS_monit       USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_UFS_base250_geom     ON base.UFS_base250     USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_MUN_base250_geom     ON base.MUN_base250     USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_BLA_base250_geom     ON base.BLA_base250     USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_BASINS_lvl2_ana_geom ON base.BASINS_lvl2_ana USING gist (shape) TABLESPACE pg_default;

-- Creates Brazilian Legal Amazon entity in WGS84 srid
DROP TABLE IF EXISTS base.BLA_BASE250_wgs84;
CREATE TABLE base.BLA_BASE250_wgs84 (amaleg varchar, shape geometry);
INSERT INTO base.BLA_BASE250_wgs84 (amaleg, shape) SELECT amaleg, st_transform(shape,4326) FROM base.BLA_BASE250;
SELECT UpdateGeometrySRID('base','bla_base250_wgs84', 'shape', 4326);
ALTER TABLE base.BLA_base250_wgs84  ADD COLUMN ID SERIAL PRIMARY KEY;
CREATE INDEX sidx_BLA_base250_wgs84_geom ON base.BLA_base250_wgs84 USING gist (shape) TABLESPACE pg_default;

-- Needed by routines running under the regular and query users
GRANT SELECT ON base.MUN_base250           TO {main_db_query};
GRANT SELECT ON base.MUN_base250           TO {main_db_user};
GRANT SELECT ON base.BLA_base250_wgs84     TO {main_db_user};
GRANT SELECT ON base.BLA_base250           TO {main_db_user};
GRANT SELECT ON base.BLA_base250           TO {main_db_query};
GRANT SELECT ON base.BASINS_lvl2_ana       TO {main_db_user};
GRANT SELECT ON base.BASINS_lvl2_ana       TO {main_db_query};
