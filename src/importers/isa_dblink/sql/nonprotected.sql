-- Builds non-proteced portions of UFs and MUNs

-- Set search path
SET search_path to base,public;

-- Begin transaction
BEGIN;

-- Remove any existing base.territory_tmp table
DROP TABLE IF EXISTS base.territory_tmp;

-- Create the base.territory_tmp table
-- Use a temporary table to avoid locking on the main territory table in case
-- of Tileserver or API requests during imports
CREATE TABLE base.territory_tmp AS SELECT * from base.territory;

-- Update geometry
SELECT UpdateGeometrySRID('base','territory_tmp', 'geom', {srid});

-- Create indexes
CREATE INDEX sidx_territory_tmp_geom ON base.territory_tmp USING gist (geom) TABLESPACE pg_default;
CREATE INDEX sidx_territory_tmp_type ON base.territory_tmp            (type) TABLESPACE pg_default;

--
-- Less code, slower approach.
-- Uses a single mask with all ARPs.
--

----CREATE TABLE base.territory_non_protected AS
--INSERT INTO base.territory_tmp (id_orig, type, name, geom)
--  WITH arp_mask AS (
--    SELECT ST_UNION(
--      ARRAY(
--        SELECT geom
--        FROM base.territory t, base.bla_base250 b
--        WHERE t.type != 'MUN' AND t.type != 'UF' AND t.type NOT LIKE '%_B%' AND t.geom && b.shape
--      )
--    ) AS mask
--  )
--  SELECT t.id_orig AS orig, CONCAT(t.type, 'D'::varchar) AS type, t.name AS name, safe_diff(t.geom, m.mask) AS geom
--  FROM base.territory t, arp_mask m, base.bla_base250 b
--  WHERE (t.type = 'MUN' OR t.type = 'UF') AND t.geom && b.shape;

--
-- More code, maybe faster.
-- Use one mask per UF of MUN with only the overlapping ARPs.
--

----CREATE TABLE base.territory_non_protected AS
--INSERT INTO base.territory_tmp (id_orig, type, name, geom)
--  WITH units AS (
--    SELECT * FROM base.territory WHERE type = 'MUN' OR type = 'UF'
--  ),
--  arp_mask AS (
--    SELECT ST_UNION(
--      ARRAY(
--        SELECT b.geom
--        FROM base.territory b, units u, base.bla_base250 b
--        WHERE b.type != 'MUN' AND b.type != 'UF' AND b.type NOT LIKE '%_B%' AND b.geom && b.shape AND b.geom && u.geom
--      )
--    ) AS mask
--  )
--  SELECT u.id_orig AS id_orig, CONCAT(u.type, 'D'::varchar) AS type, u.name AS name, safe_diff(u.geom, m.mask) AS geom
--  FROM units u, arp_mask m, base.bla_base250 b
--  WHERE (u.type = 'MUN' OR u.type = 'UF') AND u.geom && b.shape;

--
-- Even more code, but perhaps even faster.
-- Use one mask per UF of MUN with only the overlapping ARPs together with an
-- intermediate table. Requires diskspace.
--

---- Start afresh
--DROP TABLE IF EXISTS base.arp_mask;
--
---- Create the masks table
--CREATE TABLE base.arp_mask AS
--  WITH units AS (
--    SELECT * FROM base.territory WHERE type = 'MUN' OR type = 'UF'
--  ),
--  arp_mask AS (
--    SELECT ST_UNION(
--      ARRAY(
--        SELECT b.geom
--        FROM base.territory t, units u, base.bla_base250 b
--        WHERE t.type != 'MUN' AND t.type != 'UF' AND t.type NOT LIKE '%_B%' AND t.geom && b.shape AND t.geom && u.geom
--      )
--    ) AS mask
--  )
--  SELECT u.id_orig AS id_orig, u.type AS type, m.mask AS mask
--  FROM units u, arp_mask m;
--
---- Create mask indexes
--CREATE INDEX sidx_arp_mask_geom ON base.arp_mask USING gist (mask) TABLESPACE pg_default;
--CREATE INDEX sidx_arp_mask_type ON base.arp_mask            (type) TABLESPACE pg_default;
--
---- Populate the mask index
--ANALYZE base.arp_mask;
--
---- Calculate non-protected areas
----CREATE TABLE territory_non_protected AS
--INSERT INTO base.territory_tmp (id_orig, type, name, geom)
--  SELECT u.id_orig AS id_orig, CONCAT(u.type, 'D'::varchar) AS type, u.name AS name, safe_diff(u.geom, m.mask) AS geom
--  FROM base.territory u, arp_mask m, base.bla_base250 b
--  WHERE (u.type = 'MUN' OR u.type = 'UF') AND u.type = m.type AND u.id_orig = m.id_orig AND u.geom && b.shape AND u.geom && m.mask;
--
---- Cleanup
--DROP TABLE base.arp_mask;

--
-- Another approach: create the mask from the difference between Brazilian
-- Legal Amazon (BLA) and the ARPs.
--
-- This will only work while the system is restricted to the BLA.
--
-- Create the masks table

-- Start afresh
DROP TABLE IF EXISTS base.arp_mask;

CREATE TABLE base.arp_mask AS
  WITH arp_mask AS (
    SELECT ST_UNION(
      ARRAY(
        SELECT t.geom
        FROM base.territory t, base.bla_base250 b
        WHERE t.type != 'MUN' AND t.type != 'UF' AND t.type NOT LIKE '%_B%' AND ST_Intersects(t.geom, b.shape)
      )
    ) AS mask
  )
  SELECT safe_diff(b.shape, m.mask) AS mask
  FROM base.bla_base250 b, arp_mask m;

-- Create mask index
CREATE INDEX sidx_arp_mask_geom ON base.arp_mask USING gist (mask) TABLESPACE pg_default;

-- Populate the mask index
ANALYZE base.arp_mask;

-- Calculate non-protected areas
--CREATE TABLE base.territory_non_protected AS
INSERT INTO base.territory_tmp (id_orig, type, name, geom)
  SELECT u.id_orig AS id_orig, CONCAT(u.type, 'D'::varchar) AS type, u.name AS name, ST_Intersection(u.geom, m.mask) AS geom
  FROM base.territory u, arp_mask m, base.bla_base250 b
  WHERE (u.type = 'MUN' OR u.type = 'UF') AND u.geom && b.shape;

-- Create non-protected index
--CREATE INDEX sidx_base_territory_non_protected_geom ON base.territory_non_protected USING gist (geom) TABLESPACE pg_default;

-- Populate the non-protected index
--ANALYZE base.territory_non_protected;

-- Cleanup
--DROP TABLE base.arp_mask;

-- Commit transaction
COMMIT;

--
-- BEGIN transaction for replacement
--
BEGIN;

-- Remove any existing base.territory table
DROP TABLE IF EXISTS base.territory;

-- Put the new table in place
ALTER TABLE territory_tmp RENAME TO territory;

-- Rename indexes
ALTER INDEX IF EXISTS sidx_territory_tmp_geom RENAME TO sidx_territory_geom;
ALTER INDEX IF EXISTS sidx_territory_tmp_type RENAME TO sidx_territory_type;

--GRANT SELECT ON {db}.territory TO {main_db_user}; -- PostgreSQL 13 onwards?
GRANT SELECT ON base.territory TO {main_db_user};
GRANT SELECT ON base.territory TO {main_db_query};

--
-- COMMIT transaction for replacement
--
COMMIT;
