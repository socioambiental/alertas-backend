#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ISA data loader.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from lib.database.decorators.psycopg2 import connect
from lib.importer.load.sql            import SqlLoad
from lib.config                       import config as global_config
from lib.params                       import srid

class IsaDblinkLoad(SqlLoad):
    @connect('main_db_admin')
    def __init__(self, config, args = {}):
        self.dirname = os.path.dirname(__file__)

        super().__init__(config, args)

        self.operations['copy'] = {
                'info'  : 'Copying external tables',
                'sql'   : open(self.sql_local + "load.sql", "r").read(),
                'params': {
                    'main_db_admin'          : global_config['main_db_admin']['user'],
                    'main_db_user'           : global_config['main_db']['user'],
                    'main_db_query'          : global_config['main_db_query']['user'],
                    'monitoramento_host'     : global_config['monitoramento_db']['host'],
                    'monitoramento_port'     : global_config['monitoramento_db']['port'],
                    'monitoramento_user'     : global_config['monitoramento_db']['user'],
                    'monitoramento_password' : global_config['monitoramento_db']['password'],
                    'monitoramento_db'       : global_config['monitoramento_db']['db'],
                    'tematicos_host'         : global_config['tematicos_db']['host'],
                    'tematicos_port'         : global_config['tematicos_db']['port'],
                    'tematicos_user'         : global_config['tematicos_db']['user'],
                    'tematicos_password'     : global_config['tematicos_db']['password'],
                    'tematicos_db'           : global_config['tematicos_db']['db'],
                    'analises_host'          : global_config['analises_db']['host'],
                    'analises_port'          : global_config['analises_db']['port'],
                    'analises_user'          : global_config['analises_db']['user'],
                    'analises_password'      : global_config['analises_db']['password'],
                    'analises_db'            : global_config['analises_db']['db'],
                    'ibge_host'              : global_config['ibge_db']['host'],
                    'ibge_port'              : global_config['ibge_db']['port'],
                    'ibge_user'              : global_config['ibge_db']['user'],
                    'ibge_password'          : global_config['ibge_db']['password'],
                    'ibge_db'                : global_config['ibge_db']['db'],
                    'srid'                   : srid,
                    },
                }
