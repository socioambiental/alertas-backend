#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ISA data transform.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from lib.database.decorators.psycopg2 import connect
from lib.importer.transform.sql       import SqlTransform
from lib.config                       import config as global_config
from lib.params                       import srid

class IsaDblinkTransform(SqlTransform):
    @connect('main_db_admin')
    def __init__(self, config, args = {}):
        self.dirname = os.path.dirname(__file__)

        super().__init__(config, args)

        self.operations['transform'] = {
                'info'  : 'Creating clipping table',
                'sql'   : open(self.sql_local + "transform.sql", "r").read(),
                'params': {
                    'db'           : global_config['main_db_admin']['db'],
                    'main_db_user' : global_config['main_db']['user'],
                    'main_db_query': global_config['main_db_query']['user'],
                    'srid'         : srid,
                    },
                }

        #self.operations['nonprotected'] = {
        #        'info'  : 'Including non-protected polygons into the territory table',
        #        'sql'   : open(self.sql_local + "nonprotected.sql", "r").read(),
        #        'params': {
        #            'main_db_user' : global_config['main_db']['user'],
        #            'main_db_query': global_config['main_db_query']['user'],
        #            'srid'         : srid,
        #            },
        #        }
