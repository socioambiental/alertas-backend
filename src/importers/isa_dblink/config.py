#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ISA dblink default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic configuration
name        = 'isa_dblink'
collection  = 'isa'
description = 'Imports ISA territory dataset collection using a direct dblink connection with ISA internal database'
pipeline    = 'lt'
type        = 'territory'

# Metadata
metadata = {
        'name'       : 'Territórios - ISA',
        'description': {
            'en'   : 'Territory information produced and compiled by ISA',
            'pt-br': 'Informações territoriais produzidas e compiladas pelo ISA',
            'es'   : 'Informaciones territoriales produzidas e compiladas por ISA',
            },
        'url'            : 'https://alertas.socioambiental.org/doc/notes.html',
        'methodology_url': 'https://alertas.socioambiental.org/doc/notes.html',
        'license_type'   : 'CC BY-SA 4.0',
        'license_url'    : 'https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR',
        'license_text': {
            'en'   : '',
            'pt-br': '',
            'es'   : '',
            },
        'citation_doi': '',
        'citation'    : {
            'en'   : '',
            'pt-br': '',
            'es'   : '',
            },
        }

# Archive management
archive = {
        'redistribute': True,
        'safeguard'   : False,
        }
