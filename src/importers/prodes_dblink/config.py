#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ISA default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic configuration
name         = 'prodes_dblink'
collection   = 'prodes'
description  = 'Imports PRODES dataset using a direct dblink connection with ISA internal database'
dependencies = [ 'isa', 'biomass', 'land_cover' ]
pipeline     = 'lt'

# Available datasets, location and filename params
#datasets   = [ 'amz', 'cerrado' ]
datasets    = [ 'amz' ]

# Class information in the final converted dataset, after transform stage
classes = {
            1: {
                'name': 'deforestation',
                'subclasses': {
                    1: 'deforestation'
                    },
                'emission_factor': 1
                }
          }

# Metadata
from importers.deter.config import metadata as deter_metadata
metadata                = dict(deter_metadata)
metadata['name']        = 'PRODES'
metadata['description'] = {
            'en'   : 'PRODES/INPE Deforestation',
            'pt-br': 'Desmatamento PRODES/INPE',
            'es'   : 'Desmatamiento PRODES/INPE',
        }

# Archive management
from importers.deter.config import archive
