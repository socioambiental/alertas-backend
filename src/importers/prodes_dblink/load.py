#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# PRODES data loader.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from lib.database.decorators.psycopg2         import connect
from lib.importer.load.sql                    import SqlLoad
from lib.config                               import config as global_config
from lib.params                               import srid
from importers.prodes_acumulado_dblink.config import accumulated_until

class ProdesDblinkLoad(SqlLoad):
    @connect('main_db_admin')
    def __init__(self, config, args = {}):
        self.dirname = os.path.dirname(__file__)

        super().__init__(config, args)

        self.operations['copy'] = {
                'info'  : 'Copying external tables',
                'sql'   : open(self.sql_local + "load.sql", "r").read(),
                'params': {
                    'main_db_admin'         : global_config['main_db_admin']['user'],
                    'main_db_user'          : global_config['main_db']['user'],
                    'desmatamento_host'     : global_config['desmatamento_db']['host'],
                    'desmatamento_port'     : global_config['desmatamento_db']['port'],
                    'desmatamento_user'     : global_config['desmatamento_db']['user'],
                    'desmatamento_password' : global_config['desmatamento_db']['password'],
                    'desmatamento_db'       : global_config['desmatamento_db']['db'],
                    'srid'                  : srid,
                    'e_type'                : self.config.collection,

                    # Use two double quotes for dblink escaping, see
                    # https://stackoverflow.com/questions/6615732/postgres-dblink-escape-single-quote#6615830
                    'amz_where_clause'      : "legenda != ''d{}''".format(accumulated_until),
                    },
                }
