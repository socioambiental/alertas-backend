#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# PRODES dataset transformer.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import datetime
from lib.database.decorators.psycopg2 import connect
from lib.importer.transform.sql       import SqlTransform
from lib.params                       import srid
from lib.logger                       import logger
from .config                          import classes

class ProdesDblinkTransform(SqlTransform):
    @connect()
    def __init__(self, config, args = {}):
        self.dirname = os.path.dirname(__file__)

        super().__init__(config, args)

        # Needed for carbon emissions calculation
        self.classes = classes

        # Dataset selection
        self.dataset_selection()

        self.operations['transform'] = {
                'info'            : 'Initial processing of {collection} dataset',
                'sql'             : open(self.sql_local + "transform.sql", "r").read(),
                'pivot_partition' : 'event',
                'params'          : {
                        'srid'                   : srid,
                        'e_type'                 : self.config.collection,
                        'canonical_union_clauses': ''.join(self.union_clauses),
                    },
                }

        # PRODES is by design already restricted to the Brazilian Legal Amazon
        #self.operations['bla'] = {
        #        'info'           : 'Filtering events only inside the Brazilian Legal Amazon',
        #        'sql'            : open(self.sql_global + "bla_filter.sql", "r").read(),
        #        'pivot_partition': 'event',
        #        }

        self.operations['compute'] = {
                'info' : 'Intersecting {collection} datasets with territories',
                'sql'  : open(self.sql_global + "compute_event_territory_table.sql", "r").read(),
                }

        self.operations['process'] = {
                'info'            : 'Processing {collection} intersection with territories',
                'sql'             : open(self.sql_global + "process_event_territory_table.sql", "r").read(),
                'pivot_partition' : 'event_territory',
                }

        self.operations['carbon_emissions'] = {
               'info': 'Computing associated carbon emissions',
               'sql' : open(self.sql_global + "compute_carbon_emissions.sql", "r").read(),
               }

    def dataset_selection(self):
        """Dataset selection logic"""

        # To hold UNION clauses
        self.union_clauses = []

        # Check for the mandatory dataset
        if 'amz' not in self.config.datasets:
            logger('warning', "Dataset 'amz' is not defined at config.py, but it's currently a mandatory dataset so importing it anyway.")

            self.config.datasets.append('amz')

        # Check whether to include the "cerrado" dataset
        if 'cerrado' in self.config.datasets:
            logger('info', "Including the 'cerrado' dataset...")

            self.union_clauses.append('UNION SELECT * FROM {e_type}_cer_canonical')

        # Handle provisional UNIONs
        if self.check_provisional():
            logger('info', "Including the provisional table for the 'amz' dataset...")

            self.union_clauses.append('UNION SELECT * FROM event_canonical.{e_type}_amz_prov_canonical')

            if 'cerrado' in self.config.datasets:
                logger('info', "Including the provisional table for the 'cerrado' dataset...")

                self.union_clauses.append('UNION SELECT * FROM {e_type}_cer_prov_canonical')
        else:
            logger('info', "Ignoring the provisional tables given the main table already have the previous year's data...")

    def check_provisional(self):
        """Check if the provisional tables shall be used"""

        today         = datetime.datetime.today()
        current_year  = int(today.strftime('%Y'))
        previous_year = current_year - 1
        sql           = "SELECT legenda FROM event_canonical.prodes_amz_canonical WHERE legenda = 'd{previous_year}' LIMIT 1;"

        self.cur.execute(
                sql.format(
                    previous_year = previous_year,
                    )
                )

        result = self.cur.fetchone()

        # If the previous year is absent at the main table, then the
        # provisional tables should be used
        if result is None:
            return True
        else:
            return False
