-- Initial preparation of {e_type} dataset

-- Set search path to include the wanted schemas in order
SET search_path to event_processed,public;

--
-- BEGIN transaction for computation
--
BEGIN;

-- Deletes any previous work table
DROP TABLE IF EXISTS {e_type}_tmp;

CREATE TABLE {e_type}_tmp AS
    WITH {e_type}_tmp AS (
        SELECT * FROM event_canonical.{e_type}_amz_canonical
        {canonical_union_clauses}
    )
    SELECT objectid AS orig_id,
            1 AS class_1,
            'DESMATAMENTO_CR' AS class_2,
            -- Convention: dates set to the begining of the "PRODES year", which is Aug. 1st of the previous year
            TO_DATE((SUBSTRING(legenda FROM 2 FOR 4)::decimal - 1)::varchar || '-08-01'::varchar, 'YYYY-MM-DD') AS view_date,
            --st_makevalid(st_buffer(st_snaptogrid(shape, 0.00001),0)) as geom
            st_makevalid(shape) AS geom
    FROM {e_type}_tmp
    WHERE legenda!='Nuvem';

-- Populate Geometry columns
ALTER TABLE {e_type}_tmp ADD COLUMN type varchar;
UPDATE {e_type}_tmp SET type='{e_type}';
--SELECT Populate_Geometry_Columns('public.{e_type}_tmp'::regclass);

-- Creates Indexes
ALTER TABLE {e_type}_tmp ADD ID SERIAL PRIMARY KEY;
CREATE INDEX sidx_{e_type}_tmp_geom
    ON {e_type}_tmp USING gist
    (geom)
    TABLESPACE pg_default;
ANALYZE {e_type}_tmp;

-- Change the storage type
ALTER TABLE {e_type}_tmp
  ALTER COLUMN geom
  SET STORAGE EXTERNAL;

-- Force the column to rewrite
-- UPDATE {e_type}_tmp SET geom = ST_SetSRID(geom, {srid});

--
-- COMMIT transaction for computation
--
COMMIT;

--
-- BEGIN transaction for insertion
--
BEGIN;

-- Do not create a partition right now to avoid a lock in the event table in
-- case of simultaneous imports
--DROP TABLE IF EXISTS event_{e_type};
--CREATE TABLE event_{e_type} PARTITION OF event
--    FOR VALUES IN ('{e_type}');

-- Deletes any previous work table
DROP TABLE IF EXISTS event_{e_type}_new;

--SELECT Populate_Geometry_Columns('public.event'::regclass);
--CREATE TABLE event_{e_type} AS
CREATE TABLE event_{e_type}_new (LIKE event INCLUDING DEFAULTS INCLUDING CONSTRAINTS);
INSERT INTO event_{e_type}_new (orig_id,type,class_1,class_2,date,coduf,uf,codmun,mun,geom)
  SELECT a.orig_id AS orig_id, a.type AS type, a.class_1 AS class_1, a.class_2 AS class_2,
  a.view_date AS date, mun.cod_uf AS coduf, mun.siglauf AS uf, mun.cod_mun AS mun,
  mun.nome,geom AS geom
  FROM {e_type}_tmp a
  JOIN base.MUN_base250 mun ON st_within(ST_Centroid(a.geom),mun.shape);

-- Deletes work tables
DROP TABLE IF EXISTS {e_type}_tmp;

--
-- COMMIT transaction for insertion
--
COMMIT;
