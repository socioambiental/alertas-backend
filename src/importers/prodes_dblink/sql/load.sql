-- Set search path to include the wanted schemas in order
SET search_path to event_canonical,public;

-- Remove any existing servers
DROP SERVER IF EXISTS server_desmatamento CASCADE;

-- Remove any existing canonical tables
DROP TABLE IF EXISTS {e_type}_amz_canonical;
DROP TABLE IF EXISTS {e_type}_cer_canonical;
DROP TABLE IF EXISTS {e_type}_amz_prov_canonical;
DROP TABLE IF EXISTS {e_type}_cer_prov_canonical;

-- Create servers
CREATE SERVER IF NOT EXISTS server_desmatamento FOREIGN DATA WRAPPER dblink_fdw OPTIONS (host '{desmatamento_host}', dbname '{desmatamento_db}', port '{desmatamento_port}');

-- Create user mappings
CREATE USER MAPPING FOR {main_db_admin} SERVER server_desmatamento OPTIONS (user '{desmatamento_user}', password '{desmatamento_password}');

-- Set permissions
GRANT USAGE ON FOREIGN SERVER server_desmatamento TO {main_db_admin};

-- Setup connections
SELECT dblink_connect('conn_db_link_desmatamento', 'server_desmatamento');

--
-- Legal Amazon
--

CREATE TABLE {e_type}_amz_canonical AS
  SELECT * FROM dblink('conn_db_link_desmatamento', 'SELECT objectid, legenda, area_ha, shape FROM isa.desmatacumuladoamz_f WHERE {amz_where_clause}') AS
      {e_type}_amz_canonical (objectid int, legenda varchar, area_ha numeric, shape geometry);
ALTER TABLE {e_type}_amz_canonical ADD COLUMN ID SERIAL PRIMARY KEY;

-- Deletes d2007 records (pre-2008 accumulated deforestation)
-- This was needed in the past when the {e_type}_amz_canonical table creation
-- statement did not have the WHERE clause
--DELETE FROM {e_type}_amz_canonical WHERE legenda = 'd2007';

-- This is a temporary table that might exist in the canonical dataset, containing provisory data.
-- Tipically, this table is used to gather data while a new PRODES dataset is not fully released.
-- If it's not, it will be harmless to import it anyways.
-- What really matters is if it's included in the final canonical table during
-- the transform phase (see UNION clauses at transform.sql).
CREATE TABLE {e_type}_amz_prov_canonical AS
  SELECT * FROM dblink('conn_db_link_desmatamento', 'SELECT objectid, legenda, area_ha, shape FROM isa.desmatestimadoamz') AS
      {e_type}_amz_prov_canonical (objectid int, legenda varchar, area_ha numeric, shape geometry);
ALTER TABLE {e_type}_amz_prov_canonical ADD COLUMN ID SERIAL PRIMARY KEY;

--
-- Cerrado
--

-- Populate a "cerrado" table anyways even if this dataset is not being used.
-- What really matters is if it's included in the final canonical table during
-- the transform phase (see UNION clauses at transform.sql).
CREATE TABLE {e_type}_cer_canonical AS
  SELECT * FROM dblink('conn_db_link_desmatamento', 'SELECT objectid, legenda, area_ha, shape FROM isa.desmatacumuladocer_f') AS
      {e_type}_cer_canonical (objectid int, legenda varchar, area_ha numeric, shape geometry);
ALTER TABLE {e_type}_cer_canonical ADD COLUMN ID SERIAL PRIMARY KEY;

-- This is a temporary table that might exist in the canonical dataset, containing provisory data.
-- Tipically, this table is used to gather data while a new PRODES dataset is not fully released.
-- If it's not, it will be harmless to import it anyways.
-- What really matters is if it's included in the final canonical table during
-- the transform phase (see UNION clauses at transform.sql).
CREATE TABLE {e_type}_cer_prov_canonical AS
  SELECT * FROM dblink('conn_db_link_desmatamento', 'SELECT objectid, legenda, area_ha, shape FROM isa.desmatestimadoamz') AS
      {e_type}_cer_prov_canonical (objectid int, legenda varchar, area_ha numeric, shape geometry);
ALTER TABLE {e_type}_cer_prov_canonical ADD COLUMN ID SERIAL PRIMARY KEY;

-- Disconnect
SELECT dblink_disconnect('conn_db_link_desmatamento');

--SELECT UpdateGeometrySRID('{e_type}_amz_canonical',      'shape', {srid});
--SELECT UpdateGeometrySRID('{e_type}_cer_canonical',      'shape', {srid});
--SELECT UpdateGeometrySRID('{e_type}_amz_prov_canonical', 'shape', {srid});
--SELECT UpdateGeometrySRID('{e_type}_cer_prov_canonical', 'shape', {srid});

-- Create indexes
CREATE INDEX sidx_{e_type}_amz_canonical_geom      ON {e_type}_amz_canonical      USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_{e_type}_cer_canonical_geom      ON {e_type}_cer_canonical      USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_{e_type}_amz_prov_canonical_geom ON {e_type}_amz_prov_canonical USING gist (shape) TABLESPACE pg_default;
CREATE INDEX sidx_{e_type}_cer__provcanonical_geom ON {e_type}_cer_prov_canonical USING gist (shape) TABLESPACE pg_default;

-- Needed by routines running under the regular user
GRANT SELECT ON {e_type}_amz_canonical      TO {main_db_user};
GRANT SELECT ON {e_type}_cer_canonical      TO {main_db_user};
GRANT SELECT ON {e_type}_amz_prov_canonical TO {main_db_user};
GRANT SELECT ON {e_type}_cer_prov_canonical TO {main_db_user};
