#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ISA default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from importers.isa_dblink.config import type, collection, metadata, archive
from lib.metadata.upstream       import get_download_id
from lib.config                  import config

# Basic configuration
name        = 'isa'
description = 'Imports ISA territory dataset collection from upstream Alertas+'
pipeline    = 'el'

# Available datasets, location and filename params
datasets     = [ 'basins_lvl2_ana', 'bla_base250', 'bla_base250_wgs84', 'mun_base250', 'territory', 'ufs_base250', ]
base_prefix  = 'isa-'
filename     = base_prefix + '{dataset}-{download_id}.sql.bz2'
base_url     = config['upstream']['url'] + '/data/exports/tables/' + filename

# Set a fixed download_id
#download_id = 'sample'
#download_id = 20210721

# Get the download_id from the canonical API.
# Ask for the "isa_territory_table" which has the same download_id from all
# other datasets since they're exported together.
download_id  = get_download_id('isa_territory_table')
