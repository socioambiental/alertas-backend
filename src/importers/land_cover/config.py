#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Land cover default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
#from lib.metadata.upstream import get_download_id
from lib.config             import config

# Basic configuration
name        = 'land_cover'
collection  = 'land_cover'
description = ''
description = 'Imports land cover raster from Alertas+ and based on MapBiomas collection'
pipeline    = 'el'
type        = 'other'

# Available datasets, location and filename params
datasets     = [ 'main' ]
base_prefix  = 'mapbiomas-land_cover-'
filename     = base_prefix + '{download_id}.sql.bz2'
base_url     = config['upstream']['url'] + '/data/exports/tables/' + filename

# Set the download id
#download_id = 'sample'
#download_id = get_download_id('land_cover_land_cover_table')
download_id = 20210627

# Metadata
metadata = {
        'name'       : 'Land cover based on MapBiomas',
        'description': {
            'en'   : 'Land cover compiled using MapBiomas Collection 5 (1985-2019)',
            'pt-br': 'Cobertura vegetal compilada usando o a Coleção 5 (1985-2019) do MapBiomas',
            'es'   : 'Cobertura vegetal compilada usando la Coleción 5 (1985-2019) del MapBiomas',
            },
        'url'            : 'https://mapbiomas.org/colecoes-mapbiomas-1',
        'methodology_url': 'https://mapbiomas.org/visao-geral-da-metodologia',
        'license_type'   : 'CC BY-SA 4.0',
        'license_url'    : 'https://mapbiomas.org/termosdeuso',
        'license_text': {
            'en'   : 'Project MapBiomas - Collection [version] of Brazilian Land Cover & Use Map Series, accessed on ' + str(download_id) + ' through the link: https://mapbiomas.org/colecoes-mapbiomas-1. MapBiomas Project - is a multi-institutional initiative to generate annual land cover and use maps using automatic classification processes applied to satellite images. The complete description of the project can be found at http://mapbiomas.org',
            'pt-br': 'Projeto MapBiomas – Coleção 5 da Série Anual de Mapas de Uso e Cobertura da Terra do Brasil, acessado em ' + str(download_id) + ' através do link: https://mapbiomas.org/colecoes-mapbiomas-1. Projeto MapBiomas - é uma iniciativa multi-institucional para gerar mapas anuais de uso e cobertura da terra a partir de processos de classificação automática aplicada a imagens de satélite. A descrição completa do projeto encontra-se em http://mapbiomas.org',
            'es'   : 'Projeto MapBiomas – Coleción 5 de la Serie Anual de Mapas de Uso y Cobertura de Terra do Brasil, acessado en ' + str(download_id) + ' atraves del link: https://mapbiomas.org/colecoes-mapbiomas-1. Projecto MapBiomas - es una iniciativa multi-institucional para generar mapas anuais de uso y cobertura de la terra a partir de processos de classificación automatica aplicada a imagenes de satelite. La descrición completa del projeto si encuentra en http://mapbiomas.org',
            },
        'citation_doi': '10.3390/rs12172735',
        'citation'    : {
            'en'   : 'Souza at. al. (2020) - Reconstructing Three Decades of Land Use and Land Cover Changes in Brazilian Biomes with Landsat Archive and Earth Engine - Remote Sensing, Volume 12, Issue 17, 10.3390/rs12172735.',
            'pt-br': 'Souza at. al. (2020) - Reconstructing Three Decades of Land Use and Land Cover Changes in Brazilian Biomes with Landsat Archive and Earth Engine - Remote Sensing, Volume 12, Issue 17, 10.3390/rs12172735.',
            'es'   : 'Souza at. al. (2020) - Reconstructing Three Decades of Land Use and Land Cover Changes in Brazilian Biomes with Landsat Archive and Earth Engine - Remote Sensing, Volume 12, Issue 17, 10.3390/rs12172735.',
            },
        }

# Archive management
archive = {
        'redistribute': True,
        'safeguard'   : False,
        }
