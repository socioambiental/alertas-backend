/**
 * This is the Google Earth Engine script used to generate the Land Cover raster.
 * Originally hosted at https://code.earthengine.google.com/89f4fe5faeedaa0dcaecfa4463bfed3d
 */

var mapaMB = ee.Image('projects/mapbiomas-workspace/public/collection5/mapbiomas_collection50_integration_v1')

print (mapaMB)

var BLA = ee.FeatureCollection('users/juandb/PRODES2019/brazilian_legal_amazon')

var remap_origin=[1,2,3,4,5,9,10,11,12,32,29,13,14,15,18,19,39,20,41,36,21,22,23,24,30,25,26,33,31,27]
var remap_transf=[1,1,1,2,1,1,2,2,2,2,4,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4]

function remap_band(band) { return mapaMB.remap(remap_origin,remap_transf,null,band) }

var bands=mapaMB.bandNames()

var mapaMB_remap = ee.ImageCollection.fromImages(bands.map(remap_band)).toBands().clip(BLA).toByte().rename(bands)

print (mapaMB_remap)

Map.addLayer(mapaMB)
Map.addLayer(mapaMB_remap.select('classification_2000'),{palette:['green','brown','yellow','blue'],min:1,max:4})

Export.image.toDrive({
  image:mapaMB_remap,
  description:'mapaMB_remap',
  region:BLA,
  scale:250,
  maxPixels: 1388696320
})
