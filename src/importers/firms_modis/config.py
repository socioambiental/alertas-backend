#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# FirmsModis default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic configuration
name         = 'firms_modis'
collection   = 'firms_modis'
description  = 'Imports last 24 hour MODIS fire data from FIRMS'
dependencies = [ 'isa', 'biomass', 'land_cover' ]

# Available datasets, location and filename params
datasets    = [ 'daily' ]
base_url    = 'https://firms2.modaps.eosdis.nasa.gov/data/active_fire/c6/shapes/zips/MODIS_C6_South_America_24h.zip'
base_prefix = 'firms_modis-'
base_suffix = ''
shapefiles  = ['MODIS_C6_South_America_24h.shp']

# How to deal with new data
update_mode = 'accumulate'

# Date pattern in the dataset file name
#date_pattern = r"([0-9]{4})([a-zA-Z][a-z]{2})([0-9]{2})"

# Hardcoded month table as used in the dataset file name
# months = {
#            'Jan': '01',
#            'Fev': '02',
#            'Mar': '03',
#            'Abr': '04',
#            'Mai': '05',
#            'Jun': '06',
#            'Jul': '07',
#            'Ago': '08',
#            'Set': '09',
#            'Out': '10',
#            'Nov': '11',
#            'Dez': '12'
#          }

# Class information in the final converted dataset, after transform stage
classes = {
        1: {
            'name': 'forest_fire',
            'subclasses': {
                1: 'fire_hotspot'
                },
            'emission_factor': 41.18669418
            },
        2: {
            'name': 'savanna_fire',
            'subclasses': {
                1: 'fire_hotspot'
            },
            'emission_factor': 32.3725524
        },
        3: {
            'name': 'agriculture_fire',
            'subclasses': {
                1: 'fire_hotspot'
            },
            'emission_factor': 10.19969235
        },
        4: {
            'name': 'other_fires',
            'subclasses': {
                1: 'fire_hotspot'
            },
            'emission_factor': 0.0
        }
}

# Metadata
metadata = {
        'name'       : 'FIRMS MODIS NRT',
        'description': {
            'en'   : 'NASA FIRMS MODIS Collection 61 NRT',
            'pt-br': 'NASA FIRMS MODIS Collection 61 NRT',
            'es'   : 'NASA FIRMS MODIS Collection 61 NRT',
            },
        'url'            : 'https://firms.modaps.eosdis.nasa.gov',
        'methodology_url': 'https://earthdata.nasa.gov/faq/firms-faq',
        'license_type'   : 'custom',
        'license_url'    : 'https://earthdata.nasa.gov/earth-observation-data/near-real-time/citation',
        'license_text': {
            'en'   : "We acknowledge the use of data and/or imagery from NASA's Fire Information for Resource Management System (FIRMS) (https://earthdata.nasa.gov/firms), part of NASA's Earth Observing System Data and Information System (EOSDIS).",
            'pt-br': "Reconhecemos o uso de dados e/ou imagens da NASA's Fire Information for Resource Management System (FIRMS) (https://earthdata.nasa.gov/firms), parte do NASA's Earth Observing System Data and Information System (EOSDIS).",
            'es'   : "Reconocemos el uso de datos y/o imágenes de NASA's Fire Information for Resource Management System (FIRMS) (https://earthdata.nasa.gov/firms), parte do NASA's Earth Observing System Data and Information System (EOSDIS).",
            },
        'citation_doi': '10.5067/FIRMS/MODIS/MCD14DL.NRT.0061',
        'citation'    : {
            'en'   : 'MODIS Collection 61 NRT Hotspot / Active Fire Detections MCD14DL distributed from NASA FIRMS. Available on-line [https://earthdata.nasa.gov/firms]. 10.5067/FIRMS/MODIS/MCD14DL.NRT.0061',
            'pt-br': 'MODIS Collection 61 NRT Hotspot / Active Fire Detections MCD14DL distribuído pela NASA FIRMS. Disponível on-line [https://earthdata.nasa.gov/firms]. 10.5067/FIRMS/MODIS/MCD14DL.NRT.0061',
            'es'   : 'MODIS Collection 61 NRT Hotspot / Active Fire Detections MCD14DL distribuido por NASA FIRMS. Disponible on-line [https://earthdata.nasa.gov/firms]. 10.5067/FIRMS/MODIS/MCD14DL.NRT.0061',
            },
        }

# Archive management
archive = {
        'redistribute': False,
        'safeguard'   : False,
        }
