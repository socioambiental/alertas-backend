#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# FirmsModis dataset transformer.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from lib.database.decorators.psycopg2 import connect
from lib.importer.transform.sql       import SqlTransform
from .config                          import classes

class FirmsModisTransform(SqlTransform):
    @connect()
    def __init__(self, config, args = {}):
        self.dirname = os.path.dirname(__file__)

        super().__init__(config, args)

        # Needed for carbon emissions calculation
        self.classes = classes

        self.operations['preparation'] = {
                'info': 'Initial processing of {collection} dataset',
                'sql' : open(self.sql_local + "preparation.sql", "r").read(),
                }

        # This already done during preparation in a way that is not compatible with
        # the global BLA filtering script.
        #self.operations['bla'] = {
        #        'info'           : 'Filtering events only inside the Brazilian Legal Amazon',
        #        'sql'            : open(self.sql_global + "bla_filter.sql", "r").read(),
        #        'pivot_partition': 'event',
        #        }

        self.operations['compute'] = {
                'info': 'Intersecting {collection} datasets with territories',
                'sql' : open(self.sql_local + "compute_event_territory_table_pt.sql", "r").read(),
                }

        self.operations['carbon_emissions'] = {
               'info': 'Computing associated carbon emissions',
               'sql' : open(self.sql_global + "compute_carbon_emissions_hotspots.sql", "r").read(),
               }
