-- Set search path to include the wanted schemas in order
SET search_path to event_canonical,public;

-- Initial preparation of FIRMS MODIS dataset
UPDATE {e_type}_canonical SET geom = ST_SetSRID(geom, 4326) WHERE instrument = 'MODIS_daily';

-- Removal of data points outside Brazil Legal Amazon (BLA)
DELETE FROM {e_type}_canonical f WHERE
f.id IN (
    SELECT a.id FROM
      {e_type}_canonical a, base.bla_base250_wgs84 b
      WHERE a.instrument = 'MODIS_daily'
            AND NOT ST_intersects(a.geom, b.shape)
);

-- Input in EVENT table
--
-- Since this is a daily incremental importer with a punctual dataset, we don't
-- need to worry about lock concurrency when attaching the partition before
-- adding data.
CREATE TABLE IF NOT EXISTS event_processed.event_{e_type} PARTITION OF event_processed.event
    FOR VALUES IN ('{e_type}');

INSERT INTO event_processed.event_{e_type} (orig_id,type,class_1,class_2,date,coduf,uf,codmun,mun,geom)
SELECT f.id,'{e_type}'::varchar,ST_Value(lc.rast,least(extract(year from acq_date)::int-1984,35),f.geom),'FOCO_daily'::varchar,f.acq_date,mun.cod_uf,mun.siglauf,mun.cod_mun,mun.nome,st_transform(f.geom,{srid})
FROM {e_type}_canonical f
    JOIN base.MUN_base250 mun ON st_within(st_transform(f.geom,{srid}),mun.shape)
    JOIN base.landcover_MB lc ON st_intersects(f.geom,lc.rast)
    WHERE f.instrument = 'MODIS_daily';

UPDATE {e_type}_canonical SET instrument = 'MODIS' WHERE instrument = 'MODIS_daily';
