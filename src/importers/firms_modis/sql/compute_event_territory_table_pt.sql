-- Set search path to include the wanted schemas in order
SET search_path to event_processed,public;

-- Since this is a daily incremental importer with a punctual dataset, we don't
-- need to worry about lock concurrency wen attaching the partition before
-- adding data.
CREATE TABLE IF NOT EXISTS event_processed.event_territory_{e_type}
  PARTITION OF event_processed.event_territory
  FOR VALUES IN ('{e_type}');

-- Insert newly loaded fire hotspot data on event_territory table
INSERT INTO event_territory_{e_type} (e_id,e_type, e_class_1, e_class_2, e_date, t_type, t_name, t_id_orig, id_mun, id_uf, name_mun, name_uf, basin, geom)
  SELECT e.orig_id, e.type, e.class_1, 'FOCO'::varchar, e.date, t.type, t.name, t.id_orig, e.codmun, e.coduf, e.mun, e.uf, basin.bacia, e.geom AS geom
        FROM event_{e_type} e
        JOIN base.territory t ON st_intersects(e.geom,t.geom)
        JOIN base.BASINS_lvl2_ana basin ON st_intersects(e.geom,basin.shape)
        WHERE class_2 = 'FOCO_daily';
        --WHERE e.UF='AC';

-- Rename type attribute of new fire hotspot data
UPDATE event_{e_type} SET class_2 = 'FOCO' WHERE class_2='FOCO_daily';
