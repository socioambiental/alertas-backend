#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# FirmsModis dataset loader.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.importer.load.shapely import ShapelyLoad

class FirmsModisLoad(ShapelyLoad):
    """Process a FirmsModis shapefile."""

    def create_model(self):
        """Create the database model."""

        # Import model definition
        from .datamodel import FirmsModisCanonical

        # Ensure the database has the modeled tables and cols
        FirmsModisCanonical.metadata.create_all(self.engine)

        return FirmsModisCanonical

    def add_record(self, record, session, Model):
        """
        Add a single shape record into the database.

        :param record  object: Shapefile record (pyshp).
        :param session object: Database session.
        :param Model   object: Model.
        """

        from shapely.geometry import Point, shape

        # Put shape record attributes into the event structure
        record = Model(
                latitude    = record.record[0],
                longitude   = record.record[1],
                brightness  = record.record[2],
                scan        = record.record[3],
                track       = record.record[4],
                acq_date    = record.record[5],
                acq_time    = record.record[6],
                satellite   = record.record[7],
                instrument  = 'MODIS_daily',
                confidence  = record.record[8],
                version     = record.record[9],
                #bright_t31 = record.record[10],
                frp         = record.record[11],
                daynight    = record.record[12],
                #geom       = Point(tuple(record.shape.points)).wkt,
                geom        = shape(record.shape.__geo_interface__).wkt,
                )

        session.add(record)
