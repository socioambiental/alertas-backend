-- Set search path to include the wanted schemas in order
SET search_path to event_canonical,public;

-- Remove any existing servers
DROP SERVER IF EXISTS server_desmatamento CASCADE;

-- Remove any existing canonical tables
DROP TABLE IF EXISTS sad_canonical;

-- Create servers
CREATE SERVER IF NOT EXISTS server_desmatamento FOREIGN DATA WRAPPER dblink_fdw OPTIONS (host '{desmatamento_host}', dbname '{desmatamento_db}', port '{desmatamento_port}');

-- Create user mappings
CREATE USER MAPPING FOR {main_db_admin} SERVER server_desmatamento OPTIONS (user '{desmatamento_user}', password '{desmatamento_password}');

-- Set permissions
GRANT USAGE ON FOREIGN SERVER server_desmatamento TO {main_db_admin};

-- Setup connections
SELECT dblink_connect('conn_db_link_desmatamento', 'server_desmatamento');

CREATE TABLE sad_canonical AS
  SELECT * from dblink('conn_db_link_desmatamento', 'select objectid, sistema, hectares, mesano, uf, shape from isa.alertasareassad') AS
      sad_canonical (objectid int, sistema varchar, hectares numeric, mesano varchar, uf varchar, shape geometry);

--SELECT UpdateGeometrySRID('sad_canonical', 'shape', {srid});

-- Disconnect
SELECT dblink_disconnect('conn_db_link_desmatamento');

-- Add primary keys
ALTER TABLE sad_canonical ADD COLUMN ID SERIAL PRIMARY KEY;

-- Create indexes
CREATE INDEX sidx_sad_canonical_geom ON sad_canonical   USING gist (shape) TABLESPACE pg_default;

-- Needed by routines running under the regular user
GRANT SELECT ON sad_canonical TO {main_db_user};
