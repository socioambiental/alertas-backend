#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ISA default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic configuration
name         = 'sad_dblink'
collection   = 'sad'
description  = 'Imports SAD/Imazon dataset using a direct dblink connection with ISA internal database'
pipeline     = 'lt'
dependencies = [ 'isa', 'biomass', 'land_cover' ]

# Class information in the final converted dataset, after transform stage
classes = {
            1: {
                'name': 'deforestation',
                'subclasses': {
                    1: 'deforestation'
                    },
                'emission_factor': 1
                },

            2: {
                'name': 'degradation',
                'subclasses': {
                    1: 'degradation',
                    },
                'emission_factor': 0.35
                }
            }

# Metadata
metadata = {
        'name'       : 'Sistema de Alerta de Desmatamento - SAD/Imazon',
        'description': {
            'en'   : 'The Deforestation Alert System is an Amazon monitoring tool based on satellite images, developed by Imazon in 2008 to monthly report the pace of deforestation and forest degradation in the region. Deforestation is the total conversion of the forest to another land cover and use, while degradation is a partial disturbance in the forest caused by logging and/or forest fires.',
            'pt-br': 'O Sistema de Alerta de Desmatamento é uma ferramenta de monitoramento da Amazônia baseada em imagens de satélites, desenvolvida pelo Imazon em 2008, para reportar mensalmente o ritmo do desmatamento e da degradação florestal na região. O desmatamento consiste na conversão total da floresta para outra cobertura e uso do solo, enquanto a degradação é um distúrbio parcial na floresta causado pela extração de madeira e/ou por queimadas florestais.',
            'es'   : 'El Sistema de Alerta de Deforestación es una herramienta de seguimiento de la Amazonia basada en imágenes de satélite, desarrollada por Imazon en 2008 para informar mensualmente sobre el ritmo de deforestación y degradación de los bosques en la región. La deforestación consiste en la conversión total del bosque en otra cubierta y uso del suelo, mientras que la degradación es una alteración parcial del bosque causada por la extracción de madera y/o los incendios forestales.',
            },
        #'url'           : 'https://imazon.org.br/boletim-do-desmatamento-sad/',
        #'url'           : 'https://imazongeo.org.br',
        'url'            : 'https://imazon.org.br/publicacoes/faq-sad/',
        'methodology_url': 'https://imazon.org.br/PDFimazon/Portugues/transparencia_florestal/amazonia_legal/SAD-Marco2015.pdf',
        'license_type'   : '',
        'license_url'    : '',
        'license_text': {
            'en'   : '',
            'pt-br': '',
            'es'   : '',
            },
        'citation_doi': '',
        'citation'    : {
            'en': 'Instituto do Homem e Meio Ambiente da Amazônia (IMAZON). Boletim Transparência Florestal. May 2021. Available at: <http://www.imazon.org.br/>. Acessed at 05 July 2021.',
            'pt-br': 'Instituto do Homem e Meio Ambiente da Amazônia (IMAZON). Boletim Transparência Florestal. Maio de 2021. Disponível em: <http://www.imazon.org.br/>. Acesso em 05 jul. 2021.',
            'es': 'Instituto do Homem e Meio Ambiente da Amazônia (IMAZON). Boletim Transparência Florestal. Maio de 2021. Disponble en: <http://www.imazon.org.br/>. Acesso en 05 jul. 2021.',
            },
        }

# Archive management
archive = {
        'redistribute': False,
        'safeguard'   : False,
        }
