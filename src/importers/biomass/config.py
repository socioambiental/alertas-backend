#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Biomass default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
#from lib.metadata.upstream import get_download_id
from lib.config             import config

# Basic configuration
name        = 'biomass'
collection  = 'biomass'
description = 'Imports biomass raster from Alertas+ and based on ESA biomass density maps'
pipeline    = 'el'
type        = 'other'

# Available datasets, location and filename params
datasets     = [ 'main' ]
base_prefix  = 'esa-biomassdensity-'
filename     = base_prefix + '{download_id}.sql.bz2'
base_url     = config['upstream']['url'] + '/data/exports/tables/' + filename

# Set the download id
#download_id = 'sample'
#download_id = get_download_id('biomass_biomass_table')
download_id = 20210627

# Metadata
metadata = {
        'name'       : 'ESA Biomass Climate Change Initiative (Biomass_cci): Global datasets of forest above-ground biomass for the years 2010, 2017 and 2018, v2',
        'description': {
            'en'   : 'Map of above ground biomass, generated using space observations, is set to help our understanding of global carbon cycling and support forest management, emissions reduction and sustainable development policy goals.',
            'pt-br': 'Mapa da biomassa acima do solo, gerada através de observações espaciais, é estabelecido para ajudar nosso entendimento do ciclo global de carbono e apoiar o manejo florestal, a redução de emissões e as metas da política de desenvolvimento sustentável',
            'es'   : 'Mapa de la biomasa sobre el suelo, generado a partir de observaciones espaciales, ayudará a comprender el ciclo global del carbono y apoyará la gestión forestal, la reducción de emisiones y los objetivos de la política de desarrollo sostenible',
            },
        'url'            : 'https://data.ceda.ac.uk/neodc/esacci/biomass/data/agb/maps/v2.0/',
        'methodology_url': 'https://climate.esa.int/en/news-events/maps-improve-forest-biomass-estimates/',
        'license_type'   : 'custom',
        'license_url'    : 'https://artefacts.ceda.ac.uk/licences/specific_licences/esacci_biomass_terms_and_conditions.pdf',
        'license_text': {
            'en'   : 'Data provided by the ESA Climate Change Initiative and the Sea Surface Salinity CCI together with the individual data providers',
            'pt-br': 'Dados disponibilizados pela ESA Climate Change Initiative e pelo Sea Surface Salinity CCI juntamente com provedores de dados individuais',
            'es'   : 'Datos facilitados por la ESA Climate Change Initiative y el projecto Sea Surface Salinity CCI, junto con proveedores de datos individuales',
            },
        'citation_doi': '10.5285/84403d09cef3485883158f4df2989b0c',
        'citation'    : {
            'en'   : 'Santoro, M.; Cartus, O. (2021): ESA Biomass Climate Change Initiative (Biomass_cci): Global datasets of forest above-ground biomass for the years 2010, 2017 and 2018, v2. Centre for Environmental Data Analysis, 17 March 2021. doi:10.5285/84403d09cef3485883158f4df2989b0c. http://dx.doi.org/10.5285/84403d09cef3485883158f4df2989b0c',
            'pt-br': 'Santoro, M.; Cartus, O. (2021): ESA Biomass Climate Change Initiative (Biomass_cci): Global datasets of forest above-ground biomass for the years 2010, 2017 and 2018, v2. Centre for Environmental Data Analysis, 17 March 2021. doi:10.5285/84403d09cef3485883158f4df2989b0c. http://dx.doi.org/10.5285/84403d09cef3485883158f4df2989b0c',
            'es'   : 'Santoro, M.; Cartus, O. (2021): ESA Biomass Climate Change Initiative (Biomass_cci): Global datasets of forest above-ground biomass for the years 2010, 2017 and 2018, v2. Centre for Environmental Data Analysis, 17 March 2021. doi:10.5285/84403d09cef3485883158f4df2989b0c. http://dx.doi.org/10.5285/84403d09cef3485883158f4df2989b0c',
            },
        }

# Archive management
archive = {
        # From the upstream license:
        #
        # "The CCI datasets held on the CCI Open Data Portal may be used
        # by any user for any purpose, with the following terms and conditions [...]
        # Users of the CCI data are encouraged to interact with the CCI
        # programme on use of the products, and to provide a copy of all
        # reports and publications using the dataset."
        'redistribute': True,
        'safeguard'   : True,
        }
