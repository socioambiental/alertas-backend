#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# FirmsModis Arquive default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic configuration
name         = 'firms_modis_arquive'
collection   = 'firms_modis'
description  = 'Imports MODIS fire data archive downloads'
dependencies = [ 'isa', 'biomass', 'land_cover' ]

# Available datasets, location and filename params
download_id = 239606
datasets    = [ 'arquive' ]
#base_url   = 'https://firms2.modaps.eosdis.nasa.gov/data/download/DL_FIRE_M6_{download_id}.zip'
#base_url   = 'https://firms.modaps.eosdis.nasa.gov/data/download/DL_FIRE_M6_{download_id}.zip'
base_url    = 'https://firms.modaps.eosdis.nasa.gov/data/download/DL_FIRE_M-C61_{download_id}.zip'
base_prefix = 'firms_modis-'
#shapefiles = ['fire_nrt_M6_{id}.shp', 'fire_archive_M6_{id}.shp']
shapefiles  = ['fire_nrt_M-C61_{id}.shp', 'fire_archive_M-C61_{id}.shp']

# How to deal with new data
update_mode = 'truncate_old_data'
date_field  = 'acq_date'

# Date pattern in the dataset file name
#date_pattern = r"([0-9]{4})([a-zA-Z][a-z]{2})([0-9]{2})"

# Hardcoded month table as used in the dataset file name
# months = {
#            'Jan': '01',
#            'Fev': '02',
#            'Mar': '03',
#            'Abr': '04',
#            'Mai': '05',
#            'Jun': '06',
#            'Jul': '07',
#            'Ago': '08',
#            'Set': '09',
#            'Out': '10',
#            'Nov': '11',
#            'Dez': '12'
#          }

# Class information in the final converted dataset, after transform stage
from importers.firms_modis.config import classes

# Metadata
metadata = {
        'name'       : 'FIRMS MODIS Standard',
        'description': {
            'en'   : 'NASA FIRMS MODIS Collection 61 Standard processing',
            'pt-br': 'NASA FIRMS MODIS Collection 61 Standard processing',
            'es'   : 'NASA FIRMS MODIS Collection 61 Standard processing',
            },
        'url'            : 'https://firms.modaps.eosdis.nasa.gov',
        'methodology_url': 'https://earthdata.nasa.gov/faq/firms-faq',
        'license_type'   : 'custom',
        'license_url'    : 'https://earthdata.nasa.gov/earth-observation-data/near-real-time/citation',
        'license_text': {
            'en'   : "We acknowledge the use of data and/or imagery from NASA's Fire Information for Resource Management System (FIRMS) (https://earthdata.nasa.gov/firms), part of NASA's Earth Observing System Data and Information System (EOSDIS).",
            'pt-br': "Reconhecemos o uso de dados e/ou imagens da NASA's Fire Information for Resource Management System (FIRMS) (https://earthdata.nasa.gov/firms), parte do NASA's Earth Observing System Data and Information System (EOSDIS).",
            'es'   : "Reconocemos el uso de datos y/o imágenes de NASA's Fire Information for Resource Management System (FIRMS) (https://earthdata.nasa.gov/firms), parte do NASA's Earth Observing System Data and Information System (EOSDIS).",
            },
        'citation_doi': '10.5067/FIRMS/MODIS/MCD14ML',
        'citation'    : {
            'en'   : 'MODIS Collection 6 Hotspot / Active Fire Detections MCD14ML distributed from NASA FIRMS. Available on-line [https://earthdata.nasa.gov/firms]. doi: 10.5067/FIRMS/MODIS/MCD14ML',
            'pt-br': 'MODIS Collection 6 Hotspot / Active Fire Detections MCD14ML distributed from NASA FIRMS. Available on-line [https://earthdata.nasa.gov/firms]. doi: 10.5067/FIRMS/MODIS/MCD14ML',
            'es'   : 'MODIS Collection 6 Hotspot / Active Fire Detections MCD14ML distributed from NASA FIRMS. Available on-line [https://earthdata.nasa.gov/firms]. doi: 10.5067/FIRMS/MODIS/MCD14ML',
            },
        }

# Archive management
from importers.firms_modis.config import archive
