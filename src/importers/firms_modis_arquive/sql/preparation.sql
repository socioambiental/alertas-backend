-- Insert FIRMS MODIS dataset on event table

-- Set search path to include the wanted schemas in order
SET search_path to event_processed,public;

--
-- BEGIN transaction for insertion
--
BEGIN;

-- Do not create a partition right now to avoid a lock in the event table in
-- case of simultaneous imports
--DROP TABLE IF EXISTS event_{e_type};
--CREATE TABLE event_{e_type} PARTITION OF event
--    FOR VALUES IN ('{e_type}');

-- Deletes any previous work table
DROP TABLE IF EXISTS event_{e_type}_new;

-- Input arquive data in EVENT table
--CREATE TABLE event_{e_type} AS
CREATE TABLE event_{e_type}_new (LIKE event INCLUDING DEFAULTS INCLUDING CONSTRAINTS);
INSERT INTO event_{e_type}_new (orig_id,type,class_1,class_2,date,coduf,uf,codmun,mun,geom)
  SELECT f.id AS orig_id, '{e_type}'::varchar AS type,ST_Value(lc.rast,least(extract(year from acq_date)::int-1984,35),f.geom),
    'FOCO'::varchar AS class_2, f.acq_date AS date, mun.cod_uf AS coduf, mun.siglauf AS uf,
    mun.cod_mun AS codmun, mun.nome AS mun, st_transform(f.geom,{srid}) AS geom
  FROM event_canonical.{e_type}_canonical f
  JOIN base.MUN_base250 mun ON st_within(st_transform(f.geom,{srid}),mun.shape)
  JOIN base.landcover_MB lc ON st_intersects(f.geom,lc.rast);

--
-- COMMIT transaction for insertion
--
COMMIT;
