#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# FirmsModis arquive dataset loader.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
from psycopg2                   import Error
from lib.importer.load.ogr      import OgrLoad
from lib.logger                 import logger
from importers.firms_modis.load import FirmsModisLoad

class FirmsModisArquiveLoad(OgrLoad):
    """Process a FirmsModis arquive shapefile."""

    def __init__(self, config, args = {}):
        super().__init__(config, args)

        # Set the model so truncate_table can initialize the model after
        # droping the table.
        #
        # This is ensures that Arquive importer can run before the regular
        # importer in a fresh database.
        self.model = FirmsModisLoad

        # Local SQL
        dirname        = os.path.dirname(__file__)
        self.sql_local = os.path.join(dirname, "sql") + os.sep
