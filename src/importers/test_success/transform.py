#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Success transformer.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.importer.success import SuccessImporter

class TestSuccessTransform(SuccessImporter):
    """Transform a success"""

if __name__ == "__main__":
    instance = TestSuccessTransform(config, args)

    instance.run()
