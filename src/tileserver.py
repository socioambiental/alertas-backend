#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Painel de Tileserver.
# Inspired by https://github.com/pramsey/minimal-mvt/blob/master/minimal-mvt.py
#             https://blog.crunchydata.com/blog/dynamic-vector-tiles-from-postgis
#             https://www.zimmi.cz/posts/2017/serving-mapbox-vector-tiles-with-postgis-nginx-and-python-backend/
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from flask               import Flask
from flask_cors          import CORS

from webargs             import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser, abort

import lib.cache.manager
import lib.api.config
import lib.tileserver.helpers as helpers

# App params
app = Flask(__name__)

# CORS support
CORS(app)

#
# Main route
#

@app.route("/")
def index():
    #abort(404)
    return ""

#
# Events tiles
#

from query import Query

lib.api.config.parse_action('tiles', Query.tiles, exclude_params = [ 'verbose' ])

@app.route("/events")
@use_args(lib.api.config.webargs['tiles'], location='query')
def events(args):
    args = lib.api.config.strip_params(args)

    # Instantiate
    inquiry = Query()

    # Dispatch
    #result = inquiry.tiles(**args)
    result  = lib.cache.manager.get('tiles', inquiry.tiles, **args)

    # Teardown
    inquiry.close()

    return helpers.build_response(result)

#
# Territory tiles
#

lib.api.config.parse_action('territories', Query.territories, exclude_params = [ 'verbose' ])

@app.route("/territories")
@use_args(lib.api.config.webargs['territories'], location='query')
def territories(args):
    args = lib.api.config.strip_params(args)

    # Instantiate
    inquiry = Query()

    # Dispatch
    #result = inquiry.tiles(**args)
    result  = lib.cache.manager.get('tiles', inquiry.territories, **args)

    # Teardown
    inquiry.close()

    return helpers.build_response(result)

#
# Main
#

if __name__ == "__main__":
    app.run()
