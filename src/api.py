#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Painel de Alertas API.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from flask                 import Flask
from flask_restful         import Api, Resource
from flask_cors            import CORS

from webargs               import fields, validate
from webargs.flaskparser   import use_args, use_kwargs, parser, abort

import lib.cache.manager
import lib.api.config

# Create the app and the REST API
app = Flask(__name__)
api = Api(app)

# CORS support
CORS(app)

#
# Events
#

from lib.api.actions.events import GetEvents

api.add_resource(GetEvents, '/events')

#
# Ranking
#

from lib.api.actions.ranking import GetRanking

api.add_resource(GetRanking, '/ranking')

#
# Series
#

from lib.api.actions.series import GetSeries

api.add_resource(GetSeries, '/series')

#
# Totals
#

from lib.api.actions.totals import GetTotals

api.add_resource(GetTotals, '/totals')

#
# Outside
#

from lib.api.actions.outside import GetOutside

api.add_resource(GetOutside, '/outside')

#
# GeoJSON
#

from lib.api.actions.geojson import GetGeoJSON

api.add_resource(GetGeoJSON, '/geojson')

#
# Heat
#

from lib.api.actions.heat import GetHeat

api.add_resource(GetHeat, '/heat')

#
# Basin
#

from lib.api.actions.basin import GetBasin

api.add_resource(GetBasin, '/basin')

#
# SisARP: ARP
#

from lib.api.actions.arp import GetArp

api.add_resource(GetArp, '/arp')

#
# SisARP: UF
#

from lib.api.actions.uf import GetUf

api.add_resource(GetUf, '/uf')

#
# Municipality
#

#from lib.api.actions.sisarp      import GetMunicipality
from lib.api.actions.municipality import GetMunicipality

api.add_resource(GetMunicipality, '/municipality')

#
# News
#

from lib.api.actions.news import GetNews

api.add_resource(GetNews, '/news')

#
# Metadata
#

from lib.api.actions.meta import GetMeta

api.add_resource(GetMeta, '/meta')

#
# Spec
#

from lib.api.actions.spec import GetSpec

api.add_resource(GetSpec, '/spec')

#
# Index
#

from lib.api.actions.index import GetIndex

api.add_resource(GetIndex, '/')

#
# Main
#

if __name__ == '__main__':
    #app.run(debug=True, host="0.0.0.0", port="5001")
    #app.run(host="0.0.0.0", port="5001")
    app.run()
