#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Biomass data exporter.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.exporter.base import BaseExporter

class BiomassExport(BaseExporter):
    def __init__(self, config, args = {}):
        super().__init__(config, args)

        # Export the Biomass table
        #
        # This is intended for those that wish to run their own alertas-backend
        # instance.
        #
        # We use a database export procedure instead of a simple file copying
        # because this dataset is compiled by an analyst using an alertas-bakend
        # database instance.
        self.operations["biomass_table"] = {
                'info'  : 'Exporting the biomass table',
                'type'  : 'table',
                'prefix': 'esa-biomassdensity',
                'params': 'base.biomassdensity',
                }

        # Export the Biomass raster as GeoTIFF
        self.operations["biomass_geotiff"] = {
                'info'  : 'Exporting the biomass raster',
                'type'  : 'geotiff',
                'prefix': 'esa-biomassdensity',
                'params': 'base.biomassdensity',
                }
