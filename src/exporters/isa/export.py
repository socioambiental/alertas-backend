#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ISA data exporter.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.exporter.base import BaseExporter

class IsaExport(BaseExporter):
    def __init__(self, config, args = {}):
        super().__init__(config, args)

        # Export territory polygons
        #
        # This enables ISA to keep an updated and publilcy available territory
        # dataset.
        self.operations["territory_shapefiles"] = {
                'info'  : 'Exporting territory shapefiles',
                'type'  : 'shapefile',
                'prefix': 'isa-territory',
                'params': 'SELECT * FROM base.territory;',
                }

        # Export the base schema
        #
        # This enables ISA to keep an updated and publilcy available territory
        # dataset.
        #
        # It's also intended for those that wish to run their own alertas-backend
        # instance.
        #
        # But this procedure is too intensive.
        #self.operations["base_schema"] = {
        #        'info'  : 'Exporting the base schema',
        #        'type'  : 'schema',
        #        'prefix': 'isa-base-schema',
        #        'params': 'base',
        #        }

        # Export the territory table
        #
        # This enables ISA to keep an updated and publilcy available territory
        # dataset.
        #
        # It's also intended for those that wish to run their own alertas-backend
        # instance.
        self.operations["territory_table"] = {
                'info'  : 'Exporting the territory table',
                'type'  : 'table',
                'prefix': 'isa-territory',
                'params': 'base.territory',
                }

        # Export the basins shapefile
        self.operations["basins_shapefile"] = {
                'info'  : 'Exporting the basins table',
                'type'  : 'shapefile',
                'prefix': 'isa-basins_lvl2_ana',
                'params': 'SELECT * FROM base.basins_lvl2_ana',
                }

        # Export the basins table
        #
        # This is intended for those that wish to run their own alertas-backend
        # instance.
        self.operations["basins_table"] = {
                'info'  : 'Exporting the basins table',
                'type'  : 'table',
                'prefix': 'isa-basins_lvl2_ana',
                'params': 'base.basins_lvl2_ana',
                }

        # Export the municipality shapefile
        self.operations["municipality_shapefile"] = {
                'info'  : 'Exporting the municipality shapefile',
                'type'  : 'shapefile',
                'prefix': 'isa-mun_base250',
                'params': 'SELECT * FROM base.mun_base250',
                }

        # Export the municipality table
        #
        # This is intended for those that wish to run their own alertas-backend
        # instance.
        self.operations["municipality_table"] = {
                'info'  : 'Exporting the municipality table',
                'type'  : 'table',
                'prefix': 'isa-mun_base250',
                'params': 'base.mun_base250',
                }

        # Export the UFs shapefile
        self.operations["ufs_shapefile"] = {
                'info'  : 'Exporting the UFs shapefile',
                'type'  : 'shapefile',
                'prefix': 'isa-ufs_base250',
                'params': 'SELECT * FROM base.ufs_base250',
                }

        # Export the UFs table
        #
        # This is intended for those that wish to run their own alertas-backend
        # instance.
        self.operations["ufs_table"] = {
                'info'  : 'Exporting the UFs table',
                'type'  : 'table',
                'prefix': 'isa-ufs_base250',
                'params': 'base.ufs_base250',
                }

        # Export the BLA shapefile
        self.operations["bla_shapefile"] = {
                'info'  : 'Exporting the BLA shapefile',
                'type'  : 'shapefile',
                'prefix': 'isa-bla_base250',
                'params': 'SELECT * FROM base.bla_base250',
                }

        # Export the BLA table
        #
        # This is intended for those that wish to run their own alertas-backend
        # instance.
        self.operations["bla_table"] = {
                'info'  : 'Exporting the BLA table',
                'type'  : 'table',
                'prefix': 'isa-bla_base250',
                'params': 'base.bla_base250',
                }

        # Export the BLA shapefile - WGS84 version
        self.operations["bla_shapefile_wgs84"] = {
                'info'  : 'Exporting the BLA shapefile',
                'type'  : 'shapefile',
                'prefix': 'isa-bla_base250_wgs84',
                'params': 'SELECT * FROM base.bla_base250_wgs84',
                }

        # Export the BLA table - WGS84 version
        #
        # This is intended for those that wish to run their own alertas-backend
        # instance.
        self.operations["bla_table_wgs84"] = {
                'info'  : 'Exporting the BLA table',
                'type'  : 'table',
                'prefix': 'isa-bla_base250_wgs84',
                'params': 'base.bla_base250_wgs84',
                }
