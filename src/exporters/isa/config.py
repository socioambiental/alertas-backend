#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ISA default parameters.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Use basic settings from the ISA importer
from importers.isa_dblink.config import *

# Basic configuration
name        = 'isa'
description = 'Exports ISA territory dataset collection processed by the backend'

# Also depend on the ISA importer
dependencies = [ 'isa', ]
