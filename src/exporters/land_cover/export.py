#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Land cover data exporter.
#
# Copyright (C) 2021 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
from lib.exporter.base import BaseExporter

class LandCoverExport(BaseExporter):
    def __init__(self, config, args = {}):
        super().__init__(config, args)

        # Export the Land cover table
        #
        # This is intended for those that wish to run their own alertas-backend
        # instance.
        #
        # We use a database export procedure instead of a simple file copying
        # because this dataset is compiled by an analyst using an alertas-bakend
        # database instance.
        self.operations["land_cover_table"] = {
                'info'  : 'Exporting the land_cover table',
                'type'  : 'table',
                'prefix': 'mapbiomas-land_cover',
                'params': 'base.landcover_mb',
                }

        # Export the Land cover raster as GeoTIFF
        self.operations["land_cover_geotiff"] = {
                'info'  : 'Exporting the land_cover geotiff',
                'type'  : 'geotiff',
                'prefix': 'mapbiomas-land_cover',
                'params': 'base.landcover_mb',
                }
