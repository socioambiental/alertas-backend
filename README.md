# Alertas Backend

A system to download, ingest, process and provide data about socioenvironmental
threats used in the [Alertas+](https://alertas.socioambiental.org/?lang=en).

Compile multiple datasets, including:

* Pressures/menaces events:
  * [DETER / INPE](http://www.obt.inpe.br/OBT/assuntos/programas/amazonia/deter/deter)
    available from the [TerraBrasilis](http://terrabrasilis.dpi.inpe.br) platform.
  * [PRODES / INPE](http://www.obt.inpe.br/OBT/assuntos/programas/amazonia/prodes).
  * [FIRMS MODIS and VIIRS / NASA](https://firms.modaps.eosdis.nasa.gov/).
  * [Amazon Dashboard / GFED](https://globalfiredata.org/pages/en/amazon-dashboard/).
  * [SAD / Imazon](https://imazon.org.br/publicacoes/faq-sad/).

* Territories:
  * Protected Areas database from [Instituto Socioambiental](https://socioambiental.org).
  * Political entities (States and municipalities) from [IBGE](https://ibge.gov.br).

**ATTENTION: The project is in a stable phase, but still requires
documentation and usability improvements so that it can be more easily more
easily used by the community. Tests, suggestions and merge requests are welcome
:)**

## Documentation

* The general project documentation can be found at https://alertas.socioambiental.org/doc/en/.
* The portuguese version of this README is [here](README.pt-br.md).

## Requirements

Expertise:

* To run the backend instance, you need basic knowledge and familiarity with
  with containerized environments. Experience with with GNU/Linux system
  administration is also recommended.

Applications:

* [GNU Make](https://www.gnu.org/software/make/), for project-level tasks
  (see the [Makefile](Makefile)).
* [GNU Screen](https://www.gnu.org/software/screen), for basic service
  monitoring.
* [Docker v20.10](https://docs.docker.com) or greater, to create and run
  container services.
* [Docker Compose v1.27](https://docs.docker.com/compose/) or greater, to
  manage the services.

Services:

* A PostgreSQL 12 database with the extensions `postgis`, `postgis_raster`,
  `pgcrypto` and optionally `dblink` (only if you plan to use the `_dblink`
  importers).
* A PostgreSQL 12 database for the task manager [Apache Airflow](https://airflow.apache.org),
  if you plan to use this subsystem.

Minimal resources for development:

* 4 CPUs.
* 8GBB RAM.
* 50GB+ diskspace.

It's recommended to adjust resources depending on the number of request and
quantity of importers

## Installation

The basic backend installation involves the following steps:

0. Installation of dependencies.

1. Creating the PostgreSQL database with the necessary extensions, users, and
   permissions.

2. Editing the configuration files from the templates provided in this
   repository.

3. Provisioning the services (containers).

### Dependencies

In a Debian GNU/Linux-like system, dependencies can be installed with

    sudo apt-get install -y make screen

In the case of [Docker](https://docs.docker.com) and [Docker
Compose](https://docs.docker.com/compose/), it's recommended to follow the
official docs.

### Database

For Debian GNU/Linux based systems, there are convenience scripts that can be
used that require superuser (sudo) access:

    make provision-database

This will install PostgreSQL on your system and create the backend database.

If you prefer, manually set up a user and a PostgreSQL database with the
necessary schemas.

If you intend to import data directly from other PostgreSQL databases, `dblink`
also needs to be set up as well as a user with special privileges to perform
the imports.

### Configuration files

* Copy the file [config.ini.dist](config/config.ini.dist) to
  `config/config.ini` and edit it according to the PostgreSQL database access
  data you have created.

* Include the database access information in the `config/pgpass` file
  file in [pgpass format](https://wiki.postgresql.org/wiki/Pgpass),
  such as `host.docker.internal:5432:alertas:alertas_query:n7clJVsv4eY8mCgz96N`.
  This file can be used for database authentication in some maintenance routines.

* Optionally, include the database access information in the `~/.pgpass` file
  of the user who will access the database using the
  [pgpass format](https://wiki.postgresql.org/wiki/Pgpass), such as
  `localhost:5432:deter:deter:n7clJVsv4eY8mCgz96N`. This will make it easier to
  access to the database without having to enter a password.

### Provisioning

First, an `.env` file should be created with the needed parameters:

    make provision-container-env

Customize the `.env` file if needed before running the next step.

Then, provisioning might be requested with the following command:

    make run-containers

It does the following:

* Builds the basic [serviços](compose.yaml) básicos, installing all the needed dependencies.
* Initializes the services defined by the `COMPOSE_PROFILES` variable from the`.env` file.

By default, all services are built and started (`COMPOSE_PROFILES=full`).
The profiles are available in the [services definiton file](compose.yaml).
As an exanple, to run only basic services without [Apache Airflow](https://airflow.apache.org),
use the following value in the `.env` file:

    COMPOSE_PROFILES=main

### Initialization

Once the basic services are initialized, the system will:

* Bootstrap the database schemas.
* Imported the basic data required by the events/alerts processing.

To monitor the services, use the command

    make container-logs

Or, if you prefer, start the basic monitoring console:

    make console

### Web interface

The backend is composed by a number of web interfaces defined in the
[services file](compose.yaml). In special:

* A proxy for accesing all the services.
* A consulting API.
* A Tile Server for maps.
* An [Apache Airflow](https://airflow.apache.org) interface.

## Running

By convention, commands are issued inside the `worker` service definied at `compose.yaml`.
Commands may be invoked inside this service using the `./bin/runner` script.

As an example, to initialize the database, download and ingest the most recent data, use

    ./bin/runner worker make bootstrap
    ./bin/runner worker make import

## Credits

* Developed at [Instituto Socioambiental](https://www.socioambiental.org).
* Distributed under [GNU GPLv3+ License](./LICENSE).
