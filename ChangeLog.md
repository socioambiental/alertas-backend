# ChangeLog

## 1.3.5 - Upcoming

### List of completed tasks

* [x] Exporters:
  * [x] Prune old exports to avoid filling all available disk space.

## 1.3.5 - 20210813

### Highlights

### Description of changes

1. FIRMS: Remove all `bright_` fields from database. It was noted that those fields
   were changing in the dataset, leading to import errors. Removing them is harmless
   since they're currently not in use.

2. Changes in the `event_territory` index requires the following manual
   migration:

    SET search_path to event_processed;
    DROP INDEX idx_event_territory_type;
    CREATE INDEX idx_event_territory_type ON event_processed.event_territory(e_type,e_class_1,e_date,t_type,t_id_orig,id_mun,id_uf,basin);
    ANALYZE event_processed.event_territory;

3. Renamed dblink-based importers:
  * `isa` to `isa_dblink`.
  * `prodes` to `prodes_dblink`
  * `prodes_acumulado` to `prodes_acumulado_dblink`
  * `sad` to `sad_dblink`

  This requires the following database update:

    UPDATE meta.import SET name = 'sad_dblink'              WHERE name = 'sad';
    UPDATE meta.import SET name = 'isa_dblink'              WHERE name = 'isa';
    UPDATE meta.import SET name = 'prodes_dblink'           WHERE name = 'prodes';
    UPDATE meta.import SET name = 'prodes_acumulado_dblink' WHERE name = 'prodes_acumulado';

### List of completed tasks

* [x] Fixes:
  * [x] Export: shapefiles: zip: files should not be inside a folder, but in the
        parent folder from the zipfile so they can be directly opened at QGIS.
  * [x] Query: extra parameter validation.
  * [x] API: close database connections after getting data.
  * [x] Airflow:
    * [x] Support for custom email config at DAG args.
    * [x] Rename `airflow_test_` services to `airflow_`.
  * [x] Increase calculation: check if previous period is available via metadata
        before calculating increases; or optionally leave this calculation out
        if previous interval's values are zero.
* [x] Importers:
  * [x] Rename importers `prodes` and `sad` with a `_dblink` suffix.
  * [x] FileLoad: support for multiple files.
  * [x] SAD: Change DAG scheduling for weekly.
  * [x] FIRMS VIIRS Arquive: sensor fixes.
  * [x] PRODES: fix dates.
  * [x] ISA:
    * [x] Rename current `isa` importer to `isa_dblink`.
    * [x] Create a new `isa` importer extracting from Alertas+ upstream and loading
          using `FileLoad`.
    * [x] Determine `download_id` from /meta API query.
* [x] Exporters:
    * [x] ~~Aplicar restrição nos dados exportados: apenas territórios na
          Amazônia Legal. Implementação inicial no branch `feature/exporters`~~.
          Não há necessidade, conforme decidido em reunião de 28/07/2021.
* [x] Performance:
  * [x] ~~Tune PostgreSQL query timeout.~~ Timeouts are disabled by default.
  * [x] Tune Gunicorn.
  * [x] Tune NGINX proxy for API and tileserver.
  * [x] Cache refresh target:
    * [x] Not only cleans old cached data but also fills
        the cache with common requests, including: /meta and default actions
        from the landing and dashboard pages. Use this `refresh` target instead
        of the `clear` target by default, which might also reduce locking issues.
    * [x] Initial implementation and refactor.
    * [x] More presets.
    * [x] ~~Check if event is available, if interval is available~~. No need, no error involved.
    * [x] ~~Check if date range is available for increases?~~ No need since increase calculation
          already does that.
  * [x] Add more rows into the `event_territory` and `territory` table indexes.
  * [x] Optimize `is_poligon` function by looking up the the event type
        geometry in some other way (like a cached query or at the import type's
        `config.py`) instead of doing a database query every time, saving SQL
        connections; or use a query cache.
* [x] Documentation:
  * [x] Major doc update.
  * [x] Pressupostos, características e limitações:
    * [x] Tratamento de sobreposições de eventos:
      * [x] DETER: confirmar sobre sobreposições: há priorização do evento mais
            novo quando há sobreposição ou isso é feito por classe?
            Versões anteriores da Especificação continham a seguinte nota:

            > Sobre as eventuais sobreposições de eventos DETER, deve ser
            > adotado um algoritmo de tratamento do que leve em conta as
            > sobreposições (unicamente para poligonos de categoria 1) conforme
            > a metodologia de limpeza de dados realizada pelo INPE, que de
            > acordo com consulta com a equipe do DETER ocorre da seguinte
            > maneira:
            >
            > 1. Anualmente é feita uma limpeza na máscara do DETER usando uma máscara do
            >    PRODES, ficando apenas os eventos que foram confirmados pelo PRODES.
            >
            > 2. A priorização dos eventos que permanecem é sempre do mais novo para o mais
            >    velho.

## 1.3.4 - 20210726

### Highlights

### Description of changes

1. Changes in the `event_territory` index requires the following manual
   migration:

    SET search_path to event_processed;
    DROP INDEX idx_event_territory_type;
    CREATE INDEX idx_event_territory_type ON event_processed.event_territory(e_type,t_type,e_class_1,e_date);
    ANALYZE event_processed.event_territory;

1. Changes in the permission scheme:

    GRANT SELECT ON base.BLA_base250 TO {main_db_user};

### List of completed tasks

* [x] Importers:
  * [x] Explict filtering to Brazilian Legal Amazon in all importers or at least
        indicate that this filter exists for datasets that already has this
        restriction upstream.
  * [x] ~~Always ensure an `.htaccess` file into the imports folder restricting
        web access.~~ Solved with a simple permission enforcement in the bootstrap
        process.
  * [x] ISA: still some locking issues with concurrent access between ISA importer
        and the API. Possible fixes:
    * [x] SQL Alchemy and pyscopg2 lock timeouts for API/queries.
* [x] Workflow:
  * [x] Airflow: datas de execução parecem estar 1 período defasadas, tarefas
        diárias somente rodando a partir de terça etc.
        Confirmado que os agendamentos estão corretos.
        O que ocorre é que o Airflow indica como "execution date" a data do
        início de um período, enquanto que a data de início de uma execução
        ocorre após o período concluir.
        Por exemplo, para o importador ISA, temos:
        * Execution date: 2021-07-04, 00:00:00
        * Start date: 2021-07-11, 00:00:00
        * End date: 2021-07-11, 00:52:46
* [x] API:
  * [x] Como ponderar a área acumulada pela área total do território no caso de
        agrupamentos (`group_by`)? Talvez realizando este tipo de cálculo apenas
        para seleção de um único território? Solução: criar um método que determina
        a área total da seleção.
* [x] Tile Server:
  * [x] Territories:
    * [x] UF restriction.
    * [x] MUN restriction.
    * [x] Basin restriction.
* [x] Performance:
  * [x] Add (or replace with `e_date`) `e_class_1` at `idx_event_territory_type`.

## 1.3.3 - 20210710

### Highlights

* Misc bugfixes and features.

### Description of changes

* No relevant info.

### List of completed tasks

* [x] Importers:
  * [x] Review:
    * [x] Check if/which datasets have dates in UTC. Marked task as completed
          as the review team did not mark it as an issue.
    * [x] Cômputo de intersecções: `&&` e `ST_Intersects` não apenas performam
          diferente mas podem ter resultados distintos. Avaliar se vale substituir o
          uso de "&&" nas operações de carga dos dados por `ST_Intersects` por isso pode
          dar erro na computação de eventos. Talvez o melhor seja uma abordagem mais
          conservadora e que gaste mais processamento na incorporação.
          Discussão interessante aqui: https://dba.stackexchange.com/questions/191666/vs-st-intersects-performance#191669.
  * [x] Metadata:
    * [x] Exports: populate using the `meta.export` table, including licenses
          and other relevant information for the frontend.
    * [x] Exports table.
  * [x] ISA: excluir crítica 4 da importação de territórios ISA. Exemplo: TI Muratuba.
  * [x] Test if `biomass` and `land_cover` new download IDs are now idempotent.
  * [x] FileExtract: fetch file from HTTP(S).
  * [x] Conjunto de dados territoriais construídos e compilados pelo ISA.
  * [x] Biomass, Landcover:
    * [x] Importer metadata.
    * [x] Add a `DROP TABLE IF EXISTS` and a `DROP INDEX IF
          EXISTS` in the SQL scripts (inside the first transaction), making the
          importer idempotent.
* [x] Exporters:
  * [x] Fix SHA256 hash generation:
    * [x] Go to the asset folder before generating it, so hash files does not end up with paths.
    * [x] Generate GeoTIFF hashes.
  * [x] Incluir funcionalidade/rotina de exportação/dump dentro de um Política de
        Dados Abertos.
  * [x] Generate file hashes.
  * [x] Build assets and only then move to their publicly available names, preventing
        users from downloading incomplete sets (`mv $asset.tmp $asset`).

## 1.3.2 - 20210626

### Highlights

* [x] Performance:
  * [x] Cache invalidation fixes.
* [x] Metadata:
  * [x] Refactoring.
  * [x] Update script.
  * [x] New field `geometry_type` for each event.
  * [x] New field `area_ha` to indicate total area for each event dataset
        (will be zero for punctual data).

### Description of changes

#### Metadata

Event metadata model was changed and needs the following manual update in the
database:

    ALTER TABLE meta.event ADD COLUMN area_ha       integer;
    ALTER TABLE meta.event ADD COLUMN geometry_type character varying;

Import metadata model was changed and needs the following manual update in the
database:

    ALTER TABLE meta.import ADD COLUMN meta    character varying;
    ALTER TABLE meta.import ADD COLUMN archive character varying;

### List of completed tasks

#### Importers

* [x] Metadata:
  * [x] Informação de licenciamento (três idiomas) e perfil de acesso à
        informação no config.py de acordo com proposta de Política de Dados
        do ISA para cada importador, incluindo todas as fontes de dados
        utilizadas.
  * [x] Ensure the redistribution field goes along import metadata to control
        if the frontend should generate download links.
  * [x] Licença de uso e créditos para os outros conjuntos de dados, incluindo
        mas não restringindo a:
    * [x] [Licença de uso e créditos para o DETER/INPE e pesquisadores](http://terrabrasilis.dpi.inpe.br/citacoes-e-licenca-de-uso/).
    * [x] Mapa de biomassa:
        * [License](https://dap.ceda.ac.uk/neodc/esacci/biomass/data/agb/maps/v2.0//00README_catalogue_and_licence.txt).
        * [Terms and conditions](https://artefacts.ceda.ac.uk/licences/specific_licences/esacci_biomass_terms_and_conditions.pdf).
  * [x] Completed imports: param to restrict only to the latest import (DISTINCT
        ORDER BY DATE DESC).
  * [x] Events: sort by name (this aids the frontend select box sorting).
* [x] Extract:
  * [x] Fix: use importer name as importer path to avoid conflict between
        different `download_id` schemes.
  * [x] Amazon Dashboard: check/adapted changes from recent upstream updates
        on pre-2021 data:
        https://globalfiredata.org/data/amazonDashboard/amazon_dashboard_data.zip
* [x] Load:
  * [x] Amazon Dashboard: 2020 data filenames aren't based on 20201231 but on 20201103.
        Perhaps that should be hardcoded in the Airflow DAG when asking for the 2020
        dataset instead of asking 20201231.
  * [x] Amazon Dashboard: not processing 2020 data. Issue was with `set_import_id()`
        which is returning the current year.
* [x] Transform:
  * [x] PRODES: enhanced transform algorithm: detecção automática da entrada de um
        novo conjunto no banco (alternância entre o uso da tabela provisória). Se o
        script roda em 2022 e não há dado de 2021, assume-se que a tabela provisória
        deve ser utilizada. Do contrário, se roda em 2022 e há dado de 2021, então a
        tabela provisória é ignorada.
  * [x] ISA: deadlock issues: conflicts between ISA importer and API/Tileserver
        on territories table; consider two databases (replication), one only for
        `alertas_query` user; and/or restarting services before importing; and/or
        rotate the territory table after importing it (`base.territory_tmp`).
        Among those options, the choice was for rotating the territory table
        after importing.
* [x] Queries:
  * [x] Better param names, like renaming `type_territory` to `territory_type`.
* [x] API:
  * [x] OpenAPI 3 / swagger with webargs compatibility:
    * [x] Use flasgger or apispec.
    * [x] Generate only the API spec endpoint.
    * [x] UI should be handled in the frontend.
* [x] Documentation:
  * [x] Built docs are not rendering tables. Try to fix with
        https://pypi.org/project/sphinx-markdown-tables/
  * [x] https://apidocs.socioambiental.org/alertas/v1 (OpenAPI).
        Done: https://alertas.socioambiental.org/api
  * [x] Switch https://alertas.socioambiental.org/docs to the production
        backend when available.

#### Query

* [x] Ensure psycopg2's readonly and autocommit modes to avoid table locking
      with could conflict with imports.

#### API

* [x] Rename parameters closer to the frontend variable names.

## 1.3.1

### Highlights

* Novo tipo de "evento" e importador `prodes_acumulado` com o desmatamento
  acumulado até 2007.

* Novos tipos de território UF e MUN disponíveis nos cruzamentos, nas queries e
  na API.

* Rename 'cumulate' config param to 'update_mode', supporting both cumulative
  and truncation modes. Truncation can be specific to a given criteria, allowing
  importers to erase just a subset of the existing canonical data.

  Suporte a várias estratégias de atualização de dados: substituição total (exemplo:
  DETER), substituição de um único período (exemplo: Amazon Dashboard) ou substituição
  de dados antigos (FIRMS Arquive). Isso deve resolver o problema de "gaps" de dados
  nos focos de calor.

* FIRMS Arquive: fill new canonical table with recent events from the daily
  importer to avoid data gaps if the arquive dataset doesn't cover newer dates.

  Possível problema que pode comprometer a abrangência dos dados FIRMS caso haja
  um desalinhamento entre a data dos pacotes FIRMS Arquive e as rotinas diárias.

  Exemplo: pacote FIRMS gerado de 2000-11-01 até 2021-05-17, mas o script só roda
  aos sábados, isto é, a próxima rodada seria em 2021-04-22.

  Quando o script rodar, os dados diários incorporados de 2021-05-18 a 2021-05-21
  serão apagados do banco!

  É arriscada uma solução baseada numa sincronia fina da data de geração do
  pacote (que atualmente é feito via preenchimento manual de formulário) com a
  data de execução do script (que pode ser disparado manual ou automaticamente).

  Para resolver isso, haviam as seguintes alternativas:

  1. As rotinas FIRMS Arquive sendo disparadas apenas manualmente, porém
     ainda sujeito a erro humano (por exemplo formulário preenchido num dia
     e rotina disparada num outro).

  2. Incorporação dos eventos recentes que não estão presentes no pacote Arquive.
     Poderia ser feito assim na etapa "load" (exemplo para o MODIS):

     * Ao invés de ser truncada, a tabela "firms_modis_canonical" é renomeada
       para "firms_modis_canonical_old".

     * Uma nova tabela canônica "firms_modis_canonical" é criada e alimentada
       com os dados dos shapes.

     * A data do dado mais recente de "firms_modis_canonical" é determinada.

     * É feito um INSERT em "firms_modis_canonical" a partir de um SELECT
       em "firms_modis_canonical_old" tendo como WHERE as datas de evento
       mais recentes do que a última data disponível em "firms_modis_canonical".

     * Finalmente a tabela "firms_modis_canonical_old" é apagada.

  A implementação da opção 2 envolveria um método `truncate_table` customizado
  em relação ao disponível na classe OgrLoad.

  A janelas de dados FIRMS é uma pendência que bloqueia a homologação desse dado
  e respectiva disponibilização em produção.

  A opção implementada foi a 2.

  Notar que, apesar dessa checagem, vazios de dados ainda podem existir caso a
  rotina diária de importação demore mais de um dia para rodar.

### Description of changes

#### Carbon emission

Needs the following database model change:

    ALTER TABLE event_processed.event_territory ADD COLUMN c_emission numeric;

#### Metadata

Importer dependency evaluation needs the following database model update:

    ALTER TABLE meta.import ADD COLUMN collection character varying;

### List of completed tasks

#### Importers

* [x] "Land cover" importer:
  * [x] As dependency for all event-based importers.
  * [x] Export emission factors table to the docs.
  * [x] Export Google Earth Engine script to the repository.
* [x] PRODES:
  * [x] Issue with DROP events with `legenda != 'd2007'`.
  * [x] Update importer to the new `d2020` dataset:
    * [x] Comentar as linhas referentes à incorporação da tabela provisória.
    * [x] Avaliar um DROP na tabela de dados provisória.
* [x] Create a `prodes_acumulado` "event" type to allow computations of
      cummulated deforestation. See meeting notes from 2021-05-25.
      Importador `prodes_acumulado` reaproveitando boa parte do código do `prodes`.
  * [x] Reuse PRODES SQLs.
  * [x] Handle UNION clause at `transform.sql`: sometimes prodes needs it,
        but prodes_acumulado should never use it, perhaps creating
        a dummy table for prodes_acumulado.
  * [x] What would happen if suddenly the prodes importer included Cerrado dataset?
        Nothing since the recent refactor where canonical datasets are imported
        anyways BUT only included in the final canonical database table if present
        in a UNION clause controlled by a template.
  * [x] Consider naming those `prodes_acumulado` importer and event to
        `prodes_acumulado_2007` or `prodes_acumulado_partial`? No, keep it
        `prodes_acumulado` but provide an explanation about it's nature.
* [x] ISA: incluir tipo de território UF.
* [x] Check if required data is available before starting procedure by implementing
      a metadata check on imported collections.

      Esta funcionalidade melhora a interação administrativa
      ao informar sobre um requisito não-satisfeito, porém não é prioritária uma
      vez que um importador que não executa por falta de dados já vai dar um
      erro, o que pode gerar uma notificação via airflow.

      Initially planned approach was: considerar rotina pra checar se requisitos
      de dados estão disponíveis (ISA e FIRMS por exemplo?). User um decorator
      `require_populated_table` que aceita como argumento o nome de tabela (e schema)
      que precisa existir e ter ao menos um registro.

#### Queries

* [x] Query for a specific territory buffers, like an ARP buffer.
* [x] Make `id_territory`, `id_uf` and `id_mun` as lists.
* [x] Rename parameters closer to the frontend variable names.

#### Documentation

* [x] Abrangência:
  * [x] Restrição de municípios da Amazônia Legal.
* [x] Classes:
  * [x] Explicação sobre a reclassificação de eventos do DETER Amazônia
        conforme discutido na reunião do dia 06/01/2021.
  * [x] Explicação sobre a classificação do DETER Cerrado conforme
        explicação baseada na do Juan:

        > DETER Cerrado não tem divisão de classes, mas são todos referentes
        > a desmatamento de corte raso. Por isso no processamento eles são
        > colocados como sendo classe_1=1 e classe 2='DESMATAMENTO_CR'.
        > Fazendo isso, os dois conjuntos de dados ficam compatíveis,
        > permitindo uma seleção única na interface já que ambos compartilham
        > o mesmo tipo de evento.

## 1.3.0

### Highlights

* Criação de metadados de importação já disponíveis em API, incluindo os tipos
  de eventos disponíveis, classes, data da última importação, quantidade de
  registros importados, data dos eventos mais recentes e mais antigos. Já estão
  disponíveis via API.

* Importadores não executam mais tarefas que já foram realizadas, como download
  replicado de arquivos ou cargas de dados já incorporados. Implementado
  através da checagem de metadados.

* Criação de "travas" (locks) para impedir que um mesmo importador rode mais de
  uma vez simultaneamente.

* Criação de rotina inicial para exportação dos dados em formatos SQL e
  Shapefile. Ainda precisa de bastante trabalho nesse item.

* Refatoração das rotinas de importação para que possam rodar em paralelo sem
  causar deadlocks ou outras travas no banco de dados.

* Mudanças nas configurações de agendamento das rotinas de importação dentro do
  esquema do Apache Airflow.

* Importadores FIRMS Arquive não requerem mais que FIRMS MODIS e FIRMS VIIRS
  tenham rodado ao menos uma vez (confirmar). Mesmo assim permanece a questão
  do agendamento das rotinas diária e de carga de arquivo para evitar tanto dados
  duplicados quanto períodos de sombra.

* Mudanças no importador do Amazon Dashboard devido a atualizações de modelo
  feitas no dado canônico.

### Description of changes

#### Transações

Nesta versão, foi introduzido o uso de `autocommit` nas conexões `psycopg2`.
De acordo com a documentação:

> Warning
>
> By default, any query execution, including a simple SELECT will start a
> transaction: for long-running programs, if no further action is taken, the
> session will remain “idle in transaction”, an undesirable condition for several
> reasons (locks are held by the session, tables bloat…). For long lived scripts,
> either ensure to terminate a transaction as soon as possible or use an
> autocommit connection.
>
> --- https://www.psycopg.org/docs/connection.html
>     https://www.psycopg.org/docs/usage.html#transactions-control
>     https://www.psycopg.org/docs/connection.html#connection.autocommit

Alguns processamentos foram divididos em transações (BEGIN/COMMIT).

O objetivo desta mudança foi evitar travas na execução paralela de
importadores.

#### Particionamento

Implementadas mudanças relativas ao particionamento por tipo de evento. O
particionamento foi feito para as tabelas event e event_territory.

Também foi implementada uma distribuição das tabelas por esquemas. Agora temos
3 esquemas: base, event_canonical e event_processed.

Detalhamento: o posprocessamento mais demorado é aquele que avalia as
sobreposições internas do dataset PRODES em relação aos territórios, para
possibilitar o uso do smart_area. Essa etapa, feita pelo script
precompute/process_event_territory_table.sql pode ser deixada para mais tarde
(próximo final de semana), se a gente não for usar o smart_area nas primeiras
avaliações de performance do particionamento, para isso bastando comentar a
linha correspondente no transform.py do importador PRODES.

Também pode rodar em segundo plano, mas pode prejudicar um pouco a performance
dos queries, pois as todas tabelas estão no mesmo disco...

As seguintes migrações manuais da base são necessárias (assumindo que o usuário
principal da base é `alertas`):

    CREATE SCHEMA base AUTHORIZATION alertas;
    CREATE SCHEMA event_canonical AUTHORIZATION alertas;
    CREATE SCHEMA event_processed AUTHORIZATION alertas;

Um novo usuário é usado a partir de agora somente para queries, cuja seção
no arquivo de configuração é `main_db_query`. Assumindo que o nome desse
usuário é `alertas_query`, o seguintes `GRANTs` são necessários:

    GRANT USAGE  ON               SCHEMA base            TO alertas_query;
    GRANT USAGE  ON               SCHEMA event_processed TO alertas_query;
    GRANT SELECT ON ALL TABLES IN SCHEMA event_processed TO alertas_query;
    ALTER DEFAULT PRIVILEGES FOR ROLE alertas IN SCHEMA event_processed GRANT SELECT ON TABLES TO alertas_query;

#### Metadados

Schema criado para armazenar metadados:

    CREATE SCHEMA meta AUTHORIZATION alertas;
    GRANT USAGE  ON    SCHEMA meta TO alertas_query;
    ALTER DEFAULT PRIVILEGES FOR ROLE alertas IN SCHEMA meta GRANT SELECT ON TABLES TO alertas_query;

#### Amazon Dashboard

A tabela `event_canonical.amazon_dashboard_canonical` mudou de esquema. Durante
o desenvolvimento, recomenda-se dar o seguinte comando para que ela seja
automaticamente criada com o novo esquema na próxima importação:

    DROP TABLE event_canonical.amazon_dashboard_canonical;

### List of completed tasks

* [x] Importers:
  * [x] Amazon Dashboard: update 2021 model reflecting upstream changes.
  * [x] Recast importer config variable `cumulate` to boolean.
  * [x] Adds importer config variable `truncate_criteria` allowing for datasets
        that needs to preserver past imports but removing some data; currently
        supports only `all` and `dataset_year`.
  * [x] Lockfile or database lock for each importer stage. Now importers like
        FIRMS VIIRS and FIRMS VIIRS Arquive cannot run at the same time; also,
        only one instance of each importer is allowed to be running.
  * [x] New Amazon Dashboard classification (4 main classes instead of 1 classes
        with 4 subclasses).
  * [x] New DETER classification ("mining" should be a separate class).
  * [x] Check if class definitions at each importer's `config.py` actually match
        those in the SQL statements.
  * [x] ISA: Include basin dataset.
  * [x] Support for `--date` param at `import.py`. Useful to get older Amazon
        Dashboard datasets.
  * [x] Amazon Dashboard: support for arbirtrary dates at extract and load.
  * [x] Checagem para evitar repetição de downloads já concluídos.
  * [x] Checar se uma fase de um importador já rodou.
        Se for constatado que uma fase já rodou, passar para a próxima.
        Isto pode ser implementado como decoradores juntamente com uma tabela na
        base de dados indicando a data em que cada estágio foi executado.
        Atualmente se o arquivo é o mesmo, o download não é feito, mas o
        processamento sim. Seria interessante mudar isso.
        Ou seja, seria necessário implementar uma lógica ao redor de uma
        tabela "meta.importer" com os campos "name", "stage", "stage_date" e "stage_id".
  * [x] Support for per-importer ETL mode instead of mandatory ELT.
  * [x] Rename `importer` to `collection` at each importer's `config.py`?
        Requires some refactor in the ELT classes.
  * [x] Update documentation (if needed) with the revised class scheme for each
        importer.
  * [x] Now both FIRMS VIIRS Arquive and FIRMS MODIS Arquive can run in a fresh
        database either before or after FIRMS VIIRS and FIRMS MODIS as they also
        take care the canonical database model creation.
  * [x] Extract:
    * [x] Remove previous folder (if exists) before extracting.
    * [x] Prune: remove interrupted downloads (`.part` files).
  * [x] Transform:
    * [x] Add basin data into `event_territory` entries to allow selection by basin.
    * [x] DETER: considerar classe específica para mineração.
    * [x] Add a comment on the "ORDER BY" at INSERTs on `event_territory` partitions
          telling that this is needed to avoid fragmentation in the data, keeping the
          index less complex and decreasing query time.
  * [x] Do not run database bootstraping on importers to avoid database locking issues.
        Instead, use a custom bootstrapping script, Makefile target and container service.
  * [x] Set `psycopg2` connection in `autocommit` mode.
  * [x] Refactor SQL logic to reduce locks on `event` and `event_territory` tables.
        All computation happens in temporary tables and in tables with a
        `{e_type}_new` name suffix. Tables are only attached as partitions of `event`
        or `event_territory` after all processing.
* [x] Queries:
  * [x] Adapt query to include basin selection.
  * [x] Regular `alertas_query` psql user for enhanced security: ensures that any
        injections that might exists are harmless since the database user using
        in a query has only basic permissions.
  * [x] Rename downloads path to imports, create exports folder. Or go for a
        `data/{imports,exports}`. Automigrate from the older to the new format.
* [x] Performance:
  * [x] Partitioning by event type.
  * [x] Put default benchmarks in an Airflow DAG.
* [x] Airflow:
  * [x] Choose between setting the timezone or updating DAG intervals relative to UTC:
        https://airflow.apache.org/docs/apache-airflow/stable/timezone.html
  * [x] Fix email configuration (maybe even creating a mail test DAG):
        https://airflow.apache.org/docs/apache-airflow/stable/howto/email-config.html
  * [x] Amazon Dashboard Archive DAG: ensure datasets from previous years are imported,
        such as the latest 2020 dataset; requires `--date` param at `importer.py`
        so this DAG runs the load phase for Dec 31th of each previous year.
  * [x] Remove airflow-worker.pid and other dangling lockfiles at airflow_test_init?
        It might prevent airflow celery worker to start. Needs attention to remove
        those locks before starting other airflow services.
* [x] Exporters:
  * [x] Create a "data" service to offer static imports and exports downloads.
  * [x] Metadata for exported files. Alternative was simply to keep a
        fancy directory listing and information on `/data` HTTP endpoint.
* [x] API:
  * [x] Basin action.
  * [x] Auxiliar actions / lookup tables:
    * [x] Events and classes, reading info from each importer's `config.py`.
    * [x] Status, including earch importer metadata:
      * Last succesful imported date
      * First date in the data collection
      * Last date in the data collection
      * Total entries in the collection
* [x] Tile Server:
  * [x] Adapt query to include basin selection.
* [x] Performane:
  * [x] Consider additional database schema adjusts (partitioning by date).
        Currently not needed since event_territory tables are being ingested
        if ORDER BY date to reduce index fragmentation.

## 1.2.2

* Maintenance release with fixes, preparing for major changes.

## 1.2.1

Completed Cycle 2 tasks:

* [x] Tests:
  * [x] Test suite integrated with the benchmarks.
* [x] Deployment:
  * [x] Support for environment variable for database connection string at airflow test services.
* [x] Performance:
  * [x] Benchmark: adapt to the new `group_by` parameter.

## 1.2.0

Completed Cycle 2 tasks:

* [x] Workflow:
  * [x] Containerized environment with Docker and Docker Compose.
  * [x] Management:
    * [x] Upgrade/reboot lock during the import process: system upgrades and
          reboots should also be scheduled to avoid unneeded import interruptions;
          this was set in the development server and need to be done as well in
          the upcoming production environment; docs was updated accordingly.
    * [x] Per-importer logfiles or concurrent logfile support to allow simultaneous
          imports. Implemented using https://pypi.org/project/concurrent-log-handler/
    * [x] Proper error handling with exit codes at `importers.py` (`--abort-on-error`).
    * [x] Airflow instance with pipelines supporting simultaneous tasks:
      * [x] SSH service for the worker container.
      * [x] SSH keypair generation in a `ssh` volume.
      * [x] SSH access to the worker container by any service in the pod.
      * [x] Persistent SSH keys in the worker container.
      * [x] Airflow test service using sqlite with DAGs from `src/airflow`.
      * [x] DAGs for each importer.
      * [x] Support for PostgreSQL database in the test instance for multi-threading tests.
    * [x] Check and cleanup old extracted files to save diskspace (`maintenance_prune` DAG).
  * [x] Order and frequencies:
    * [x] Stage 0 (semanal):
      * [x] ISA (tabelas de territórios, requisito para todos os cruzamentos).
    * [x] Stage 1 (diário, paralelo):
      * DETER: diário: considerar o horário de extração dos dados do DETER,
        levando em conta que aparentemente o site TerraBrasilis entra em modo
        manutenção durante parte da madrugada.
      * Amazon dashboard: diário.
      * SAD: diário.
      * FIRMS MODIS.
      * FIRMS VIIRS.
    * [x] Stage 1 (semestral):
      * PRODES: duas vezes por ano, lançado manualmente.
        Fazer apenas após a primeira madrugada de segunda do mês, já que esta
        foi a data acordada/combinada com o time de Informática para a
        atualização/reinicialização periódica para as máquinas de desenvolvimento
        e produção; documentar necessidade de desligar temporariamente qualquer
        atualização automática durante a importação.
* [x] API:
  * [x] Heatmaps.
  * [x] Municipality: restrict to Legal Amazon municipalities:
        https://www.ibge.gov.br/geociencias/cartas-e-mapas/mapas-regionais/15819-amazonia-legal.html?=&t=acesso-ao-produto
  * [x] Always set `smart_area` to `True`.
  * [x] Chart: sorting, date conversion etc.
* [x] Tile Server:
  * [x] Move main route to '/events'.
  * [x] Serve territories at '/territories'.
* [x] Documentation:
  * [x] Setup [autodoc](https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html).
  * [x] Move project specs to this repository.
  * [x] Setup public URLs like:
      * [x] https://alertas.socioambiental.org/docs (Sphinx).
* [x] Deployment:
  * [x] Use a non-privileged `alertas-backend` in the container images.
  * [x] NGINX container ("proxy" service at `compose.yaml`).
  * [x] Systemd unit for service containers initialization.

## 1.1.0

Completed Cycle 2 tasks:

* [x] Mapbox Tile Server implementation.
* [x] Integração novos dados poligonais:
  * [x] DETER Cerrado: alertas de desmatamento no Cerrado fornecidos pelo INPE.
  * [x] SAD: sistema de alerta de desmatamento criado pelo IMAZON. Oferece
        dados alternativos aos fornecidos pelo sistema DETER/INPE.
  * [x] PRODES: dados anuais de desmatamento fornecidos pelo INPE, nos biomas Cerrado e Amazônia.
  * [x] Global Fire Emissions Database - Amazon Dashboard: dados diários sobre
        extensão e tipologia de incêndios ativos e inativos na pan-amazônia
        elaborados a partir de dados [VIIRS](https://globalfiredata.org/pages/amazon-dashboard/).
    * [x] Take into account if Amazon Dashboard zip files are incremental or if the latest
          dump have all the historical data.
  * [x] Demora do dado PRODES: otimização das queries de incorporação.
  * [x] Checar se a Amazônia Legal está incluída nos recortes territoriais,
        criando um importador caso necessário ou incluindo uma rotina dentro do
        importador `isa`? Checar necessidade.
* [x] Integração de dados pontuais:
  * [x] Adaptação e reformulação das funções da API atualmente em
        funcionamento, no que diz respeito ao tratamento das interseções,
        quantificação de eventos e parâmetros de entrada do usuário.
  * [x] NRT VIIRS 375 m active fire data: dados de focos de calor de alta
        resolução fornecidos pelo [FIRMS](https://firms.modaps.eosdis.nasa.gov/). A
        série temporal completa começa em janeiro de 2012.
  * [x] Focos de calor referência: dados diários de focos de calor MODIS/Aqua,
        com resolução nominal de 1km. A escolha da fonte do dado referencia será
        discutida com o corpo técnico do ISA. As possíveis escolhas são:
        * a) O banco de dados corporativo do ISA
        * b) O banco de dados [BDQUEIMADAS do
             INPE](http://queimadas.dgi.inpe.br/queimadas/bdqueimadas/)
        ->* c) O banco de dados [FIRMS da NASA](https://firms.modaps.eosdis.nasa.gov/)
* [x] Load/Transform:
  * [x] Adaptar SQLs de precomputação (`src/sql/precompute`) para poderem rodar
        em paralelo, usando uma tabela `event_territory_{e_type}_tmp` ao invés
        de apenas `event_territory_tmp`.
  * [x] Filtro de ingestão de Amazônia Legal, para restringir a abrangência
        do sistema no Ciclo 2.
* [x] Workflow:
  * [x] Considerar o que pode ser rodado paralelamente: conforme reunião de 06/01/2021:
        importadores podem operar em paralelo (ELT pipeline), desde que o importer
        "isa" já tenha sido importado ao menos uma única vez.
* [x] Query:
  * [x] Create recorte: buffer names at "nome" fields instead of just the original
        outline name. Inside the db or include dynamically during query using
        `r_type` field. Useful to distinguish buffers from the regular outlines.
        Solved by composing the output name with the type_territory field in the
        API level.
* [x] API:
  * [x] Problema no eixo horizontal de data nos gráficos temporais.
  * [x] Consider a two level caching: in the database query and in the API call itself,
        as some API calls rely on the same database query and also do some postprocessing.
  * [x] Ranking: output formats:
    * [x] Table.
    * [x] Chart (not "series").
* [x] Documentation
  * [x] Setup using python-sphinx.

## 1.0.0

Completed Cycle 1 tasks:

* [x] General:
  * [x] Log facility: rotation.
* [x] Importers:
  * [x] Importação incremental? Definição de como funcioná o pipeline:
        haveria uma "rodada incremental" e uma "rodada completa" ou apenas a
        completa, recalculando tudo a cada importação periódica? Em termos de
        programação, é mais fácil começar com a rodada completa e podemos avaliar a
        necessidade da incremental no futuro.
  * [x] Pré-computação incremental. Idem à importação incremental.
  * [x] Re-estruturar esquema de incorporação: até terça-feira (12/01/2021):
    * [x] Script de importação `src/importer.py` que processa um importador, podendo
          também processar apenas uma etapa ELT de um importador.
  * [x] Uniformizar siglas, parâmetros e eventualmente nomes de campos nas
        bases de dados: até terça-feira (12/01/2021).
* [x] DETER:
  * [x] Class levels 1 and 2 redefinition based on Juan's proposal.
* [x] Queries:
  * [x] Aplicar mudanças de terminologia no código: até terça-feira (12/01/2021).
* [x] API:
  * [x] Separar em repositórios: `alertas-processor` (incorporação) e `alertas-api`?
  * [x] Event counting.
  * [x] Event GeoJSON.

## 0.6.0

Major changes:

* Precomputing of intersections with territories is now made after every
  dataset import.

## 0.5.0

Major changes:

* Abstract shape loading classes into ShapeLoad.

* New importers:
  * Amazon Dashboard.
  * SAD.
  * Prodes.

* API: params support for /arp and /municipality.

* Fix: SisArp: restrict UFs to Legal Amazon and remove some metadata.

* Added initial documentation.

## 0.4.5

* New `empty-db` script.

* Makefile cleanup.

## 0.4.4

Changes:

* New import scheme using ELT flows.
* New download folder scheme.
* DETER cerrado incorporation by default.
* Added a basic log facility.
* Parameter/field renaming like these:

    find src -type f -exec sed -i -e 's/evento/event/g'      {} \;
    find src -type f -exec sed -i -e 's/recorte/territory/g' {} \;
    find src -type f -exec sed -i -e 's/tipo/type/g'         {} \;

* Database schema migration:

    ALTER TABLE    recorte              RENAME                  TO territory;
    ALTER SEQUENCE recorte_id_seq       RENAME                  TO territory_id_seq;
    ALTER TABLE    territory            RENAME COLUMN nome      TO name;
    ALTER TABLE    event_recorte        RENAME                  TO event_territory;
    ALTER SEQUENCE event_recorte_id_seq RENAME                  TO event_territory_id_seq;
    ALTER TABLE    event_territory      RENAME COLUMN r_type    TO t_type;
    ALTER TABLE    event_territory      RENAME COLUMN r_nome    TO t_name;
    ALTER TABLE    event_territory      RENAME COLUMN nome_uf   TO name_uf;
    ALTER TABLE    event_territory      RENAME COLUMN nome_mun  TO name_mun;
    ALTER TABLE    event_territory      RENAME COLUMN r_id_orig TO t_id_orig;

Description:

1. Adotado o paradigma ELT (Extract Load Transform) para os scripts de
   incorporação, já preparando o terreno para que sejam gerenciados via Apache
   Airflow (e não pelo cron). O script "src/importer.py" gerencia a execução dos
   importadores e detecta automaticamente os importadores. Foi considerado até o
   procedimento de incorporação dos dados do ISA como um importador específico. O
   grafo de dependências entre os scripts precisa ser definido posteriormente via
   Airflow. Por enquanto os scripts podem ser executados através dos alvos que
   estão definidos no Makefile ou diretamente via linha de comando.

2. Também implementada orientação a objeto nos scripts, o que facilita a
   abstração e reutilização do código.

3. Procedida a mudança de nomenclatura conforme combinamos na nossa conversa do
   dia 06/01/2021: agora não apenas os parâmetros da query estão em inglês como
   os nomes de várias tabelas e campos, exceto aqueles referentes aos dados
   canônicos que continuam no formato original.

4. Por padrão, o script do DETER já está importando os dados do DETER Cerrado,
   porém não foi realizada nenhuma conferência sobre os mesmos (parece que eles
   não tem distinção de classe, correto?) e nem foram separados numa tabela
   canônica própria. Vale considerar a necessidade de uma tabela própria (e um
   tipo de evento próprio) para distingui-lo dos eventos do tipo DETER "comuns".

5. Implementada uma facilidade de log (registros do sistema), emitindo avisos
   tanto no console como num arquivo.

6. Nova estrutura de armazenamento dos downloads. Agora os dados extraídos
   também ficam na pasta `downloads`. Cada importador tem sua pasta própria de
   dados brutos e extraídos.

7. Mudanças diversas no código.
