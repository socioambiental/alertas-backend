#!/usr/bin/env bash
#
# Prune extracted and interrupted downloads.
#

# Parameters
BASENAME="`basename $0`"
DIRNAME="$(cd `dirname $0` &> /dev/null && pwd)"
BASEPATH="$DIRNAME/.."
IMPORTS="$BASEPATH/data/imports"
EXPORTS="$BASEPATH/data/exports"
CWD="`pwd`"

# Change to imports folder
cd $IMPORTS &> /dev/null || exit 1

# Iterate over all datasets, finding and sorting by date (ascending)
# all extracted archives.
for dataset in *; do
  total="`find $dataset | grep -E 'extracted/[0-9]{8}$' | sort | wc -l`"
  items="`find $dataset | grep -E 'extracted/[0-9]{8}$' | sort`"
  count="1"

  if [ "$total" == "0" ] || [ "$total" == "1" ]; then
    echo "Skipping $dataset as it only have ${total} item(s)..."
    continue
  fi

  for item in $items; do
    if ((count >= total)); then
      break
    fi

    echo "Removing $item..."
    rm -rf $item

    let count++
  done
done

# Remove interrupted downloads
# Might conflict if an import action is running
#find $IMPORTS -name '*.part' -exec rm {} \;

# Change to exports folder
cd $EXPORTS &> /dev/null || exit 1

# Iterate over all exported datasets
find -type f | sed -e 's/-[0-9].*//' | sort -u | while read export; do
  total="`ls -1 $export* | grep -v '.sha256$' | wc -l`"
  items="`ls -1 $export* | grep -v '.sha256$' | sort`"
  count="1"

  # Skip the index.html placeholder file
  if [ "$export" == "index.html" ]; then
    continue
  fi

  # Always leave the latest and the previous export, leaving the older archives
  # still available for a while for users that might eventually downloading them.
  if [ "$total" == "0" ] || [ "$total" == "1" ] || [ "$total" == "2" ]; then
    echo "Skipping $export as it only have ${total} item(s)..."
    continue
  fi

  for item in $items; do
    if ((count >= total)); then
      break
    fi

    echo "Removing $item..."
    rm -f $item*

    let count++
  done
done

# Remove interrupted exports
# Might conflict if an export action is running
#find -name '*.new' -exec rm {} \;

# Teardown
cd "$CWD" &> /dev/null
