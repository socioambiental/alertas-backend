# Alertas Backend - Tasks

## Cycle 3

### Importers

#### Metadata

* [ ] Respect the `safeguard` field: if set to `True`, preserve all downloaded
      archives, but if `False` keep only the latest.

#### New importers

* [ ] New importers `prodes` and `sad` from publicly available
      datasets/shapefiles/dumps.
* [ ] [MapBiomas Alerta](https://plataforma.alerta.mapbiomas.org/downloads)
      (branch feature/map_biomas_alerta):
    * [ ] Carga e transformação.
    * [ ] Cruzamento com a planilha de refinamento e publicação?
    * [ ] Este conjunto pode ser importante pois contém o laudo de cada alerta,
          podendo ser usado como dado de validação adicional.
* [ ] Incidência de casos de COVID-19 em territórios protegidos:
    * [ ] Importação dos novos tipos de recorte DSEI e Pólo Base?
    * [ ] Possibilidades:
      * Brasil.IO (datasets em CSV, fácil de importar). Espacialidade via código IBGE apenas.
      * SESAI (raspagem diária de site e JSONs históricos compilados pelo ISA).
        Espacialidade via DSEI e Pólo Base.
      * APIB: planilha Google. Espacialidade via código IBGE (Painel COVID-19).
      * CONAQ: planilha Google. Espacialidade via código IBGE (Painel COVID-19).
      * MapBiomas: GeoCOVID-19: de repente um datalink? http://portalcovid19.uefs.br

#### All phases

* [ ] Auto-resolve import dependencies (such as `biomass`, `land_cover` and `isa`).
* [ ] Sometimes the ConcurrentRotatingFileHandler generates errors. Needs to
      reproduce and fix this behavior or change to a per-service (API, Tile Server,
      import) logfile and optionally to a per-importer logfile.
* [ ] Support for Jinja or other templating system for SQL scripts, allowing
      advanced logic such as selection of execution blocks etc.
* [ ] PRODES: DAG schedule to run every quarter, setting the `download_id`
      according to the state of the previous import or the availability of
      new data?

#### Extract

* [ ] FetchImporter:
  * [ ] Download check using hashes?
  * [ ] Check/fallback when upstream (eg. terrabrasilis) is offline?
* [ ] FileExtract:
  * [ ] Test with `biomass`, `land_cover` and other file-based importers once
        their base_urls become public.
* [ ] Amazon Dashboard: check how upstream will offer archived data (years
      past): if they would keep the convention from 2021 (all past years in a
      single file) or move to a new scheme with per-year datasets.
* [ ] Biomass:
  * [ ] Extract routine to build the raster fetching data from https://catalogue.ceda.ac.uk.
* [ ] FIRMS Arquive:
  * [ ] Automation: not a priority since the absence of the manual
        step should generate an Airflow email alert, but can save
        time in the long run:
    * [ ] Automated procedure to request new download tokens.
    * [ ] Delay loop to check if remote file is available before downloading.

#### Load

* [ ] ISA:
  * [ ] Inclusion of Quilombola Areas.
  * [ ] To be defined: table name and acronym for Quilombolas Territories. According to João:
      > Existem duas categorias de quilombos nos bancos de dados:
      >
      >     QUI - Territórios Quilombolas
      >     PAQ - Projetos de assentamento quilombola
      >
      > Com a revisão desse módulo do SisArp pode ser que seja criado novas
      > categorias (comunidade quilombola estão cogitando). Nas APIs e nos bancos de
      > dados do ISA, eu convencionei "QUI" qdo eu quero buscar todos os tipos
      > (âmbito) de territórios. Assim como usamos com UC, que vale para várias
      > categorias (FLONA, REBIO, PARNA, etc).
* [ ] Land cover: rename table `base.landcover_mb` to `base.land_cover`?
* [ ] Amazon Dashboard: check with upstream why they changed the date in the
      2020 shapefile: it was formerly `fire_atlas_events_20201231.shp` but
      now it's `fire_atlas_events_20201103.shp`. Does this means that the
      2020 dataset is incomplete? This has impact in the `amazon_dashboard_archive`
      DAG logic.
* [ ] Watch format changes in the canonical data (field checking):
      Check and require existing fields in the canonical data.

#### Transform

* [ ] Lock issues:
  * [ ] Between API and `isa_dblink`/`territory` table.
  * [ ] In this `SELECT`:
        https://gitlab.com/socioambiental/alertas-backend/-/blob/8285e55b61fa7297e80f3ede738f92525dfa3d8c/src/sql/precompute/compute_event_territory_table.sql#L23
        Perhaps municipalities and basins tables should be copied to temporary
        tables before this statement.
* [ ] Basic multi-thread support on transform operations, by splitting datasets
      on several tables, processing them in parallell and then merging everything
      aftwerwards like a map/reduce strategy.
* [ ] ISA: generate non-protected areas? Proposed implementation at
      `src/importers/isa/sql/nonprotected.sql` orginally developed at branch
      `feature/non-protected_areas`.
* [ ] DETER: excluir polígonos menores que a área mínima mapeada (1 ha?) de acordo
      com a sugestão dada na homologação dos dados do Ciclo 1.
* [ ] Ampliar abrangência territorial, considerando que tal mudança requer
      revisão em várias sub-rotinas, incluindo por exemplo o cômputo da densidade
      de árvores derrubadas:
  * [ ] Avaliar outra "máscara de abrangência" do tipo "Amazônia Legal Estendida"
        ou somente "Amazônia Estendida" contemplando a Amazônia Legal e mais algumas
        bacias, por exemplo a do Xingu, compatibilizando a produção de dados com as
        regiões onde o ISA atua.
  * [ ] Ampliação para todo o território brasileiro.
* [ ] FIRMS (hotspots):
  * [ ] Automatically determine band number for landcover table
        based on the last year available. For instance, with
        the latest MapBiomas collection (2020), `numYears` is 35.
        ST_Value on preparation scripts should include this paramater, eg. on
        `ST_Value(lc.rast,least(extract(year from acq_date)::int-1984,{numYears}),f.geom)`.

### Exporters

* [ ] Exporter logic:
  * [ ] If redistribution is False, ensure that are no datasets in the exports
        folder.
  * [ ] If redistribution is True, proceed with the export.
  * [ ] Exporter type `copy` which just copies files from the import folder
        to the export folder (or just make symlinks to save space).
* [ ] Prune interrupted exports (files with `.new` extensions at the exports folders).
* [ ] New exporters:
  * [ ] Shapefiles:
    * [ ] Outputs: `event_territory`. Attention with an eventual issue for
          exporting geometry collections.
    * [ ] Both incremental (daily or weekly) and the full dump.
    * [ ] Use psql2shp, but with two queries for `event_territory`: points and areas?
    * [ ] Use shapely or GDAL?
  * [ ] Consider incremental dumps (`tables`, `shapefiles`).
  * [ ] Other raster formats like NetCDF and HDF4:
    * [Raster drivers — GDAL documentation](https://gdal.org/drivers/raster/index.html)
    * [frmts_wtkraster.html – GDAL](https://trac.osgeo.org/gdal/wiki/frmts_wtkraster.html)
    * [Chapter�10.�PostGIS Raster Frequently Asked Questions](http://postgis.refractions.net/documentation/manual-svn/RT_FAQ.html#id569097)
    * [frmts_wtkraster.html – GDAL](https://trac.osgeo.org/gdal/wiki/frmts_wtkraster.html#a3.2-Readingrasterdatafromthedatabase)
    * [List of GDAL raster file extensions - Geographic Information Systems Stack Exchange](https://gis.stackexchange.com/questions/175610/list-of-gdal-raster-file-extensions)
  * [ ] RSS de eventos recentes.
  * [ ] Bot pras redes sociais pra fazer uma divulgação diária
        automática soltando um post e linkando para o Painel
        (telegram/twitter):
      * [ ] Incluindo presets de cômputos e mensagens.
      * [ ] Exepmlo de mensages:
        * [ ] "Na última semana, estimativa de X árvores derrubadas ilegalmente
              na amazonia brasileira, sendo Y em áreas protegidas -- {short-url}".
        * [ ] "Brasil perdeu X% da Amazônia na última semana. Neste ritmo,
              faltam apenas Y anos para toda a Amazônia ser destruída".
      * [ ] Não enviar mensagem se o cômputo for zero ou pouco significativo
            para reduzir ruído de mensagens.
      * [ ] Twitter: [tweepy](https://github.com/tweepy/tweepy).
      * [ ] Telegram: [python-telegram-bot](https://python-telegram-bot.org/).

### Workflow

* [ ] Homologação de software livre: testar e aprimorar o projeto como um todo,
      checando se está funcional e amigável para utilização por terceiros;
      usar uma configuração inicial padrão que funciona.
* [ ] Support for Airflow DAG arguments.
* [ ] Support for import concurrency across different systems:
  * [ ] Implementation for multiple Redis instances.
  * [ ] Distributed lock for importers (Redis) with
        [python-redis-lock](https://pypi.org/project/python-redis-lock/) or similar.
* [ ] Support for Airflow Scheduler log rotation:
  * [Exemple config](https://unix.stackexchange.com/questions/530828/airflow-log-rotation#539360), but using
    `/home/alertas-backend/airflow/logs/scheduler`.
  * [Feature request](https://github.com/apache/airflow/issues/8759)
  * [Example script](https://github.com/teamclairvoyant/airflow-maintenance-dags/blob/master/log-cleanup/airflow-log-cleanup.py)

### Queries

* [ ] Support for pandas/geopandas data frame output.
* [ ] Additional validation:
  * [ ] Check if territory type is allowed.
  * [ ] Validate query parameters according to each importer's `config.py` class definition.
  * [ ] Do not allow queries with both 'MUN'/'UF' and any other territory type,
        because that might lead to double counting of some events.
* [ ] GeoJSON:
  * [ ] Simplify if point is too big if a `simplify` param is set:
    * [ ] Simplify with tolerance t = f(zoom, bounds, npoints).
    * [ ] Do it via python or javascript? Better in the backend using `shapely` library
          or directly via PostGIS?
* [ ] Fill gaps no pós-processamento da query (em python), conforme
      [discutido em reunião](https://gitlab.com/socioambiental/alertas/-/issues/1#note_478696791).
      Reavaliar necessidade.
* [ ] Support for RSS/ATOM (XML / feed) output format? Or that should be
      post-processed in the API stage?
* [ ] Support for filtering out all ARPs in a query, leaving only the part of a
      territory that is not overlaped by an ARp (MUN and UF without ARPs).
      Perhaps using special UF and MUN-based territory types.

### API

Logic:

* [ ] Additional error handling, like if `query.events()` returns `False`.
* [ ] Webargs: usar tipo `Date` em alguns parâmetros ao invés de texto?
* [ ] OpenAPI:
  * [ ] Enhanced metadata, with detailed descriptions and response schemas.
  * [ ] Cache `/spec` endpoint?
* [ ] Licenças de uso em cada resposta da API (seção `meta`), podendo
      incluir mais de uma licença, por exemplo: ISA e IBGE (territórios),
      ANA (bacias), ESA (carbono), INPE (dados de destruição ambiental)?
* [ ] Refresh: run:
  * [ ] During bootstrap.
  * [ ] After imports?
  * [ ] More often (maybe twice a day)?

Actions:

* [ ] ARP: SisARP API v2 includes ARPs with "critica = 4", which should be
      excluded from the panel. Perhaps this needs some `check_arp_id()` method
      to see if the ARP ID is within those available in the `base.territory` table.
* [ ] Create an /rss or /atom action for event output in feed format?
* [ ] GeoJSON: incluir os IDs dos eventos.

### Tile Server

* [ ] OpenAPI support?
* [ ] Territories:
  * [ ] UF: restrict to the Legal Amazon (right now Bahia and other
        states are being rendered); check if that's happening also with MUN.
* [ ] Basins tiles.

### Tests

* [ ] Add a parameter to import just a small subset (sample) of entries for quick testing.
* [ ] Test all db connections.
* [ ] Run stress tests from Airflow?
* [ ] API tests using [api-testing](https://pypi.org/project/api-testing/)?
* [ ] Audit (including Makefile target and daily DAG):
  * [ ] Showing which collections still have no (recent) data.
  * [ ] Displaying table sizes like https://www.postgresqltutorial.com/postgresql-database-indexes-table-size
  * [ ] Bookkeeping tests: contabilidades básicas automáticas para homologar diariamente os dados.
  * [ ] Config: dataset start date and optional end date, allowing:
    * [ ] Test for years without data.
    * [ ] Test for other gaps: months, weeks and days.
  * [ ] Indicar diferenças na auditoria contábil dos eventos (quantidade de
        eventos na tabela `events` versus quantidade na `event_territory`.

### Deployment

* [ ] Add a public SisARP API key at the sample configuration.
* [ ] Bootstrapping:
  * [ ] Include a built-in PostgreSQL container service.
  * [ ] Better/updated/tested/documented install/config/run procedure.
* [ ] Puppet module `alertas_backend` with all logic needed to setup the environment.
* [ ] Ansible cookbook `alertas_backend` with all logic needed to setup the environment.
* [ ] Systemd timer to reload/rebuild the containers in the 2nd of every month
      (right after the planned reboot in the prod/dev machines):
      https://gist.github.com/Luzifer/7c54c8b0b61da450d10258f0abd3c917
      Systemd timers para tarefas de manutenção do docker (image prune) e
      docker-compose (pull, build, restart); avaliar snippet para gerenciamento
      via puppet.
* [ ] Git push-to-deploy configuration, pulling/rebuilding images and restarting services.
* [ ] Secrets deployment: `config/config.ini`.
* [ ] Consider using apline-based (slimmer) images.
* [ ] Integration with separate instance of https://gitlab.com/socioambiental/airflow-isa
* [ ] Host documentation directly from the proxy service?
* [ ] Support for environment vars for exposed ports.
* [ ] Support for PostgreSQL replication / multiple instances.
* [ ] Support for database migration tasks in the bootstrap phase, using
      alembic or some simple script tracking schema versions like a `meta.system`
      table (EAV) with database.version field; folder
      src/sql/bootstrap/migration/XYZ/000-task.sql.

### Performance

* [ ] Determine optimum system resources.
* [ ] Anti-abuse measures:
  * [ ] API Session tokens to limit abuse?
  * [ ] Rate limiting using a throttling system (anti-abuse at NGINX, HAProxy or
        even Kong).
* [ ] Cache:
  * [ ] Fix cache refresh operation for amazon dashboard fires in all territories
        outside protected areas. Currently there's a workaround via cronjob to
        do a "curl https://api.socioambiental.org/alertas/v1/outside..."
        on this selection. There might be a bug in the current airflow operation
        or the frontend making the request using different parameters from the
        refresh action.
  * [ ] Queuing at `lib.cache.get` for the same function/parameters,
        parameters, avoiding multiple instances asking for the same selection.
  * [ ] Configure caches at NGINX vhosts for the API and the Tileserver or
        use a Varnish service for both?
  * [ ] Refactor to clear all caches after an import succeeds (currently is
        done on `@save_metadata` decorator).
  * [ ] If caches are cleared after each successful import, the cache timestamp
        can be increased to more than a day and the cache clear DAG can even
        be disable. This may ensure an optimum caching strategy as values
        doesn't change if processed data hasn't changed.
  * [ ] Stats for the most requested actions and params to define which goes
        to the cache during refresh?
  * [ ] Implement caching directly at `query.py`?
* [ ] Tuning:
  * [ ] Connection limits for the query user, always leaving room for the
        importing procedures. Max connections are greater than the number
        of API/tileserver workers/threads, but limiting in the PostgreSQL
        settings could be an additional measure.
  * [ ] [PostgreSQL HA](https://www.postgresql.org/docs/13/high-availability.html),
        perhaps with HAProxy.
* [ ] Benchmarks:
  * [ ] Add all event types, some date ranges and ARPs.
  * [ ] HTTP requests to the API and the Tileserver, using
        [hey](https://github.com/rakyll/hey) or similar.
  * [ ] Benchmark the host in various levels (hdparm, PostgreSQL, etc).
  * [ ] Benchmark class supporting multiple query param spaces.
  * [ ] Implement more benchmarks and comparisons.
  * [ ] Consider an HTML output with HTTP endpoint for the benchmarks.

### Documentation

* [ ] Incluir slides de apresentação do projeto.
* [ ] Structure:
  * [ ] Translate docs to english and spanish.
  * [ ] Setup a bettet theme, maybe based on Material Design.
  * [ ] Enhance code documentation, explaining how function/methods/classes works,
        especially for the SQLs.
* [ ] Specs:
  * [ ] Incluir gráfico e descrição da arquitetura do sistema.
  * [ ] Terminologia:
    * [ ] Definição de ARP:
      * [ ] Menção ao recorte geral (Amazônia Legal).
      * [ ] Quais Áreas Protegidas estão incluídas, qual o critério, o que isso
            implica para cômputos dentro e fora de ARPs etc.
* [ ] HOWTOs:
  * [ ] Incluir instruções para uso do sistema noutros territorios com outros
        conjuntos de dados.
* [ ] Conventions:
  * [ ] Importadores: convenções.
  * [ ] Convenção de schemas da base de dados.
* [ ] Operação do sistema:
  * [ ] Rotinas e agendamentos (desde upgrades/reboots/recargas periódicas até
        tarefas de manutenção e importação de conjuntos), incluindo:
    * [ ] PRODES: rodar ao menos duas vezes ao ano após a lançamento dos dados
          canônicos.
    * [ ] PRODES e PRODES acumulado: importar mais de uma vez ao semestre pra
          incorporar eventuais mudanças nos territórios.
    * [ ] PRODES Acumulado: rodar também duas vezes ao ano em virtude de eventuwis
          alterações na tabela de territórios.
* [ ] Concepts:
  * [ ] Conceito de interesse territorial?
  * [ ] Como conceituar obras? Eventos e/ou territórios?

### Refactoring

* [ ] Import/export: change `date` command-line argument to `download_id`,
      defaulting to the current date? This might need a lot of refactoring.
* [ ] Single `alertas` CLI?
* [ ] Plugin system for custom queries and API actions?
* [ ] Rename `src/sql/precompute` to `src/sql/transform`.
* [ ] SQL: `load/truncate_old_data.sql` with statements from `src/lib/importer/load/ogr.py`.
* [ ] Uniformizar nomes de tabelas.
* [ ] Move database schema params from `create_schemas()` to lib.params.
      Apply config into SQL files and other database queries.

## Incoming

Incoming tasks for triage:

* [ ] ...
